<?php
namespace PHP\CLASSES;

use \PHP\CLASSES\CYLPDB;


class CACAMPAIGN
{

    private $apiurl = '';

    private $apikey = '';

    function __construct()
    {
        $this->apiurl = YLP_ACURL;
        $this->apikey = YLP_ACKEY;
    }

    public function AddContact($email,$fname,$lname,$phone,$ip,int $list){
        $params = array(
            'api_key'      => $this->apikey,
            'api_action'   => 'contact_add',
            'api_output'   => 'json'
        );

        $post = array(
            'email' => $email,
            'first_name' => $fname,
            'last_name' => $lname,
            'phone' => $phone,
            'tags' => 'unsubscribed',
            'ip4' => $ip,
            "p[$list]" => $list
        );
        $this->ProcessRequest($params,$post);
    }

    public function AddToAutomation($email,$automation){
        $params = array(
            'api_key'      => $this->apikey,
            'api_action'   => 'automation_contact_add',
            'api_output'   => 'json'
        );

        $post = array(
            "contact_email" => $email,
            "automation" => $automation,
        );
        $this->ProcessRequest($params,$post);
    }

    public function RemoveFromAutomation($email,$automation){
        $params = array(
            'api_key'      => $this->apikey,
            'api_action'   => 'automation_contact_remove',
            'api_output'   => 'json'
        );

        $post = array(
            "contact_email" => $email,
            "automation" => $automation,
        );
        $this->ProcessRequest($params,$post);
    }

    public function AddTagToContact($email,$tag){
        $params = array(
            'api_key'      => $this->apikey,
            'api_action'   => 'contact_tag_add',
            'api_output'   => 'json'
        );

        $post = array(
            'email' => $email,
            'tags' => $tag,
        );
        $this->ProcessRequest($params,$post);
    }

    public function RemoveTagFromContact($email,$tag){
        $params = array(
            'api_key'      => $this->apikey,
            'api_action'   => 'contact_tag_remove',
            'api_output'   => 'json'
        );

        $post = array(
            'email' => $email,
            'tags' => $tag,
        );
        $this->ProcessRequest($params,$post);
    }

    private function ProcessRequest($params,$post){
        $query = "";
        foreach( $params as $key => $value ) $query .= urlencode($key) . '=' . urlencode($value) . '&';
        $query = rtrim($query, '& ');

        $data = "";
        foreach( $post as $key => $value ) $data .= urlencode($key) . '=' . urlencode($value) . '&';
        $data = rtrim($data, '& ');

        $this->apiurl = rtrim($this->apiurl, '/ ');
        if ( !function_exists('curl_init') ) die('CURL not supported. (introduced in PHP 4.0.2)');
        if ( $params['api_output'] == 'json' && !function_exists('json_decode') ) {
            // die('JSON not supported. (introduced in PHP 5.2.0)');
        }
        $api = $this->apiurl . '/admin/api.php?' . $query;
        $request = curl_init($api);
        curl_setopt($request, CURLOPT_HEADER, 0);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($request, CURLOPT_POSTFIELDS, $data);
        curl_setopt($request, CURLOPT_FOLLOWLOCATION, true);

        $response = (string)curl_exec($request);

        if (DEBUG == 1){
            file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/cacampaign.txt' , "active campain add user fail $response\r\n", FILE_APPEND);
            // die('Nothing was returned. Do you have a connection to Email Marketing server?');
        }
        curl_close($request);
    }

}


?>