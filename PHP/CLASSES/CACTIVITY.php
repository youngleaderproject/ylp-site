<?php
namespace PHP\CLASSES;

use \PHP\CLASSES\CYLPDB;


class CACTIVITY
{

    /* @var $dba CYLPDB */
    private $dba;

    function GET_ACTIVITIES($track,$activity)
    {
        $ActualDataArray = Array();
        $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $query = "SELECT * FROM ylpactivities WHERE ylpActivityTrack = '$track'and ylpActivitySeq = $activity";
        $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);
        return $ActualDataArray;
    }

    function GET_ALLACTIVITIES($track)
    {
        $ActualDataArray = Array();
        $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $query = "SELECT * FROM ylpactivities WHERE ylpActivityTrack = '$track' ORDER BY ylpActivitySeq ASC";
        $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);
        return $ActualDataArray;
    }

    function GET_WORKBOOKS()
    {
        $ActualDataArray = Array();
        $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $query = "SELECT * FROM ylpactivities WHERE ylpActivityTrack = 'WB' ORDER BY ylpActivitySeq ASC";
        $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);
        return $ActualDataArray;
    }

}

?>