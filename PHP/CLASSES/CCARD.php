<?php

/**
 * CCARD short summary.
 *
 * CCARD description.
 *
 * @version 1.0
 * @author mgleleux
 */

namespace PHP\CLASSES;

use \Stripe\Stripe;
use \DateTime;

class CCARD
{
    function CREATE_CUSTOMER($token, $ylpemail)
    {
        $stripe = new Stripe();
        $stripe->setApiKey(YLP_STRIPE_SK);

        $customer = \Stripe\Customer::create([
            'source' => $token,
            'email' => $ylpemail,
        ]);

        return $customer;
    }

    function RETRIEVE_CUSTOMER($userid)
    {
        $stripe = new Stripe();
        $stripe->setApiKey(YLP_STRIPE_SK);

        $user = \Stripe\Customer::retrieve($userid);

        return $user;
    }

    function UPDATE_CUSTOMER_CARD($userid, $token){
        $stripe = new Stripe();
        $stripe->setApiKey(YLP_STRIPE_SK);

        $user = \Stripe\Customer::retrieve($userid);
        $user->source = $token; // obtained with Stripe.js
        $user->save();

        return $user;
    }

    function DELETE_CUSTOMER($userid)
    {
        $stripe = new Stripe();
        $stripe->setApiKey(YLP_STRIPE_SK);

        $user = \Stripe\Customer::retrieve($userid);
        $user->delete();

        return $user;
    }

    function CREATE_CHARGE($id, $subscription){

        $stripe = new Stripe();
        $stripe->setApiKey(YLP_STRIPE_SK);

        $charge = \Stripe\Charge::create([
            'amount' => $subscription['sub_price'] . '00',
            'currency' => 'usd',
            'description' => "Charged: " . $subscription['sub_desc'],
            'customer' => $id
        ]);

        return $charge;
    }

    function RETRIEVE_CHARGE($chargeid){

        $stripe = new Stripe();
        $stripe->setApiKey(YLP_STRIPE_SK);

        $charge = \Stripe\Charge::retrieve($chargeid);

        return $charge;
    }

    function CHARGE_FAILED($request){
        $ActualDataArray = Array();
        $db = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $customer = $request->data->object->customer;

        ///Check to see if customer is still active.. If not make sure
        $query = "SELECT * FROM ylpactivesub WHERE (user_token = 0x".unpack("H*hex", $customer)["hex"] . ")";
        $NumberOfRecords = $db->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);

        if($NumberOfRecords > 0){
            $today = new DateTime();
            $date = new DateTime($ActualDataArray['sub_enddate']);
            $interval = $today->diff($date);
            $interval = $interval->format("%r%a");
            if($interval >= 0){

            }else{
                $updrecord = array();
                $where = array();
                $updrecord['sub_activated'] = false;
                $where['user_id'] = $ActualDataArray['user_id'];
                $where['active_id'] = $ActualDataArray['active_id'];
                $db->UPD_RECORD('ylpactivesub', $updrecord, $where);
            }
        }
    }

    function CHARGE_REFUNDED($request){
        $ActualDataArray = Array();
        $db = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $customer = $request->data->object->customer;

        $query = "SELECT ylpusrtb.user_login FROM ylpusrtb INNER JOIN ylpactivesub ON ylpusrtb.user_id = ylpactivesub.user_id WHERE ylpactivesub.user_token = 0x".unpack("H*hex", $customer)["hex"];
        $NumberOfRecords = $db->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);

        if($NumberOfRecords > 0){
            $userlogin = $ActualDataArray['user_login'];
            $refundamt = $request->data->object->amount_refunded;
            $refundid = $request->data->object->refunds->data[0]->id;
            $loada = array();
            $loada['user_login'] = "'$userlogin'";
            $loada['audit_type'] = "'REFUND'";
            $loada['audit_info'] = "'The user was refunded in the amount of $refundamt. Refund ID is $refundid.'";
            $db->ADD_RECORD('ylpauditinfo', $loada);
        }
    }

    function CHARGE_SUCCEEDED($request){
        $ActualDataArray = Array();
        $db = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $customer = $request->data->object->customer;

        ///Check to see if customer is still active.. If not make sure
        $query = "SELECT * FROM ylpactivesub WHERE (user_token = 0x".unpack("H*hex", $customer)["hex"] . ")";
        $NumberOfRecords = $db->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);

        if($NumberOfRecords > 0){
            $today = new DateTime();
            $date = new DateTime($ActualDataArray['sub_enddate']);
            $interval = $today->diff($date);
            $interval = $interval->format("%r%a");
            //if($interval >= 0){
            //    $today = $today->format("Y-m-d");
            //    $enddate = date('Y-m-d', strtotime($date. ' + 1 month'));
            //}else{
                $dt1 = new DateTime();
                $today = $dt1->format("Y-m-d");
                $dt2 = new DateTime("+1 month");
                $enddate = $dt2->format("Y-m-d");
            //}

            $updrecord = array();
            $where = array();
            $updrecord['sub_begdate'] = "'$today'";
            $updrecord['sub_enddate'] = "'$enddate'";
            $updrecord['sub_activated'] = true;
            $where['user_id'] = $ActualDataArray['user_id'];
            $where['active_id'] = $ActualDataArray['active_id'];
            $db->UPD_RECORD('ylpactivesub', $updrecord, $where);
        }
    }

    function CUSTOMER_CREATED($request){
        $ActualDataArray = Array();
        $db = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $customer = $request->data->object->id;

        $query = "SELECT ylpusrtb.user_login FROM ylpusrtb INNER JOIN ylpactivesub ON ylpusrtb.user_id = ylpactivesub.user_id WHERE ylpactivesub.user_token = 0x".unpack("H*hex", $customer)["hex"];
        $NumberOfRecords = $db->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);

        if($NumberOfRecords > 0){
            $userlogin = $ActualDataArray['user_login'];
            $today = new DateTime();
            $loada = array();
            $loada['user_login'] = "'$userlogin'";
            $loada['audit_type'] = "'STRIPE CUSTOMER CREATED'";
            $loada['audit_info'] = "'The user customer account was created on $today. The Customers ID is $customer.'";
            $db->ADD_RECORD('ylpauditinfo', $loada);
        }
    }

    function CUSTOMER_UPDATED($request){
        $ActualDataArray = Array();
        $db = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $customer = $request->data->object->id;

        $query = "SELECT ylpusrtb.user_login FROM ylpusrtb INNER JOIN ylpactivesub ON ylpusrtb.user_id = ylpactivesub.user_id WHERE ylpactivesub.user_token = 0x".unpack("H*hex", $customer)["hex"];
        $NumberOfRecords = $db->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);

        if($NumberOfRecords > 0){
            $userlogin = $ActualDataArray['user_login'];
            $today = new DateTime();
            $loada = array();
            $loada['user_login'] = "'$userlogin'";
            $loada['audit_type'] = "'STRIPE CUSTOMER UPDATED'";
            $loada['audit_info'] = "'The user customer account was updated on $today. The Customers ID is $customer.'";
            $db->ADD_RECORD('ylpauditinfo', $loada);
        }
    }
}