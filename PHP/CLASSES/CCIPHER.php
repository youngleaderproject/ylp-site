<?php
namespace PHP\CLASSES;
/**
 * CCIPHER short summary.
 *
 * CCIPHER description.
 *
 * @version 1.0
 * @author mgleleux
 */
use \PHP\CLASSES\CYLPDB;
use \PHP\CLASSES\CUSER;

class CCIPHER
{

	private $uAlphabet;    		// English Alphabet (UPPERCASE)
	private $lAlphabet;    		// English Alphabet (lowercase)
	function __construct()
	{
		$this->uAlphabet = range('A', 'Z');
		$this->lAlphabet = range('a', 'z');
	}
	/**
     *	EncryptOrDecrypt ()
     *
     *	it encrypts or decrypts messages depends on the third parameter
     *
     * @param $message (string) -> User's Message
     * @param $secretMsg (string) -> User's Secret Message
     * @param $type (string) -> ENCRYPT || DECRYPT
     *
     * @return $outputMsg (string) -> Encrypted Or Decrypted Message
     */
	public function EncryptOrDecrypt ($message, $secretMsg, $type = "ENCRYPT")
	{
		$secretMsg = strtolower($secretMsg);
		/*
        if $type is DECRYPT then reverse the $secretMsg -> (26 - $secretMsg[$i]) to encrypt the message rather than decrypting it
         */
		if ($type == 'DECRYPT') {
			$string = '';
			for ($i = 0; $i < strlen($secretMsg); $i++) {
				$string .= $this->lAlphabet[(26 - array_search($secretMsg[$i], $this->lAlphabet)) % 26];
			}
			$secretMsg = $string;
		}
		$outputMsg = ''; // final decrypted or encrypted message
		$counter = 0;
        if($type==="DECRYPT")$message = str_replace("~at","@",$message);
		for ($i = 0; $i < strlen($message); $i++) {
			if (array_search($message[$i], $this->uAlphabet) !== false)
			{ // if this character is uppercase character
				$outputMsg .= $this->uAlphabet[(array_search($secretMsg[$counter], $this->lAlphabet) + array_search($message[$i], $this->uAlphabet)) % 26];
			}
			elseif (array_search($message[$i], $this->lAlphabet) !== false)
			{ // if this character is lowercase character
				$outputMsg .= $this->lAlphabet[(array_search($secretMsg[$counter], $this->lAlphabet) + array_search($message[$i], $this->lAlphabet)) % 26];

			} else
			{ // if this character is not an english alphabet characters
				$outputMsg.= $message[$i]; continue;
			}
			if ($counter == strlen($secretMsg) - 1) $counter = 0;
			else $counter++;
		}
        if($type==="ENCRYPT")$outputMsg = str_replace("@","~at",$outputMsg);
		return $outputMsg;
	}

    public function AddCipher($ciphertext,$useremail){
        $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $encryption = simple_crypt($ciphertext, 'e');
        $user = new CUSER();
        $userinfo = $user->GET_USER_BY_EMAIL($useremail);
        if(sizeof($userinfo) > 0){
            $loada = Array();
            $loada['user_id'] = $userinfo[0]['user_id'];
            $loada['access_code'] = "'$ciphertext'";
            $loada['access_encrypt'] = "'$encryption'";
            $dba->ADD_RECORD('ylpaccess', $loada);
        }
    }

    public function GetCipher($userid){
        $ActualDataArray = Array();
        $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $query = "SELECT access_code FROM ylpaccess WHERE user_id = $userid";
        $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);
        if(sizeof($ActualDataArray)){
            return $ActualDataArray['access_code'];
        }else{
            return 'kufrrpyx7';
        }

    }

    public function GetCipherEncrypt($userid){
        $ActualDataArray = Array();
        $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $query = "SELECT access_encrypt FROM ylpaccess WHERE user_id = $userid";
        $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);

        return $ActualDataArray['access_encrypt'];
    }

}