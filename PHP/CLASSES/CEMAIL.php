<?php
namespace PHP\CLASSES;

use PHP\CLASSES\CUSER;
use PHP\CLASSES\CYLPDB;
use PHPMailer\PHPMailer;
use PHPMailer\Exception;
use DateTime;

class CEMAIL
{
    public $useremail;
    public $username;

    public function __construct()
	{
        $this->useremail = "";
        $this->username = "";
        $this->heykiddoflag = false;
	}

    public function GetEmailList():array{
        $db = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $db->crsr->unicode = true;
        $query = "SELECT * FROM ylpemails where emailBranch <> 'T' ORDER BY emailBranch, emailSeq ASC";
        $db->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);
        $db->crsr->unicode = false;
        return $ActualDataArray;
    }

    public function GetBetaEmailList($branch,$seq,$remind):array{
        $db = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        if($remind === 'Y'){
            $query = "SELECT DISTINCT(user_email),user_name FROM `ylpbetasub` WHERE user_email not IN (SELECT user_login FROM ylpauditinfo where audit_type LIKE '%$branch $seq%')";
        }else{
            $query = "SELECT DISTINCT(user_email),user_name FROM `ylpbetasub` WHERE user_email not IN (SELECT user_email FROM ylpemailsrec where emailBranch = '$branch' and emailSeq = $seq)";
        }
        $db->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);
        return $ActualDataArray;
    }

    public function GetActiveEmailList($branch,$seq,$remind):array{
        $db = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        if($remind === 'Y'){
            $query = "SELECT user_email, user_name FROM ylpusrtb WHERE user_status <> 0 and user_email not IN (SELECT user_login FROM ylpauditinfo where audit_type LIKE '%$branch $seq%')";
        }else{
            $query = "SELECT user_email, user_name FROM ylpusrtb WHERE user_status <> 0 and user_email not IN (SELECT user_email FROM ylpemailsrec where emailBranch = '$branch' and emailSeq = $seq)";
        }
        $db->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);
        return $ActualDataArray;
    }

    public function BetaSignupYLP($username,$useremail){
        $this->username = $username;
        $this->useremail = $useremail;
        $url = YLP_ACURL;
        $params = array(
            'api_key'       => YLP_ACKEY,
            'api_action'    => 'campaign_send',
            'api_output'    => 'serialize',
            'email'         => $useremail,
            'campaignid'    => 1,
            'messageid'     => 0,
            'action'        => 'send'
        );
        $query = "";
        foreach( $params as $key => $value ) $query .= urlencode($key) . '=' . urlencode($value) . '&';
        $query = rtrim($query, '& ');

        $url = rtrim($url, '/ ');

        //  if ( !function_exists('curl_init') ) die('CURL not supported. (introduced in PHP 4.0.2)');

        if ( $params['api_output'] == 'json' && !function_exists('json_decode') ) {
            // die('JSON not supported. (introduced in PHP 5.2.0)');
        }

        $api = $url . '/admin/api.php?' . $query;

        $request = curl_init($api);
        curl_setopt($request, CURLOPT_HEADER, 0);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($request, CURLOPT_FOLLOWLOCATION, true);

        $response = (string)curl_exec($request);

        curl_close($request);

        if ( !$response ) {
            file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/cuser.txt' , "active campain send email fail $useremail\r\n", FILE_APPEND);
            // die('Nothing was returned. Do you have a connection to Email Marketing server?');
        }
        if (DEBUG == 1)
        {
            $result = unserialize($response);
            file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/cemail.txt' , print_r($result,true)."\r\n", FILE_APPEND);
            file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/cemail.txt' , print_r($response,true)."\r\n", FILE_APPEND);
            file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/cemail.txt' , print_r($params,true)."\r\n", FILE_APPEND);
        }
        //$subject = explode(',',file_get_contents($_SERVER[ "DOCUMENT_ROOT" ] . '/PHP/text/subjects.txt'));
        //$altbody = "Welcome! We have so much cool stuff in store for you!";
        //$body = file_get_contents($_SERVER[ "DOCUMENT_ROOT" ] . '/PHP/html/betaSignupYLP.html', true);
        //$image = $_SERVER[ "DOCUMENT_ROOT" ] . '/images/Emailheader.png';
        //$this->SendEmail($subject[0], $body, $altbody, [[$image,'logo_2u']]);
    }

    public function BetaSignupHK($username,$useremail){
        $this->username = $username;
        $this->useremail = $useremail;
        $url = YLP_ACURL;
        $params = array(
            'api_key'       => YLP_ACKEY,
            'api_action'    => 'campaign_send',
            'api_output'    => 'serialize',
            'email'         => $useremail,
            'campaignid'    => 1,
            'messageid'     => 0,
            'action'        => 'send'
        );
        $query = "";
        foreach( $params as $key => $value ) $query .= urlencode($key) . '=' . urlencode($value) . '&';
        $query = rtrim($query, '& ');

        $url = rtrim($url, '/ ');

        //  if ( !function_exists('curl_init') ) die('CURL not supported. (introduced in PHP 4.0.2)');

        if ( $params['api_output'] == 'json' && !function_exists('json_decode') ) {
            // die('JSON not supported. (introduced in PHP 5.2.0)');
        }

        $api = $url . '/admin/api.php?' . $query;

        $request = curl_init($api);
        curl_setopt($request, CURLOPT_HEADER, 0);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($request, CURLOPT_FOLLOWLOCATION, true);

        $response = (string)curl_exec($request);

        curl_close($request);

        if ( !$response ) {
            file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/cuser.txt' , "active campain send email fail $useremail\r\n", FILE_APPEND);
            // die('Nothing was returned. Do you have a connection to Email Marketing server?');
        }
        if (DEBUG == 1)
        {
            $result = unserialize($response);
            file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/cemail.txt' , print_r($result,true)."\r\n", FILE_APPEND);
            file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/cemail.txt' , print_r($response,true)."\r\n", FILE_APPEND);
            file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/cemail.txt' , print_r($params,true)."\r\n", FILE_APPEND);
        }
        //$this->heykiddoflag = true;
        //$subject = explode(',',file_get_contents($_SERVER[ "DOCUMENT_ROOT" ] . '/PHP/text/subjects.txt'));
        //$altbody = "Better conversations at your fingertips";
        //$body = file_get_contents($_SERVER[ "DOCUMENT_ROOT" ] . '/PHP/html/betaSignupHK.html', true);
        //$image = $_SERVER[ "DOCUMENT_ROOT" ] . '/images/NewImages/logo-horizontal-tagline@2x.png';
        //$image2 = $_SERVER[ "DOCUMENT_ROOT" ] . '/images/NewImages/Subscribe-Characters@2x.png';
        //$this->SendEmail($subject[1], $body, $altbody, [[$image,'logo_2u'],[$image2,'hk-chars']]);
    }

    public function Branch0($userinfo, $emailinfo, $emailbody, $branch, $seq, $remind){
        if($remind === 'Y')$emailinfo['emailSub'] = "REMINDER: " . $emailinfo['emailSub'];
        $path = (isset($emailinfo['emailPath']) ? $emailinfo['emailPath'] : '/PHP/html/404.html');
        $body = file_get_contents($_SERVER[ "DOCUMENT_ROOT" ] . $path, true);
        $body = str_replace("~~YLPHTMLBODY~~", $emailbody, $body);
        $body = str_replace("~~USERNAME~~", $userinfo['user_name'], $body);
        $imagearray = $this->GetImages($body);
        $this->EmailFunc($userinfo, $emailinfo, $body, $imagearray, $branch, $seq);
    }

    public function AddCountry($email, $country, $language){
        $this->heykiddoflag = true;
        $body = "<html>This is an internal message request:<br><br>Add Country - <b>$country</b><br><br>Requested Language - <b>$language</b></b><br><br>Requested by Email Address - <b>$email</b><br><br> --End Message</html>";
        $userinfo = [
            "user_email"=>"support@hey-kiddo.com",
            "user_name"=>"HeyKiddo Admin Email"
        ];
        $emailinfo = [
          'emailSub'=>"HeyKiddo Add Country",
          'emailAltB'=>"Add Country Request"
        ];
        return $this->EmailFunc($userinfo, $emailinfo, $body, [], "ADC", 0);
    }

    public function ForgotPass($userinfo){
        $this->heykiddoflag = true;
        $emailinfo = [
          'emailSub'=>"HeyKiddo Forgot Password",
          'emailAltB'=>"Forgotten Password"
        ];
        $path = '/PHP/Emails/HK_ForgotPass.html';
        $body = file_get_contents($_SERVER[ "DOCUMENT_ROOT" ] . $path, true);
        $body = str_replace("~~USERPASS~~", $userinfo['user_pass'], $body);
        $body = str_replace("~~USERNAME~~", $userinfo['user_name'], $body);
        $imagearray = $this->GetImages($body);
        $this->EmailFunc($userinfo, $emailinfo, $body, $imagearray, "FPW", 0);
    }

    public function BranchS($userinfo, $emailinfo, $emailbody, $branch, $seq, $remind){
        if($remind == 'Y')$emailinfo['emailSub'] = "REMINDER: " . $emailinfo['emailSub'];
        $path = (isset($emailinfo['emailPath']) ? $emailinfo['emailPath'] : '/PHP/html/404.html');
        $body = file_get_contents($_SERVER[ "DOCUMENT_ROOT" ] . $path,true);
        $body = str_replace("~~YLPHTMLBODY~~",$emailbody,$body);
        $body = str_replace("~~USERNAME~~",$userinfo['user_name'],$body);
        $imagearray = [];
        if(strpos($body,"cid:logo_top")){
            $logo = $_SERVER[ "DOCUMENT_ROOT" ] . '/images/NewImages/Survery-Header-image@2x.png';
            array_push($imagearray,[$logo,"logo_top"]);
        }
        if(strpos($body,"cid:survey_btn")){
            $surveybtn = $_SERVER[ "DOCUMENT_ROOT" ] . '/images/NewImages/Survery-Primary-Button@2x.png';
            array_push($imagearray,[$surveybtn,"survey_btn"]);
        }
        $this->EmailFunc($userinfo, $emailinfo, $body, $imagearray, $branch, $seq);
    }

    public function BranchM($userinfo, $emailinfo, $emailbody, $branch, $seq, $remind){
        if($remind === 'Y')$emailinfo['emailSub'] = "REMINDER: " . $emailinfo['emailSub'];
        $path = (isset($emailinfo['emailPath']) ? $emailinfo['emailPath'] : '/PHP/html/404.html');
        $body = file_get_contents($_SERVER[ "DOCUMENT_ROOT" ] . $path, true);
        $body = str_replace("~~YLPHTMLBODY~~", $emailbody, $body);
        $body = str_replace("~~USERNAME~~", $userinfo['user_name'], $body);
        $imagearray = $this->GetImages($body);
        $this->EmailFunc($userinfo, $emailinfo, $body, $imagearray, $branch, $seq);
    }

    public function GetImages($body){
        $imagearray = [];
        if(!($this->heykiddoflag) && strpos($body,"cid:logo_top")){
            $logo = $_SERVER[ "DOCUMENT_ROOT" ] . '/images/NewImages/YLPMVPlogo.png';
            array_push($imagearray,[$logo,"logo_top"]);
        }
        if($this->heykiddoflag && strpos($body,"cid:heykiddologo_top")){
            $logo = $_SERVER[ "DOCUMENT_ROOT" ] . '/heykiddo/images/logo-horizontal-tagline.png';
            array_push($imagearray,[$logo,"heykiddologo_top"]);
        }
        if(strpos($body,"cid:high_five")){
            $highfive = $_SERVER[ "DOCUMENT_ROOT" ] . '/heykiddo/images/highfive.png';
            array_push($imagearray,[$highfive,"high_five"]);
        }
        if(strpos($body,"cid:logo_fb")){
            $fblogo = $_SERVER[ "DOCUMENT_ROOT" ] . '/images/NewImages/fb@2x.png';
            array_push($imagearray,[$fblogo,"logo_fb"]);
        }
        if(strpos($body,"cid:logo_twitter")){
            $twitterlogo = $_SERVER[ "DOCUMENT_ROOT" ] . '/images/NewImages/twitter@2x.png';
            array_push($imagearray,[$twitterlogo,"logo_twitter"]);
        }
        if(strpos($body,"cid:logo_insta")){
            $instalogo = $_SERVER[ "DOCUMENT_ROOT" ] . '/images/NewImages/ig@2x.png';
            array_push($imagearray,[$instalogo,"logo_insta"]);
        }
        if(strpos($body,"cid:pdf_button")){
            $pdfbtn = $_SERVER[ "DOCUMENT_ROOT" ] . '/images/NewImages/PDF-Button.png';
            array_push($imagearray,[$pdfbtn,"pdf_button"]);
        }
        if(strpos($body,"cid:welcome_img")){
            $welcomeimg = $_SERVER[ "DOCUMENT_ROOT" ] . '/images/NewImages/welcome-image.png';
            array_push($imagearray,[$welcomeimg,"welcome_img"]);
        }
        if(strpos($body,"cid:text_img")){
            $textimg = $_SERVER[ "DOCUMENT_ROOT" ] . '/images/NewImages/text@2x.png';
            array_push($imagearray,[$textimg,"text_img"]);
        }
        return $imagearray;
    }

    public function EmailFunc($userinfo,$emailinfo,$body,$images=[],$branch,$seq): bool{
        $db = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $this->useremail = (string)$userinfo['user_email'];
        $this->username = (strlen($userinfo['user_name']) === 0 ? "Young Leader Parent": (string)$userinfo['user_name']);

        $subject = $emailinfo['emailSub'];
        $altbody = $emailinfo['emailAltB'];
        $body .= "<img style='display:none;' border='0' src='https://youngleaderproject.com/PHP/commands/emailtracker.php?email=" . $this->useremail . "&branch=" . $branch . "&seq=" . $seq . "' width='1' height='1' alt='Email Tracker' >";
        $success = $this->SendEmail($subject, $body, $altbody, $images);

        if($success){
            $host = ($this->heykiddoflag ? 'HK' : 'YLP');
            $loada = array();
            $loada['user_email'] = "'$this->useremail'";
            $loada['emailSeq'] = $seq;
            $loada['emailHost'] = "'$host'";
            $loada['emailBranch'] = "'$branch'";
            $db->ADD_RECORD('ylpemailsrec', $loada);
            return true;
        }else {
            $today = new DateTime();
            $today = $today->format('Y-m-d H:i:s');
            if (DEBUG)
                file_put_contents( $_SERVER["DOCUMENT_ROOT"]. '/logs/emailclass.txt' , "Failed to send Branch $branch Seq $seq Email to $this->useremail. ---  $today " . "\r\n", FILE_APPEND);
            return false;
        }
    }

    private function SendEmail($subject, $body, $altbody, $images = [], $attachments = []){
        $mail = new PHPMailer(true);
        //$mail->SMTPDebug = 1;
        $mailerfrom = $this->heykiddoflag ? HKMAILFROM : YLPMAILFROM;
        try {
            //Server settings
            $mail->CharSet = 'UTF-8';
            $mail->isSMTP();
            $mail->Host = $this->heykiddoflag ? HKMAILSERVER : YLPMAILSERVER;
            $mail->SMTPAuth = true;
            $mail->Username = $mailerfrom;
            $mail->Password = MAILFROMPW;
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;

            //Recipients
            $mail->setFrom($mailerfrom, $this->heykiddoflag ? 'HeyKiddo Crew�' : 'YLP Misson Control');
            $mail->addAddress(urldecode($this->useremail),$this->username);
            //Embedded Images
            if(sizeof($images) > 0){
                for($i = 0; $i < sizeof($images); $i++){
                    $mail->AddEmbeddedImage($images[$i][0],$images[$i][1]);
                }
            }

            // Attachments
            if(sizeof($attachments) > 0){
                for($j = 0; $j < sizeof($attachments); $j++){
                    $mail->addAttachment($attachments[$j]);
                }
            }

            // Content
            $mail->msgHTML($body);
            $mail->isHTML(true);
            $mail->Subject = $subject;
            $mail->AltBody = $altbody;

            $mail->send();
            return true;
        }
        catch (Exception $e) {
            $today = new DateTime();
            $today = $today->format('Y-m-d H:i:s');
            if (DEBUG)
                file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/ylpemailerr.txt' , "Emailing Error -- $today --- $mail->ErrorInfo" . "\r\n", FILE_APPEND);
            echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
            return false;
        }
    }
}
?>