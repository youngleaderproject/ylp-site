<?php
namespace PHP\CLASSES;

use PHP\CLASSES\COPTIONS;
use \PHP\CLASSES\CYLPDB;

class CLOGINRETURN
{
    public $success = false;
    public $isActive = "";
    public $isAdmin = "";
    public $user_email = "";
    public $nickname = "";
    public $user_name = "";
    public $user_pw = "";
    public $device = "";
    public $statusmsg = "";
    public $optioncodes = [];


    function __construct($success, $user_values)
    {
        $this->success = $success;
        if ($success == true)
        {
            $this->user_email = $user_values['ylpemail'];
            $this->user_name = $user_values['ylpusr'];
            $this->user_pw = $user_values['ylppwr'];
            $this->isActive= $user_values['userstatus'] ===  0 ? false : true;
            $this->isAdmin= $user_values['isadmin'];
            $this->device = session_id();
        }else{
            $this->statusmsg= $user_values['statusmsg'];
        }
    }

}