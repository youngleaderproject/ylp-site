<?php
namespace PHP\CLASSES;
/**
 * CCIPHER short summary.
 *
 * CCIPHER description.
 *
 * @version 1.0
 * @author mgleleux
 */
use \PHP\CLASSES\CYLPDB;
use \PHP\CLASSES\CUSER;
use DateTime;

class CMEDIA
{

    public function AddImage($usrname, $picid, $picname, $stage,$file)
    {
        $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $loada = Array();
        $usrarray = [];
        $user = new CUSER();
        $usrarray = $user->GET_USER_BY_NAME($usrname);
        $picpath = str_replace( '\\', '/', $_SERVER[ "DOCUMENT_ROOT" ] ) .'/userstorage/'. $usrname . '/pics/'. $stage . '/';
        if (sizeof($usrarray) > 0) {
            $loada['user_id'] = $usrarray['user_id'];
            $loada['pic_id'] = "'$picid'";
            $loada['pic_path'] = '/userstorage/'. $usrname . '/pics/' . $stage . '/' . $picname . "'";
            $dba->ADD_RECORD('ylpusrpics', $loada);

            UploadMedia($usrname,$picname,$picpath,$file);
            return true;
        }else{
            $today = new DateTime();
            $today = $today->format('Y-m-d H:i:s');
            if (DEBUG)
                file_put_contents( $_SERVER["DOCUMENT_ROOT"]. '/logs/cmedia.txt' , "Image was sent to server with bad usrname $usrname.  $today" . "\r\n", FILE_APPEND);
        }
        return false;
    }

    public function AddVideo($usrname, $vidid, $vidname, $stage, $file)
    {
        $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $loada = Array();
        $usrarray = [];
        $user = new CUSER();
        $usrarray = $user->GET_USER_BY_NAME($usrname);
        $vidpath = str_replace( '\\', '/', $_SERVER[ "DOCUMENT_ROOT" ] ) .'/userstorage/'. $usrname . '/vids/'. $stage . '/';
        if (sizeof($usrarray) > 0) {
            $loada['user_id'] = $usrarray['user_id'];
            $loada['vid_id'] = "'$vidid'";
            $loada['vid_path'] = "'". $vidpath . $vidname . "'";
            $dba->ADD_RECORD('ylpusrvids', $loada);

            UploadMedia($usrname,$vidname,$vidpath,$file);

            return true;
        }else{
            $today = new DateTime();
            $today = $today->format('Y-m-d H:i:s');
            if (DEBUG)
                file_put_contents( $_SERVER["DOCUMENT_ROOT"]. '/logs/cmedia.txt' , "Video was sent to server with bad usrname $usrname.  $today" . "\r\n", FILE_APPEND);
        }
        return false;
    }

    public function CheckVideo($usrid, $vidid)
    {
        $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $ActualDataArray = Array();
        $query = "SELECT * FROM ylpusrvids WHERE (user_id = $usrid AND vid_id = '$vidid')";
        $NumberOfRecords = $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);

        if($NumberOfRecords > 0){
            return true;
        }
        return false;;
    }

    public function GetImage($usrid, $picid)
    {
        $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $ActualDataArray = Array();
        $query = "SELECT * FROM ylpusrpics WHERE (user_id = $usrid AND pic_id = '$picid')";
        $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);

        return $ActualDataArray;
    }
}