<?php
namespace PHP\CLASSES;
use \Exception;

class COPTIONS
{
    public $AddredactionAuthority = "N";
    public $MaintenanceAuthority = "N";
    public $WorkbasketAuthority = "N";
    public $PasswordAuthority = "N";
    public $SignaturefilestampAuthority = "N";
    public $CdAuthority = "N";
    public $QueueforindexingAuthority = "N";
    public $AuditAuthority = "N";
    public $VersionAuthority = "N";
    public $CallbackremindersAuthority = "N";
    public $EmailAuthority = "N";
    public $StapleAuthority = "N";
    public $ApprovalhistoryAuthority = "N";
    public $CheckinAuthority = "N";
    public $ScreenscrapemaintenanceAuthority = "N";
    public $ApprovalAuthority = "N";
    public $MovetosubwbAuthority = "N";
    public $AddNotesAuthority = "N";
    public $CheckoutAuthority = "N";
    public $PopqueueAuthority = "N";
    public $RouteAuthority = "N";
    public $ScanAuthority = "N";
    public $WorkflowauditAuthority = "N";
    public $DetailmaintAuthority = "N";
    public $WbgroupmaintenanceAuthority = "N";
    public $ZoptAuthority = "N";
    public $DeleteAuthority = "N";
    public $UnredactedAuthority = "N";
    public $DownloadAuthority = "N";
    public $ExtraSearchAuthority = "N";
    public $BatchPrintAuthority = "N";

    public function __construct()
    {
        if (func_num_args() > 1) {

            for ($i = 0; $i < func_num_args()-1; $i++) {
                $this->AddredactionAuthority = func_get_arg(0);
                $this->MaintenanceAuthority = func_get_arg(1);
                $this->WorkbasketAuthority = func_get_arg(2);
                $this->PasswordAuthority = func_get_arg(3);
                $this->SignaturefilestampAuthority = func_get_arg(4);
                $this->CdAuthority = func_get_arg(5);
                $this->DownloadAuthority = func_get_arg(6);
                $this->ExtraSearchAuthority = func_get_arg(7);
                $this->QueueforindexingAuthority = func_get_arg(8);
                $this->AuditAuthority = func_get_arg(9);
                $this->VersionAuthority = func_get_arg(10);
                $this->CallbackremindersAuthority = func_get_arg(11);
                $this->EmailAuthority = func_get_arg(12);
                $this->StapleAuthority = func_get_arg(13);
                $this->ApprovalhistoryAuthority = func_get_arg(14);
                $this->CheckinAuthority = func_get_arg(15);
                $this->ScreenscrapemaintenanceAuthority = func_get_arg(16);
                $this->ApprovalAuthority = func_get_arg(17);
                $this->MovetosubwbAuthority = func_get_arg(18);
                $this->AddNotesAuthority = func_get_arg(19);
                $this->CheckoutAuthority = func_get_arg(20);
                $this->PopqueueAuthority = func_get_arg(21);
                $this->RouteAuthority = func_get_arg(22);
                $this->ScanAuthority = func_get_arg(23);
                $this->WorkflowauditAuthority = func_get_arg(24);
                $this->BatchPrintAuthority = func_get_arg(25);
                $this->DetailmaintAuthority = func_get_arg(26);
                $this->WbgroupmaintenanceAuthority = func_get_arg(27);
                $this->ZoptAuthority = func_get_arg(28);
                $this->DeleteAuthority = func_get_arg(29);
                $this->UnredactedAuthority = func_get_arg(30);
            }
        } elseif (func_num_args() == 1) {
            try {
                $options = func_get_arg(0);
                $this->AddredactionAuthority = substr($options, 0, 1);
                $this->MaintenanceAuthority = substr($options, 1, 1);
                $this->WorkbasketAuthority = substr($options, 2, 1);
                $this->PasswordAuthority = substr($options, 3, 1);
                $this->SignaturefilestampAuthority = substr($options, 4, 1);
                $this->CdAuthority = substr($options, 5, 1);
                $this->DownloadAuthority = substr($options, 6, 1);
                $this->ExtraSearchAuthority = substr($options, 7, 1);
                $this->QueueforindexingAuthority = substr($options, 8, 1);
                $this->AuditAuthority = substr($options, 9, 1);
                $this->VersionAuthority = substr($options, 10, 1);
                $this->CallbackremindersAuthority = substr($options, 11, 1);
                $this->EmailAuthority = substr($options, 12, 1);
                $this->StapleAuthority = substr($options, 13, 1);
                $this->ApprovalhistoryAuthority = substr($options, 14, 1);
                $this->CheckinAuthority = substr($options, 15, 1);
                $this->ScreenscrapemaintenanceAuthority = substr($options, 16, 1);
                $this->ApprovalAuthority = substr($options, 17, 1);
                $this->MovetosubwbAuthority = substr($options, 18, 1);
                $this->AddNotesAuthority = substr($options, 19, 1);
                $this->CheckoutAuthority = substr($options, 20, 1);
                $this->PopqueueAuthority = substr($options, 21, 1);
                $this->RouteAuthority = substr($options, 22, 1);
                $this->ScanAuthority = substr($options, 23, 1);
                $this->WorkflowauditAuthority = substr($options, 24, 1);
                $this->BatchPrintAuthority = substr($options, 25, 1);
                $this->DetailmaintAuthority = substr($options, 26, 1);
                $this->WbgroupmaintenanceAuthority = substr($options, 27, 1);
                $this->ZoptAuthority = substr($options, 28, 1);
                $this->DeleteAuthority = substr($options, 29, 1);
                $this->UnredactedAuthority = substr($options, 30, 1);
            } catch(Exception $e) {}
        }
    }
}