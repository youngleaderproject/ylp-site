<?php

/**
 * CPAYPAL short summary.
 *
 * CPAYPAL description.
 *
 * @version 1.0
 * @author mgleleux
 */

namespace PHP\CLASSES;

use \DateTime;
use \DateInterval;
use \PHP\CLASSES\CUSER;
use \PHP\CLASSES\CYLPDB;
use \PHP\CLASSES\CSUBSCRIPTION;
use \PHP\CLASSES\CACAMPAIGN;

class CPAYPAL
{
    function SUB_PAYMENTFAIL($request)
    {
        $db = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $customerid = $request->resource->id;
        $subscription = new CSUBSCRIPTION();
        $subinfo = $subscription->CHECK_ACTIVE($customerid);
        $today = new DateTime();
        $campaign = new CACAMPAIGN();
        if($subinfo > 0){
            $user = new CUSER();
            $user->GET_USER_BY_PAYID($customerid);
            $ylpusr = $user->ylpemail;

            $updrecord = array();
            $where = array();
            $updrecord['sub_activated'] = 3;
            $where['user_id'] = $user->ylpid;
            $db->UPD_RECORD('ylpactivesub', $updrecord, $where);
            $campaign->RemoveTagFromContact($user->ylpemail,'subscribed');
            $campaign->AddTagToContact($user->ylpemail,'suspended');
            $loada = array();
            $loada['user_login'] = "'$ylpusr'";
            $loada['audit_type'] = "'PAYPAL SUBSCRIPTION PAYMENT FAILED'";
            $today = $today->format("Y-m-d");
            $loada['audit_info'] = "'The user customer account had a failed payment on $today. The Customers ID is $customerid.'";
            $db->ADD_RECORD('ylpauditinfo', $loada);
        }else{
            $today = $today->format('Y-m-d H:i:s');
            if (DEBUG)
                file_put_contents( $_SERVER["DOCUMENT_ROOT"]. '/logs/cpaypal.txt' , "Subscription tried to cancel but no user exists for $customerid.  $today" . "\r\n", FILE_APPEND);
        }
    }
    function SUB_CANCELLED($request)
    {
        $db = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $customerid = $request->resource->id;
        $subscription = new CSUBSCRIPTION();
        $subinfo = $subscription->CHECK_ACTIVE($customerid);
        $today = new DateTime();
        $campaign = new CACAMPAIGN();
        if($subinfo > 0){
            $user = new CUSER();
            $user->GET_USER_BY_PAYID($customerid);
            $ylpusr = $user->ylpemail;
            $today = $today->format("Y-m-d");
            $updrecord = array();
            $where = array();
            $updrecord['sub_activated'] = 0;
            $updrecord['unsubscribe_date'] = "'$today'";
            $where['user_id'] = $user->ylpid;
            $db->UPD_RECORD('ylpactivesub', $updrecord, $where);
            $campaign->RemoveTagFromContact($user->ylpemail,'subscribed');
            $campaign->AddTagToContact($user->ylpemail,'unsubscribed');
            $loada = array();
            $loada['user_login'] = "'$ylpusr'";
            $loada['audit_type'] = "'PAYPAL SUBSCRIPTION CANCELED'";

            $loada['audit_info'] = "'The user customer account was canceled on $today. The Customers ID is $customerid.'";
            $db->ADD_RECORD('ylpauditinfo', $loada);
        }else{
            $today = $today->format('Y-m-d H:i:s');
            if (DEBUG)
                file_put_contents( $_SERVER["DOCUMENT_ROOT"]. '/logs/cpaypal.txt' , "Subscription tried to cancel but no user exists for $customerid.  $today" . "\r\n", FILE_APPEND);
        }
    }

    function SUB_ACTIVATED($payerid, $subemail, $prodname)
    {
        $db = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $subscription = new CSUBSCRIPTION();
        $usersubinfo = $subscription->CHECK_ACTIVE($payerid);
        $user = new CUSER();
        $subdesc = $subscription->SUBSCRIPTION_INFO($prodname);
        $user->GET_USER_BY_PAYID($payerid);
        $dt1 = new DateTime();
        $today = $dt1->format("Y-m-d");
        $subtrial = $subdesc[0]['sub_trialdays'];
        $dt2 = new DateTime();
        $dt2->add(new DateInterval("$subtrial"));
        $enddate = $dt2->format("Y-m-d");
        $loada = array();
        $loada['user_id'] = $user->ylpid;
        $loada['sub_begdate'] = "'$today'";
        $loada['sub_enddate'] = "'$enddate'";
        $loada['sub_code'] = "'$prodname'";
        $loada['user_token'] = "'$payerid'";
        $loada['sub_email'] = "'$subemail'";
        $loada['sub_activated'] = 1;
        if($usersubinfo > 0){
            $where['user_id'] = $user->ylpid;
            $db->UPD_RECORD('ylpactivesub', $loada, $where);
        }else{
            $db->ADD_RECORD('ylpactivesub', $loada);
        }
    }

    function SUB_PAYED($request){
        $db = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $customerid = $request->resource->billing_agreement_id;
        $subscription = new CSUBSCRIPTION();
        $subinfo = $subscription->CHECK_ACTIVE($customerid);
        $today = new DateTime();
        $user = new CUSER();
        if($subinfo > 0){
            $userinfo = $subscription->USER_SUBSCRIPTION($customerid);
            $subdesc = $subscription->SUBSCRIPTION_INFO($userinfo[0]['sub_code']);
            $user->GET_USER_BY_PAYID($customerid);
            $dt1 = new DateTime();
            $today = $dt1->format("Y-m-d");
            $subtrial = $subdesc[0]['sub_length'];
            $dt2 = new DateTime("$subtrial");
            $enddate = $dt2->format("Y-m-d");
            $updrecord = array();
            $where = array();
            $updrecord['sub_enddate'] = "'$enddate'";
            $updrecord['sub_payed'] = 1;
            $where['user_id'] = $user->ylpid;
            $db->UPD_RECORD('ylpactivesub', $updrecord, $where);
        }else{
            $today = $today->format('Y-m-d H:i:s');
            if (DEBUG)
                file_put_contents( $_SERVER["DOCUMENT_ROOT"]. '/logs/cpaypal.txt' , "Subscription does not exists for $customerid.  $today" . "\r\n", FILE_APPEND);
        }
    }

    function SUB_CREATED($request){
        $ActualDataArray = Array();
        $db = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $customerid = $request->resource->payer->payer_info->payer_id;

        $query = "SELECT ylpusrtb.user_login FROM ylpusrtb INNER JOIN ylpactivesub ON ylpusrtb.user_id = ylpactivesub.user_id WHERE ylpactivesub.user_token = 0x".unpack("H*hex", $customerid)["hex"];
        $NumberOfRecords = $db->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);
        $today = new DateTime();
        file_put_contents( $_SERVER["DOCUMENT_ROOT"]. '/logs/cpaypal.txt' , "$NumberOfRecords" . "\r\n", FILE_APPEND);
        if($NumberOfRecords > 0){
            $userlogin = $ActualDataArray['user_login'];
            $loada = array();
            $loada['user_login'] = "'$userlogin'";
            $loada['audit_type'] = "'PAYPAL SUBSCRIPTION CREATED'";
            $loada['audit_info'] = "'The user customer account was created on $today. The Customers ID is $customerid.'";
            $db->ADD_RECORD('ylpauditinfo', $loada);
        }else{
            $today = $today->format('Y-m-d H:i:s');
            if (DEBUG)
                file_put_contents( $_SERVER["DOCUMENT_ROOT"]. '/logs/cpaypal.txt' , "Failed logging paypal subscription create for $customerid.  $today " . "\r\n", FILE_APPEND);
        }
    }

    function SUB_REACTIVATED($request){
        $db = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $customerid = $request->resource->id;
        $user = new CUSER();
        $userrec = $user->GET_USER_BY_PAYID($customerid);
        $today = new DateTime();
        $campaign = new CACAMPAIGN();
        $subscription = new CSUBSCRIPTION();
        if(sizeof($userrec) > 0){
            $userinfo = $subscription->USER_SUBSCRIPTION($customerid);
            $subdesc = $subscription->SUBSCRIPTION_INFO($userinfo[0]['sub_code']);
            $dt1 = new DateTime();
            $today = $dt1->format("Y-m-d");
            $subtrial = $subdesc[0]['sub_length'];
            $dt2 = new DateTime("$subtrial");
            $enddate = $dt2->format("Y-m-d");
            $updrecord = array();
            $where = array();
            $updrecord['sub_enddate'] = "'$enddate'";
            $updrecord['sub_payed'] = 1;
            $updrecord['sub_activated'] = 1;
            $where['user_id'] = $user->ylpid;
            $db->UPD_RECORD('ylpactivesub', $updrecord, $where);
            $campaign->RemoveTagFromContact($user->ylpemail,'suspended');
            $campaign->AddTagToContact($user->ylpemail,'subscribed');
        }else{
            $today = $today->format('Y-m-d H:i:s');
            if (DEBUG)
                file_put_contents( $_SERVER["DOCUMENT_ROOT"]. '/logs/cpaypal.txt' , "Subscription tried to cancel but no user exists for $customerid.  $today" . "\r\n", FILE_APPEND);
        }
    }

    function SUB_SUSPENDED($request){
        $db = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $customerid = $request->resource->id;
        $subscription = new CSUBSCRIPTION();
        $subinfo = $subscription->CHECK_ACTIVE($customerid);
        $today = new DateTime();
        $campaign = new CACAMPAIGN();
        if($subinfo > 0){
            $user = new CUSER();
            $user->GET_USER_BY_PAYID($customerid);
            $ylpusr = $user->ylpemail;

            $updrecord = array();
            $where = array();
            $updrecord['sub_activated'] = 2;
            $where['user_id'] = $user->ylpid;
            $db->UPD_RECORD('ylpactivesub', $updrecord, $where);
            $campaign->RemoveTagFromContact($user->ylpemail,'subscribed');
            $campaign->AddTagToContact($user->ylpemail,'suspended');
            $loada = array();
            $loada['user_login'] = "'$ylpusr'";
            $loada['audit_type'] = "'PAYPAL SUBSCRIPTION SUSPENDED'";
            $today = $today->format("Y-m-d");
            $loada['audit_info'] = "'The user customer account was canceled on $today. The Customers ID is $customerid.'";
            $db->ADD_RECORD('ylpauditinfo', $loada);
        }else{
            $today = $today->format('Y-m-d H:i:s');
            if (DEBUG)
                file_put_contents( $_SERVER["DOCUMENT_ROOT"]. '/logs/cpaypal.txt' , "Subscription tried to cancel but no user exists for $customerid.  $today" . "\r\n", FILE_APPEND);
        }
    }

    function SUB_UPDATED($request){
        $db = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $customerid = $request->resource->payer->payer_info->payer_id;
        $subemail = $request->resource->payer->payer_info->email;

        ///Check to see if customer is still active.. If not make sure
        $query = "SELECT * FROM ylpactivesub WHERE (user_token = '$customerid')";
        $NumberOfRecords = $db->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);
        $today = new DateTime();

        if($NumberOfRecords > 0){
            $updrecord = array();
            $where = array();
            $updrecord['user_token'] = $customerid;
            $updrecord['sub_email'] = $subemail;
            $where['user_id'] = $ActualDataArray['user_id'];
            $where['active_id'] = $ActualDataArray['active_id'];
            $db->UPD_RECORD('ylpactivesub', $updrecord, $where);
        }else{
            $today = $today->format('Y-m-d H:i:s');
            if (DEBUG)
                file_put_contents( $_SERVER["DOCUMENT_ROOT"]. '/logs/cpaypal.txt' , "Failed updating paypal subscription for user $subemail.  $today" . "\r\n", FILE_APPEND);
        }
    }
}