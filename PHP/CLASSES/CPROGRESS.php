<?php
namespace PHP\CLASSES;

use \PHP\CLASSES\CYLPDB;


class CPROGRESS
{

    /* @var $dba CRVIDB */
    private $dba;
    public $ylpid = 0;
    public $kidid = 0;

    function GET_KID_PROGRESS($ylpid,$kidid,$planetid)
    {
        $ActualDataArray = Array();
        $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $query = "SELECT * FROM ylpprogress WHERE user_id = $ylpid AND kid_id = $kidid AND planet_id = $planetid";
        $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);
        return $ActualDataArray;
    }

    function UPDATE_PROGRESS($ylpid,$kidid,$planetid,$planetperc)
    {
        $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $updrecord['planet_percent'] = $planetperc;
        $where['user_id'] = $ylpid;
        $where['kid_id'] = $kidid;
        $where['planet_id'] = $planetid;
        $dba->UPD_RECORD('ylpprogress', $updrecord, $where);
    }

    function ADD_PLANET_PROGRESS($ylpid,$kidid,$planetid){
        $loada = Array();
        $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $loada['user_id'] = $ylpid;
        $loada['kid_id'] = $kidid;
        $loada['planet_id'] = $planetid;
        $dba->ADD_RECORD('ylpprogress', $loada);
    }

    function MISSION_CONTROL_PROGRESS($ylpid,$kidid){
        $progress = $this->GET_KID_PROGRESS($ylpid,$kidid,1);
        if(sizeof($progress) > 0){
            return $progress['planet_percent'];
        }else{
            return '0';
        }
    }

    function PLANET_SELFIE_PROGRESS($ylpid,$kidid){
        $progress = $this->GET_KID_PROGRESS($ylpid,$kidid,2);
        if(sizeof($progress) > 0){
            return $progress['planet_percent'];
        }else{
            return '0';
        }
    }

    function PLANET_OHM_PROGRESS($ylpid,$kidid){
        $progress = $this->GET_KID_PROGRESS($ylpid,$kidid,3);
        if(sizeof($progress) > 0){
            return $progress['planet_percent'];
        }else{
            return '0';
        }
    }

    function PLANET_NEURO_PROGRESS($ylpid,$kidid){
        $progress = $this->GET_KID_PROGRESS($ylpid,$kidid,4);
        if(sizeof($progress) > 0){
            return $progress['planet_percent'];
        }else{
            return '0';
        }
    }


}

?>