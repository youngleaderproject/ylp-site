<?php
namespace PHP\CLASSES;
//use \PDO;
//use \PDOException;
//use \PDOStatement;
define("sql_error_reporting", true);
// true to display errors, false to hide
class CSQL
{

    // Generic DataBase Access
    /* var $Conn PDO */
    private $Conn;

    private $naming = '/';

    private $TransOpen = false;

    public $unicode = false;

    public $LastSqlErr;

    public $LastSQLMsg = "";

    function __construct($ylp_db_user, $ylp_db_pass, $ylp_db_host, $ylp_conn_db)
    {

        $user = $ylp_db_user;
        $pass = $ylp_db_pass;
        $host = explode("=", $ylp_db_host);
        $port = 3306;
        $db = $ylp_conn_db;
        try {
            $this->Conn =  mysqli_connect(
               $host[1],
               $user,
               $pass,
               $db,
               $port
            );

        }
        catch (\mysqli_sql_exception $e) {
            echo 'Connection failed: ' . $e->getMessage();
            $this->LastSQLMsg = $e->getMessage();
            $this->LastSqlErr = $e->getCode();
            log_trace();
        }
    }

    function __destruct()
    {
        $this->FREECONNECTION();
    }

    public function FREECONNECTION()
    {
        if ($this->TransOpen) {
            $this->Conn->rollBack();
        }
    }

    public function SETAUTOCOMMIT($bool)
    {
         $this->Conn->autocommit($bool);
    }

    public function BEGINTRANSACTION()
    {
        if ($this->TransOpen === false) {
            $this->Conn->begin_transaction();
            $this->TransOpen = true;
        }
    }

    public function ENDTRANSACTION($status)
    {
        if ($this->TransOpen === true) {
            if ($status == "R") {
                $this->Conn->rollBack();
            } else {
                $this->Conn->commit();
            }
            $this->TransOpen = false;
        }
    }

    public function GETNAMINGCONVENTION()
    {
        return $this->naming;
    }

    public function CRSR_OPEN($stmt)
    {
        $this->CLEARLASTERR();
        $crsr_hdl = FALSE;
        if($this->unicode === true){
            $this->Conn->set_charset('utf8mb4');
        }
        $crsr_hdl = mysqli_query($this->Conn, $stmt);

        if ($crsr_hdl === FALSE) {
            $this->SETSQLERROR();
            log_trace();
        }

        return ($crsr_hdl);
    }

    public function CRSR_CLOSE($crsr_hdl)
    {
        $crsr_hdl = NULL;
    }

    public function CRSR_GETROW( $crsr_hdl, &$rec_view)
    {
        $err = 0;
        $this->CLEARLASTERR();
        $rec_view = mysqli_fetch_array($crsr_hdl);
        if ($rec_view === false) {
            $this->SETSQLERROR();
            $err = - 1;
        }

        return $err;
    }

    public function CRSR_DATAVIEW($crsr_hdl, &$rec_view)
    {
        if ($crsr_hdl === false) {
            log_trace();
            return - 1;
        }

        $this->CLEARLASTERR();
        $rec_view = mysqli_fetch_all($crsr_hdl,MYSQLI_ASSOC);
        return (mysqli_num_rows($crsr_hdl));
    }

    public function INSERTID()
    {
        $insertid = $this->Conn->insert_id;
        return ($insertid);
    }

    public function GETLASTERR($stmt, &$err, &$errMsg)
    {
        $err = 0;
        $errMsg = null;

        $err = $this->LastSqlErr;
        $errMsg = $this->LastSQLMsg;
        if (DEBUG)
            file_put_contents ($_SERVER["DOCUMENT_ROOT"]. '/logs/csql.txt' , "$stmt returned $errMsg\r\n", FILE_APPEND);
    }

    public function NUMOFQUERYROWS($crsr_hdl)
    {
        return mysqli_num_rows($crsr_hdl);
    }

    public function GETLASTSTATE()
    {
        $tab = $this->Conn->connect_error;
        return ($tab[0]);
    }

    private function SETSQLERROR()
    {
        $this->CLEARLASTERR();
        $tab = $this->Conn->connect_error;
        $this->LastSqlErr = $tab[0];
        $this->LastSQLMsg = "SQL Error:" . $tab[0] . "(" . $tab[1] . ") " . $tab[2] . "\n";
        return;
    }

    private function CLEARLASTERR()
    {
        $this->LastSqlErr = 0;
        $this->LastSQLMsg = "";
    }
}

?>