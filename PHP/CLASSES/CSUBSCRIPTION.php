<?php

namespace PHP\CLASSES;

use \DateTime;
use \stdClass;
use PHP\CLASSES\CCARD;
use PHP\CLASSES\CPAYPAL;
/**
 * CSUBSCRIPTION short summary.
 *
 * CSUBSCRIPTION description.
 *
 * @version 1.0
 * @author mgleleux
 */
class CSUBSCRIPTION
{
    /* @var $db CRVIDB */
    private $db;

    public $paymenttype = "";

    function CHECK_PROMO($promocode)
    {
        $ActualDataArray = Array();
        $db = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $query = "SELECT * FROM ylppromocodes where promo_code = '$promocode'";
        $db->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);
        return $ActualDataArray;
    }

    function CHECK_ACTIVE($ylppayid)
    {
        $ActualDataArray = Array();
        $db = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $query = "SELECT * FROM ylpactivesub WHERE user_token = '$ylppayid' AND sub_activated = 1";

        return $db->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);
    }

    function CHECK_DISABLED($ylppayid)
    {
        $ActualDataArray = Array();
        $db = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $query = "SELECT * FROM ylpactivesub WHERE user_token = '$ylppayid' AND sub_activated = 0";
        $NumberOfRecords = $db->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);

        return $NumberOfRecords;
    }

    function CHECK_ACTIVE_DATES($ylppayid)
    {
        $ActualDataArray = Array();
        $db = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $query = "SELECT * FROM ylpactivesub WHERE user_token = '$ylppayid' AND sub_activated = 1";
        $db->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);

        $today = new DateTime();
        $date = new DateTime($ActualDataArray['sub_enddate']);
        $interval = $today->diff($date);
        $interval = $interval->format("%r%a");

        return $interval;
    }

    function NEW_SUBSCRIPTION($ylpid,$ylpemail)
    {
        $db = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);

        $dt1 = new DateTime();
        $today = $dt1->format("Y-m-d");

        $dt2 = new DateTime("+1 month");
        $enddate = $dt2->format("Y-m-d");

        $card = new CCARD();

        $token = $_POST['stripeToken'];
        //$subcode = 'sub2';
        $subcode = $_SESSION['YLP']['SUBTYPE'];

        $customer = $card->CREATE_CUSTOMER($token,$ylpemail);
        if($this->paymenttype === 'card'){
            $id = $customer->id;
        }else{
            $id = 'paypal';   /// this needs to change once you get paypal set up.
        }

        $subinfo = $this->SUBSCRIPTION_INFO($subcode);

        $charge = $this->NEWSUBSCRIPTION_CHARGE($customer,$subinfo);

        if($charge->status === 'succeeded'){
            $loada = array();
            $loada['user_id'] = $ylpid;
            $loada['sub_begdate'] = "'$today'";
            $loada['sub_email'] = "'$ylpemail'";
            $loada['sub_enddate'] = "'$enddate'";
            $loada['sub_code'] = "'$subcode'";
            $loada['user_token'] = "'$id'";
            $loada['sub_activated'] = true;
            $db->ADD_RECORD('ylpactivesub', $loada);
        }

        return $charge;
    }

    function SUBSCRIPTION_INFO($subcode){
        $ActualDataArray = Array();
        $db = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $query = "SELECT * FROM ylpsubscriptions WHERE sub_code = '$subcode'";
        $db->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);

        return $ActualDataArray;
    }

    function NEWSUBSCRIPTION_CHARGE($customer, $subscription){
        $db = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $card = new CCARD();
        $charge = new stdClass();
        if($this->paymenttype === 'card'){
            $charge = $card->CREATE_CHARGE($customer->id,$subscription);
            if($charge->status === 'succeeded'){
                $subid = $subscription['sub_id'];
                $loada = array();
                $loada['customer_id'] = "'$charge->customer'";
                $loada['status'] = true;
                $loada['total'] = "'$charge->amount'";
                $loada['trans_id'] = "'$charge->id'";
                $loada['details'] = "'$charge->receipt_url'";
                $loada['subscription_id'] = "'$subid'";
                $db->ADD_RECORD('ylpccpay', $loada);
            }
        }else{

        }
        return $charge;
    }

    function RENEW_SUBSCRIPTION($user){

        $subscription = $this->SUBSCRIPTION_INFO($user['sub_code']);
        $db = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $card = new CCARD();
        $charge = new stdClass();
        if($this->paymenttype === 'card'){
            $charge = $card->CREATE_CHARGE($user['user_token'], $subscription);

            if($charge->status === "succeeded"){
                $dt1 = new DateTime();
                $today = $dt1->format("Y-m-d");

                $dt2 = new DateTime("+1 month");
                $enddate = $dt2->format("Y-m-d");

                $updrecord = array();
                $where = array();
                $updrecord['sub_begdate'] = "'$today'";
                $updrecord['sub_enddate'] = "'$enddate'";
                $updrecord['sub_activated'] = true;
                $where['user_id'] = $user['user_id'];
                $where['active_id'] = $user['active_id'];
                $db->UPD_RECORD('ylpactivesub', $updrecord, $where);
            }else{
                $updrecord = array();
                $where = array();
                $updrecord['sub_activated'] = false;
                $where['user_id'] = $user['user_id'];
                $where['active_id'] = $user['active_id'];
                $db->UPD_RECORD('ylpactivesub', $updrecord, $where);
            }
        }

        return $charge;
    }

    function USER_SUBSCRIPTION($ylpid){
        $ActualDataArray = Array();
        $db = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $query = "SELECT * FROM ylpactivesub WHERE user_token = '$ylpid'";
        $db->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);

        return $ActualDataArray;
    }

    function CYCLE_MONTHLY(){
        $ActualDataArray = Array();
        $db = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $query = "SELECT * FROM ylpactivesub WHERE sub_enddate <= CURRENT_DATE AND sub_activated = 1";
        $db->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);

        return $ActualDataArray;
    }
}