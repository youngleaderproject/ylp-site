<?php
namespace PHP\CLASSES;

use \PHP\CLASSES\CYLPDB;
use \PHP\CLASSES\CEMAIL;
use \PHP\CLASSES\CSMS;
use \PHP\CLASSES\CACAMPAIGN;
use \PHP\CLASSES\CSUBSCRIPTION;
use \DateTime;
use \DateInterval;


class CUSER
{

    public $ylppwr = '';

    public $ylpusr = '';

    public $ylpadmin = '';

    public $ylpemail = '';

    public $ylpname = '';

    public $ylplname = '';

    public $ylpphone = '';

    public $ylptrack = '';

    public $ylpstatus = 0;

    public $ylpactivity = 0;

    public $ylpid = 0;

    public $ylptracktime = 0;

    public $ylpusrexists = false;

    public $statusmessage = '';


    /* @var $dba CRVIDB */
    private $dba;

    // *********************************************************************
    // Get User Email
    // *********************************************************************

    function GetUserList()
    {
        $ActualDataArray = Array();
        $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $query = "SELECT a.*, b.name as countryname,c.unsubscribe_date as unsubscribed,c.sub_enddate FROM ylpusrtb a inner JOIN ylpcountries b ON a.user_country = b.ccode left join ylpactivesub c on a.user_id = c.user_id order by a.user_status DESC";
        $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);
        return $ActualDataArray;
    }

    function GET_USER_EMAIL($ylpusr)
    {
        $email = '';
        $ActualDataArray = Array();
        $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $query = "SELECT user_email FROM ylpusrtb WHERE (user_login = 0x".unpack("H*hex", $ylpusr)["hex"] . ")";
        $NumberOfRecords = $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);
        if ($NumberOfRecords > 0) {
            $email = trim($ActualDataArray[0]['user_email']);
        }
        $this->ylpemail = $email;
        return $email;
    }

    function GET_USER_BY_NAME($ylpusr)
    {
        $ActualDataArray = Array();
        $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $query = "SELECT * FROM ylpusrtb WHERE (user_login = 0x".unpack("H*hex", $ylpusr)["hex"] . ")";
        $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);

        return $ActualDataArray;
    }

    function GET_USER_BY_EMAIL($ylpemail)
    {
        $ActualDataArray = Array();
        $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $query = "SELECT * FROM ylpusrtb WHERE user_email = '$ylpemail'";
        $NumberOfRecords = $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);
        if ($NumberOfRecords > 0) {
            $this->ylpemail = (String)$ActualDataArray[0]['user_email'];
            $this->ylpusr = (String)$ActualDataArray[0]['user_login'];
            $this->ylppwr = (String)$ActualDataArray[0]['user_pass'];
            $this->ylpid = $ActualDataArray[0]['user_id'];
            $this->ylpphone = (String)$ActualDataArray[0]['user_phone'];
            $this->ylpname = (String)$ActualDataArray[0]['user_name'] . " " .(String)$ActualDataArray[0]['user_lastname'];
            $this->ylptrack = (String)$ActualDataArray[0]['user_track'];
            $this->ylptracktime = (int)$ActualDataArray[0]['user_tracktime'];
            $this->ylpstatus = (int)$ActualDataArray[0]['user_status'];
            $this->ylpactivity = (int)$ActualDataArray[0]['user_activity'];
            $this->ylpusrexists = true;
        }
        return $ActualDataArray;
    }

    function GET_USER_BY_PHONE($ylpphone)
    {
        $ActualDataArray = Array();
        $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $query = "SELECT * FROM ylpusrtb WHERE user_phone = '$ylpphone'";
        $NumberOfRecords = $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);
        if ($NumberOfRecords > 0) {
            $this->ylpemail = (String)$ActualDataArray[0]['user_email'];
            $this->ylpusr = (String)$ActualDataArray[0]['user_login'];
            $this->ylppwr = (String)$ActualDataArray[0]['user_pass'];
            $this->ylpid = $ActualDataArray[0]['user_id'];
            $this->ylpphone = (String)$ActualDataArray[0]['user_phone'];
            $this->ylpname = (String)$ActualDataArray[0]['user_name'] . " " .(String)$ActualDataArray[0]['user_lastname'];
            $this->ylptrack = (String)$ActualDataArray[0]['user_track'];
            $this->ylptracktime = (int)$ActualDataArray[0]['user_tracktime'];
            $this->ylpstatus = (int)$ActualDataArray[0]['user_status'];
            $this->ylpactivity = (int)$ActualDataArray[0]['user_activity'];
            $this->ylpusrexists = true;
        }
        return $ActualDataArray;
    }

    function GET_USER_BY_ID($ylpid)
    {
        $ActualDataArray = Array();
        $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $query = "SELECT * FROM ylpusrtb WHERE user_id =  $ylpid";
        $NumberOfRecords = $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);
        if ($NumberOfRecords > 0) {
            $this->ylpemail = (String)$ActualDataArray[0]['user_email'];
            $this->ylpusr = (String)$ActualDataArray[0]['user_login'];
            $this->ylppwr = (String)$ActualDataArray[0]['user_pass'];
            $this->ylpid = $ActualDataArray[0]['user_id'];
            $this->ylpphone = (String)$ActualDataArray[0]['user_phone'];
            $this->ylpname = (String)$ActualDataArray[0]['user_name'] . " " .(String)$ActualDataArray[0]['user_lastname'];
            $this->ylptrack = (String)$ActualDataArray[0]['user_track'];
            $this->ylptracktime = (int)$ActualDataArray[0]['user_tracktime'];
            $this->ylpstatus = (int)$ActualDataArray[0]['user_status'];
            $this->ylpactivity = (int)$ActualDataArray[0]['user_activity'];
            $this->ylpusrexists = true;
        }

        return $ActualDataArray;
    }

    function GET_USER_BY_PAYID($ylppayid)
    {
        $ActualDataArray = Array();
        $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $query = "SELECT * FROM ylpusrtb WHERE user_payid = '$ylppayid'";
        $NumberOfRecords = $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);
        if ($NumberOfRecords > 0) {
            $this->ylpemail = (String)$ActualDataArray[0]['user_email'];
            $this->ylpusr = (String)$ActualDataArray[0]['user_login'];
            $this->ylppwr = (String)$ActualDataArray[0]['user_pass'];
            $this->ylpid = $ActualDataArray[0]['user_id'];
            $this->ylpphone = (String)$ActualDataArray[0]['user_phone'];
            $this->ylpname = (String)$ActualDataArray[0]['user_name'] . " " .(String)$ActualDataArray[0]['user_lastname'];
            $this->ylptrack = (String)$ActualDataArray[0]['user_track'];
            $this->ylptracktime = (int)$ActualDataArray[0]['user_tracktime'];
            $this->ylpstatus = (int)$ActualDataArray[0]['user_status'];
            $this->ylpactivity = (int)$ActualDataArray[0]['user_activity'];
            $this->ylpusrexists = true;
        }

        return $ActualDataArray;
    }

    function GET_USER_BY_ENCRYPT($ylpencrypt)
    {
        $ActualDataArray = Array();
        $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $query = "SELECT * FROM ylpusrtb INNER JOIN ylpaccess ON ylpusrtb.user_id = ylpaccess.user_id WHERE ylpaccess.access_encrypt = '$ylpencrypt'";
        $NumberOfRecords = $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);
        if ($NumberOfRecords > 0) {
            $this->ylpemail = (String)$ActualDataArray[0]['user_email'];
            $this->ylpusr = (String)$ActualDataArray[0]['user_login'];
            $this->ylppwr = (String)$ActualDataArray[0]['user_pass'];
            $this->ylpid = $ActualDataArray[0]['user_id'];
            $this->ylpphone = (String)$ActualDataArray[0]['user_phone'];
            $this->ylpname = (String)$ActualDataArray[0]['user_name'] . " " .(String)$ActualDataArray[0]['user_lastname'];
            $this->ylptrack = (String)$ActualDataArray[0]['user_track'];
            $this->ylptracktime = (int)$ActualDataArray[0]['user_tracktime'];
            $this->ylpstatus = (int)$ActualDataArray[0]['user_status'];
            $this->ylpactivity = (int)$ActualDataArray[0]['user_activity'];
            $this->ylpusrexists = true;
        }

        return $ActualDataArray;
    }

    function CHECK_PHONE($ylpphone)
    {
        $ActualDataArray = Array();
        $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $query = "SELECT * FROM ylpusrtb WHERE user_phone = '$ylpphone' and user_status > 0 and user_status < 999";
        $NumberOfRecords = $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);

        return $NumberOfRecords;
    }

    // function
    public function AddUser($email,$pass,$fname,$lname,$phone,$country,$track,$time,int $status,$payid = '',$payemail = '',$timezone = '',$promocode = '')
    {
        /* @var $dbmvc CRVIDB */
        if (strlen($email) === 0)return false;

        $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $loada = Array();
        if($this->ylpusrexists){
            if($status === 0 && $this->ylpstatus > 900){
                $status = $this->ylpstatus;
            }
            $loada['user_login'] = "'$email'";
            $loada['user_pass'] = "'" . password_hash($pass, PASSWORD_DEFAULT) . "'";
            $loada['user_email'] = "'$email'";
            $loada['user_phone'] = "'$phone'";
            $loada['user_country'] = "'$country'";
            $loada['user_name'] = "'$fname'";
            $loada['user_lastname'] = "'$lname'";
            $loada['user_track'] = "'$track'";
            $loada['user_payid'] = !empty($payid) ? "'$payid'" : "NULL";
            $loada['user_payemail'] = !empty($payemail) ? "'$payemail'" : "NULL";
            $loada['user_status'] = $status;
            $loada['user_tracktime'] = $time;
            $loada['user_promo'] = "'$promocode'";
            $where['user_email'] = "'$email'";
            $where['user_login'] = "'$email'";

            $dba->UPD_RECORD('ylpusrtb', $loada, $where);
        }else{
            $campaign = new CACAMPAIGN();
            $campaign->AddContact($email,$fname,$lname,$phone,$this->getUserIp(),2);
            $campaign->AddTagToContact($email,'unsubscribed');
            $campaign->AddToAutomation($email,"4");
            $loada['user_login'] = "'$email'";
            $loada['user_pass'] = "'" . password_hash($pass, PASSWORD_DEFAULT) . "'";
            $loada['user_email'] = "'$email'";
            $loada['user_phone'] = "'$phone'";
            $loada['user_country'] = "'$country'";
            $loada['user_name'] = "'$fname'";
            $loada['user_lastname'] = "'$lname'";
            $loada['user_track'] = "'$track'";
            $loada['user_status'] = $status;
            $loada['user_tracktime'] = $time;
            $loada['user_timezone'] = "'$timezone'";
            $dba->ADD_RECORD('ylpusrtb', $loada);


        }
        return true;
    }

    public function UserIntro($email,$fname,$lname,$phone,$track,$country){
        $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $cipher = new CCIPHER();
        $cipheredtext = $cipher->EncryptOrDecrypt($email,"YoungLeaderMVP", "ENCRYPT"); // ADD TO NEW TABLE FOR CIPHERED UNIQUE CODES.
        $cipher->AddCipher($cipheredtext,$email);

        $ActualDataArray = [];
        $sms = new CSMS();
        $ActualDataArray = [];
        $ActualDataArray2 = [];
        $ActualDataArray3 = [];
        $ActualDataArray4 = [];
        $echoice = (isset($_SESSION["signupuser"]["echoice"])?$_SESSION["signupuser"]["echoice"]:'');
        $code = $sms->countryArray[$country]['code'];
        $countrycode = ( $code > "" ? $code : "1");
        $phone = '+'.$countrycode.$phone;
        $dba->crsr->unicode = true;
        $query = "SELECT * FROM ylptextmsg WHERE textMsgTrack = '$track' and textMsgBranch = 'INTRO' AND textMsgPos = 1 ORDER BY textMsgSeq";
        $NumberOfRecords2 = $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);
        $dba->crsr->unicode = false;
        $query = "SELECT user_phone FROM ylptextmsgrec WHERE user_phone='$phone' AND textMsgBranch = 'INTRO' AND textMsgPos = 0" ;
        $NumberOfRecords3 = $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray2);
        if($NumberOfRecords3 === 0){
            if($echoice === "Y"){
                $dba->crsr->unicode = true;
                $query = "SELECT * FROM ylptextmsg WHERE textMsgTrack = '0' and textMsgBranch = 'EXC'";
                $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray4);
                $body = $ActualDataArray4[0]['textMsgBody'];
                $dba->crsr->unicode = false;
                $body = str_replace("~~USERTRACK~~",$sms->getTrackName($track),$body);
                $sms->SMSFunc($phone,$body,[],$track,'INTRO',0,0,false,false,false);
            }
            for($j=0; $j < $NumberOfRecords2; $j++){
                $pos = $ActualDataArray[$j]['textMsgPos'];
                $seq = $ActualDataArray[$j]['textMsgSeq'];
                $body = $ActualDataArray[$j]['textMsgBody'];
                $body = str_replace("~~FIRSTNAME~~",$fname.' '.$lname,$body);
                $query = "SELECT b.user_id from ylpaccess a inner join ylpusrtb b on a.user_id = b.user_id where b.user_email = '$email'";
                if($dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray3) > 0){
                    if(strpos($body,"~~USERKEY~~") > -1){
                        $body = str_replace("~~USERKEY~~","k=".$ActualDataArray3[0]["user_id"],$body);
                        $linkbody = $body;
                        $linkbody = str_replace("\r\n",' ', $linkbody);
                        $linkpos = strpos($linkbody,"https");
                        $linkpos2 = strpos($linkbody," ",$linkpos);
                        $link = substr($linkbody,$linkpos,$linkpos2-$linkpos);
                        $newlink = $sms->shortenLink($link);
                        $body = str_replace($link,$newlink,$body);
                    }
                    $media = (isset($ActualDataArray[$j]['textMsgMedia']) ? explode(",",$ActualDataArray[$j]['textMsgMedia']) : []);
                    $sms->SMSFunc($phone,$body,$media,$track,'INTRO',$pos,$seq,false,false,false);
                }else{
                    file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/cuser.txt' ,"User Access code wasnt created for $email\r\n", FILE_APPEND);
                }
            }
        }
        unset($_SESSION["signupuser"]["firsttime"]);
    }

    public function AddAdminUser($email,$login,$pass)
    {
        /* @var $dbmvc CRVIDB */
        if (strlen($email) === 0)return false;

        $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
         $country = $this->getLocationInfoByIp()['country'];
         $country = ($country !== '' ? $country : 'US');
        $loada = Array();
        $loada['user_pass'] = "'" . password_hash($pass, PASSWORD_DEFAULT) . "'";
        $loada['user_email'] = "'$email'";
        $loada['user_country'] = "'$country'";
        $loada['user_login'] = "'$login'";
        $dba->ADD_RECORD('ylpusradmin', $loada);
        return true;
    }

    public function UserList()
    {
        $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $ActualDataArray = array();
        $query = "SELECT * FROM ylpusrtb ORDER BY user_login";
        $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);
        return $ActualDataArray;
    }

    public function CheckUser($user, $email)
    {
        $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $ActualDataArray = array();
        $query = "";
        if(strlen($user) > 0){
            $query = "SELECT * FROM ylpusrtb WHERE user_login = 0x".unpack("H*hex", $user)["hex"];
        }else if(strlen($email) > 0){
            $query = "SELECT * FROM ylpusrtb WHERE user_email= 0x" .unpack("H*hex", $email)["hex"];
        }
        $NumberOfRecords = $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);
        return $NumberOfRecords;
    }

    public function CheckUserStatus($userid)
    {
        $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $ActualDataArray = array();
        $query = "";
        $query = "SELECT * FROM ylpusrtb WHERE user_status <> 0 AND user_id = $userid";
        $NumberOfRecords = $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);
        if($NumberOfRecords > 0){
            $_SESSION['YLP']['ACTIVE'] = 'Y';
            $_SESSION['user']['subtype'] = $ActualDataArray[0]['user_status'];
            return true;
        }else{
            $_SESSION['YLP']['ACTIVE'] = 'N';
            return false;
        }
    }

    public function SetUserStatus($userid, $status)
    {
        $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $updrecord['user_status'] = $status;
        $where['user_id'] = $userid;
        $dba->UPD_RECORD('ylpusrtb', $updrecord, $where);
    }

    public function DeleteUser($ylpid)
    {

        $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $query = "SELECT * FROM ylpusrtb WHERE user_id = $ylpid";
        $NumberOfRecords = $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);

        if($NumberOfRecords > 0){
            $phone = $ActualDataArray[0]["user_phone"];
            $email = $ActualDataArray[0]["user_email"];
            $dba->DELETE_RECORD("ylpactivesub", array(
                'user_id' => $ylpid
            ));

            $dba->DELETE_RECORD("ylpaccess", array(
                'user_id' => $ylpid
            ));

            $dba->DELETE_RECORD("ylpusrtb", array(
                'user_id' => $ylpid
            ));

            $dba->DELETE_RECORD("ylpemailsrec", array(
                'user_email' => "'$email'"
            ));

            $dba->DELETE_RECORD("ylptextmsgrec", array(
                'user_phone' => "L'%$phone%'"
            ));
            return true;
        }else{
            return false;
        }


        /*

        // IMMMSGS
        $dbmvc->DELETE_RECORD("IMMMSGS", array(
            'CMPC' => $this->cmpc,
            'MMUSER' => "'" . $rveml . "'"
        ));

        // IMMSQDPF - Reminders
        $dbmvc->DELETE_RECORD("IMMSQDPF", array(
            'CMPC' => $this->cmpc,
            'SDUSER' => "'" . $rveml . "'"
        ));
        */
    }

    public function UpdateUserPW($pwnew)
    {
        $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $updrecord = array();
        $where = array();
        $updrecord['user_pass'] = "'" . password_hash($pwnew, PASSWORD_DEFAULT) . "'";
        $where['user_id'] = $this->ylpid;
        return $dba->UPD_RECORD('ylpusrtb', $updrecord, $where);
    }

    public function UpdateUserActivity($userid,$activity)
    {
        $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $updrecord = array();
        $where = array();
        $updrecord['user_activity'] = $activity;
        $where['user_id'] = $userid;
        return $dba->UPD_RECORD('ylpusrtb', $updrecord, $where);
    }

    public function UpdateUserTrack($track)
    {
        $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $updrecord = array();
        $where = array();
        $updrecord['user_activity'] = 1;
        $updrecord['user_track'] = "'$track'";
        $where['user_id'] = $this->ylpid;
        return $dba->UPD_RECORD('ylpusrtb', $updrecord, $where);
    }

    public function UpdateUser()
    {
        $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $NumberOfRecords = $dba->GETDATACOUNT('ylpusrtb', "(user_login ='$this->ylpusr')");
        if ($NumberOfRecords < 1) {
            return false;
        }
        $updrecord = array();
        $where = array();
        $updrecord['user_login'] = "'" . $this->ylpusr . "'";
        $updrecord['user_pass'] = "'" . $this->ylppwr . "'";
        $updrecord['user_email'] = "'" . $this->ylpemail . "'";
        $updrecord['user_phone'] = "'" . $this->ylpphone . "'";
        $updrecord['user_name'] = "'" . $this->ylpname . "'";
        $updrecord['user_lastname'] = "'" . $this->ylplname . "'";
        $updrecord['user_track'] = "'" . $this->ylptrack . "'";
        $updrecord['user_tracktime'] = $this->ylptracktime;
        $updrecord['user_status'] = $this->ylpstatus;
        $where['user_id'] = $this->ylpid;
        return $dba->UPD_RECORD('ylpusrtb', $updrecord, $where);
    }

    public function UserInfo($ylpusr)
    {
        try {
            // validate user name and password
            $this->GetUserInfo($ylpusr, NULL, 'info');
            try {
               // $this->GetConfigInfo(); or any other info
            }
            catch (\Exception $e) {
                throw new \Exception($e);
            }

        }
        catch (\Exception $e) {
            throw new \Exception($e);
        }
    }

    public function UserLoginUnity($access, $ylpusr,  &$user_info){
        $ActualDataArray = [];
        $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $query = "SELECT * FROM ylpaccess WHERE access_code = 0x".unpack("H*hex", $access)["hex"];
        $NumberOfRecords = $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);

        if($NumberOfRecords > 0){
            $userid = $ActualDataArray[0]['user_id'];
            $user_info = [];
            $user_data = $this->GET_USER_BY_ID($userid);

            if($ylpusr !== $user_data['user_login'])throw new \Exception('Configuration not found');

            $this->ylppwr = $user_data['user_pass'];
            $this->ylpusr = $user_data['user_login'];
            $this->ylpemail = $user_data['user_email'];

            $_SESSION["user"]["usrid"] = $this->ylpid;
            $_SESSION["user"]["usrname"] = $this->ylpusr;

            $this->CheckUserStatus($this->ylpusr);

            $user_info['ylppwr'] = $this->ylppwr;
            $user_info['ylpusr'] = $this->ylpusr;
            $user_info['ylpemail'] = $this->ylpemail;
            $user_info['isactive'] = $_SESSION['YLP']['ACTIVE'];

        }else{
            throw new \Exception('Configuration not found');
        }
    }

    public function UserLogin($ylpusr, $ylppwr, $admin, &$user_info)
    {

        $this->ylpusr = $ylpusr;
        $this->ylppwr = $ylppwr;
        $this->ylpadmin = $admin;

        try {
            $this->GetUserInfo($ylpusr, $ylppwr, $admin);
        }
        catch (\Exception $e) {
            $user_info['statusmsg'] = $this->statusmessage;
            throw new \Exception($e);
        }

        $this->CheckUserStatus($this->ylpid);
        $user_info['ylppwr'] = $this->ylppwr;
        $user_info['ylpusr'] = $this->ylpusr;
        $user_info['ylpemail'] = $this->ylpemail;
        $user_info['isactive'] = $_SESSION['YLP']['ACTIVE'];
    }

    private function GetUserInfo($ylpusr, $ylppwr, $admin)
    {
        $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);

        if ($ylpusr > '' && $this->ylpusr == '' && $admin === 'Y') {
            $this->ylpusr = $ylpusr;
        }

        $ActualDataArray = array();
        $NumberOfRecords = 0;
        if($admin == 'Y'){
            $query = "SELECT * FROM ylpusradmin WHERE (user_login = '$this->ylpusr')";
        }else{
            $query = "SELECT * FROM ylpusrtb WHERE (user_email = '$ylpusr') and user_status <> 0";
        }

        if ($ylppwr === ""){
            if (DEBUG == 1)
            {
                file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/cuser.txt' , "password was not entered. $ylpusr username was used, exception thrown\r\n", FILE_APPEND);
            }
            throw new \Exception('Configuration not found');
        }
        $NumberOfRecords = $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);
        if ($NumberOfRecords > 0) {
            if(password_verify($ylppwr, $ActualDataArray[0]['user_pass'])){
                $status = (int)$ActualDataArray[0]['user_status'];
                if($status > 950){
                    $this->statusmessage = "Subscription Expired.";
                    throw new \Exception('Configuration not found');
                }
                $signupdate = new DateTime($ActualDataArray[0]['user_registered']);
                if($status === 3){
                    $promo = $ActualDataArray[0]['user_promo'];
                    $sub = new CSUBSCRIPTION();
                    $subinfo = $sub->CHECK_PROMO($promo);
                  //  file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/cuser.txt' , print_r($subinfo)."\r\n", FILE_APPEND);
                    if(sizeof($subinfo) > 0){
                        $dateint = $subinfo[0]['promo_datetag'];
                    }else{
                        $dateint = "P1M";
                    }

                    $today = new DateTime();
                    $month = $signupdate->add(new DateInterval($dateint));
                    $datediff = (int)$month->diff($today)->format('%R%a');
                    file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/cuser.txt' , $datediff."\r\n", FILE_APPEND);
                    if($datediff === 0){
                        $this->SetUserStatus($ActualDataArray[0]['user_id'],999);
                        $this->statusmessage = "Subscription Expired.";
                        throw new \Exception('Configuration not found');
                    }
                }
                $_SESSION["user"]["usremail"] = $ylpusr;
                $_SESSION["user"]["dev"] = session_id();
                $this->ylpemail = $ActualDataArray[0]['user_email'];
                $this->ylpid = $ActualDataArray[0]['user_id'];
                $_SESSION["user"]["usrid"] = $this->ylpid;
                $_SESSION["user"]["usrpw"] = $ylppwr;
                $_SESSION["user"]["userfname"] = $ActualDataArray[0]['user_name'];
                $_SESSION["user"]["userlname"] = $ActualDataArray[0]['user_lastname'];
                $_SESSION["user"]["userstatus"] = $ActualDataArray[0]['user_status'];
                $_SESSION["user"]["userphone"] = $ActualDataArray[0]['user_phone'];
                $_SESSION["user"]["isadmin"] = $admin;
                $this->statusmessage = "Success";
            }else{
                $this->statusmessage = "Incorrect Password.";
                throw new \Exception('Configuration not found');
            }
        }else{
            $this->statusmessage = "Email and Password combination is incorrect.";
            throw new \Exception('Configuration not found');
        }
    }

    public function getUserIp(){
        $client  = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote  = @$_SERVER['REMOTE_ADDR'];
        if(filter_var($client, FILTER_VALIDATE_IP)){
            $ip = $client;
        }elseif(filter_var($forward, FILTER_VALIDATE_IP)){
            $ip = $forward;
        }else{
            $ip = $remote;
        }

        return $ip;
    }

    public function getLocationInfoByIp(){
        $client  = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote  = @$_SERVER['REMOTE_ADDR'];
        $result  = array('country'=>'');
        if(filter_var($client, FILTER_VALIDATE_IP)){
            $ip = $client;
        }elseif(filter_var($forward, FILTER_VALIDATE_IP)){
            $ip = $forward;
        }else{
            $ip = $remote;
        }
        $ip_data = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip));
        if($ip_data && $ip_data->geoplugin_countryName != null){
            $result['country'] = $ip_data->geoplugin_countryCode;
        }
        return $result;
    }
}

?>