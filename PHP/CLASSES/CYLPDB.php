<?php
namespace PHP\CLASSES;

use PHP\CLASSES\CSQL;

define("OP_ADD", 1);
define("OP_UPDATE", 2);
define("OP_DELETE", 3);
define("OP_GET", 4);

class CYLPDB
{

    private $Message = true;

    private $errMsg = '';

    /* @var $crsr CSQL */
    public $crsr;

    private $cmpc;

    public $mssql = false;

    function __construct($RV_DB_USER, $RV_DB_PASS, $RV_DB_HOST, $RV_CONN_DB)
    {
        try {
            $this->crsr = new CSQL($RV_DB_USER, $RV_DB_PASS, $RV_DB_HOST, $RV_CONN_DB);
        }
        catch (Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function UNIQUENUMBER($key)
    {
        /*
         * SET TRANSACTION ISOLATION LEVEL SERIALIZABLE; BEGIN TRANSACTION; SELECT .. FOR UPDATE; UPDATE .... ; COMMIT TRANSACTION;
         */
        $key = strtoupper ($key);
        $this->crsr->SETAUTOCOMMIT(FALSE);
        $crsr_hdl = $this->crsr->CRSR_OPEN('SET TRANSACTION ISOLATION LEVEL SERIALIZABLE');
        $this->crsr->BEGINTRANSACTION();
        $stmt = "SELECT * FROM ylpautochar WHERE AUTOTYPE = '$key' FOR UPDATE";
        $NumberOfRecords = 0;
        $ActualDataArray = array();
        if (DEBUG_DB)
            file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/cylpdb_sql.txt' , $stmt . "\r\n", FILE_APPEND);
        $crsr_hdl = $this->crsr->CRSR_OPEN($stmt);
        if ($crsr_hdl === false) {
            $err = '';
            $errMsg = '';
            $this->crsr->GETLASTERR($stmt, $err, $errMsg);
            if (DEBUG_DB)
            file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/cylpdb_errors.txt' , $stmt . " " . $errMsg. "\r\n", FILE_APPEND);
        } else { // added else statement tom
            $NumberOfRecords = $this->crsr->CRSR_DATAVIEW($crsr_hdl, $ActualDataArray);
            $this->crsr->CRSR_CLOSE($crsr_hdl);
        }
        $seqnum = 'AAAAAAAAAAAAAAAA';
        if ($NumberOfRecords > 0) {
            $seqnum = (string) $ActualDataArray[0]['SEQID'];
        }

        $array1 = array(
            'A',
            'B',
            'C',
            'D',
            'E',
            'F',
            'G',
            'H',
            'I',
            'J',
            'K',
            'L',
            'M',
            'N',
            'O',
            'P',
            'Q',
            'R',
            'S',
            'T',
            'U',
            'V',
            'W',
            'X',
            'Y',
            'Z',
            '0',
            '1',
            '2',
            '3',
            '4',
            '5',
            '6',
            '7',
            '8',
            '9'
        );
        $array2 = array();
        $array2[0] = substr($seqnum, 0, 1);
        $array2[1] = substr($seqnum, 1, 1);
        $array2[2] = substr($seqnum, 2, 1);
        $array2[3] = substr($seqnum, 3, 1);
        $array2[4] = substr($seqnum, 4, 1);
        $array2[5] = substr($seqnum, 5, 1);
        $array2[6] = substr($seqnum, 6, 1);
        $array2[7] = substr($seqnum, 7, 1);
        $array2[8] = mb_substr($seqnum, 8, 1);
        $array2[9] = mb_substr($seqnum, 9, 1);
        $array2[10] = mb_substr($seqnum, 10, 1);
        $array2[11] = mb_substr($seqnum, 11, 1);
        $array2[12] = mb_substr($seqnum, 12, 1);
        $array2[13] = mb_substr($seqnum, 13, 1);
        $array2[14] = mb_substr($seqnum, 14, 1);
        $array2[15] = mb_substr($seqnum, 15, 1);

        $i = 15;
        $n = 0;
        do {

            $result = (array_keys($array1, $array2[$i]));
            $position = $result[0];

            if ($position == 35) {
                $array2[$i] = 'A';
            } else {
                $position ++;
                $array2[$i] = $array1[$position];
                $n = 16;
            }
            $n ++;
            $i --;
        } while ($n <= 16);

        $unique = implode("", $array2);

        if ($NumberOfRecords == 0) {
            $stmt = "INSERT INTO ylpautochar (AUTOTYPE, SEQID) VALUES ('$key', '$unique')";
            if (DEBUG_DB)
                file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/cylpdb_sql.txt' , $stmt . "\r\n", FILE_APPEND);
            $crsr_hdl = $this->crsr->CRSR_OPEN($stmt);
        } else {
            $stmt = "UPDATE ylpautochar SET SEQID = '$unique' WHERE AUTOTYPE = '$key'";
            if (DEBUG_DB)
                file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/cylpdb_sql.txt' , $stmt . "\r\n", FILE_APPEND);
            $crsr_hdl = $this->crsr->CRSR_OPEN($stmt);
        }
        if ($crsr_hdl === false) {
            $err = '';
            $errMsg = '';
            $this->crsr->GETLASTERR($stmt, $err, $errMsg);
            file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/cylpdb_errors.txt' , $stmt . " " . $errMsg. "\r\n", FILE_APPEND);
        }

        $this->crsr->CRSR_CLOSE($crsr_hdl);
        $this->crsr->ENDTRANSACTION('');
        $this->crsr->SETAUTOCOMMIT(TRUE);
        return substr($unique, 0, 16);
    }

    public function DESCRIBE_TABLE($table, &$rec_view)
    {
        $numofqueryrows = 0;
        $dbo = "";
        if ($this->mssql) {
            $dbo = "dbo.";
        }
        $stmt = "DESCRIBE " . $dbo . $table;
        if ($this->mssql) {
            // $stmt = "SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'docrppf'";
            $stmt = "SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '$table'";
        }
        if (DEBUG_DB)
            file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/cylpdb_sql.txt' , $stmt . "\r\n", FILE_APPEND);
        $crsr_hdl = $this->crsr->CRSR_OPEN($stmt);
        if ($crsr_hdl === false) {
            // echo "NO GO";
            $err = '';
            $errMsg = '';
            $this->crsr->GETLASTERR($stmt, $err, $errMsg);
            file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/cylpdb_errors.txt' , $stmt . " " . $errMsg . "\r\n", FILE_APPEND);
        } else { // added else statement tom
            $numofqueryrows = $this->crsr->CRSR_DATAVIEW($crsr_hdl, $rec_view);
        }
        $this->crsr->CRSR_CLOSE($crsr_hdl);
        return $numofqueryrows;
    }

    public function ADD_RECORD($table, $dataarray)
    {
        return $this->ADDALLDBDATA($table, $this->BUILD_STMT(OP_ADD, $table, $dataarray, null, null));
    }

    public function UPD_RECORD($table, $dataarray, $wherearray)
    {
        return $this->UPDALLDBDATA($table, $this->BUILD_STMT(OP_UPDATE, $table, $dataarray, $wherearray, null));
    }

    public function DELETE_RECORD($table, $wherearray)
    {
        $this->DLTALLDBDATA($table, $this->BUILD_STMT(OP_DELETE, $table, null, $wherearray, null));
    }

    public function GETDATAWITHLIMITS($table, $select, $where, $limit, $orderby, &$rec_view)
    {
        $limitslt = '';
        $intpos = strpos($limit, ",");
        if ($intpos > 0) {
            $startingrecord = (int)substr($limit, 0, $intpos) + 1;
            $recordcount = (int)substr($limit, $intpos + 1) + $startingrecord;
            $limitslt = "row >= " . $startingrecord . " and row < " . $recordcount;
        } else {
            $limitslt = "row = $limit";
        }
        $query = "SELECT $select FROM (SELECT $select, ROW_NUMBER() OVER (ORDER BY $orderby) as row FROM dbo.$table WHERE (CMPC = $this->cmpc)" .
            (strlen($where)>0?" AND $where":"") . ") a WHERE $limitslt";
        $NumberofQueryRows = 0;

        if (DEBUG_DB)
            file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/cylpdb_sql.txt' , $query . "\r\n", FILE_APPEND);
        $crsr_hdl = $this->crsr->CRSR_OPEN($query);
        if ($crsr_hdl === false) {
            // echo "NO GO";
            $err = '';
            $errMsg = '';
            $this->crsr->GETLASTERR($query, $err, $errMsg);
            file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/cylpdb_errors.txt' , $query . " " . $errMsg. "\r\n", FILE_APPEND);
        } else { // added else statement tom
            $NumberofQueryRows = $this->crsr->CRSR_DATAVIEW($crsr_hdl, $rec_view);
            $this->crsr->CRSR_CLOSE($crsr_hdl);
        }
        return $NumberofQueryRows;
    }

    public function GETDATA($table, $wherearray, $orderby, &$rec_view)
    {
        return $this->GETALLDBDATA($table, $this->BUILD_STMT(OP_GET, $table, null, $wherearray, $orderby), $rec_view);
    }

    public function GETDATACOUNT($table, $query)
    {
        $rec_view = array();
        $numofqueryrows = 0;
        if ($query != "") {
            $stmt = "SELECT COUNT(*) FROM " . $table . " WHERE " . $query . " ";
        } else {
            $stmt = "SELECT COUNT(*) FROM " . $table . " WHERE ";
        }
        if (DEBUG_DB)
            file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/cylpdb_sql.txt' , $stmt . "\r\n", FILE_APPEND);
        $crsr_hdl = $this->crsr->CRSR_OPEN($stmt);
        if ($crsr_hdl === false) {
            $err = '';
            $errMsg = '';
            $this->crsr->GETLASTERR($stmt, $err, $errMsg);
            file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/cylpdb_errors.txt' , $stmt . " " . $errMsg. "\r\n", FILE_APPEND);
        } else { // added else statement tom
            $numofqueryrows = $this->crsr->CRSR_DATAVIEW($crsr_hdl, $rec_view);
            $this->crsr->CRSR_CLOSE($crsr_hdl);
        }
        if ($numofqueryrows > 0) {
            return $rec_view[0];
        }
        return 0;
    }

    public function GETALLDBDATAQUERYSTRING($query, &$rec_view)
    {
        $NumberofQueryRows = 0;

        if (DEBUG_DB)
            file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/cylpdb_sql.txt' , $query . "\r\n", FILE_APPEND);
        $crsr_hdl = $this->crsr->CRSR_OPEN($query);
        if ($crsr_hdl === false) {
            // echo "NO GO";
            $err = '';
            $errMsg = '';
            $this->crsr->GETLASTERR($query, $err, $errMsg);
            file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/cylpdb_errors.txt' , $query . " " . $errMsg. "\r\n", FILE_APPEND);
        } else { // added else statement tom
            $NumberofQueryRows = $this->crsr->CRSR_DATAVIEW($crsr_hdl, $rec_view);
            $this->crsr->CRSR_CLOSE($crsr_hdl);
        }
        return $NumberofQueryRows;
    }

    private function GETALLDBDATA($table, $slt, &$rec_view)
    {

        $numofqueryrows = 0;
        $dbo = "";
        if ($this->mssql) {
            $dbo = "dbo.";
        }

        if ($slt != "") {
            $stmt = "SELECT * FROM " . $dbo . $table . $slt . " ";
        } else {
            $stmt = "SELECT * FROM " . $dbo . $table . " FOR FETCH ONLY OPTIMIZE FOR 100 ROWS";
        }
        if (DEBUG_DB)
            file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/cylpdb_sql.txt' , $stmt . "\r\n", FILE_APPEND);
        $crsr_hdl = $this->crsr->CRSR_OPEN($stmt);

        if ($crsr_hdl === false) {
            $err = '';
            $errMsg = '';
            $this->crsr->GETLASTERR($stmt, $err, $errMsg);
            file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/cylpdb_errors.txt' , $stmt . " " . $errMsg. "\r\n", FILE_APPEND);
        } else { // added else statement tom
            $numofqueryrows = $this->crsr->CRSR_DATAVIEW($crsr_hdl, $rec_view);
            $this->crsr->CRSR_CLOSE($crsr_hdl);
        }
        return $numofqueryrows;
    }

    private function DLTALLDBDATA($table, $slt)
    {
        $dbo = "";
        if ($this->mssql) {
            $dbo = "dbo.";
        }

        $stmt = "DELETE  FROM " . $dbo . $table . $slt . " ";
        if (DEBUG_DB)
            file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/cylpdb_sql.txt' , $stmt . "\r\n", FILE_APPEND);

        $crsr_hdl = $this->crsr->CRSR_OPEN($stmt);
        if ($crsr_hdl === false) {
            $err = '';
            $errMsg = '';
            $this->crsr->GETLASTERR($stmt, $err, $errMsg);
            file_put_contents ($_SERVER["DOCUMENT_ROOT"]. '/logs/cylpdb_errors.txt' , $stmt . " " . $errMsg. "\r\n", FILE_APPEND);
        } else {
            $this->crsr->CRSR_CLOSE($crsr_hdl);
        }
    }

    private function UPDALLDBDATA($table, $slt)
    {
        $dbo = "";
        if ($this->mssql) {
            $dbo = "dbo.";
        }

        $stmt = "UPDATE " . $dbo . $table . " " . $slt . " ";
        if (DEBUG_DB)
            file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/cylpdb_sql.txt' , $stmt . "\r\n", FILE_APPEND);

        $crsr_hdl = $this->crsr->CRSR_OPEN($stmt);
        if ($crsr_hdl === false) {
            $err = '';
            $errMsg = '';
            $this->crsr->GETLASTERR($stmt, $err, $errMsg);
            file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/cylpdb_errors.txt' , $stmt . " " . $errMsg. "\r\n", FILE_APPEND);
            return false;
        } else {
            $this->crsr->CRSR_CLOSE($crsr_hdl);
            return true;
        }
    }

    private function ADDALLDBDATA($table, $slt)
    {
        $dbo = "";
        if ($this->mssql) {
            $dbo = "dbo.";
        }
        $stmt = "INSERT INTO " . $dbo . $table . " " . $slt . " ";
        if (DEBUG_DB)
            file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/cylpdb_sql.txt' , $stmt . "\r\n", FILE_APPEND);
        $crsr_hdl = $this->crsr->CRSR_OPEN($stmt);
        if ($crsr_hdl === false) {
            $err = 0;
            $errMsg = null;
            $this->crsr->GETLASTERR($stmt, $err, $errMsg);
            file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/cylpdb_errors.txt' , $stmt . " " . $errMsg. "\r\n", FILE_APPEND);
            return false;
        } else {
            $this->crsr->CRSR_CLOSE($crsr_hdl);
            return true;
        }
    }

    private function BUILD_STMT($operation, $table, $dataarray, $wherearray, $orderbyarray)
    {
        $coma = '';
        $do_once = '';
        $sltu = '';
        switch ($operation) {
            case OP_ADD:
                $sltu = " SET";
                break;
            case OP_UPDATE:
                $sltu = " SET";
                break;
            case OP_DELETE:
                break;
            case OP_GET:
                break;
        }
        if ($this->mssql && $operation == OP_ADD) {
            if ($dataarray != null) {
                $keys = array_keys($dataarray);
                $values = array_values($dataarray);
                // if the first char is ' then replace all the ' in between first and last char
                // if count ' is > 2
                for ($i = 0; $i < count($values); $i++)
                {
                    if (substr_count($values[$i], "'") > 2) {
                        $strlen =  strlen($values[$i]);
                        $values[$i] = "'" . str_replace("'", "''", substr($values[$i], 1, $strlen -2)) ."'";
                    }
                    $values[$i] = htmlspecialchars($values[$i]);
                }

                $sltu = "(" . implode(",", $keys) . ") VALUES (" . implode(",", $values) . ")";
            }
        } else {
            if ($dataarray != null) {
                foreach ($dataarray as $key => $value) {
                    if ($do_once == '') {
                        $do_once = 'x';
                        $coma = " ";
                    } else {
                        $coma = ", ";
                    }
                    if (substr_count($value, "'") > 2) {
                        $strlen =  strlen($value);
                        $value = "'" . str_replace("'", "''", substr($value, 1, $strlen -2)) ."'";
                    }

                    $sltu .= $coma . $key . "=" . $this->ESCAPE_MYSQL($value);
                }
            }
        }

        if ($wherearray != null) {
            $sltu .= " WHERE ";
            $do_once = '';
            foreach ($wherearray as $key => $value) {
                if (is_array($value)) {
                    foreach ($value as $value1) {
                        // important to use $key here, not $key1
                        $leading = "";
                        if (substr_count($value1, "'") > 2) {
                            $strlen =  strlen($value1);
                            $start = 1;
                            $end = $strlen -2;
                            if (substr($value1, 0, 1) != "'")
                            {
                                $start = 2; $end = $strlen -3;
                                $leading = substr($value1, 0, 1);
                            }
                            // L'COTTON O'*'
                            $value1 = $leading . "'" . str_replace("'", "''", substr($value1, $start, $end)) ."'";
                        }
                        if($leading != "")
                            $value1 = $leading . substr($value1, 1);

                        $sltu .= $this->WHERE_CLAUSE($key, $value1, $do_once) . ' ';
                    }
                } else {
                    $check_in = substr($value, 0, 3);
                    $leading = "";
                    if (substr_count($value, "'") > 2) {
                        $strlen =  strlen($value);
                        if ($check_in != 'IN ' && $check_in != 'NOT') {
                            $start = 1;
                            $end = $strlen -2;
                            if (substr($value, 0, 1) != "'")
                            {
                                $start = 2; $end = $strlen -3;
                                $leading = substr($value, 0, 1);
                            }
                            // L'COTTON O'*'
                            if ($leading != ")") {
                                $value = $leading . "'" . str_replace("'", "''", substr($value, $start, $end)) ."'";
                            }
                        }
                    }
                    if ($check_in == 'IN ' || $check_in == 'NOT')
                        $value = $leading . substr($value, 3);
                    else if (strpbrk(substr($value, 0, 1), "<>!()L|"))
                        $value = substr($value, 0, 1) . substr($value, 1);

                    $sltu .= $this->WHERE_CLAUSE($key, $value, $do_once);
                }
                $do_once = 'X';
            }
        }

        if ($orderbyarray != null) {
            $sltu .= " ORDER BY ";

            $do_once = '';
            foreach ($orderbyarray as $key => $value) {
                if ($do_once == '') {
                    $do_once = 'x';
                    $sltu .= $this->ESCAPE_MYSQL($value);
                } else {
                    $sltu .= ", " . $this->ESCAPE_MYSQL($value);
                }
            }
        }
        return $sltu;
    }

    private function WHERE_CLAUSE($key, $value, $do_once)
    {
        $strRetVal = '';
        $or = ' and ';
        switch (substr($value, 0, 1)) {
            case "<":
                $compare = ' < ';
                $value = substr($value, 1);
                break;
            case ">":
                $compare = ' > ';
                $value = substr($value, 1);
                break;
            case "!":
                $compare = " != ";
                $value = substr($value, 1);
                break;
            case "L":
                $compare = " LIKE ";
                $value = substr($value, 1);
                break;
            case "|":
                $compare = " = ";
                $or = ' OR ';
                $value = substr($value, 1);
                break;
            case "(":
                $compare = " <= ";
                $value = substr($value, 1);
                break;
            case ")":
                $compare = " >= ";
                $value = substr($value, 1);
                break;
            case "I":
                $compare = " ";
                $value = substr($value, 0);
                break;
            case "N":
                $compare = " ";
                $value = substr($value, 0);
                break;
            default:
                $compare = " = ";
        }

        if ($do_once == '') {
            $strRetVal = $key . $compare . $this->ESCAPE_MYSQL($value);
        } else {
            $strRetVal = $or . $key . $compare . $this->ESCAPE_MYSQL($value);
        }

        return $strRetVal;
    }

    private function ESCAPE_MYSQL($value)
    {
        if (! $this->mssql) {
            $valuein = substr($value, 0, 4);
            $valuenotin = substr($value, 0, 8);
            //if ($valuein == 'NOT ') {
            //$valuetemp = 'X';
            //}
            if ($valuein != 'IN (' and $valuenotin != 'NOT IN (') {
              //  $value = addslashes($value); this was removed for mysql.. add back in if sql express is being used.
            }
        }
        return $value;
        /*
         * will not convert these chars \b A backspace character. \n A newline (linefeed) character. \r A carriage return character. \t A tab character. \Z ASCII 26 (Control+Z). See note following the table.
         */
    }
}

?>