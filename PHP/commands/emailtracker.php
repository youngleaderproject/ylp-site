<?php
require_once str_replace('\\', '/', $_SERVER["DOCUMENT_ROOT"]) . '/PHP/shared/auto_load.php';

use \PHP\CLASSES\CUSER;
use \PHP\CLASSES\CYLPDB;
if (!empty($_GET['email'])) {
    $user = new CUSER();

    $branch = $_GET['branch'];
    $email = $_GET['email'];
    $seq = $_GET['seq'];
    $userinfo = $user->GET_USER_BY_EMAIL($email);
    $db = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
    $loada = array();
    $loada['user_login'] = "'$email'";
    $loada['audit_type'] = "'OPENED EMAIL $branch $seq'";
    $loada['audit_info'] = "'The user with email address $email opened email branch $branch and sequence $seq.'";
    $db->ADD_RECORD('ylpauditinfo', $loada);

    // create a new cURL resource
    $ch = curl_init();

    // set URL and other appropriate options
    curl_setopt($ch, CURLOPT_URL, "https://www.google-analytics.com/collect?v=1&tid=UA-149676926-1&cid=CLIENT_ID_NUMBER&t=event&ec=email&ea=open&el=recipient_id&cs=newsletter&cm=email&cn=SurveyMonkey1&dp=%2FSurveyMonkey%2FRound1&dt=Survey%20MVP1");
    curl_setopt($ch, CURLOPT_HEADER, 0);

    // grab URL and pass it to the browser
    curl_exec($ch);

    // close cURL resource, and free up system resources
    curl_close($ch);
    exit;
}