<?php
chdir('./public_html/');
require_once str_replace('\\', '/', $_SERVER["DOCUMENT_ROOT"]) . '/PHP/shared/auto_load.php';

use PHP\CLASSES\CUSER;
use PHP\CLASSES\CSUBSCRIPTION;

$user = new CUSER;
$subscription = new CSUBSCRIPTION;

$bills = $subscription->CYCLE_MONTHLY();

if(!is_null($bills)){
    for($i = 0;$i <= sizeof($bills); $i++){
        $charge = $subscription->RENEW_SUBSCRIPTION($bills[$i]);
        $user = $bills[$i]['user_id'];
        file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/billcycle.txt' , "Subscription attempt $charge->status for $user\r\n", FILE_APPEND);
    }
}


file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/billcycle.txt' , "-------Job Complete-------\r\n", FILE_APPEND);