<?php
use PHP\CLASSES\CYLPDB;
use PHP\CLASSES\CUSER;

$type = (isset($_REQUEST['type'])) ? $_REQUEST['type'] : '';

switch ($type) {
    case 'newdev':
        header(\YLPCONSTANTS\CONTENT_TYPE_JSON);
        echo json_encode(GetNewDevNum());
        break;
    default:
        echo "Email Function Failed";
        break;
}

function GetNewDevNum(){
    $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
    $today = new DateTime();
    $today = $today->format('Y-m-d H:i:s');
    $newobj = new stdClass();
    $newobj->DEV = $dba->UNIQUENUMBER('DEV');
    $newobj->TIME = $today;
    return $newobj;
}
?>