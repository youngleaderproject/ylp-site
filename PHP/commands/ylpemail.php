<?php
use PHPMailer\PHPMailer;
use PHPMailer\Exception;
use PHP\CLASSES\CUSER;
use PHP\CLASSES\CEMAIL;
use PHP\CLASSES\CYLPDB;


$type = (isset($_REQUEST['type'])) ? $_REQUEST['type'] : '';


switch ($type) {
    case 'list':
        header(\YLPCONSTANTS\CONTENT_TYPE_XML);
        echo BuildEmailList();
        break;
    case 'buildtemp':
        header(\YLPCONSTANTS\CONTENT_TYPE_HTML);
        BuildEmailTemplate();
        break;
    case 'buildaddtemp':
        header(\YLPCONSTANTS\CONTENT_TYPE_HTML);
        BuildAddEmailTemplate();
        break;
    case 'listbeta':
        header(\YLPCONSTANTS\CONTENT_TYPE_XML);
        echo BuildBetaList();
        break;
    case 'listactive':
        header(\YLPCONSTANTS\CONTENT_TYPE_XML);
        echo BuildActiveList();
        break;
    case 'sendemail':
        SendEmail();
        break;
    case 'submitaddemail':
        SubmitAddEmail();
        break;
    case 'addcountry':
        SendAddCountry();
        break;
    default:
        echo "Email Function Failed";
        break;
}

function SendAddCountry(){
    $email = new CEMAIL();
    $country = (isset($_REQUEST['country'])) ? $_REQUEST['country'] : '';
    $emailadd = (isset($_REQUEST['email'])) ? $_REQUEST['email'] : '';
    $language = (isset($_REQUEST['language'])) ? $_REQUEST['language'] : '';
    if($email->AddCountry($emailadd,$country,$language)){
        echo "DONE";   
    }
}

function BuildEmailList(){
    $_xml = null;
    $root = '<emails></emails>';
    $xml = [];
    $email = new CEMAIL();
    $emaillist = $email->GetEmailList();
    foreach ($emaillist as $dbvalue) {
        $xml['email']['embranch'] = $dbvalue['emailBranch'];
        $xml['email']['emseq'] = $dbvalue['emailSeq'];
        $xml['email']['emsub'] = $dbvalue['emailSub'];
        $xml['email']['emaltb'] = $dbvalue['emailAltB'];
        $xml['email']['embody'] = $dbvalue['emailBody'];
        $xml['email']['empath'] = $dbvalue['emailPath'];
        $xml['email']['emhost'] = $dbvalue['emailHost'];
        $_xml = arrayToXml($xml, $root, $_xml);
    }
    if ($_xml === null) {
        $_xml = arrayToXml($xml, $root, null);
    }
    return $_xml->asXML();
}

function BuildEmailTemplate(){
    $branch = (isset($_REQUEST['embranch'])) ? $_REQUEST['embranch'] : '';
    $seq = (isset($_REQUEST['emseq'])) ? $_REQUEST['emseq'] : '';
    $bodytext = (isset($_REQUEST['embody'])) ? $_REQUEST['embody'] : '';
    $host = (isset($_REQUEST['emhost'])) ? $_REQUEST['emhost'] : '';
    $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
    $dba->crsr->unicode = true;
    $query = "SELECT * FROM ylpemails WHERE emailBranch = '$branch' and emailSeq = $seq and emailHost = '$host'";
    $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);
    $dba->crsr->unicode = false;
    $bodytext = (isset($ActualDataArray[0]['emailBody']) ? $ActualDataArray[0]['emailBody'] : '');
    $path = (isset($ActualDataArray[0]['emailPath']) ? $ActualDataArray[0]['emailPath'] : '/PHP/html/404.html');
    switch ($branch) {
        case '0':
            $body = file_get_contents($_SERVER[ "DOCUMENT_ROOT" ] . $path,true);
            if($bodytext > ""){
                $body = str_replace("~~YLPHTMLBODY~~",$bodytext,$body);
            }
            $body = str_replace("cid:logo_top","../../images/NewImages/YLPMVPlogo.png",$body);
            $body = str_replace("cid:welcome_img","../../images/NewImages/welcome-image.png",$body);
            $body = str_replace("cid:text_img","../../images/NewImages/text@2x.png",$body);
            $body = str_replace("cid:logo_fb","../../images/NewImages/fb@2x.png",$body);
            $body = str_replace("cid:logo_twitter","../../images/NewImages/twitter@2x.png",$body);
            $body = str_replace("cid:logo_insta","../../images/NewImages/ig@2x.png",$body);
            $body = str_replace("cid:pdf_button","../../images/NewImages/PDF-Button.png",$body);
            echo $body;
            break;
        case 'S':
            $body = file_get_contents($_SERVER[ "DOCUMENT_ROOT" ] . $path,true);
            $body = str_replace("cid:logo_top","../../images/NewImages/Survery-Header-image@2x.png",$body);
            $body = str_replace("cid:survey_btn","../../images/NewImages/Survery-Primary-Button@2x.png",$body);
            echo $body;
            break;
        case 'MKT':
            $body = file_get_contents($_SERVER[ "DOCUMENT_ROOT" ] . $path,true);
            if($bodytext > ""){
                $body = str_replace("~~YLPHTMLBODY~~",$bodytext,$body);
            }
            $body = str_replace("cid:logo_top","../../images/NewImages/YLPMVPlogo.png",$body);
            $body = str_replace("cid:heykiddologo_top", '../../heykiddo/images/logo-horizontal-tagline.png',$body);
            $body = str_replace("cid:welcome_img","../../images/NewImages/welcome-image.png",$body);
            $body = str_replace("cid:text_img","../../images/NewImages/text@2x.png",$body);
            $body = str_replace("cid:logo_fb","../../images/NewImages/fb@2x.png",$body);
            $body = str_replace("cid:logo_twitter","../../images/NewImages/twitter@2x.png",$body);
            $body = str_replace("cid:logo_insta","../../images/NewImages/ig@2x.png",$body);
            $body = str_replace("cid:pdf_button","../../images/NewImages/PDF-Button.png",$body);
            echo $body;
            break;
        default:
            echo "Email Function Failed";
            break;
    }
}

function BuildAddEmailTemplate(){
    $host = (isset($_REQUEST['emhost'])) ? $_REQUEST['emhost'] : '';
    $seq = (isset($_REQUEST['emseq'])) ? $_REQUEST['emseq'] : 0;
    $html = (isset($_REQUEST['emhtml'])) ? $_REQUEST['emhtml'] : '';
    $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
    $dba->crsr->unicode = true;
    $query = "SELECT * FROM ylpemails WHERE emailHost = '$host' and emailSeq = $seq and emailBranch = 'T'";
    $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);
    $dba->crsr->unicode = false;
    $path = (isset($ActualDataArray[0]['emailPath']) ? $ActualDataArray[0]['emailPath'] : '/PHP/html/404.html');
    $body = file_get_contents($_SERVER[ "DOCUMENT_ROOT" ] . $path,true);
    if($html > ""){
        $body = str_replace("~~YLPHTMLBODY~~",$html,$body);
    }
    $body = str_replace("cid:logo_top","../../images/NewImages/YLPMVPlogo.png",$body);
    $body = str_replace("cid:heykiddologo_top", '../../heykiddo/images/logo-horizontal-tagline.png',$body);
    $body = str_replace("cid:welcome_img","../../images/NewImages/welcome-image.png",$body);
    $body = str_replace("cid:text_img","../../images/NewImages/text@2x.png",$body);
    $body = str_replace("cid:logo_fb","../../images/NewImages/fb@2x.png",$body);
    $body = str_replace("cid:logo_twitter","../../images/NewImages/twitter@2x.png",$body);
    $body = str_replace("cid:logo_insta","../../images/NewImages/ig@2x.png",$body);
    $body = str_replace("cid:pdf_button","../../images/NewImages/PDF-Button.png",$body);
    $body = str_replace("cid:survey_btn","../../images/NewImages/Survery-Primary-Button@2x.png",$body);
    echo $body;
}

function BuildBetaList(){
    $branch = (isset($_REQUEST['embranch'])) ? $_REQUEST['embranch'] : '';
    $seq = (isset($_REQUEST['emseq'])) ? $_REQUEST['emseq'] : '';
    $remind = (isset($_REQUEST['remind'])) ? $_REQUEST['remind'] : '';
    $host = (isset($_REQUEST['emhost'])) ? $_REQUEST['emhost'] : '';
    $_xml = null;
    $root = '<emails></emails>';
    $xml = [];
    $email = new CEMAIL();
    $emaillist = $email->GetBetaEmailList($branch,$seq,$remind);
    foreach ($emaillist as $dbvalue) {
        $xml['email']['emuser'] = $dbvalue['user_name'];
        $xml['email']['emadd'] = $dbvalue['user_email'];
        $_xml = arrayToXml($xml, $root, $_xml);
    }
    if ($_xml === null) {
        $_xml = arrayToXml($xml, $root, null);
    }
    return $_xml->asXML();
}

function BuildActiveList(){
    $branch = (isset($_REQUEST['embranch'])) ? $_REQUEST['embranch'] : '';
    $seq = (isset($_REQUEST['emseq'])) ? $_REQUEST['emseq'] : '';
    $remind = (isset($_REQUEST['remind'])) ? $_REQUEST['remind'] : '';
    $_xml = null;
    $root = '<emails></emails>';
    $xml = [];
    $email = new CEMAIL();
    $emaillist = $email->GetActiveEmailList($branch,$seq,$remind);
    foreach ($emaillist as $dbvalue) {
        $xml['email']['emuser'] = $dbvalue['user_name'];
        $xml['email']['emadd'] = $dbvalue['user_email'];
        $_xml = arrayToXml($xml, $root, $_xml);
    }
    if ($_xml === null) {
        $_xml = arrayToXml($xml, $root, null);
    }
    return $_xml->asXML();
}

function SubmitAddEmail(){
    $branch = (isset($_REQUEST['embranch'])) ? $_REQUEST['embranch'] : '';
    $seq = (isset($_REQUEST['emseq'])) ? $_REQUEST['emseq'] : 1;
    $host = (isset($_REQUEST['emhost'])) ? $_REQUEST['emhost'] : '';
    $sub = (isset($_REQUEST['emsub'])) ? $_REQUEST['emsub'] : '';
    $altsub = (isset($_REQUEST['emaltsub'])) ? $_REQUEST['emaltsub'] : '';
    $html = (isset($_REQUEST['emhtml'])) ? $_REQUEST['emhtml'] : '';

    $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);

    $query = "SELECT emailPath FROM ylpemails WHERE emailHost = '$host' and emailBranch = 'T'";
    $NumberOfRecords = $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);

    if($NumberOfRecords > 0){
        $emailpath = $ActualDataArray[0]['emailPath'];
    }else{
        $emailpath = '/PHP/Emails/HK_Template.html';
    }

    $loada = Array();
    $loada['emailHost'] = "'$host'";
    $loada['emailSub'] = "'$sub'";
    $loada['emailAltB'] = "'$altsub'";
    $loada['emailBody'] = "'$html'";
    $loada['emailBranch'] = "'$branch'";
    $loada['emailPath'] = "'$emailpath'";
    $loada['emailSeq'] = $seq;
    if($dba->ADD_RECORD('ylpemails', $loada)){
        echo 'COMPLETE';
    }else{
        echo 'FAILED';
    }
}

function SendEmail(){
    $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
    $dba->crsr->unicode = true;
    $branch = (isset($_REQUEST['embranch'])) ? $_REQUEST['embranch'] : '';
    $seq = (isset($_REQUEST['emseq'])) ? $_REQUEST['emseq'] : '';
    $user = (isset($_REQUEST['emuser'])) ? $_REQUEST['emuser'] : '';
    $emailadd = (isset($_REQUEST['emadd'])) ? $_REQUEST['emadd'] : '';
    $host = (isset($_REQUEST['emhost'])) ? $_REQUEST['emhost'] : '';
    $remind = (isset($_REQUEST['remind'])) ? $_REQUEST['remind'] : '';
    $query = "SELECT * FROM ylpemails WHERE emailBranch = '$branch' and emailSeq = $seq and emailHost = '$host'";
    $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);
    $dba->crsr->unicode = false;
    $emailbody = $ActualDataArray[0]['emailBody'];
    $email = new CEMAIL();
    $email->heykiddoflag = $host === "HK" ? true : false;
    $userinfo = ["user_email"=>$emailadd,"user_name"=>$user];
    switch ($branch) {
        case '0':
            $email->Branch0($userinfo, $ActualDataArray[0], $emailbody, $branch, $seq, $remind);
            break;
        case 'S':
            $email->BranchS($userinfo, $ActualDataArray[0], $emailbody, $branch, $seq, $remind);
            break;
        case 'MKT':
            $email->BranchM($userinfo, $ActualDataArray[0], $emailbody, $branch, $seq, $remind);
            break;
        default:
            echo "Email Function Failed";
            break;
    }
}

?>