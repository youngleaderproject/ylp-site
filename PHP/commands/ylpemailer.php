<?php
use PHPMailer\PHPMailer;
use PHPMailer\Exception;
use PHP\CLASSES\CUSER;
use PHP\CLASSES\CEMAIL;

$usr = (isset($_REQUEST['usrname'])) ? $_REQUEST['usrname'] : '';

if(strlen($usr) === 0){
    $today = new DateTime();
    $today = $today->format('Y-m-d H:i:s');
    if (DEBUG)
        file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/ylpemailerr.txt' , "No User -- $today" . "\r\n", FILE_APPEND);
    exit();
}

$user = new CUSER();
$email = new CEMAIL();

$userinfo = $user->GET_USER_BY_NAME($usr);
$emailtype = (isset($_REQUEST['type'])) ? $_REQUEST['type'] : '';
$emailparts = [];
$mail = new PHPMailer(true);
$imagepath = $_SERVER["DOCUMENT_ROOT"] .'/userimages/'. $usr . '/';

switch ($emailtype) {
    case "1":
        $emailparts = $email->FirstEmail();
        break;
    case "2":
        $emailparts = $email->SecondEmail();
        break;
    case "3":
        $emailparts = $email->ThirdEmail();
        break;
}

if(sizeof($emailparts) < 1){
    $today = new DateTime();
    $today = $today->format('Y-m-d H:i:s');
    if (DEBUG)
        file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/ylpemailerr.txt' , "No Email Parts -- $today" . "\r\n", FILE_APPEND);
    exit();
}


?>