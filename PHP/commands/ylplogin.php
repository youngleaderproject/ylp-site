<?php
namespace PHP\Commands;

use PHP\CLASSES\CUSER;
use PHP\CLASSES\CLOGINRETURN;

$ylpusr = (isset($_REQUEST['usrname'])) ? $_REQUEST['usrname'] : '';
$pass = (isset($_REQUEST['passw'])) ? $_REQUEST['passw'] : '';
$admin = (isset($_REQUEST['admin'])) ? $_REQUEST['admin'] : '';
$signin = (isset($_REQUEST['signin'])) ? $_REQUEST['signin'] : '';

$pass = urldecode($pass);

$signed_on = SignOn($ylpusr, $pass, $admin, $signin);
$loginret = new CLOGINRETURN($signed_on, $_SESSION['user']);
$_SESSION["user"]["loggedin"] = $signed_on;
$_SESSION["user"]["dev"] = session_id();

header('Content-type: application/json');
echo json_encode($loginret);

function SignOn($ylpusr, $pass, $admin, $signin)
{
    try {
        if($signin === 'Y')$_SESSION = [];
        $cuser = new CUSER();
        $_SESSION['user'] = array();
        $cuser->UserLogin($ylpusr, $pass, $admin, $_SESSION['user']);
        return true;
    }
    catch (\Exception $e) {
        return false;
    }
}