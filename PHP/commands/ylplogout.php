<?php
// Initialize the session
session_start();
require_once str_replace('\\', '/', $_SERVER["DOCUMENT_ROOT"]) .'/PHP/shared/auto_load.php';
// Unset all of the session variables
$_SESSION = array();

// Destroy the session.
session_destroy();

// Redirect to login page
Redirect("/");
exit;
?>