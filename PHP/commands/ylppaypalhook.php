<?php
require_once str_replace('\\', '/', $_SERVER["DOCUMENT_ROOT"]) . '/PHP/shared/auto_load.php';

use PHP\CLASSES\CPAYPAL;

header('Content-Type: application/json');
$request = file_get_contents('php://input');
$req_dump = print_r( $request, true );
$fp = file_put_contents( $_SERVER["DOCUMENT_ROOT"]. '/logs/webhook.txt' , $req_dump . "\r\n", FILE_APPEND);

http_response_code(200);
$requestdec = json_decode($request);
$paypal = new CPAYPAL;
switch($requestdec->event_type){

    case "BILLING.SUBSCRIPTION.CANCELLED":
        $paypal->SUB_CANCELLED($requestdec);
        exit();

    case "BILLING.SUBSCRIPTION.PAYMENT.FAILED":
        $paypal->SUB_PAYMENTFAIL($requestdec);
        exit();

    case "BILLING.SUBSCRIPTION.RE-ACTIVATED":
        $paypal->SUB_REACTIVATED($requestdec);
        exit();

    case "BILLING.SUBSCRIPTION.SUSPENDED":
        //$paypal->SUB_SUSPENDED($requestdec);
        exit();

    case "BILLING.SUBSCRIPTION.UPDATED":
        $paypal->SUB_UPDATED($requestdec);
        exit();

    case "PAYMENT.SALE.COMPLETED":
        $paypal->SUB_PAYED($requestdec);
        break;
}

?>