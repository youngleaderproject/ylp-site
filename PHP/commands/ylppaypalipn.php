<?php

require_once str_replace('\\', '/', $_SERVER["DOCUMENT_ROOT"]) . '/PHP/shared/auto_load.php';
use PHP\CLASSES\PaypalIPN;
use PHP\CLASSES\CPAYPAL;
$ipn = new PaypalIPN();
$verified = $ipn->verifyIPN();
if ($verified) {
    foreach ($_POST as $key => $value) {

        switch($key){

            case "recurring_payment_id":
                $payerid = $value;
                break;

            case "custom":
                $userinfo = $value;
                break;

            case "payer_email":
                $subemail = $value;
                break;

            case "item_number":
                $subitem = $value;
                break;

            case "product_name":
                $prodname = $value;
                break;

            case "txn_type":
                $subpay = $value;
                break;

        }
    if (DEBUG)
        file_put_contents( $_SERVER["DOCUMENT_ROOT"]. '/logs/paypalipn.txt' , $key . "----" . $value . "\r\n", FILE_APPEND);
    }

    if (DEBUG)
        file_put_contents( $_SERVER["DOCUMENT_ROOT"]. '/logs/paypalipn.txt' , $subpay . "--- subpay " . "\r\n", FILE_APPEND);

    $paypal = new CPAYPAL();
    if($subpay === "subscr_cancel"){
       // $paypal->SUB_CANCELLED($payerid);
    }else if($subpay === "subscr_signup"){
       // $paypal->SUB_PAYED($payerid, explode("~",$userinfo), $subemail, $subitem);
    }else if($subpay === "recurring_payment_profile_created"){
        $paypal->SUB_ACTIVATED($payerid, $subemail, $prodname);
    }


    if (DEBUG)
        file_put_contents( $_SERVER["DOCUMENT_ROOT"]. '/logs/paypalipn.txt' , "---------DONE----------" . "\r\n", FILE_APPEND);
}
http_response_code(200);
