<?php
require_once str_replace('\\', '/', $_SERVER["DOCUMENT_ROOT"]) . '/PHP/shared/auto_load.php';

use PHP\CLASSES\CCARD;

header('Content-Type: application/json');
$request = file_get_contents('php://input');
$req_dump = print_r( $request, true );
$fp = file_put_contents( $_SERVER["DOCUMENT_ROOT"]. '/logs/webhook.txt' , $req_dump . "\r\n", FILE_APPEND);

http_response_code(200);
$requestdec = json_decode($request);
$stripe = new CCARD;
switch($requestdec->type){

    case "charge.failed":
        $stripe->CHARGE_FAILED($requestdec);
        exit();

    case "charge.refunded":
        $stripe->CHARGE_REFUNDED($requestdec);
        exit();

    case "charge.succeeded":
        $stripe->CHARGE_SUCCEEDED($requestdec);
        exit();

    case "customer.created":
        $stripe->CUSTOMER_CREATED($requestdec);
        exit();

    case "customer.deleted":
        $stripe->CUSTOMER_DELETED($requestdec);
        exit();

    case "customer.updated":
        $stripe->CUSTOMER_UPDATED($requestdec);
        exit();
}

?>