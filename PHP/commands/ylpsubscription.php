<?php


use PHP\CLASSES\CUSER;
use PHP\CLASSES\CSUBSCRIPTION;

$user = new CUSER();
$subscription = new CSUBSCRIPTION;

$subscription->paymenttype = $_POST['paymenttype'];

$user->UserInfo($_SESSION["user"]["ylpusr"]);

$active = $subscription->CHECK_ACTIVE($user->ylpid);

if($active > 0){
    $activedate = $subscription->CHECK_ACTIVE_DATES($user->ylpid);
    if($activedate >= 0){
        $charge = new stdClass();
        $charge->status = 'active';
    }else{
        $userinfo = $subscription->USER_SUBSCRIPTION($user->ylpid);
        $charge = $subscription->RENEW_SUBSCRIPTION($userinfo);
    }
}else{
    $disabled = $subscription->CHECK_DISABLED($user->ylpid);
    if($disabled > 0){
        $userinfo = $subscription->USER_SUBSCRIPTION($user->ylpid);
        $charge = $subscription->RENEW_SUBSCRIPTION($userinfo);
    }else{
        $charge = $subscription->NEW_SUBSCRIPTION($user->ylpid, $user->ylpemail);
    }
}
?>