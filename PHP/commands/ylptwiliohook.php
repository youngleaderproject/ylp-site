<?php
require_once str_replace('\\', '/', $_SERVER["DOCUMENT_ROOT"]) . '/PHP/shared/auto_load.php';

use PHP\CLASSES\CSMS;
use PHP\CLASSES\CUSER;
use PHP\CLASSES\CYLPDB;

$textbody = trim((isset($_REQUEST['Body'])) ? $_REQUEST['Body'] : '');
$phone = (isset($_REQUEST['From'])) ? $_REQUEST['From'] : '';
$country = (isset($_REQUEST['FromCountry'])) ? $_REQUEST['FromCountry'] : '';
$req_dump = print_r( $_REQUEST, true );
$stoparray = ["STOP", "STOPALL", "UNSUBSCRIBE", "CANCEL", "END", "QUIT"];

file_put_contents( $_SERVER["DOCUMENT_ROOT"]. '/logs/twiliowebhook.txt' , $req_dump . "\r\n", FILE_APPEND);

if(strlen($phone) > 0){
    $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
    $query = "select phonecode from ylpcountries where ccode = '$country'";
    $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);
    $countrycode = $ActualDataArray[0]['phonecode'];
    $user = new CUSER();
    $sms = new CSMS();
    $userphone = str_replace($countrycode,"",$phone);
    $userinfo = $user->GET_USER_BY_PHONE($userphone);
    if(sizeof($userinfo) == 0) exit();
    if(in_array(strtoupper(trim($textbody)),$stoparray)){
        $user->SetUserStatus($user->ylpid,901);
        exit();
    }

    $query = "select * from ylptextmsgrec where user_phone = '$phone' and textMsgDT IN(SELECT MAX(textMsgDT) from ylptextmsgrec WHERE user_phone = '$phone')";
    $NumberOfRecords = $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);
    if($NumberOfRecords === 0)exit();
    $last = sizeof($ActualDataArray) - 1;
    $track = (isset($ActualDataArray[$last]['textMsgTrack']) ? $ActualDataArray[$last]['textMsgTrack'] : '');
    $branch = (isset($ActualDataArray[$last]['textMsgBranch']) ? $ActualDataArray[$last]['textMsgBranch'] : '');
    $pos = (int)(isset($ActualDataArray[$last]['textMsgPos']) ? $ActualDataArray[$last]['textMsgPos'] : 0);
    $seq = (int)(isset($ActualDataArray[$last]['textMsgSeq']) ? $ActualDataArray[$last]['textMsgSeq'] : 0);
    $response = (int)(isset($ActualDataArray[$last]['textMsgResponse']) ? $ActualDataArray[$last]['textMsgResponse'] : '');

    if($response === 1)$seq = intval($seq) + 1;
    $dba->crsr->unicode = true;
    $query = "SELECT * FROM ylptextmsgres WHERE textMsgTrack = '$track' AND textMsgBranch = '$branch' AND textMsgPos = $pos AND textMsgSeq = $seq ORDER BY textMsgSeq";
    $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray2);
    $dba->crsr->unicode = false;
    for($j = 0; $j < sizeof($ActualDataArray2);$j++){
        $value = explode("-",$ActualDataArray2[$j]['textMsgResponse']);
        file_put_contents( $_SERVER["DOCUMENT_ROOT"]. '/logs/twiliowebhook.txt' , print_r($value,true) . "\r\n", FILE_APPEND);
        for($i=0;$i<sizeof($value);$i++){
            if($value[$i] === trim($textbody)){

                if($branch === "CW8" && $pos === 4 && $seq === 1){

                    $tracks = explode("-",$user->ylptrack);
                    if((int)$textbody === (7-sizeof($tracks))){
                        $trackdesc = $sms->trackArray;
		                for($m=0;$m<sizeof($tracks);$m++){
                            unset($trackdesc[$tracks[$m]]);
                        }
                        $random_keys=array_rand($trackdesc,1);
                        $newtrack = $user->ylptrack."-".$random_keys;
                    }else{
                        $newtrack = $user->ylptrack."-".trim($textbody);
                    }

                    $user->UpdateUserTrack($newtrack);
                }
                $body = $ActualDataArray2[$j]['textMsgBody'];
                $media = (isset($ActualDataArray2[$j]['textMsgMedia']) ? explode(",",$ActualDataArray2[$j]['textMsgMedia']) : []);

                if(substr($phone,0,1) !== "+"){
                    if(strlen($phone) < 11){
                        $code = $sms->countryArray[$country]['code'];
                        $countrycode = ( $code > "" ? $code : "1");
                        $phone = '+'.$countrycode.$phone;
                    }else if (strlen($phone) === 11){
                        $phone = '+'.$phone;
                    }
                }
                $sms->SMSFunc($phone,$body,$media,$track,$branch,$pos,$seq,false,true,false);
                break;
            }
        }
    }
}


?>