<?php
use PHPMailer\PHPMailer;
use PHPMailer\Exception;
use PHP\CLASSES\CUSER;
use PHP\CLASSES\CEMAIL;
use PHP\CLASSES\CSMS;
use PHP\CLASSES\CYLPDB;


$type = (isset($_REQUEST['type'])) ? $_REQUEST['type'] : '';


switch ($type) {
    case 'list':
        header(\YLPCONSTANTS\CONTENT_TYPE_XML);
        echo BuildTxtMsgList();
        break;
    case 'listrecs':
        header(\YLPCONSTANTS\CONTENT_TYPE_XML);
        echo BuildTxtMsgRecList();
        break;
    case 'buildtemp':
        header(\YLPCONSTANTS\CONTENT_TYPE_HTML);
        BuildTextMsgTemplate();
        break;
    case 'listbeta':
        header(\YLPCONSTANTS\CONTENT_TYPE_XML);
        echo BuildBetaList();
        break;
    case 'listactive':
        header(\YLPCONSTANTS\CONTENT_TYPE_XML);
        echo BuildActiveList();
        break;
    case 'sendtext':
        SendText();
        break;
    case 'dlttext':
        DeleteText();
        break;
    case 'updatetext':
        UpdateText();
        break;
    case 'updateprops':
        UpdateProps();
        break;
    case 'addtext':
        AddText();
        break;
    default:
        echo "Email Function Failed";
        break;
}

function BuildTxtMsgList(){
    $_xml = null;
    $root = '<txtmsgs></txtmsgs>';
    $xml = [];
    $textmsg = new CSMS();
    $textmsglist = $textmsg->GetTxtMsgList();
    foreach ($textmsglist as $dbvalue) {
        $xml['txtmsg']['txtid'] = $dbvalue['textMsgID'];
        $xml['txtmsg']['txttrack'] = $dbvalue['textMsgTrack'];
        $xml['txtmsg']['txtbranch'] = $dbvalue['textMsgBranch'];
        $xml['txtmsg']['txtpos'] = $dbvalue['textMsgPos'];
        $xml['txtmsg']['txtseq'] = $dbvalue['textMsgSeq'];
        $xml['txtmsg']['txtmsgbody'] = $dbvalue['textMsgBody'];
        $xml['txtmsg']['txtmsgmedia'] = $dbvalue['textMsgMedia'];
        $xml['txtmsg']['txtassesment'] = $dbvalue['textMsgAssessment'];
        $_xml = arrayToXml($xml, $root, $_xml);
    }
    if ($_xml === null) {
        $_xml = arrayToXml($xml, $root, null);
    }
    return $_xml->asXML();
}

function BuildTxtMsgRecList(){
    $_xml = null;
    $root = '<txtmsgs></txtmsgs>';
    $xml = [];
    $textmsg = new CSMS();
    $textmsglist = $textmsg->GetTxtMsgRecList();
    foreach ($textmsglist as $dbvalue) {
        $xml['txtmsg']['txttrack'] = $dbvalue['textMsgTrack'];
        $xml['txtmsg']['txtbranch'] = $dbvalue['textMsgBranch'];
        $xml['txtmsg']['txtpos'] = $dbvalue['textMsgPos'];
        $xml['txtmsg']['txtseq'] = $dbvalue['textMsgSeq'];
        $xml['txtmsg']['txtmsgdate'] = $dbvalue['textMsgDT'];
        $xml['txtmsg']['txtphone'] = $dbvalue['user_phone'];
        $_xml = arrayToXml($xml, $root, $_xml);
    }
    if ($_xml === null) {
        $_xml = arrayToXml($xml, $root, null);
    }
    return $_xml->asXML();
}

function BuildTextMsgTemplate(){
    $track = (isset($_REQUEST['txttrack'])) ? $_REQUEST['txttrack'] : '';
    $branch = (isset($_REQUEST['txtbranch'])) ? $_REQUEST['txtbranch'] : '';
    $pos = (isset($_REQUEST['txtpos'])) ? $_REQUEST['txtpos'] : '';
    $seq = (isset($_REQUEST['txtseq'])) ? $_REQUEST['txtseq'] : '';
    $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
    $dba->crsr->unicode = true;
    $query = "SELECT * FROM ylptextmsg WHERE textMsgTrack = '$track' AND textMsgBranch = '$branch' AND textMsgPos = $pos AND textMsgSeq = $seq";
    $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);
    $dba->crsr->unicode = false;
    $bodytext = (isset($ActualDataArray[0]['textMsgBody']) ? $ActualDataArray[0]['textMsgBody'] : '');
    $body = file_get_contents($_SERVER[ "DOCUMENT_ROOT" ] . '/PHP/html/phoneTemplate.html',true);
    if(strlen($bodytext) > 0){
        $body = str_replace("~~TEXTBODY~~",'<div class="message last">'.nl2br($bodytext).'</div>',$body);
    }else{
        $body = str_replace("~~TEXTBODY~~",$bodytext,$body);
    }

    $media = (isset($ActualDataArray[0]['textMsgMedia']) ? $ActualDataArray[0]['textMsgMedia'] : '');
    if(strlen($media) > 0){
        $body = str_replace("~~TEXTMEDIA~~","<div class='message last'><img src='$media' class='img-fluid'></div>",$body);
    }else{
        $body = str_replace("~~TEXTMEDIA~~",$media,$body);
    }
    echo $body;
}

function BuildBetaList(){
    $branch = (isset($_REQUEST['txtbranch'])) ? $_REQUEST['txtbranch'] : '';
    $seq = (isset($_REQUEST['txtseq'])) ? $_REQUEST['txtseq'] : '';
    $remind = (isset($_REQUEST['remind'])) ? $_REQUEST['remind'] : '';
    $_xml = null;
    $root = '<txtmsgs></txtmsgs>';
    $xml = [];
    $txtmsg = new CSMS();
    $txtmsglist = $txtmsg->GetBetaPhoneList($branch,$seq,$remind);
    foreach ($txtmsglist as $dbvalue) {
        if($dbvalue['user_phone'] === null)continue;
        $xml['txtmsg']['txtuser'] = $dbvalue['user_name'];
        $xml['txtmsg']['txtadd'] = $dbvalue['user_phone'];
        $xml['txtmsg']['txtcntry'] = $dbvalue['user_country'];
        $_xml = arrayToXml($xml, $root, $_xml);
    }
    if ($_xml === null) {
        $_xml = arrayToXml($xml, $root, null);
    }
    return $_xml->asXML();
}

function BuildActiveList(){
    $track = (isset($_REQUEST['txttrack'])) ? $_REQUEST['txttrack'] : '';
    $branch = (isset($_REQUEST['txtbranch'])) ? $_REQUEST['txtbranch'] : '';
    $seq = (isset($_REQUEST['txtseq'])) ? $_REQUEST['txtseq'] : '';
    $remind = (isset($_REQUEST['remind'])) ? $_REQUEST['remind'] : '';
    $_xml = null;
    $root = '<txtmsgs></txtmsgs>';
    $xml = [];
    $sms = new CSMS();
    $phonelist = $sms->GetActivePhoneList($track,$branch,$seq,$remind);
    foreach ($phonelist as $dbvalue) {
        if($dbvalue['user_phone'] === null)continue;
        $xml['txtmsg']['txtuser'] = $dbvalue['user_name'];
        $xml['txtmsg']['txtadd'] = $dbvalue['user_phone'];
        $xml['txtmsg']['txtcntry'] = $dbvalue['user_country'];
        $_xml = arrayToXml($xml, $root, $_xml);
    }
    if ($_xml === null) {
        $_xml = arrayToXml($xml, $root, null);
    }
    return $_xml->asXML();
}

function DeleteText(){
    $id = (isset($_REQUEST['txtid'])) ? $_REQUEST['txtid'] : '';
    $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
    $dba->DELETE_RECORD("ylptextmsg", array('textMsgID' => $id));
}

function UpdateText(){
    $id = (isset($_REQUEST['txtid'])) ? $_REQUEST['txtid'] : '';
    $body = (isset($_REQUEST['txtmsgbody'])) ? $_REQUEST['txtmsgbody'] : '';
    $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
    $updrecord['textMsgBody'] = "'$body'";
    $where['textMsgID'] = $id;
    $dba->crsr->unicode = true;
    $dba->UPD_RECORD('ylptextmsg', $updrecord, $where);
    $dba->crsr->unicode = false;
}

function UpdateProps(){
    $id = (isset($_REQUEST['txtid'])) ? $_REQUEST['txtid'] : '';
    $branch = (isset($_REQUEST['txtbranch'])) ? $_REQUEST['txtbranch'] : '';
    $seq = (isset($_REQUEST['txtseq'])) ? $_REQUEST['txtseq'] : '';
    $pos = (isset($_REQUEST['txtpos'])) ? $_REQUEST['txtpos'] : '';
    $assessment = (isset($_REQUEST['txtassesment'])) ? $_REQUEST['txtassesment'] : '';
    $track = (isset($_REQUEST['txttrack'])) ? $_REQUEST['txttrack'] : '';
    $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
    $updrecord['textMsgBranch'] = "'$branch'";
    $updrecord['textMsgSeq'] = $seq;
    $updrecord['textMsgPos'] = $pos;
    $updrecord['textMsgAssessment'] = $assessment;
    $updrecord['textMsgTrack'] = "'$track'";
    $where['textMsgID'] = $id;
    $dba->UPD_RECORD('ylptextmsg', $updrecord, $where);
}

function AddText(){
    $branch = (isset($_REQUEST['txtbranch'])) ? $_REQUEST['txtbranch'] : '';
    $seq = (isset($_REQUEST['txtseq'])) ? $_REQUEST['txtseq'] : '';
    $pos = (isset($_REQUEST['txtpos'])) ? $_REQUEST['txtpos'] : '';
    $assessment = (isset($_REQUEST['txtassesment'])) ? ($_REQUEST['txtassesment'] === 'false'?'0':'1') : '';
    $track = (isset($_REQUEST['txttrack'])) ? $_REQUEST['txttrack'] : '';
    $body = (isset($_REQUEST['txtmsgbody'])) ? $_REQUEST['txtmsgbody'] : '';
    $media = (isset($_REQUEST['txtmedia'])) ? $_REQUEST['txtmedia'] : '';
    $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
    $loada = array();
    $loada['textMsgBranch'] = "'$branch'";
    $loada['textMsgSeq'] = $seq;
    $loada['textMsgPos'] = $pos;
    $loada['textMsgAssessment'] = $assessment;
    $loada['textMsgTrack'] = "'$track'";
    $loada['textMsgBody'] = "'$body'";
    $loada['textMsgMedia'] = !empty($payid) ? "'$media'" : "NULL";
    $dba->crsr->unicode = true;
    $dba->ADD_RECORD('ylptextmsg', $loada);
    $dba->crsr->unicode = false;
}

function SendText(){
    $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
    $dba->crsr->unicode = true;
    $branch = (isset($_REQUEST['txtbranch'])) ? $_REQUEST['txtbranch'] : '';
    $track= (isset($_REQUEST['txttrack'])) ? $_REQUEST['txttrack'] : '';
    $seq = (isset($_REQUEST['txtseq'])) ? $_REQUEST['txtseq'] : '';
    $pos = (isset($_REQUEST['txtpos'])) ? $_REQUEST['txtpos'] : '';
    $name = (isset($_REQUEST['txtuser'])) ? $_REQUEST['txtuser'] : '';
    $phone = (isset($_REQUEST['txtadd'])) ? $_REQUEST['txtadd'] : '';
    $country = (isset($_REQUEST['txtcntry'])) ? $_REQUEST['txtcntry'] : '';
    if(substr($phone,0,1) === "+"){
        $phone = substr($phone,1);
    }
    $remind = (isset($_REQUEST['remind']) && $_REQUEST['remind'] === 'Y') ? true : false;
    $query = "SELECT * FROM ylptextmsg WHERE textMsgTrack = '$track' AND textMsgBranch = '$branch' AND textMsgPos = $pos AND textMsgSeq = $seq";
    $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);
    $dba->crsr->unicode = false;
    $body = $ActualDataArray[0]['textMsgBody'];
    $query = "SELECT * from ylpusrtb where user_phone like '%$phone%'";
    $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray3);
    $sms = new CSMS();
    if(strpos($body,"~~USERKEY~~") > -1){
        $tracks = explode("-",$ActualDataArray3[0]["user_track"]);
        $currenttrack = $tracks[sizeof($tracks)-1];
        $userid = $ActualDataArray3[0]["user_id"];
        $body = str_replace("~~USERKEY~~","k=".$userid."&t=".$currenttrack,$body);
        $linkbody = $body;
        $linkbody = str_replace("\r\n",' ', $linkbody);
        $linkpos = strpos($linkbody,"https");
        $linkpos2 = strpos($linkbody," ",$linkpos);
        $link = substr($linkbody,$linkpos,$linkpos2-$linkpos);
        $newlink = $sms->shortenLink($link);
        $body = str_replace($link,$newlink,$body);
    }
    if(strpos($body,"~~TRACKNAME~~") > -1){
        $tracks = explode("-",$ActualDataArray3[0]['user_track']);
        $body = str_replace("~~TRACKNAME~~",$sms->getTrackName($track),$body);
        $body = str_replace("~~TRACKSEQ~~",$sms->getNextSeq($tracks),$body);
    }

    $media = (isset($ActualDataArray[0]['textMsgMedia']) ? explode(",",$ActualDataArray[0]['textMsgMedia']) : []);
    if(substr($phone,0,1) !== "+"){
        $code = $sms->countryArray[$country]['code'];
        $countrycode = ( $code > "" ? $code : "1");
        $phone = '+'.$countrycode.$phone;
    }
    $body= str_replace("~~FIRSTNAME~~",$name,$body);

    $sms->SMSFunc($phone,$body,$media,$track,$branch,$pos,$seq,$remind,false,false);
}

?>