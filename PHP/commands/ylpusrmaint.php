<?php
use PHP\CLASSES\CUSER;
use PHP\CLASSES\CYLPDB;
use PHP\CLASSES\CACAMPAIGN;
use PHP\CLASSES\CEMAIL;

$user = new CUSER();
$type = (isset($_REQUEST['type'])) ? $_REQUEST['type'] : '';
$email = (isset($_REQUEST['email'])) ? $_REQUEST['email'] : '';
$name = (isset($_REQUEST['name'])) ? $_REQUEST['name'] : '';
$phone = (isset($_REQUEST['phone'])) ? $_REQUEST['phone'] : '';
$admin = (isset($_REQUEST['admin'])) ? $_REQUEST['admin'] : '';
$login = (isset($_REQUEST['userlogin'])) ? $_REQUEST['userlogin'] : '';
$pass = (isset($_REQUEST['userpass'])) ? $_REQUEST['userpass'] : '';

switch($type){
    case "A":
        if($admin === 'Y'){
            if($user->AddAdminUser($email,$login,$pass))
                echo 'SUCCESS';
            else
                echo 'FAILED';
        }else{
            if(sizeof($user->GET_USER_BY_EMAIL($email)) > 0){
                echo 'EXISTS';
                return;
            }
            $country = $user->getLocationInfoByIp()['country'];
            $country = ($country !== '' ? $country : 'US');
            echo "COUNTRY=$country";
        }
        break;
    case "U":
        $fieldtype = (isset($_REQUEST['fieldtype'])) ? $_REQUEST['fieldtype'] : '';
        $fieldval = (isset($_REQUEST['fieldvalue'])) ? $_REQUEST['fieldvalue'] : '';
        if(sizeof($user->GET_USER_BY_EMAIL($email)) > 0){
            switch($fieldtype){
                case 'user_name':
                    $names = explode(" ",$fieldval);
                    $user->ylpname = $names[0];
                    if(sizeof($names) > 1)$user->ylplname = $names[sizeof($names)-1];
                    break;
                case 'user_email':
                    $user->ylpemail = $fieldval;
                    break;
                case 'user_pass':
                    $user->ylppwr = password_hash($fieldval, PASSWORD_DEFAULT);
                    break;
                case 'user_phone':
                    $user->ylpphone = $fieldval;
                    break;
            }
            if($user->UpdateUser()){
                echo 'SUCCESS';
            }else{
                echo 'FAILED';
            }
        }
        break;
    case "hksignup":
        $hkfname = (isset($_REQUEST['hkinputfname'])) ? $_REQUEST['hkinputfname'] : '';
        $hklname = (isset($_REQUEST['hkinputlname'])) ? $_REQUEST['hkinputlname'] : '';
        $hkpass = (isset($_REQUEST['hkinputpassword'])) ? $_REQUEST['hkinputpassword'] : '';
        $hkphone = (isset($_REQUEST['hkinputphone'])) ? $_REQUEST['hkinputphone'] : '';
        $hktimezone = (isset($_REQUEST['hktimezone'])) ? $_REQUEST['hktimezone'] : '';
        $hkphone = preg_replace("/[^0-9]/", "", $hkphone);
        $hkemail = (isset($_REQUEST['hkinputemail'])) ? $_REQUEST['hkinputemail'] : '';
        $_SESSION["signupuser"] = [];
        $_SESSION["signupuser"]['firsttime'] = true;
        $_SESSION["signupuser"]["userfname"] = $hkfname;
        $_SESSION["signupuser"]["userlname"] = $hklname;
        $_SESSION["signupuser"]["userpassword"] = $hkpass;
        $_SESSION["signupuser"]["userphone"] = $hkphone;
        $_SESSION["signupuser"]["useremail"] = $hkemail;
        $_SESSION["signupuser"]["dev"] = session_id();
        if(sizeof($user->GET_USER_BY_EMAIL($hkemail)) > 0 && $user->ylpstatus > 0 && $user->ylpstatus < 900){
            echo 'EXISTS';
            return;
        }
        if($user->CHECK_PHONE($hkphone) > 0){
            echo 'PHONEEXISTS';
            return;
        }
        file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/heykiddosign.txt' , print_r($_SESSION,true)."\r\n", FILE_APPEND);
        $country =  strtoupper((isset($_REQUEST['hkcountry'])) ? $_REQUEST['hkcountry'] : 'US');
        $user->AddUser($hkemail,$hkpass,$hkfname,$hklname,$hkphone,$country,0,0,0,'','',$hktimezone);
        break;
    case "hkFREE":
        $_SESSION["signupuser"]["usertrack"] = (isset($_REQUEST['hktrack']) ? $_REQUEST['hktrack'] : '');
        $_SESSION["signupuser"]["userttime"] = (isset($_REQUEST['hktime']) ? $_REQUEST['hktime'] : '');
        $_SESSION["signupuser"]["echoice"] = (isset($_REQUEST['echoice']) ? $_REQUEST['echoice'] : 'N');
        if(SignIn(2)){
            echo 'COMPLETE';
        }else{
            echo 'FAILED';
        }
        break;
    case "hkFREE30":
        $_SESSION["signupuser"]["usertrack"] = (isset($_REQUEST['hktrack']) ? $_REQUEST['hktrack'] : '');
        $_SESSION["signupuser"]["userttime"] = (isset($_REQUEST['hktime']) ? $_REQUEST['hktime'] : '');
        $_SESSION["signupuser"]["echoice"] = (isset($_REQUEST['echoice']) ? $_REQUEST['echoice'] : 'N');
        if(SignIn(3)){
            echo 'COMPLETE';
        }else{
            echo 'FAILED';
        }
        break;
    case "hkFREETEMP":
        $_SESSION["signupuser"]["usertrack"] = (isset($_REQUEST['hktrack']) ? $_REQUEST['hktrack'] : '');
        $_SESSION["signupuser"]["userttime"] = (isset($_REQUEST['hktime']) ? $_REQUEST['hktime'] : '');
        $_SESSION["signupuser"]["echoice"] = (isset($_REQUEST['echoice']) ? $_REQUEST['echoice'] : 'N');
        if(SignIn(3)){
            echo 'COMPLETE';
        }else{
            echo 'FAILED';
        }
        break;
    case "hkintro":
        exit();
        $useremail = (isset($_SESSION["user"]["usremail"]) ? $_SESSION["user"]["usremail"] : '');
        $user->GET_USER_BY_EMAIL($useremail);
        $selection = $user->ylptrack;
        $firstname = $user->ylpname;
        $lastname = $user->ylplname;
        $username = $firstname . " " . $lastname;
        $userphone = $user->ylpphone;
        $country = $user->getLocationInfoByIp()['country'];
        $country = ($country !== '' ? $country : 'US');
        $user->UserIntro($useremail,$firstname,$lastname,$userphone,$selection,$country);
        break;
    case "hkDONE":
        $_SESSION["signupuser"]["usertrack"] = (isset($_REQUEST['hktrack']) ? $_REQUEST['hktrack'] : '');
        $_SESSION["signupuser"]["userttime"] = (isset($_REQUEST['hktime']) ? $_REQUEST['hktime'] : '');
        $_SESSION["signupuser"]["echoice"] = (isset($_REQUEST['echoice']) ? $_REQUEST['echoice'] : 'N');
        $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : '';
        if(checkPayPal($id)){
            if(SignIn(1)){
                echo 'COMPLETE';
            }else{
                echo 'FAILED';
            }
        }
        break;
    case "fpass":
        $email = new CEMAIL();
        $hkemail = (isset($_REQUEST['hkemail'])) ? $_REQUEST['hkemail'] : '';
        if(sizeof($user->GET_USER_BY_EMAIL($hkemail)) > 0){
            $newpass = random_strings(10);
            if($user->UpdateUserPW($newpass)){
                $userinfo = [
                  "user_name"=>$user->ylpname,
                  "user_email"=>$user->ylpemail,
                  "user_pass"=>$newpass
                ];
                $email->ForgotPass($userinfo);
                echo "DONE";
            }
        }
        break;
    case "list":
        header(\YLPCONSTANTS\CONTENT_TYPE_XML);
        echo BuildUserList();
        break;
    case "upduser":
        UpdateUser();
        break;
    case "dltuser":
        DeleteUser();
        break;
    case "unsubscribe":
        UnsubscribeUser();
        break;
    default:

}

function BuildUserList(){
    $_xml = null;
    $root = '<users></users>';
    $xml = [];
    $user = new CUSER();
    $userlist = $user->GetUserList();
    foreach ($userlist as $dbvalue) {
        $xml['user']['user_id'] = $dbvalue['user_id'];
        $xml['user']['user_email'] = $dbvalue['user_email'];
        $phone = preg_replace("/^1?(\d{3})(\d{3})(\d{4})$/", "$1-$2-$3", $dbvalue['user_phone']);
        $xml['user']['user_phone'] = $phone;
        $xml['user']['user_track'] = $dbvalue['user_track'];
        $xml['user']['user_activity'] = $dbvalue['user_activity'];
        $xml['user']['user_tracktime'] = $dbvalue['user_tracktime'];
        $xml['user']['user_country'] = $dbvalue['countryname'];
        $dt1 = new DateTime($dbvalue['user_registered']);
        $date = $dt1->format("m-d-Y");
        $xml['user']['user_registered'] = $date;
        $xml['user']['user_status'] = $dbvalue['user_status'];
        $xml['user']['user_name'] = $dbvalue['user_name'];
        $xml['user']['user_lastname'] = $dbvalue['user_lastname'];
        $xml['user']['unsubscribe_date'] = $dbvalue['unsubscribed'];
        $xml['user']['sub_enddate'] = $dbvalue['sub_enddate'];
        $_xml = arrayToXml($xml, $root, $_xml);
    }
    if ($_xml === null) {
        $_xml = arrayToXml($xml, $root, null);
    }
    return $_xml->asXML();
}

function UpdateUser(){
    $useremail = (isset($_REQUEST['user_email'])) ? $_REQUEST['user_email'] : '';
    $userid = (isset($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : '';
    $phone = (isset($_REQUEST['user_phone'])) ? $_REQUEST['user_phone'] : '';
    $phone = preg_replace("/[^0-9]/", "", $phone);
    $track = (isset($_REQUEST['user_track'])) ? $_REQUEST['user_track'] : '';
    $time = (isset($_REQUEST['user_tracktime'])) ? $_REQUEST['user_tracktime'] : '';
    $status = (isset($_REQUEST['user_status'])) ? $_REQUEST['user_status'] : '';
    $user = new CUSER();
    $user->GET_USER_BY_ID($userid);
    $user->ylpemail = $useremail;
    $user->ylpphone = $phone;
    $user->ylptrack = $track;
    $user->ylptracktime = $time;
    $user->ylpstatus = $status;
    $user->UpdateUser();
}

function DeleteUser(){
    $user = new CUSER();
    $userid = (isset($_REQUEST['id']) ? $_REQUEST['id'] : '');
    $user->DeleteUser($userid);
}

function UnsubscribeUser(){
    $user = new CUSER();
    $email = (isset($_REQUEST['email'])) ? $_REQUEST['email'] : '';
    $selection = (isset($_REQUEST['unsubscribesel'])) ? $_REQUEST['unsubscribesel'] : '';
    $other = (isset($_REQUEST['otherreason'])) ? $_REQUEST['otherreason'] : '';
    $extra = (isset($_REQUEST['canweimprove'])) ? $_REQUEST['canweimprove'] : '';
    if(sizeof($user->GET_USER_BY_EMAIL($email)) > 0){
        $user->SetUserStatus($user->ylpid,996);
        $campaign = new CACAMPAIGN();
        $campaign->RemoveTagFromContact($user->ylpemail,'subscribed');
        $campaign->AddTagToContact($user->ylpemail,'unsubscribed');
        $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $loada = Array();
        $loada['user_id'] = $user->ylpid;
        $loada['unsubSEL'] = "'$selection'";
        $loada['unsubOTHER'] = "'$other'";
        $loada['unsubEXTRA'] = "'$extra'";
        $dba->ADD_RECORD('ylpunsubscribed', $loada);
        $_SESSION = array();
        session_destroy();
        echo "DONE";
        exit();
    }else{
        echo 'Failed';
    }
}


function SignIn($status){
    $selection = (isset($_SESSION["signupuser"]["usertrack"]) ? $_SESSION["signupuser"]["usertrack"] : 1);
    $time = (isset($_SESSION["signupuser"]["userttime"]) ? $_SESSION["signupuser"]["userttime"] : 1);
    $user = new CUSER();
    $firstname = (isset($_SESSION["signupuser"]["userfname"]) ? $_SESSION["signupuser"]["userfname"] : '');
    $lastname = (isset($_SESSION["signupuser"]["userlname"]) ? $_SESSION["signupuser"]["userlname"] : '');
    $useremail = (isset($_SESSION["signupuser"]["useremail"]) ? $_SESSION["signupuser"]["useremail"] : '');
    $userphone = (isset($_SESSION["signupuser"]["userphone"]) ? $_SESSION["signupuser"]["userphone"] : '');
    $userpassword = (isset($_SESSION["signupuser"]["userpassword"]) ? $_SESSION["signupuser"]["userpassword"] : '');
    $country = $user->getLocationInfoByIp()['country'];
    $country = ($country !== '' ? $country : 'US');
    $user->ylpusrexists = true;
    file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/usermaint.txt' , print_r($_SESSION,true)."\r\n", FILE_APPEND);
	$_SESSION['user'] = array();
    $customerid = (isset($_SESSION["signupuser"]["payid"]) ? $_SESSION["signupuser"]["payid"] : '');
    $subemail = (isset($_SESSION["signupuser"]["payemail"]) ? $_SESSION["signupuser"]["payemail"] : '');
    $promocode = (isset($_SESSION["signupuser"]["userpromo"]) ? $_SESSION["signupuser"]["userpromo"] : '');
    if($user->AddUser($useremail,$userpassword,$firstname,$lastname,$userphone,$country,$selection,$time,$status,$customerid,$subemail,'',$promocode)){
        try{
            $admin = 'N';
            $user->UserLogin($useremail,$userpassword,$admin,$_SESSION['user']);
            $_SESSION["user"]["loggedin"] = true;
            $campaign = new CACAMPAIGN();
            $campaign->AddTagToContact($useremail,'subscribed');
            $campaign->AddToAutomation($useremail,"5");
        }
        catch(\Exception $e){
            file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/usermaint.txt' ,$e->getMessage()."\r\n", FILE_APPEND);
            $_SESSION["user"]["loggedin"] = false;
            return false;
        }
        return true;
    }else{
        return false;
    }
}

function checkPayPal($id){
    $ch = curl_init();
    $clientId = YLP_PPKEY;
    $secret = YLP_PPSEC;
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, "https://api.paypal.com/v1/billing/subscriptions/".$id);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($ch, CURLOPT_USERPWD, $clientId.":".$secret);


    $headers = array();
    $headers[] = 'Content-Type: application/json';
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = curl_exec($ch);
    $json = json_decode($result);
    file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/paypallogs.txt' , "$result\r\n", FILE_APPEND);
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }
    curl_close($ch);
    if($json->status === "ACTIVE" || $json->status === "APPROVED" || $json->status === "APPROVAL_PENDING"){
         $_SESSION["signupuser"]["payid"] = $json->id;
         $_SESSION["signupuser"]["payemail"] = $json->subscriber->email_address;
         return true;
     }else{
         file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/paypallogs.txt' , "Error validating user ---- $result\r\n", FILE_APPEND);
         return false;
     }
}

function random_strings($length)
{
    $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    return substr(str_shuffle($str_result),0, $length);
} 