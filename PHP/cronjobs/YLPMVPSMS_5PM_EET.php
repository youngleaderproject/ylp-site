<?php
$_SERVER['DOCUMENT_ROOT'] = "/home3/youngle6/public_html";
require_once str_replace('\\', '/', $_SERVER["DOCUMENT_ROOT"]) . '/PHP/shared/auto_load.php';

use PHP\CLASSES\CYLPDB;
use PHP\CLASSES\CSMS;
use PHP\CLASSES\CUSER;

$dateTime = new DateTime();
$ActualDataArray = [];
$ActualDataArray2 = [];
$ActualDataArray3 = [];
$dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
$query = "SELECT * FROM ylpusrtb WHERE user_status > 0 AND user_status < 900 AND user_tracktime = 3 AND DATE(user_registered) <> CURDATE() AND user_timezone IN ('Europe/Athens','Asia/Beirut','Africa/Blantyre','Europe/Bucharest','Africa/Bujumbura','Africa/Cairo','Europe/Chisinau','Asia/Damascus','Africa/Gaborone','Asia/Gaza',
'Africa/Harare','Asia/Hebron','Europe/Helsinki','Europe/Istanbul','Asia/Jerusalem','Africa/Johannesburg','Europe/Kiev','Africa/Kigali','Africa/Lubumbashi',
'Africa/Lusaka','Africa/Maputo','Europe/Mariehamn','Africa/Maseru','Africa/Mbabane','Asia/Nicosia','Europe/Riga','Europe/Simferopol','Europe/Sofia','Europe/Tallinn',
'Europe/Uzhgorod','Europe/Vilnius','Europe/Zaporozhye')";
$NumberOfRecords = $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);
$sms = new CSMS();

$trackarray = [
    0=>"CW1",
    1=>"CW2",
    2=>"CW3",
    3=>"CW4",
    4=>"CW5",
    5=>"CW6",
    6=>"CW7",
    7=>"CW8",
];
sleep(5);
for($i=0;$i<$NumberOfRecords;$i++){
    if(!isset($ActualDataArray[$i]))break;
    $processed = false;
    $tracks = explode("-",$ActualDataArray[$i]['user_track']);
    $track = $tracks[sizeof($tracks)-1];
    $phone = $ActualDataArray[$i]['user_phone'];
    $country = $ActualDataArray[$i]['user_country'];
    $name = $ActualDataArray[$i]['user_name'];
    $userid = $ActualDataArray[$i]['user_id'];
    $activity = (int)$ActualDataArray[$i]['user_activity'];
    $code = $sms->countryArray[$country]['code'];
    $countrycode = ( $code > "" ? $code : "1");
    $phone = '+'.$countrycode.$phone;
    for($m=0;$m<sizeof($trackarray);$m++){

        $branch = $trackarray[$m];

        //// This is for the 1st text of the week
        $query = "SELECT user_phone FROM ylptextmsgrec WHERE user_phone='$phone' AND textMsgTrack = '$track' AND textMsgPos = 1 AND textMsgBranch = '$branch' AND textMsgResponse = 0" ;
        $NumberOfRecords3 = $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray3);
        if($NumberOfRecords3 === 0){
            $dba->crsr->unicode = true;
            $query = "SELECT * FROM ylptextmsg WHERE textMsgTrack = '$track' AND textMsgBranch = '$branch' AND textMsgPos = 1 ORDER BY textMsgPos,textMsgSeq ASC";
            $NumberOfRecords2 = $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray2);
            $dba->crsr->unicode = false;
            for($j=0; $j < $NumberOfRecords2; $j++){
                $pos = $ActualDataArray2[$j]['textMsgPos'];
                $seq = $ActualDataArray2[$j]['textMsgSeq'];
                $body = $ActualDataArray2[$j]['textMsgBody'];
                $body = str_replace("~~FIRSTNAME~~",$name,$body);
                if(strpos($body,"~~USERKEY~~") > -1){
                    $user = new CUSER();
                    $body = str_replace("~~USERKEY~~","k=".$userid."&t=".$track,$body);
                    $linkbody = $body;
                    $linkbody = str_replace("\r\n",' ', $linkbody);
                    $linkpos = strpos($linkbody,"https");
                    $linkpos2 = strpos($linkbody," ",$linkpos);
                    $link = substr($linkbody,$linkpos,$linkpos2-$linkpos);
                    $newlink = $sms->shortenLink($link);
                    $body = str_replace($link,$newlink,$body);
                    $user->UpdateUserActivity($userid,$activity+1);
                }
                $media = (isset($ActualDataArray2[$j]['textMsgMedia']) ? explode(",",$ActualDataArray2[$j]['textMsgMedia']) : []);
                $sms->SMSFunc($phone,$body,$media,$track,$branch,$pos,$seq,false,false,false);
                $processed = true;
            }
        }

        if($processed){
            unset($ActualDataArray[$i]);
            break;
        }

        //// This is for the 2nd text of the week
        $NumberOfRecords2 = 0;
        $NumberOfRecords3 = 0;
        $ActualDataArray2 = [];
        $ActualDataArray3 = [];
        $j=0;
        $k=0;
        $query = "SELECT user_phone FROM ylptextmsgrec WHERE user_phone='$phone' AND textMsgTrack = '$track' AND textMsgPos = 2 AND textMsgBranch = '$branch' AND textMsgResponse = 0" ;
        $NumberOfRecords3 = $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray3);
        if($NumberOfRecords3 === 0){
            $dba->crsr->unicode = true;
            $query = "SELECT * FROM ylptextmsg WHERE textMsgTrack = '$track' AND textMsgBranch = '$branch' AND textMsgPos = 2 ORDER BY textMsgPos,textMsgSeq ASC";
            $NumberOfRecords2 = $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray2);
            $dba->crsr->unicode = false;
            for($j=0; $j < $NumberOfRecords2; $j++){
                $pos = $ActualDataArray2[$j]['textMsgPos'];
                $seq = $ActualDataArray2[$j]['textMsgSeq'];
                $body = $ActualDataArray2[$j]['textMsgBody'];
                $body = str_replace("~~FIRSTNAME~~",$name,$body);
                if(strpos($body,"~~USERKEY~~") > -1){
                    $user = new CUSER();
                    $body = str_replace("~~USERKEY~~","k=".$userid."&t=".$track,$body);
                    $linkbody = $body;
                    $linkbody = str_replace("\r\n",' ', $linkbody);
                    $linkpos = strpos($linkbody,"https");
                    $linkpos2 = strpos($linkbody," ",$linkpos);
                    $link = substr($linkbody,$linkpos,$linkpos2-$linkpos);
                    $newlink = $sms->shortenLink($link);
                    $body = str_replace($link,$newlink,$body);
                    $user->UpdateUserActivity($userid,$activity+1);
                }
                $media = (isset($ActualDataArray2[$j]['textMsgMedia']) ? explode(",",$ActualDataArray2[$j]['textMsgMedia']) : []);
                $sms->SMSFunc($phone,$body,$media,$track,$branch,$pos,$seq,false,false,false);
                $processed = true;
            }
        }

        if($processed){
            unset($ActualDataArray[$i]);
            break;
        }

        //// This is for the 3rd text of the week
        $NumberOfRecords2 = 0;
        $NumberOfRecords3 = 0;
        $ActualDataArray2 = [];
        $ActualDataArray3 = [];
        $j=0;
        $k=0;
        $query = "SELECT user_phone FROM ylptextmsgrec WHERE user_phone='$phone' AND textMsgTrack = '$track' AND textMsgPos = 3 AND textMsgBranch = '$branch' AND textMsgResponse = 0" ;
        $NumberOfRecords3 = $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray3);
        if($NumberOfRecords3 === 0){
            $dba->crsr->unicode = true;
            $query = "SELECT * FROM ylptextmsg WHERE textMsgTrack = '$track' AND textMsgBranch = '$branch' AND textMsgPos = 3 ORDER BY textMsgPos,textMsgSeq ASC";
            $NumberOfRecords2 = $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray2);
            $dba->crsr->unicode = false;
            for($j=0; $j < $NumberOfRecords2; $j++){
                $pos = $ActualDataArray2[$j]['textMsgPos'];
                $seq = $ActualDataArray2[$j]['textMsgSeq'];
                $body = $ActualDataArray2[$j]['textMsgBody'];
                $body = str_replace("~~FIRSTNAME~~",$name,$body);
                if(strpos($body,"~~USERKEY~~") > -1){
                    $user = new CUSER();
                    $body = str_replace("~~USERKEY~~","k=".$userid."&t=".$track,$body);
                    $linkbody = $body;
                    $linkbody = str_replace("\r\n",' ', $linkbody);
                    $linkpos = strpos($linkbody,"https");
                    $linkpos2 = strpos($linkbody," ",$linkpos);
                    $link = substr($linkbody,$linkpos,$linkpos2-$linkpos);
                    $newlink = $sms->shortenLink($link);
                    $body = str_replace($link,$newlink,$body);
                    $user->UpdateUserActivity($userid,$activity+1);
                }
                $media = (isset($ActualDataArray2[$j]['textMsgMedia']) ? explode(",",$ActualDataArray2[$j]['textMsgMedia']) : []);
                $sms->SMSFunc($phone,$body,$media,$track,$branch,$pos,$seq,false,false,false);
                $processed = true;
            }
        }else{
            continue;
        }

        //// This is for the 4th(Assessment) text of the week
        $NumberOfRecords2 = 0;
        $ActualDataArray2 = [];
        $j=0;
        $k=0;
        $dba->crsr->unicode = true;
        $query = "SELECT * FROM ylptextmsg WHERE textMsgTrack = '$track' AND textMsgBranch = '$branch' AND textMsgPos = 4  ORDER BY textMsgPos,textMsgSeq ASC";
        $NumberOfRecords2 = $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray2);
        $dba->crsr->unicode = false;
        if($NumberOfRecords2 > 0){
            for($j=0; $j < $NumberOfRecords2; $j++){
                $pos = $ActualDataArray2[$j]['textMsgPos'];
                $seq = $ActualDataArray2[$j]['textMsgSeq'];
                $body = $ActualDataArray2[$j]['textMsgBody'];
                $body = str_replace("~~FIRSTNAME~~",$name,$body);
                if(strpos($body,"~~USERKEY~~") > -1){
                    $user = new CUSER();
                    $body = str_replace("~~USERKEY~~","k=".$userid."&t=".$track,$body);
                    $linkbody = $body;
                    $linkbody = str_replace("\r\n",' ', $linkbody);
                    $linkpos = strpos($linkbody,"https");
                    $linkpos2 = strpos($linkbody," ",$linkpos);
                    $link = substr($linkbody,$linkpos,$linkpos2-$linkpos);
                    $newlink = $sms->shortenLink($link);
                    $body = str_replace($link,$newlink,$body);
                    $user->UpdateUserActivity($userid,$activity+1);
                }
                $body = str_replace("~~TRACKNAME~~",$sms->getTrackName($track),$body);
                $body = str_replace("~~TRACKSEQ~~",$sms->getNextSeq($tracks),$body);
                $media = (isset($ActualDataArray2[$j]['textMsgMedia']) ? explode(",",$ActualDataArray2[$j]['textMsgMedia']) : []);
                $sms->SMSFunc($phone,$body,$media,$track,$branch,$pos,$seq,false,false,false);
                $processed = true;
            }
        }

        if($processed){
            unset($ActualDataArray[$i]);
            break;
        }
    }
}
exit();
?>