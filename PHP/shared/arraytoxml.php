<?php
/**
 *
 * @param array $array
 *            the array to be converted
 * @param string ? $rootElement
 *            if specified will be taken as root element, otherwise defaults to
 *            <root>
 * @param
 *            xml? if specified content will be appended, used for recursion
 * @return SimpleXMLElement XML version of $array
 */
function arrayToXml($array, $rootElement = '', SimpleXMLElement $xml = null)
{
    $_xml = $xml;

    if ($xml === null) {
        $_xml = new SimpleXMLElement($rootElement !== '' ? $rootElement : '<root></root>');
    }

    foreach ($array as $k => $v) {
        if (is_array($v)) { // nested array
            arrayToXml($v, $k, $_xml->addChild($k));
        } else {
            $v = str_replace("&", "&amp;", $v);
            $v = str_replace("<", "&lt;", $v);
            $v = str_replace(">", "&gt;", $v);
            $v = str_replace("'", "&#39;", $v);
            $v = str_replace('"', "&#34;", $v);
            $_xml->addChild($k, $v);
        }
    }

    return $_xml;
}