<?php
function my_autoload ($pClassName) {
$pClassName = str_replace('\\', '/', $pClassName);
    if(strtolower(substr($pClassName,0,6)) == "stripe"){
        require_once($_SERVER["DOCUMENT_ROOT"] . "/PHP/vendor/stripe/stripe-php/lib/" . substr($pClassName,6,strlen($pClassName)-6)  . '.php');
    }else if(strtolower(substr($pClassName,0,9)) == "phpmailer"){
        require_once($_SERVER["DOCUMENT_ROOT"] . "/PHP/vendor/phpmailer/src/" . substr($pClassName,9,strlen($pClassName)-9)  . '.php');
    }else if(strtolower(substr($pClassName,0,6)) == "twilio"){
        require_once($_SERVER["DOCUMENT_ROOT"] . "/PHP/vendor/twilio-php-master/src/Twilio/" . substr($pClassName,6,strlen($pClassName)-6)  . '.php');
    }else{
        require_once($_SERVER["DOCUMENT_ROOT"] . "/" . $pClassName . ".php");
    }
}
spl_autoload_register("my_autoload");
require_once $_SERVER["DOCUMENT_ROOT"] . "//YLPCFG.txt";
require_once $_SERVER["DOCUMENT_ROOT"] . '//PHP//shared//utility.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '//PHP//shared//constants.php';
require_once $_SERVER["DOCUMENT_ROOT"] . "//PHP//vendor//twilio-php-master//src//Twilio//autoload.php";


?>