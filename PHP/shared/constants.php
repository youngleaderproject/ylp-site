<?php
namespace YLPCONSTANTS {
    const CONTENT_TYPE = 'Content-Type: ';
    const CONTENT_TYPE_XML = 'Content-type: text/xml';
    const CONTENT_TYPE_HTML = 'Content-type: text/html';
    const CONTENT_TYPE_PLAIN = 'Content-type: text/plain';
    const CONTENT_TYPE_JSON = 'Content-type: application/json';
    const CONTENT_TYPE_PDF = 'Content-Type: application/pdf';
    const CONTENT_FILE_TRANSFER = 'Content-Description: File Transfer';
    const CONTENT_DISPOSITION = 'Content-Disposition: attachment; filename=';
    const CONTENT_TRANSFER_BINARY = 'Content-Transfer-Encoding: binary';
    const HEADER_EXPIRES_0 = 'Expires: 0';
    const HEADER_CACHE_CONTROL_VALIDATE = 'Cache-Control: must-revalidate';
    const HEADER_PRAGMA_NO_CACHE = 'Pragma: no-cache';
    const HEADER_PRAGMA_PUBLIC = 'Pragma: public';
    const CHECKED = 'on';
    const BOOL_YES='Y',BOOL_NO='N', BOOL_SIG='S';
}
?>