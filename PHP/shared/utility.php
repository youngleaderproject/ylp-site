<?php

function GUID()
{
    if (function_exists('com_create_guid') === true) {
        return trim(com_create_guid(), '{}');
    }

    return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
}

function Redirect($url, $permanent = false)
{
   // if (headers_sent() === false)
  //  {
        header('Location: ' . $url, true, ($permanent === true) ? 301 : 302);
  //  }

    exit();
}

function ClearSession(){
    $_SESSION = array();
    if (ini_get("session.use_cookies")) {
        $params = session_get_cookie_params();
        setcookie(session_name(), '', time() - 42000,
            $params["path"], $params["domain"],
            $params["secure"], $params["httponly"]
        );
    }
    session_destroy();
}

function UploadMedia($usrname,$fileName,$mediapath,$file){
    $path1 = $_SERVER["DOCUMENT_ROOT"] .'/userstorage/'. $usrname . '/';
    if(!file_exists($path1)){
        mkdir($path1);
    }
    $path =  $path1 . 'vids/';
    if(!file_exists($path)){
        mkdir($path);
    }
    $path = $path1 . 'pics/';
    if(!file_exists($path)){
        mkdir($path);
    }
    $path = $mediapath;
    if(!file_exists($path)){
        mkdir($path);
    }
    $out = fopen($path . $fileName, "wb");

    if ($out) {
        // Read binary input stream and append it to temp file
        $in = fopen($file['tmp_name'], "rb");

        if ($in) {
            while ($buff = fread($in, 4096)) {
                fwrite($out, $buff);
            }
        } else {
            die('{"OK": 0, "info": "Failed to open input stream."}');
        }

        fclose($in);
        fclose($out);

        unlink($file['tmp_name']);
    } else {
        die('{"OK": 0, "info": "Failed to open output stream."}');
    }
}

function chunked_copy($from, $to) {
    $buffer_size = 65536;
    $ret = 0;
    if (file_exists($from)) {
        if (($fin = fopen($from, "rb")) === false)
            return false;
        if (($fout = fopen($to, "w")) === false) {
            fclose($fin);
            return false;
        }

        while(!feof($fin)) {
            $fwrite = fwrite($fout, fread($fin, $buffer_size));
            if ($fwrite === false) {
                fclose($fin);
                fclose($fout);
                return false;
            }
            $ret += $fwrite;
        }
        fclose($fin);
        fclose($fout);
    }
    return ($ret > 0); # return number of bytes written
}

function simple_crypt( $string, $action = 'e' ) {
    // you may change these values to your own
    $secret_key = 'YoungLeaderKey';
    $secret_iv = 'YoungLeaderIv';

    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );

    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }

    return $output;
}

function unlink_files_in_folder($path) {
    $files = get_files_from_folder($path);
    foreach ($files as $file) {
        unlink("$path/$file");
    }
}

function get_files_from_folder($path, $extfilter = "") {
    if (strlen($extfilter) > 0) {
        $chararray = str_split($extfilter);
        $extfilter = "";
        foreach($chararray as $char) {
            $extfilter .= "[" . strtolower($char) . strtoupper($char) . "]";
        }
    }
    $extfilter = strlen($extfilter)>0?"*.$extfilter":"*.*";
    $files = array_filter(glob("$path/$extfilter"), "is_file");
    $retfiles = array();
    foreach ($files as $file) {
        $retfiles[] = pathinfo($file, PATHINFO_BASENAME);
    }
    return $retfiles;
}

function get_subfolders_from_folder($path) {
    $folders = array_filter(glob($path . '/*' , GLOB_ONLYDIR), "is_dir");
    return $folders!==false?$folders:array();
}

function log_trace($message = '')
{
    $trace = debug_backtrace();
    if ($message) {
        file_put_contents( $_SERVER["DOCUMENT_ROOT"]. '/logs/trace.txt', $message . "\r\n", FILE_APPEND);
    }
    file_put_contents( $_SERVER["DOCUMENT_ROOT"]. '/logs/trace.txt', print_r(array_reverse($trace), true) . "\r\n", FILE_APPEND);

    return;
    $caller = array_shift($trace);
    $function_name = $caller['function'];
    file_put_contents(LOG_FOLDER . '\trace.txt', sprintf('%s: Called from %s:%s', $function_name, $caller['file'], $caller['line']) . "\r\n", FILE_APPEND);
    foreach ($trace as $entry_id => $entry) {
        $entry['file'] = $entry['file'] ?: '-';
        $entry['line'] = $entry['line'] ?: '-';
        if (empty($entry['class'])) {
            file_put_contents(LOG_FOLDER . '\trace.txt', sprintf('%s %3s. %s() %s:%s', $function_name, $entry_id + 1, $entry['function'], $entry['file'], $entry['line']) . "\r\n", FILE_APPEND);
        } else {
            file_put_contents(LOG_FOLDER . '\trace.txt', sprintf('%s %3s. %s->%s() %s:%s', $function_name, $entry_id + 1, $entry['class'], $entry['function'], $entry['file'], $entry['line']) . "\r\n", FILE_APPEND);
        }
    }
}

?>