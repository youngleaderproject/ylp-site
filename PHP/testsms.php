<?php
require_once str_replace('\\', '/', $_SERVER["DOCUMENT_ROOT"]) . '/PHP/shared/auto_load.php';

use PHP\CLASSES\CYLPDB;
use PHP\CLASSES\CSMS;

$dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
$query = "SELECT a.* FROM ylpusrtb a inner join ylpactivesub b on a.user_id = b.user_id WHERE a.user_status = 1 and b.sub_activated = 1";
$NumberOfRecords = $dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray);
$dba->crsr->unicode = true;
$query = "SELECT textMsgBody FROM ylptextmsg WHERE textMsgID = 319";
$dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray2);
$sms = new CSMS();
for($i=0;$i<$NumberOfRecords;$i++){
    $phone = $ActualDataArray[$i]['user_phone'];
    $country = $ActualDataArray[$i]['user_country'];
    $body = $ActualDataArray2[0]['textMsgBody'];
    $code = $sms->countryArray[$country]['code'];
    $countrycode = ( $code > "" ? $code : "1");
    $phone = '+'.$countrycode.$phone;
    $sms->SMSFunc($phone,$body,[],0,"SUSPEND",0,0,false,false,false);
}

?>