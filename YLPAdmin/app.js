/* global Ext */

Ext.Loader.setConfig({
    enabled: true,
    paths: {
        'Ext.ux': '/sencha/extjs/examples/ux',
        'YLP.ux': '../YLPAdmin/ux'
    }
});

Ext.require([
    'Ext.ux.exporter.Exporter'
]);

Ext.onReady(function () {
    if (Ext.isIE) {
        Ext.useShims = true;
    }
});

Ext.apply(Ext.form.VTypes, {
    'phone': function () {
        var re = /^[\(\)\.\- ]{0,}[0-9]{3}[\(\)\.\- ]{0,}[0-9]{3}[\(\)\.\- ]{0,}[0-9]{4}[\(\)\.\- ]{0,}$/;
        return function (v) { return re.test(v); };
    }(), 'phoneText': 'The phone number format is wrong, ie: 123-456-7890 (dashes optional) Or (123) 456-7890',
    'fax': function () {
        var re = /^[\(\)\.\- ]{0,}[0-9]{3}[\(\)\.\- ]{0,}[0-9]{3}[\(\)\.\- ]{0,}[0-9]{4}[\(\)\.\- ]{0,}$/;
        return function (v) { return re.test(v); };
    }(), 'faxText': 'The fax format is wrong',
    'zipCode': function () {
        var re = /^\d{5}(-\d{4})?$/;
        return function (v) { return re.test(v); };
    }(), 'zipCodeText': 'The zip code format is wrong, e.g., 94105-0011 or 94105',
    'ssn': function () {
        var re = /^\d{3}-\d{2}-\d{4}$/;
        return function (v) { return re.test(v); };
    }(), 'ssnText': 'The SSN format is wrong, e.g., 123-45-6789'
});

Ext.application({
    requires: ['YLPMain.config.Settings'],
    name: 'YLPMain',
    controllers: ['main'],
    stores: ['EmailList', 'EmailListBetaUsers', 'EmailListActiveUsers', 'TxtMsgList', 'TxtMsgRecList', 'TxtMsgListActiveUsers', 'TxtMsgListBetaUsers', 'UserList'],
    appFolder: '../YLPAdmin',
    autoCreateViewport: true,
    launch: function () {
        window.ylp = {};
    }

});
