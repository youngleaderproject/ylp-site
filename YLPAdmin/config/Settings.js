/* global Ext */

//config options based on post to RVIMAINNHX, rqstyp=senchsecur, usr=usr, isys=systemcode

Ext.define('YLPMain.config.Settings',{
    singleton: true,
    config: {
        YLP: {
            success: false,
            isActive: "",
            isAdmin: "",
            nickname: "",
            optioncodes: [],
            user_email: "",
            user_name: "",
            device: ""
        },
        Url: '/PHP/YLPMAIN.php'
    },
    constructor: function(config){
        this.initConfig(config);
    }
});
