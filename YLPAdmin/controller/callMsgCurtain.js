﻿/* global Rvi, Ext */

Ext.define('YLPMain.controller.callMsgCurtain', {
    extend: 'Ext.app.Controller',
    requires: ['YLP.ux.MsgCurtain'],

    onShowMsg: function (title, msg) {
        YLP.msg(title, msg);
    }
});
