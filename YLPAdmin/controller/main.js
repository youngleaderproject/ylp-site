/* global Ext */

Ext.define('YLPMain.controller.main', {
    extend: 'Ext.app.Controller',
    refs: [
        {
            ref: 'Viewport',
            selector: 'viewport'
        },
        {
            ref: 'Signon',
            selector: 'signon'
        },
        {
            ref: 'MenuList',
            selector: 'menulist'
        },
        {
            ref: 'EmailList',
            selector: 'emaillist'
        },
        {
            ref: 'TextMsgList',
            selector: 'textmsglist'
        },
        {
            ref: 'TextMsgRecList',
            selector: 'textmsgreclist'
        },
        {
            ref: 'TextMsgListForm',
            selector: 'textmsglistform'
        },
        {
            ref: 'EmailListForm',
            selector: 'emaillistform'
        },
        {
            ref: 'EmailDisplay',
            selector: 'emaildisplay'
        },
        {
            ref: 'EmailAddDisplay',
            selector: 'emailadddisplay'
        },
        {
            ref: 'GoogleDisplay',
            selector: 'googledisplay'
        },
        {
            ref: 'EmailListUsers',
            selector: 'emaillistusers'
        },
        {
            ref: 'TextMsgDisplay',
            selector: 'textmsgdisplay'
        },
        {
            ref: 'TxtMsgListUsers',
            selector: 'txtmsglistusers'
        },
        {
            ref: 'UserList',
            selector: 'userlist'
        },

    ],

    init: function () {
        this.control({

            //SignOn Form..............................................
            'signon': {
                signonform: this.onSignOn
            },
            '#signon-button': {
                click: this.onSignOn
            }
        });
    },

    onSignOn: function () {
        var me = this;
        var parameters = this.getSignon().getValues();
        var theUrl = YLPMain.config.Settings.getUrl();
        parameters.passw = encodeURIComponent(parameters.passw);


        Ext.MessageBox.show({
            title: 'YLP Login',
            msg: 'Logging in..',
            width: 300,
            wait: true,
            waitConfig: {
                interval: 200
            },
            progress: true,
            closable: false,
            modal: true
        });

        Ext.apply(parameters, {
            rqstyp: 'ylplogin',
            admin: 'Y'
        });

        Ext.Ajax.request({
            url: theUrl,
            params: parameters,
            scope: me,

            success: function (response) {
                var jsonResponse = window.Ext.decode(response.responseText);
                Ext.MessageBox.hide();
                window.YLPMain.config.Settings.setYLP(jsonResponse);
                if (false === window.YLPMain.config.Settings.YLP.success) {
                    window.alert("Access denied. Unable to validate your log in credentials");
                    return;
                }
                me.getViewport().getLayout().setActiveItem("vp-main");
            },
            failure: function () {
                Ext.Msg.alert('Error', 'Unable to connect to server');
            }
        });
    },

    onSignOff: function (callback) {
        var me = this;
        var theUrl = window.YLPMain.config.Settings.Url + '?rqstyp=ylpsignoff';
        window.Ext.Ajax.request({
            url: theUrl,
            scope: me,
            success: function (response) {
                me.getViewport().getLayout().setActiveItem("vp-signon");
                me.onHideAll(true);
                callback();
            }
        });
    },

    onShowEmailList: function () {
        var me = this;
        me.onHideWest();
        me.getEmailListForm().show();
        window.Ext.getStore('EmailList').load();
    },

    onShowTextMsgList: function () {
        var me = this;
        me.onHideAll(false);
        me.getTextMsgListForm().show();
        window.Ext.getStore('TxtMsgList').load();
        me.getViewport().down("#east-region").collapse();
    },

    onShowTextMsgRecList: function () {
        var me = this;
        me.onHideAll(false);
        me.getTextMsgRecList().show();
        window.Ext.getStore('TxtMsgRecList').load();
        me.getViewport().down("#east-region").collapse();
    },

    onShowCurrentUsers: function () {
        var me = this;
        me.onHideAll(false);
        me.getUserList().show();
        window.Ext.getStore('UserList').load();
        me.getViewport().down("#east-region").collapse();
    },

    onShowEmailTemplate: function (record) {
        var me = this;
        me.currentEmailTemplate = record;
        var embranch = record.get("embranch");
        var emseq = record.get("emseq");
        var emhost = record.get("emhost");
        var theUrl = window.YLPMain.config.Settings.Url;
        var emailUrl = theUrl + '?rqstyp=ylpemail&type=buildtemp&dev=' + window.YLPMain.config.Settings.YLP.device + "&embranch=" + embranch + "&emseq=" + emseq + "&emhost=" + emhost;
        var panel = me.getEmailDisplay();

        panel.removeAll();
        panel.add({
            xtype: 'component',
            itemId: 'pdfdocnew',
            id: 'pdfdocnew',
            autoEl: {
                tag: 'iframe',
                src: emailUrl,
                style: 'height: 100%; width: 100%; border: none'
            }
        });

        panel.show();
    },

    onShowTextMsgTemplate: function (record) {
        var me = this;
        me.currentTxtMsgTemplate = record;
        var txttrack = record.get("txttrack");
        var txtbranch = record.get("txtbranch");
        var txtpos = record.get("txtpos");
        var txtseq = record.get("txtseq");
        var theUrl = window.YLPMain.config.Settings.Url;
        var txtMsgUrl = theUrl + '?rqstyp=ylptxtmsg&type=buildtemp&dev=' + window.YLPMain.config.Settings.YLP.device + "&txttrack=" + txttrack + "&txtpos=" + txtpos + "&txtbranch=" + txtbranch + "&txtseq=" + txtseq;
        var panel = me.getTextMsgDisplay();

        panel.removeAll();
        panel.add({
            xtype: 'component',
            itemId: 'pdfdocnew',
            id: 'pdfdocnew',
            autoEl: {
                tag: 'iframe',
                src: txtMsgUrl,
                style: 'height: 100%; width: 100%; border: none'
            }
        });

        panel.show();
    },

    onEmailTemplate: function (type) {
        var me = this;
        me.remindflag = type;
        me.getViewport().down("#east-region").expand();
        var store;
        switch (me.currentEmailTemplate.get('embranch')) {
            case '0':
                store = Ext.getStore("EmailListActiveUsers");
                break;
            case 'S':
                store = Ext.getStore("EmailListBetaUsers");
                break;
            default:
                store = Ext.getStore("EmailListBetaUsers");
            // code block
        }
        store.getProxy().extraParams.embranch = me.currentEmailTemplate.get('embranch');
        store.getProxy().extraParams.emseq = me.currentEmailTemplate.get('emseq');
        store.getProxy().extraParams.remind = type > "" ? type : '';
        store.load(); 
        me.getEmailListUsers().reconfigure(store);
        me.getEmailListUsers().show();
    },

    onTextMessageTemplate: function (type) {
        var me = this;
        me.remindflag = type;
        me.getViewport().down("#east-region").expand();
        var store = Ext.getStore("TxtMsgListActiveUsers");
        //switch (me.currentTxtMsgTemplate.get('txtbranch')) {  use this if you need to switch between active users and non active users
        //    case '0':
        //        store = Ext.getStore("TxtMsgListActiveUsers");
        //        break;
        //    case 'S':
        //        store = Ext.getStore("TxtMsgListBetaUsers");
        //        break;
        //    default:
        //    // code block
        //}
        store.getProxy().extraParams.txtbranch = me.currentTxtMsgTemplate.get('txtbranch');
        store.getProxy().extraParams.txttrack = me.currentTxtMsgTemplate.get('txttrack');
        store.getProxy().extraParams.txtseq = me.currentTxtMsgTemplate.get('txtseq');
        store.getProxy().extraParams.remind = type > "" ? type : '';
        store.load();
        me.getTxtMsgListUsers().reconfigure(store);
        me.getTxtMsgListUsers().show();
    },

    onSendManualEmail: function () {
        var me = this;
        me.tempwindow = Ext.create('Ext.window.Window', {
            title: 'YLP Manual Email',
            layout: 'fit',
            closeAction: 'destroy',
            constrain: true,
            modal: true,
            itemId: 'manualemailwindow',
            items: [{
                xtype: 'form',
                id: 'manualemailform',
                defaults: {
                    anchor: '100%',
                    padding: 10
                },
                bbar: [
                    {
                        xtype: 'tbfill'
                    },
                    {
                        xtype: 'button',
                        text: 'Submit',
                        formBind: true,
                        handler: function (btn) {
                            Ext.MessageBox.show({
                                title: 'YLP Admin',
                                msg: 'Sending Email..',
                                width: 300,
                                wait: true,
                                waitConfig: {
                                    interval: 200
                                },
                                progress: true,
                                closable: false,
                                modal: true
                            });
                            var record = Ext.getCmp('manualemailform').getForm().getValues();
                            var embranch = me.currentEmailTemplate.get("embranch");
                            var emseq = me.currentEmailTemplate.get("emseq");
                            var emhost = me.currentEmailTemplate.get("emhost");
                            var theUrl = window.YLPMain.config.Settings.Url + '?rqstyp=ylpemail&type=sendemail&dev=' + window.YLPMain.config.Settings.YLP.device + "&emseq=" + emseq + "&embranch=" + embranch + "&emuser=" + record.emuser + "&emadd=" + record.emadd + "&emhost=" + emhost;
                            window.Ext.Ajax.request({
                                url: theUrl,
                                scope: me,
                                success: function (response) {
                                    Ext.MessageBox.hide();
                                    me.getController("callMsgCurtain").onShowMsg('Manual Email', 'Sent!');
                                    me.tempwindow.destroy();
                                }
                            });
                        }
                    }
                ],
                items: [
                    {
                        xtype: 'textfield',
                        name: 'emuser',
                        fieldLabel: 'Enter Name',
                        allowBlank: false
                    },
                    {
                        xtype: 'textfield',
                        name: 'emadd',
                        vtype: 'email',
                        fieldLabel: 'Enter Email',
                        allowBlank: false
                    }
                ]
            }]
        }).show();
    },

    onSendManualTextMsg: function () {
        var me = this;
        me.tempwindow = Ext.create('Ext.window.Window', {
            title: 'YLP Manual Text Message',
            layout: 'fit',
            closeAction: 'destroy',
            constrain: true,
            modal: true,
            itemId: 'manualtextmsgwindow',
            items: [{
                xtype: 'form',
                id: 'manualtextmsgform',
                defaults: {
                    anchor: '100%',
                    padding: 10
                },
                bbar: [
                    {
                        xtype: 'tbfill'
                    },
                    {
                        xtype: 'button',
                        text: 'Submit',
                        formBind: true,
                        handler: function (btn) {
                            Ext.MessageBox.show({
                                title: 'YLP Admin',
                                msg: 'Sending Text Message..',
                                width: 300,
                                wait: true,
                                waitConfig: {
                                    interval: 200
                                },
                                progress: true,
                                closable: false,
                                modal: true
                            });
                            var record = Ext.getCmp('manualtextmsgform').getForm().getValues();
                            var txttrack = me.currentTxtMsgTemplate.get("txttrack");
                            var txtbranch = me.currentTxtMsgTemplate.get("txtbranch");
                            var txtpos = me.currentTxtMsgTemplate.get("txtpos");
                            var txtseq = me.currentTxtMsgTemplate.get("txtseq");
                            if (record.txtcntry === "") record.txtcntry = 'US';
                            var theUrl = window.YLPMain.config.Settings.Url + '?rqstyp=ylptxtmsg&type=sendtext&dev=' + window.YLPMain.config.Settings.YLP.device + "&txtpos=" + txtpos + "&txttrack=" + txttrack + "&txtseq=" + txtseq + "&txtbranch=" + txtbranch + "&txtuser=" + record.txtuser + "&txtadd=" + record.txtadd + "&txtcntry=" + record.txtcntry;
                            window.Ext.Ajax.request({
                                url: theUrl,
                                scope: me,
                                success: function (response) {
                                    Ext.MessageBox.hide();
                                    me.getController("callMsgCurtain").onShowMsg('Manual Text', 'Sent!');
                                    me.tempwindow.destroy();
                                }
                            });
                        }
                    }
                ],
                items: [
                    {
                        xtype: 'textfield',
                        name: 'txtuser',
                        fieldLabel: 'Enter Name',
                        allowBlank: false
                    },
                    {
                        xtype: 'textfield',
                        name: 'txtadd',
                        vtype: 'phone',
                        maxLength: 10,
                        minLength: 10,
                        enforceMaxLength: true,
                        fieldLabel: 'Enter Phone',
                        allowBlank: false
                    },
                    {
                        name: 'txtcntry',
                        value: 'US',
                        fieldLabel: 'Select Country',
                        valueField: 'code',
                        displayField: 'name',
                        typeAhead: true,
                        allowBlank: false,
                        xtype: 'combobox',
                        queryMode: 'local',
                        lastQuery: '',
                        store: Ext.create('Ext.data.Store', {
                            fields: ['code', 'name'],
                            data: Ext.ux.Countries.CountryArray
                        })
                    }
                ]
            }]
        }).show();
    },

    onSendEmailTemplate: function () {
        var me = this;
        var grid = me.getEmailListUsers();
        var istrue = me.getEmailListUsers().getSelectionModel().hasSelection();
        var selectedRows = me.getEmailListUsers().getSelectionModel().getSelection();
        me.counter = 0;
        if (istrue) {
            Ext.MessageBox.show({
                title: 'YLP Admin',
                msg: 'Emailing Template..',
                width: 300,
                wait: true,
                waitConfig: {
                    interval: 200
                },
                progress: true,
                closable: false,
                modal: true
            });
            var loopArray = function (arr) {
                me.SendEmailFunc(arr[me.counter], function () {
                    if (me.counter < arr.length) {
                        loopArray(arr);
                    } else {
                        grid.getSelectionModel().deselectAll();
                        Ext.MessageBox.hide();
                        me.getController("callMsgCurtain").onShowMsg('Selected Email', 'Sent!');
                        switch (me.currentEmailTemplate.get('embranch')) {
                            case '0':
                                store = Ext.getStore("EmailListActiveUsers");
                                break;
                            case 'S':
                                store = Ext.getStore("EmailListBetaUsers");
                                break;
                            default:
                            // code block
                        }
                        store.load();
                        me.remindflag = '';
                    }
                });
            };

            loopArray(selectedRows);
        } else {
            Ext.Msg.show({
                title: 'Checked Items',
                msg: 'You requested to email checked items, however,<br>you did not mark any check boxes to display.<br>Please make your selection(s) and try again.',
                buttons: Ext.Msg.OK,
                icon: Ext.Msg.INFO
            });
        }
    },

    SendEmailFunc: function (rec, callback) {
        var me = this;
        var embranch = me.currentEmailTemplate.get("embranch");
        var emseq = me.currentEmailTemplate.get("emseq");
        var emhost = me.currentEmailTemplate.get("emhost");
        if (rec) {
            var theUrl = window.YLPMain.config.Settings.Url + '?rqstyp=ylpemail&type=sendemail&dev=' + window.YLPMain.config.Settings.YLP.device + "&emseq=" + emseq + "&embranch=" + embranch + "&emuser=" + rec.get("emuser") + "&emadd=" + rec.get("emadd") + "&remind=" + me.remindflag + "&emhost=" + emhost;
            window.Ext.Ajax.request({
                url: theUrl,
                scope: me,
                success: function (response) {
                    me.counter += 1;
                    callback();
                }
            });
        } else {
            me.counter += 1;
            callback();
        }
    },

    onSendTxtMsgTemplate: function () {
        var me = this;
        var grid = me.getTxtMsgListUsers();
        var istrue = me.getTxtMsgListUsers().getSelectionModel().hasSelection();
        var selectedRows = me.getTxtMsgListUsers().getSelectionModel().getSelection();
        me.counter = 0;
        if (istrue) {
            Ext.MessageBox.show({
                title: 'YLP Admin',
                msg: 'Texting Template..',
                width: 300,
                wait: true,
                waitConfig: {
                    interval: 200
                },
                progress: true,
                closable: false,
                modal: true
            });
            var loopArray = function (arr) {
                me.SendTxtMsgFunc(arr[me.counter], function () {
                    if (me.counter < arr.length) {
                        loopArray(arr);
                    } else {
                        grid.getSelectionModel().deselectAll();
                        Ext.MessageBox.hide();
                        me.getController("callMsgCurtain").onShowMsg('Selected Messages', 'Sent!');
                        store = Ext.getStore("TxtMsgListActiveUsers");
                        store.load();
                        me.remindflag = '';
                    }
                });
            };

            loopArray(selectedRows);
        } else {
            Ext.Msg.show({
                title: 'Checked Items',
                msg: 'You requested to SMS checked items, however,<br>you did not mark any check boxes to display.<br>Please make your selection(s) and try again.',
                buttons: Ext.Msg.OK,
                icon: Ext.Msg.INFO
            });
        }
    },

    SendTxtMsgFunc: function (rec, callback) {
        var me = this;
        var txttrack = me.currentTxtMsgTemplate.get("txttrack");
        var txtbranch = me.currentTxtMsgTemplate.get("txtbranch");
        var txtseq = me.currentTxtMsgTemplate.get("txtseq");
        var txtpos = me.currentTxtMsgTemplate.get("txtpos");
        if (rec) {
            var theUrl = window.YLPMain.config.Settings.Url + '?rqstyp=ylptxtmsg&type=sendtext&dev=' + window.YLPMain.config.Settings.YLP.device + "&txtseq=" + txtseq + "&txtbranch=" + txtbranch + "&txttrack=" + txttrack + "&txtuser=" + rec.get("txtuser") + "&txtpos=" + txtpos + "&txtadd=" + rec.get("txtadd") + "&txtcntry=" + rec.get("txtcntry") + "&remind=" + me.remindflag;
            window.Ext.Ajax.request({
                url: theUrl,
                scope: me,
                success: function (response) {
                    me.counter += 1;
                    callback();
                }
            });
        } else {
            me.counter += 1;
            callback();
        }
    },

    onShowGoogleEvents: function () {
        var me = this;
        me.onHideWest();
        var panel = me.getGoogleDisplay();

        panel.removeAll();
        panel.add({
            xtype: 'component',
            itemId: 'pdfdocnew',
            id: 'pdfdocnew',
            autoEl: {
                tag: 'iframe',
                src: './resources/Google/GoogleAnalyticsEvents.html',
                style: 'height: 100%; width: 100%; border: none'
            }
        });

        panel.show();
    },

    onCreateNewUser: function () {
        var me = this;
        me.tempwindow = Ext.create('Ext.window.Window', {
            title: 'YLP New User',
            layout: 'fit',
            closeAction: 'destroy',
            constrain: true,
            modal: true,
            itemId: 'newuserwindow',
            items: [{
                xtype: 'form',
                id: 'newuserform',
                defaults: {
                    anchor: '100%',
                    padding: 10
                },
                bbar: [
                    {
                        xtype: 'tbfill'
                    },
                    {
                        xtype: 'button',
                        text: 'Submit',
                        formBind: true,
                        handler: function (btn) {
                            var record = Ext.getCmp('newuserform').getForm().getValues();
                            var email = record.useremail;
                            var login = record.userlogin;
                            var pass = record.userpass;
                            if (record.userpass === record.userpass2) {
                                var theUrl = window.YLPMain.config.Settings.Url + '?rqstyp=ylpusrmaint&type=A&admin=Y&dev=' + window.YLPMain.config.Settings.YLP.device + "&email=" + email + "&userlogin=" + login + "&userpass=" + pass;
                                window.Ext.Ajax.request({
                                    url: theUrl,
                                    scope: me,
                                    success: function (response) {
                                        if (response.responseText.trim() === 'SUCCESS') {
                                            me.onSignOff(function () {
                                                me.getController("callMsgCurtain").onShowMsg('Admin User', 'Added!');
                                                me.tempwindow.destroy();
                                            });
                                        } else {
                                            Ext.Msg.alert('Error', 'User Not Added');
                                            me.tempwindow.destroy();
                                        }
                                    }
                                });
                            }
                        }
                    }
                ],
                items: [
                    {
                        xtype: 'textfield',
                        name: 'userlogin',
                        fieldLabel: 'User Name',
                        allowBlank: false
                    },
                    {
                        xtype: 'textfield',
                        name: 'useremail',
                        vtype: 'email',
                        fieldLabel: 'User Email',
                        allowBlank: false
                    },
                    {
                        xtype: 'textfield',
                        name: 'userpass',
                        inputType: 'password',
                        fieldLabel: 'Password',
                        allowBlank: false
                    },
                    {
                        xtype: 'textfield',
                        name: 'userpass2',
                        inputType: 'password',
                        fieldLabel: 'Re-Enter Password',
                        allowBlank: false
                    }
                ]
            }]
        }).show();
    },

    onAddNewEmail: function () {
        var me = this;
        var panel = me.getEmailAddDisplay();
        me.getViewport().down("#west-region").collapse();
        me.getViewport().down("#east-region").collapse();
        panel.removeAll();
        panel.add(
            {
                xtype: 'panel',
                id: 'emailaddeditor',
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                flex: 1,
                items: [
                    {
                        xtype: 'form',
                        flex: 0.75,
                        bbar: [
                            {
                                xtype: 'tbfill'
                            },
                            {
                                xtype: 'button',
                                text: 'Finalize',
                                id: 'emailaddfinalize',
                                handler: function (btn) {
                                    var formvalues = btn.up('form').getForm().getValues();
                                    var htmleditor = Ext.getCmp('thehtmleditor').getValue();
                                    var theUrl = window.YLPMain.config.Settings.Url;
                                    me.form = document.createElement("FORM");
                                    me.formDataToUpload = new FormData(me.form);
                                    me.formDataToUpload.append("emhtml", htmleditor);
                                    for (var key in formvalues) {
                                        me.formDataToUpload.append(key, formvalues[key]);
                                    }
                                    $.ajax({
                                        url: theUrl + '?rqstyp=ylpemail&type=submitaddemail&dev=' + window.YLPMain.config.Settings.YLP.device,
                                        data: me.formDataToUpload,
                                        type: "POST",
                                        contentType: false,
                                        processData: false,
                                        error: function (data) {

                                        },
                                        success: function (data) {
                                            if (data.trim() === 'COMPLETE') {
                                                me.onHideCenter();
                                                me.onShowEmailList();
                                            } else {
                                                Ext.Msg.alert('Error', 'Something Went Wrong');
                                            }
                                        },
                                        complete: function () {
                                        }
                                    });
                                }
                            }
                        ],
                        items: [
                            {
                                xtype: 'combobox',
                                name: 'emhost',
                                padding: 5,
                                anchor: '95%',
                                id: 'emailaddhost',
                                fieldLabel: 'Email Host',
                                valueField: "value",
                                displayField: "desc",
                                store: ['YLP','HK'],
                                allowBlank: false,
                                listeners: {
                                    change: function (fld) {
                                        var theUrl = window.YLPMain.config.Settings.Url;
                                        var emailUrl = theUrl + '?rqstyp=ylpemail&type=buildaddtemp&dev=' + window.YLPMain.config.Settings.YLP.device + "&emhost=" + fld.value;
                                        var panel = Ext.getCmp('emailaddtemplatecontainer');
                                        
                                        panel.removeAll();
                                        panel.add({
                                            xtype: 'component',
                                            itemId: 'pdfdocnew',
                                            id: 'pdfdocnew',
                                            autoEl: {
                                                tag: 'iframe',
                                                src: emailUrl,
                                                style: 'height: 100%; width: 100%; border: none'
                                            }
                                        });
                                    }

                                }
                            },
                            {
                                xtype: 'textfield',
                                name: 'emsub',
                                padding: 5,
                                anchor: '95%',
                                fieldLabel: 'Email Subject',
                                allowBlank: false
                            },
                            {
                                xtype: 'textfield',
                                name: 'emaltsub',
                                padding: 5,
                                anchor: '95%',
                                fieldLabel: 'Email Alt Subject',
                                allowBlank: false
                            },
                            {
                                xtype: 'combobox',
                                name: 'embranch',
                                padding: 5,
                                anchor: '95%',
                                fieldLabel: 'Email Branch',
                                store: ['CW1', 'CW2', 'CW3', 'CW4', 'CW5', 'CW6', 'CW7', 'MKT'],
                                allowBlank: false
                            },
                            {
                                xtype: 'numberfield',
                                anchor: '95%',
                                name: 'emseq',
                                fieldLabel: 'Email Sequence',
                                minValue: 1,
                                value: 1,
                                padding: 5,
                                hideTrigger: true,
                                keyNavEnabled: false,
                                mouseWheelEnabled: false
                            }
                        ]
                    },
                    {
                        xtype: 'container',
                        flex: 1,
                        layout: 'fit',
                        items: [
                            {
                                xtype: 'htmleditor',
                                name: 'embody',
                                id: 'thehtmleditor',
                                fontFamilies: ['Arial', 'Courier New', 'Tahoma', 'Times New Roman', 'Verdana', 'SF-Ui-Lite'],
                                listeners: {
                                    change: function (fld) {
                                        if (fld.value > "") {
                                            Ext.getCmp('emailaddreviewbtn').setDisabled(false);
                                            Ext.getCmp('emailaddfinalize').setDisabled(false);
                                        } else {
                                            Ext.getCmp('emailaddfinalize').setDisabled(true);
                                            Ext.getCmp('emailaddreviewbtn').setDisabled(true);
                                        }
                                    }
                                }
                            }
                        ]
                    }
                ]
            },
            {
                xtype: 'container',
                flex: 1,
                border: 1,
                id: 'emailaddtemplatecontainer'
            }
        );

        panel.show();
    },

    onHandleMenuOpts: function (menuopt) {
        var me = this;
        switch (menuopt) {
            case '1':
                me.onShowEmailList();
                break;
            case '2':
                me.onShowTextMsgList();
                break;
            case '3':
                me.onShowGoogleEvents();
                break;
            case '4':
                me.onShowCurrentUsers();
                break;
            case '5':
                me.onShowTextMsgRecList();
                break;
        }
    },

    onShowMenu: function () {
        var me = this;
        me.getViewport().down("#west-region").expand();
        me.getMenuList().show();
    },

    onHideCenter: function () {
        var me = this;
        me.getEmailDisplay().hide();
        me.getUserList().hide();
        me.getGoogleDisplay().hide();
        me.getTextMsgListForm().hide();
        me.getTextMsgRecList().hide();
        me.getEmailAddDisplay().hide();
        me.getEmailListForm().hide();
    },

    onHideEast: function () {
        var me = this;
        me.getEmailListUsers().hide();
        me.getTxtMsgListUsers().hide();
    },

    onHideWest: function () {
        var me = this;
        me.getViewport().down("#west-region").collapse();
        me.getMenuList().hide();
    },

    onHideAll: function (collapse) {
        var me = this;
        if (collapse) {
            me.getViewport().down("#west-region").collapse();
            me.getViewport().down("#east-region").collapse();
            me.getViewport().down("#south-region").collapse();
        }
        me.onHideCenter();
        me.onHideEast();
        me.onHideWest();
    }


    
});