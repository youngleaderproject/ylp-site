/* global Ext */

Ext.define('YLPMain.model.Rvabrep', {
    extend: 'Ext.data.Model',
    fields: [],
    proxy: {
        type: 'ajax',
        url: YLPMain.config.Settings.getUrl(), //'/pgms/rvimainnhx.pgm',
        reader: {
            type: 'xml',
            root: 'lists',
            record: 'LIST',
            totalPropery: 'total'
        }
    }
});
