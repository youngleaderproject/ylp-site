/* global Ext */

Ext.define('YLPMain.store.EmailList', {
    extend: 'Ext.data.Store',
    fields: ['embranch', 'emsub', 'emseq', 'empath','embody', 'emhost'],
    autoLoad: false,
    remoteSort: false,
    proxy: {
        type: 'ajax',
        url: YLPMain.config.Settings.getUrl(), 
        extraParams: {
            rqstyp: 'ylpemail',
            type: 'list'
        },
        reader: {
            type: 'xml',
            record: 'email'
        }
    },
    listeners: {
        beforeload: function () {
            var deviceid = window.YLPMain.config.Settings.YLP.device;
            if (deviceid) {
                Ext.apply(this.getProxy().extraParams, {
                    dev: deviceid
                });
            }
        }
    }
});
