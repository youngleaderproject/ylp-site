/* global Ext */

Ext.define('YLPMain.store.EmailListBetaUsers', {
    extend: 'Ext.data.Store',
    fields: ['emuser', 'emadd'],
    autoLoad: false,
    remoteSort: false,
    proxy: {
        type: 'ajax',
        url: YLPMain.config.Settings.getUrl(), 
        extraParams: {
            rqstyp: 'ylpemail',
            type: 'listbeta'
        },
        reader: {
            type: 'xml',
            record: 'email'
        }
    },
    listeners: {
        beforeload: function () {
            var deviceid = window.YLPMain.config.Settings.YLP.device;
            if (deviceid) {
                Ext.apply(this.getProxy().extraParams, {
                    dev: deviceid
                });
            }
        }
    }
});
