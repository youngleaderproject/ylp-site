/* global Ext */

Ext.define('YLPMain.store.Rvabreps', {
    extend: 'Ext.data.Store',
    model: 'YLPMain.model.Rvabrep',
    autoLoad: false,
    remoteSort: false,
    pageSize: 5000,
    groupField: 'TAB',
    listeners: {
        beforeload: function () {
            var isys = Ext.ComponentQuery.query('#syscodefilter')[0];
            if (isys) {
                Ext.apply(this.getProxy().extraParams, {
                    isys: isys.getValue(),
                    rqstyp: 'inet003w'
                });
            }
            var deviceid = Ext.ComponentQuery.query('#dev')[0];
            if (deviceid) {
                Ext.apply(this.getProxy().extraParams, {
                    dev: YLPMain.config.Settings.getDeviceid() //deviceid.getValue()
                });
            }
        }
    }
});
