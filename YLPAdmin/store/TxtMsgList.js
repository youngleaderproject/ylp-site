/* global Ext */

Ext.define('YLPMain.store.TxtMsgList', {
    extend: 'Ext.data.Store',
    fields: ['txtid', 'txtbranch', 'txtpos', 'txttrack', 'txtseq', 'txtmsgbody', 'txtmsgmedia','txtassesment'],
    autoLoad: false,
    remoteSort: false,
    proxy: {
        type: 'ajax',
        url: YLPMain.config.Settings.getUrl(), 
        extraParams: {
            rqstyp: 'ylptxtmsg',
            type: 'list'
        },
        reader: {
            type: 'xml',
            record: 'txtmsg'
        }
    },
    listeners: {
        beforeload: function () {
            var deviceid = window.YLPMain.config.Settings.YLP.device;
            if (deviceid) {
                Ext.apply(this.getProxy().extraParams, {
                    dev: deviceid
                });
            }
        }
    }
});
