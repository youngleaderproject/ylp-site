/* global Ext */

Ext.define('YLPMain.store.TxtMsgListBetaUsers', {
    extend: 'Ext.data.Store',
    fields: ['txtuser', 'txtadd', 'txtcntry'],
    autoLoad: false,
    remoteSort: false,
    proxy: {
        type: 'ajax',
        url: YLPMain.config.Settings.getUrl(), 
        extraParams: {
            rqstyp: 'ylptxtmsg',
            type: 'listbeta'
        },
        reader: {
            type: 'xml',
            record: 'txtmsg'
        }
    },
    listeners: {
        beforeload: function () {
            var deviceid = window.YLPMain.config.Settings.YLP.device;
            if (deviceid) {
                Ext.apply(this.getProxy().extraParams, {
                    dev: deviceid
                });
            }
        }
    }
});
