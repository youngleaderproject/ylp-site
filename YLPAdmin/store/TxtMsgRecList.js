/* global Ext */

Ext.define('YLPMain.store.TxtMsgRecList', {
    extend: 'Ext.data.Store',
    fields: ['txtbranch', 'txtpos', 'txttrack', 'txtseq', 'txtmsgdate','txtphone'],
    autoLoad: false,
    remoteSort: false,
    proxy: {
        type: 'ajax',
        url: YLPMain.config.Settings.getUrl(), 
        extraParams: {
            rqstyp: 'ylptxtmsg',
            type: 'listrecs'
        },
        reader: {
            type: 'xml',
            record: 'txtmsg'
        }
    },
    listeners: {
        beforeload: function () {
            var deviceid = window.YLPMain.config.Settings.YLP.device;
            if (deviceid) {
                Ext.apply(this.getProxy().extraParams, {
                    dev: deviceid
                });
            }
        }
    }
});
