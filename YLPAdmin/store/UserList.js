/* global Ext */

Ext.define('YLPMain.store.UserList', {
    extend: 'Ext.data.Store',
    fields: ['user_id', 'user_email', 'user_phone', 'user_track', 'user_activity', 'user_tracktime', 'user_country', 'user_registered', 'user_status', 'user_name', 'user_lastname', 'unsubscribe_date','sub_enddate',
        {
            name: 'fullname',
            convert: function (v, rec) {
                return rec.get('user_name') + ' ' + rec.get('user_lastname');
            }
        }
    ],
    autoLoad: false,
    remoteSort: false,
    proxy: {
        type: 'ajax',
        url: YLPMain.config.Settings.getUrl(), 
        extraParams: {
            rqstyp: 'ylpusrmaint',
            type: 'list'
        },
        reader: {
            type: 'xml',
            record: 'user'
        }
    },
    listeners: {
        beforeload: function () {
            var deviceid = window.YLPMain.config.Settings.YLP.device;
            if (deviceid) {
                Ext.apply(this.getProxy().extraParams, {
                    dev: deviceid
                });
            }
        }
    }
});
