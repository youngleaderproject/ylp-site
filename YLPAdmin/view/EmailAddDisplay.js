﻿/* global Ext */

Ext.define('YLPMain.view.EmailAddDisplay', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.emailadddisplay',

    initComponent: function () {
        var me = this;
        Ext.apply(me, {
            frame: true,
            frameHeader: true,
            hideCollapseTool: true,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            padding: 5,
            tbar: me.buildTbar()

        });
        me.callParent(arguments);
    },

    buildTbar: function () {
        var me = this;
        return [
            {
                xtype: 'tbfill'
            },
            {
                xtype: 'button',
                text: 'Review',
                id: 'emailaddreviewbtn',
                disabled: true,
                handler: function (btn) {
                    var me = this;
                    if (btn.text === "Review") {
                        btn.setText('Edit');
                        Ext.getCmp('emailaddeditor').hide();
                        var htmleditor = Ext.getCmp('thehtmleditor').getValue();
                        var theUrl = window.YLPMain.config.Settings.Url;
                        var fld = Ext.getCmp('emailaddhost').getRawValue();
                        // var emailUrl =  + "&emhtml=" + encodeURIComponent(htmleditor);
                        me.panel = Ext.getCmp('emailaddtemplatecontainer');
                        me.form = document.createElement("FORM");
                        me.formDataToUpload = new FormData(me.form);
                        me.formDataToUpload.append("emhtml", htmleditor);
                        me.panel.removeAll();
                        $.ajax({
                            url: theUrl + '?rqstyp=ylpemail&type=buildaddtemp&dev=' + window.YLPMain.config.Settings.YLP.device + "&emhost=" + fld,
                            data: me.formDataToUpload,
                            type: "POST",
                            contentType: false,
                            processData: false,
                            error: function (data) {

                            },
                            success: function (data) {
                                me.panel.add({
                                    xtype: 'component',
                                    itemId: 'pdfdocnew',
                                    id: 'pdfdocnew',
                                    autoEl: {
                                        tag: 'iframe',
                                        style: 'height: 100%; width: 100%; border: none'
                                    }
                                });
                                var iframe = document.getElementById('pdfdocnew'),
                                    iframedoc = iframe.contentDocument || iframe.contentWindow.document;
                                setTimeout(function () {
                                    iframedoc.body.innerHTML = data;
                                },250);
                            },
                            complete: function () {
                            }
                        });
                    } else {
                        btn.setText('Review');
                        Ext.getCmp('emailaddeditor').show();
                    }
                }
            },
            {
                xtype: 'button',
                text: 'Close',
                handler: function () {
                    me.hide();
                    var main = YLPMain.app.getController("main");
                    main.getEmailDisplay().hide();
                    main.getEmailListForm().show();
                }
                
            }
        ];
    }
});
