﻿/* global Ext */

Ext.define('YLPMain.view.EmailDisplay', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.emaildisplay',

    initComponent: function () {
        var me = this;
        Ext.apply(me, {
            frame: true,
            frameHeader: true,
            hideCollapseTool: true,
            layout: 'fit',
            padding: 5,
            viewConfig: {
                deferEmptyText: true,
                emptyText: '<b> **** Unable to load email requested ***</b>',
                loadingText: 'Retrieving Email Template.....'
            },
            tbar: me.buildTbar()

        });
        me.callParent(arguments);
    },

    buildTbar: function () {
        var me = this;
        return [
            {
                xtype: 'tbfill'
            },
            {
                xtype: 'button',
                text: 'Send Manual Email',
                handler: function () {
                    YLPMain.app.getController("main").onSendManualEmail();
                }
            },
            {
                xtype: 'button',
                text: 'Send Reminder Email',
                handler: function () {
                    YLPMain.app.getController("main").onEmailTemplate('Y');
                }
            },
            {
                xtype: 'button',
                text: 'Send First Email',
                handler: function () {
                    YLPMain.app.getController("main").onEmailTemplate();
                }
            },
            {
                xtype: 'button',
                text: 'Close',
                handler: function () {
                    me.hide();
                }
                
            }
        ];
    }
});
