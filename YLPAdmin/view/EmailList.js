﻿/* global Ext */

Ext.define('YLPMain.view.EmailList', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.emaillist',

    initComponent: function () {
        var me = this;
        function createTooltip(view) {
            view.tip = window.Ext.create("Ext.tip.ToolTip", {
                tpl: new window.Ext.XTemplate(
                    '<tpl if="embranch == \'0\'">',
                    "<b>Branch: </b>MVP<br/>",
                    '</tpl>',
                    '<tpl if="embranch == \'S\'">',
                    "<b>Branch: </b>SURVEY<br/>",
                    '</tpl>',
                    '<tpl if="embranch == \'MKT\'">',
                    "<b>Branch: </b>MARKETING<br/>",
                    '</tpl>',
                    "<br/><b>Email Subject: </b>{emsub} <br/>",
                    "<br/><b>Email Body: </b>{embody} <br/>"
                ),
                target: view.el,
                delegate: ".embodycls",
                trackMouse: true,
                dismissDelay: 0,
                renderTo: window.Ext.getBody(),
                listeners: {
                    show: function (tip) {
                        var task = new window.Ext.util.DelayedTask(function () {
                            var id = tip.anchorTarget.parentElement.dataset.recordindex;
                            var record = view.store.getAt(id);
                            if (record) {
                                tip.update(record.data);
                            } else {
                                tip.hide();
                            }
                        });
                        task.delay(10);
                    }
                }
            });
        }
        Ext.apply(me, {
            store: 'EmailList',
            forceFit: true,
            frame: true,
            autoScroll: true,
            padding: 5,
            stripeRows: true,
            columnLines: true,
            layout: 'fit',
            viewConfig: {
                deferEmptyText: true,
                emptyText: '<b><center>*** No Emails Loaded ***</center></b>',
                loadingText: 'Loading Emails...',
                listeners: {
                    render: createTooltip
                }
            },
            tbar: me.buildTbar(),
            columns: me.buildColumns(),
            listeners:  {
                itemclick: function (grid, record, item, index) {
                    var main = YLPMain.app.getController("main");
                    main.onShowEmailTemplate(record);
                    main.getViewport().down("#east-region").collapse();
                }
            }
        });
        me.callParent(arguments);
    },

    buildTbar: function () {
        return [
            {
                xtype: 'tbfill'
            },
            {
                xtype: 'button',
                text: 'Add Email',
                handler: function () {
                    var main = YLPMain.app.getController("main");
                    main.getEmailListForm().hide();
                    main.onAddNewEmail();
                }
            },
            {
                xtype: 'button',
                text: 'Close',
                handler: function () {
                    var main = YLPMain.app.getController("main");
                    main.onHideCenter();
                    main.onHideEast();
                    main.getViewport().down("#east-region").collapse();
                    main.onShowMenu();
                }
            }
        ];
    },

    buildColumns: function () {
        var email = Ext.getStore('EmailList');
        var filterarray = [];
        //function to fire the trigger click and clear the filter field
        var triggerFunc = function () {
            this.setValue('');
            var dataindex = this.ownerCt.dataIndex;
            for (var i = 0; i < filterarray.length; i++) {
                var index = filterarray[i].dataindex;
                if (index === dataindex) {
                    filterarray.splice(i, 1);
                    break;
                }
            }
            filterStore();
        };
        //function to filter the store
        var filterStore = function (dataIndex, value) {
            email.clearFilter();
            for (var i = 0; i < filterarray.length; i++) {
                email.filter({
                    property: filterarray[i].dataindex,
                    value: filterarray[i].value,
                    anyMatch: false,
                    caseSensitive: false
                });
            }
        };
        return [
            {
                text: '<b>Branch</b>',
                dataIndex: 'embranch',
                flex: 0.25,
                items: {
                    xtype: 'trigger',
                    triggerCls: 'x-form-clear-trigger',
                    onTriggerClick: triggerFunc,
                    flex: 1,
                    margin: 2,
                    dataIndex: 'embranch',
                    enableKeyEvents: true,
                    listeners: {
                        keyup: function () {
                            for (var i = 0; i < filterarray.length; i++) {
                                var index = filterarray[i].dataindex;
                                if (index === this.dataIndex) {
                                    filterarray.splice(i, 1);
                                    break;
                                }
                            }
                            if (this.value > "") {
                                filterarray.push({
                                    dataindex: this.dataIndex,
                                    value: this.value
                                });
                            }
                            filterStore();
                        },
                        buffer: 500
                    }
                },
                renderer: function (value, meta, record) {
                    switch (value) {
                        case '0':
                            return "MVP";
                        case 'S':
                            return "SURVEY";
                        default:
                            return value;
                    }
                }
            },
            {
                text: '<b>Subject</b>',
                dataIndex: 'emsub',
                flex: 1,
                tdCls: "embodycls",
                items: {
                    xtype: 'trigger',
                    triggerCls: 'x-form-clear-trigger',
                    onTriggerClick: triggerFunc,
                    flex: 1,
                    margin: 2,
                    dataIndex: 'emsub',
                    enableKeyEvents: true,
                    listeners: {
                        keyup: function () {
                            for (var i = 0; i < filterarray.length; i++) {
                                var index = filterarray[i].dataindex;
                                if (index === this.dataIndex) {
                                    filterarray.splice(i, 1);
                                    break;
                                }
                            }
                            if (this.value > "") {
                                filterarray.push({
                                    dataindex: this.dataIndex,
                                    value: this.value
                                });
                            }
                            filterStore();
                        },
                        buffer: 500
                    }
                }
            }
        ];
    }
});
