﻿/* global Ext */

Ext.define('YLPMain.view.EmailListBetaUsers', {
    extend: 'Ext.grid.Panel',
    requires: 'Ext.ux.grid.FiltersFeature',
    alias: 'widget.emaillistbetausers',

    initComponent: function () {
        var me = this;

        Ext.apply(me, {
            store: 'EmailListBetaUsers',
            forceFit: true,
            frame: true,
            autoScroll: true,
            padding: 5,
            stripeRows: true,
            columnLines: true,
            viewConfig: {
                deferEmptyText: true,
                emptyText: '<b><center>*** No Emails Loaded ***</center></b>',
                loadingText: 'Loading Emails...'
            },
            selType: 'checkboxmodel',
            selModel: {
                ignoreRightMouseSelection: true,
                checkOnly: true,
                showHeaderCheckbox: true,
                onHeaderClick: function (headerCt, header, e) {
                    if (header.isCheckerHd) {
                        e.stopEvent();
                        var isChecked = header.el.hasCls(Ext.baseCSSPrefix + 'grid-hd-checker-on');
                        var checkboxtext = Ext.ComponentQuery.query('#emaillistbetausers-checkboxcount')[0];
                        if (isChecked) {
                            this.deselectAll(true);
                            checkboxCounter = 0;
                            checkboxtext.hide();
                        } else {
                            this.selectAll(true);
                            var checkedrows = me.getSelectionModel().getSelection();
                            var checkcount = checkedrows.length;
                            checkboxCounter = checkcount;
                            checkboxtext.setText('Selected: ' + checkcount);
                            checkboxtext.show();
                        }
                    }
                },
                listeners: {
                    select: function (model, record, index, opts) {
                        checkboxCounter += 1;
                        var checkboxtext = Ext.ComponentQuery.query('#emaillistbetausers-checkboxcount')[0];
                        checkboxtext.setText('Selected: ' + checkboxCounter);
                        checkboxtext.show();
                    },
                    deselect: function (model, record, index, opts) {
                        checkboxCounter -= 1;
                        var checkboxtext = Ext.ComponentQuery.query('#emaillistbetausers-checkboxcount')[0];
                        if (checkboxCounter === 0) {
                            checkboxtext.hide();
                        } else {
                            checkboxtext.setText('Selected: ' + checkboxCounter);
                            checkboxtext.show();
                        }
                    }
                }
            },
            columns: me.buildColumns(),
            tbar: me.buildTbar()
        });
        me.callParent(arguments);
    },

    buildColumns: function () {
        var email = Ext.getStore('EmailListBetaUsers');
        var filterarray = [];
        //function to fire the trigger click and clear the filter field
        var triggerFunc = function () {
            this.setValue('');
            var dataindex = this.ownerCt.dataIndex;
            for (var i = 0; i < filterarray.length; i++) {
                var index = filterarray[i].dataindex;
                if (index === dataindex) {
                    filterarray.splice(i, 1);
                    break;
                }
            }
            filterStore();
        };
        //function to filter the store
        var filterStore = function (dataIndex, value) {
            email.clearFilter();
            for (var i = 0; i < filterarray.length; i++) {
                email.filter({
                    property: filterarray[i].dataindex,
                    value: filterarray[i].value,
                    anyMatch: false,
                    caseSensitive: false
                });
            }
        };
        return [
            {
                text: '<b>UserName</b>',
                dataIndex: 'emuser',
                flex: 1,
                items: {
                    xtype: 'trigger',
                    triggerCls: 'x-form-clear-trigger',
                    onTriggerClick: triggerFunc,
                    flex: 1,
                    margin: 2,
                    dataIndex: 'emuser',
                    enableKeyEvents: true,
                    listeners: {
                        keyup: function () {
                            for (var i = 0; i < filterarray.length; i++) {
                                var index = filterarray[i].dataindex;
                                if (index === this.dataIndex) {
                                    filterarray.splice(i, 1);
                                    break;
                                }
                            }
                            if (this.value > "") {
                                filterarray.push({
                                    dataindex: this.dataIndex,
                                    value: this.value
                                });
                            }
                            filterStore();
                        },
                        buffer: 500
                    }
                }
            },
            {
                text: '<b>Email Address</b>',
                dataIndex: 'emadd',
                flex: 1,
                items: {
                    xtype: 'trigger',
                    triggerCls: 'x-form-clear-trigger',
                    onTriggerClick: triggerFunc,
                    flex: 1,
                    margin: 2,
                    dataIndex: 'emadd',
                    enableKeyEvents: true,
                    listeners: {
                        keyup: function () {
                            for (var i = 0; i < filterarray.length; i++) {
                                var index = filterarray[i].dataindex;
                                if (index === this.dataIndex) {
                                    filterarray.splice(i, 1);
                                    break;
                                }
                            }
                            if (this.value > "") {
                                filterarray.push({
                                    dataindex: this.dataIndex,
                                    value: this.value
                                });
                            }
                            filterStore();
                        },
                        buffer: 500
                    }
                }
            }
        ];
    },
    buildTbar: function () {
        return[
            {
                xtype: 'tbtext',
                text: 'Selected: ',
                itemId: 'emaillistbetausers-checkboxcount',
                hidden: true
            },
            {
                xtype: 'tbfill'
            },
            {
                xtype: 'button',
                text: 'Send Email',
                handler: function () {
                    YLPMain.app.getController("main").onSendEmailTemplate();
                }
            }
        ]
    }
});
