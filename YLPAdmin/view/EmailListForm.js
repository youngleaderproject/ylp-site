﻿/* global Ext */

Ext.define('YLPMain.view.EmailListForm', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.emaillistform',

    requires: [
        'YLPMain.view.EmailList',
        'YLPMain.view.EmailDisplay'
    ],

    initComponent: function () {
        var me = this;
        Ext.apply(me, {
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: me.buildItems()

        });
        me.callParent(arguments);
    },

    buildItems: function () {
        return [
            {
                xtype: 'panel',
                layout: {
                    type: 'hbox',
                    align: 'stretch'
                },
                flex: 1,
                items: [
                    {
                        xtype: 'emaillist',
                        flex: 0.75
                    },
                    {
                        xtype: 'emaildisplay',
                        hidden: true,
                        flex: 1
                    }
                ]
            }
        ];
    }
});
