﻿/* global Ext */

Ext.define('YLPMain.view.GoogleDisplay', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.googledisplay',

    initComponent: function () {
        var me = this;
        Ext.apply(me, {
            frame: true,
            frameHeader: true,
            hideCollapseTool: true,
            layout: 'fit',
            padding: 5,
            viewConfig: {
                deferEmptyText: true,
                emptyText: '<b> **** Unable to load Google request ***</b>',
                loadingText: 'Retrieving Email Template.....'
            },
            tbar: me.buildTbar()

        });
        me.callParent(arguments);
    },

    buildTbar: function () {
        return [
            {
                xtype: 'tbfill'
            },
            {
                xtype: 'button',
                text: 'Close',
                handler: function () {
                    var main = YLPMain.app.getController("main");
                    main.onHideCenter();
                    main.onShowMenu();
                }
            }
        ];
    }
});
