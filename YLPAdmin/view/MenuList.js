﻿/* global Ext */

Ext.define('YLPMain.view.MenuList', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.menulist',

    initComponent: function () {
        var me = this;
        Ext.apply(me, {
            store: Ext.create('Ext.data.Store', {
                fields: ['menuopt', 'menudesc'],
                data: [
                    { menuopt: '1', menudesc: "Maintain Email's" },
                    { menuopt: '2', menudesc: "Maintain Text Message's" },
                    { menuopt: '3', menudesc: 'Google Events' },
                    { menuopt: '4', menudesc: 'Maintain Users' },
                    { menuopt: '5', menudesc: 'View Text Records' },


                ]
            }),
            forceFit: true,
            frame: true,
            autoScroll: true,
            padding: 5,
            stripeRows: true,
            columnLines: true,
            viewConfig: {
                deferEmptyText: true,
                emptyText: '<b><center>*** No Menu Loaded ***</center></b>',
                loadingText: 'Loading Menu...',
            },
            header: {
                titlePosition: 0,
                items: me.buildTbar()
            },
            columns: me.buildColumns(),
            listeners:  {
                itemclick: function (grid, record, item, index) {
                    var main = YLPMain.app.getController("main");
                    main.onHideWest();
                    main.onHandleMenuOpts(record.get("menuopt"));
                }
            }
        });
        me.callParent(arguments);
    },

    buildTbar: function () {
        return [
            {
                xtype: 'tbfill'
            },
            {
                xtype: 'button',
                text: 'Menu',
                menu: [
                    {
                        text: 'Add Admin User',
                        handler: function () {
                            window.YLPMain.app.getController("main").onCreateNewUser();
                        }
                    },
                    {
                        text: 'Sign Off',
                        handler: function () {
                            window.YLPMain.app.getController("main").onSignOff(() => { });
                        }
                    }
                ]
            }
        ];
    },

    buildColumns: function () {
        return [
            {
                text: '<b>Menu Options</b>',
                dataIndex: 'menudesc',
                flex: 1
            }
        ];
    }
});
