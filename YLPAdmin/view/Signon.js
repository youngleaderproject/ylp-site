/* global Ext */

Ext.define('YLPMain.view.Signon', {
    extend: 'Ext.form.Panel',
    alias: 'widget.signon',

    initComponent: function () {
        var me = this;
        Ext.apply(me, {
            width: '100%',
            height: '100%',
            id: 'signonscreen',
            items: me.buildItems()
        });
        me.callParent(arguments);
    },

    buildItems: function () {
        return [
            {
                xtype: 'container',
                cls:'SignOnContainer',
                items: [
                    {
                        xtype: 'image',
                        src: '/images/NewYLPLogo.png',
                        cls:'SignOnLogo'
                    },
                    {
                        xtype: 'textfield',
                        enforceMaxLength: true,
                        name: 'usrname',
                        emptyText: 'User ID',
                        itemId: 'usr',
                        maxLength: 30,
                        cls:'SignOnUserField',
                        height: 25,
                        msgTarget: 'title',
                        listeners: {
                            afterrender: function (v) {
                                v.focus();
                            }
                        }
                    },
                    {
                        xtype: 'textfield',
                        enforceMaxLength: true,
                        emptyText: 'Password',
                        name: 'passw',
                        itemId: 'pw',
			            cls:'SignOnPasswordField',
                        height: 25,
                        inputType: 'password',
                        maxLength: 128,
                        msgTarget: 'side',
                        listeners: {
                            scope: this,
                            specialkey: function (fld, event) {
                                if (event.getKey() === event.ENTER) {
                                    this.fireEvent('signonform');
                                }
                            }
                        }
                    },
                    {
                        xtype: 'button',
                        itemId: 'signon-button',
                        text: 'Log In',
                        height: 25,
                        cls:'SignOnButton'
                    },
                    {
                        xtype: 'displayfield',
                        id: 'signonerror',
                        hidden: true,
                        cls:'SignOnerrorField'
                    }
                ]
            },
            {
                xtype:'tbfill'
            },
            {
                xtype: 'tbtext',
                cls: 'ylpsignontext',
                float: 'bottom left',
                text: '&copy; Young Leader Project, LLC. All Rights Reserved.'
            }
        ];
    }
});
