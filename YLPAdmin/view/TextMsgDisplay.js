﻿/* global Ext */

Ext.define('YLPMain.view.TextMsgDisplay', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.textmsgdisplay',

    initComponent: function () {
        var me = this;
        Ext.apply(me, {
            frame: true,
            frameHeader: true,
            hideCollapseTool: true,
            layout: 'fit',
            padding: 5,
            viewConfig: {
                deferEmptyText: true,
                emptyText: '<b> **** Unable to load text messages requested ***</b>',
                loadingText: 'Retrieving Text Message Template.....'
            },
            tbar: me.buildTbar()
        });
        me.callParent(arguments);
    },

    buildTbar: function () {
        var me = this;
        return [
            {
                xtype: 'tbfill'
            },
            {
                xtype: 'button',
                text: 'Send Manual Text',
                handler: function () {
                    YLPMain.app.getController("main").onSendManualTextMsg();
                }
            },
            {
                xtype: 'button',
                text: 'Send Reminder Text',
                handler: function () {
                    YLPMain.app.getController("main").onTextMessageTemplate('Y');
                }
            },
            {
                xtype: 'button',
                text: 'Send First Text',
                handler: function () {
                    YLPMain.app.getController("main").onTextMessageTemplate();
                }
            },
            {
                xtype: 'button',
                text: 'Close',
                handler: function () {
                    me.hide();
                }
                
            }
        ];
    }
});
