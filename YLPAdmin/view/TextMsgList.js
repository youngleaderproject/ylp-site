﻿/* global Ext */

Ext.define('YLPMain.view.TextMsgList', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.textmsglist',

    initComponent: function () {
        var me = this;
        me.checkboxCounter = 0;
        function createTooltip(view) {
            view.tip = window.Ext.create("Ext.tip.ToolTip", {
                tpl: new window.Ext.XTemplate(
                    "<b>Track: </b>{txttrack}<br/><br/>",
                    '<tpl if="txtbranch == \'CW1\'">',
                    "<b>Time: </b>Week 1<br/>",
                    '</tpl>',  
                    '<tpl if="txtbranch == \'CW2\'">',
                    "<b>Time: </b>Week 2<br/>",
                    '</tpl>',
                    '<tpl if="txtbranch == \'CW3\'">',
                    "<b>Time: </b>Week 3<br/>",
                    '</tpl>',
                    '<tpl if="txtbranch == \'CW4\'">',
                    "<b>Time: </b>Week 4<br/>",
                    '</tpl>',
                    '<tpl if="txtbranch == \'CW5\'">',
                    "<b>Time: </b>Week 5<br/>",
                    '</tpl>',
                    '<tpl if="txtbranch == \'CW6\'">',
                    "<b>Time: </b>Week 6<br/>",
                    '</tpl>',
                    '<tpl if="txtbranch == \'CW7\'">',
                    "<b>Time: </b>Week 7<br/>",
                    '</tpl>',
                    '<tpl if="txtbranch == \'CW8\'">',
                    "<b>Time: </b>Week 8<br/>",
                    '</tpl>',
                    "<br/><b>Text Body: </b>{txtmsgbody} <br/>"
                ),
                target: view.el,
                delegate: ".txtbodycls",
                trackMouse: true,
                dismissDelay: 0,
                renderTo: window.Ext.getBody(),
                listeners: {
                    show: function (tip) {
                        var task = new window.Ext.util.DelayedTask(function () {
                            var id = tip.anchorTarget.parentElement.dataset.recordindex;
                            var record = view.store.getAt(id);
                            if (record) {
                                tip.update(record.data);
                            } else {
                                tip.hide();
                            }
                        });
                        task.delay(10);
                    }
                }
            });
        }
        Ext.apply(me, {
            store: 'TxtMsgList',
            forceFit: true,
            frame: true,
            autoScroll: true,
            padding: 5,
            stripeRows: true,
            columnLines: true,
            layout: 'fit',
            viewConfig: {
                deferEmptyText: true,
                emptyText: '<b><center>*** No Text Messages Loaded ***</center></b>',
                loadingText: 'Loading Text Messages...',
                listeners: {
                    render: createTooltip
                }
            },
            selType: 'checkboxmodel',
            selModel: {
                ignoreRightMouseSelection: true,
                checkOnly: true,
                showHeaderCheckbox: true,
                onHeaderClick: function (headerCt, header, e) {
                    if (header.isCheckerHd) {
                        e.stopEvent();
                        var isChecked = header.el.hasCls(Ext.baseCSSPrefix + 'grid-hd-checker-on');
                        var checkboxtext = Ext.ComponentQuery.query('#txtmsglist-checkboxcount')[0];
                        if (isChecked) {
                            this.deselectAll(true);
                            me.checkboxCounter = 0;
                            checkboxtext.hide();
                        } else {
                            this.selectAll(true);
                            var checkedrows = me.getSelectionModel().getSelection();
                            var checkcount = checkedrows.length;
                            me.checkboxCounter = checkcount;
                            checkboxtext.setText('Selected: ' + checkcount);
                            checkboxtext.show();
                        }
                    }
                },
                listeners: {
                    select: function (model, record, index, opts) {
                        me.checkboxCounter += 1;
                        var checkboxtext = Ext.ComponentQuery.query('#txtmsglist-checkboxcount')[0];
                        checkboxtext.setText('Selected: ' + me.checkboxCounter);
                        checkboxtext.show();
                    },
                    deselect: function (model, record, index, opts) {
                        me.checkboxCounter -= 1;
                        var checkboxtext = Ext.ComponentQuery.query('#txtmsglist-checkboxcount')[0];
                        if (me.checkboxCounter === 0) {
                            checkboxtext.hide();
                        } else {
                            checkboxtext.setText('Selected: ' + me.checkboxCounter);
                            checkboxtext.show();
                        }
                    }
                }
            },
            columns: me.buildColumns(),
            tbar: [
                {
                    xtype: 'tbtext',
                    text: 'Selected: ',
                    itemId: 'txtmsglist-checkboxcount',
                    hidden: true
                },
                {
                    xtype: 'tbfill'
                },
                {
                    text: 'Add Text',
                    id: 'textmsglistadd',
                    icon: 'resources/images/add.png',
                    handler: function () {
                        me.updatetextwindow = Ext.create('Ext.window.Window', {
                            title: 'YLP Add Edit Text Message',
                            layout: 'fit',
                            closeAction: 'destroy',
                            constrain: true,
                            modal: true,
                            id: 'addtextmsgwindow',
                            items: [{
                                xtype: 'form',
                                id: 'addtextmsgform',
                                bbar: [
                                    {
                                        xtype: 'tbfill'
                                    },
                                    {
                                        xtype: 'button',
                                        text: 'Submit',
                                        formBind: true,
                                        handler: function (btn) {
                                            Ext.MessageBox.show({
                                                title: 'YLP Admin',
                                                msg: 'Adding Text Message..',
                                                width: 300,
                                                wait: true,
                                                waitConfig: {
                                                    interval: 200
                                                },
                                                progress: true,
                                                closable: false,
                                                modal: true
                                            });
                                            var theUrl = window.YLPMain.config.Settings.Url;
                                            var formvalues = btn.up('form').getForm().getValues();
                                            me.form = document.createElement("FORM");
                                            me.formDataToUpload = new FormData(me.form);
                                            for (var key in formvalues) {
                                                me.formDataToUpload.append(key, formvalues[key]);
                                            }
                                            $.ajax({
                                                url: theUrl + '?rqstyp=ylptxtmsg&type=addtext&dev=' + window.YLPMain.config.Settings.YLP.device,
                                                data: me.formDataToUpload,
                                                type: "POST",
                                                contentType: false,
                                                processData: false,
                                                error: function (data) {

                                                },
                                                success: function (data) {
                                                    Ext.MessageBox.hide();
                                                    me.updatetextwindow.destroy();
                                                    Ext.getStore('TxtMsgList').load();
                                                },
                                                complete: function () {
                                                }
                                            });
                                        }
                                    }
                                ],
                                items: [
                                    {
                                        xtype: 'combobox',
                                        store: ['1', '2', '3', '4', '5', '6'],
                                        name: 'txttrack',
                                        padding: 10,
                                        fieldLabel: 'Track',
                                        allowBlank: false,
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'combobox',
                                        name: 'txtbranch',
                                        store: ['INTRO', 'CW1', 'CW2', 'CW3', 'CW4', 'CW5', 'CW6', 'CW7', 'CW8'],
                                        padding: 10,
                                        fieldLabel: 'Week/Branch',
                                        allowBlank: false,
                                        editable: false,
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'combobox',
                                        name: 'txtpos',
                                        store: ['1', '2', '3'],
                                        padding: 10,
                                        fieldLabel: 'Day of Text',
                                        allowBlank: false,
                                        editable: false,
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'numberfield',
                                        anchor: '95%',
                                        name: 'txtseq',
                                        fieldLabel: 'Text Sequence',
                                        minValue: 1,
                                        padding: 10,
                                        hideTrigger: true,
                                        keyNavEnabled: false,
                                        mouseWheelEnabled: false
                                    },
                                    {
                                        xtype: 'combobox',
                                        name: 'txtassesment',
                                        store: ['True', 'False'],
                                        padding: 10,
                                        fieldLabel: 'Assesment Text',
                                        allowBlank: false,
                                        editable: false,
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'textareafield',
                                        name: 'txtmsgbody',
                                        padding: 10,
                                        fieldLabel: 'Message Body',
                                        grow: true,
                                        anchor: '100%',
                                        width: 400,
                                        height: 300
                                    },
                                    {
                                        xtype: 'textfield',
                                        name: 'txtmedia',
                                        padding: 10,
                                        fieldLabel: 'Media',
                                        anchor: '100%'
                                    },
                                ]
                            }]
                        }).show();
                    }
                },
                {
                    xtype: 'button',
                    text: 'Close',
                    handler: function () {
                        var main = YLPMain.app.getController("main");
                        main.onHideCenter();
                        main.onHideEast();
                        main.getViewport().down("#east-region").collapse();
                        main.onShowMenu();
                    }

                }
            ],
            menu: me.buildMenu(),
            listeners:  {
                itemclick: function (grid, record, item, index) {
                    var main = YLPMain.app.getController("main");
                    main.onShowTextMsgTemplate(record);
                    main.getViewport().down("#east-region").collapse();
                },
                itemcontextmenu: function (view, record, el, index, event) {
                    event.stopEvent();
                    if (me.getSelectionModel().getSelection().length > 0) {
                        Ext.getCmp('textmsglistsend').show();
                        Ext.getCmp('textmsglistedit').hide();
                    } else {
                        Ext.getCmp('textmsglistsend').hide();
                        Ext.getCmp('textmsglistedit').show();
                    }
                    me.currentRecord = record;
                    me.menu.showAt(event.getXY());
                }
            }
        });
        me.callParent(arguments);
    },
    buildMenu: function () {
        var me = this;
        return Ext.create('Ext.menu.Menu', {
            items: [
                {
                    text: 'Edit Text',
                    id: 'textmsglistedit',
                    icon: 'resources/images/edit.png',
                    handler: function () {
                     me.updatetextwindow = Ext.create('Ext.window.Window', {
                            title: 'YLP Edit Text Message',
                            layout: 'fit',
                            closeAction: 'destroy',
                            constrain: true,
                            modal: true,
                            id: 'updatetextmsgwindow',
                            items: [{
                                xtype: 'form',
                                id: 'updatetextmsgform',
                                bbar: [
                                    {
                                        xtype: 'tbfill'
                                    },
                                    {
                                        xtype: 'button',
                                        text: 'Submit',
                                        formBind: true,
                                        handler: function (btn) {
                                            Ext.MessageBox.show({
                                                title: 'YLP Admin',
                                                msg: 'Updating Text Message..',
                                                width: 300,
                                                wait: true,
                                                waitConfig: {
                                                    interval: 200
                                                },
                                                progress: true,
                                                closable: false,
                                                modal: true
                                            });
                                            var textarea = Ext.getCmp('edittextareafld').getValue();
                                            var theUrl = window.YLPMain.config.Settings.Url;
                                            me.form = document.createElement("FORM");
                                            me.formDataToUpload = new FormData(me.form);
                                            me.formDataToUpload.append("txtmsgbody", textarea);
                                            me.formDataToUpload.append("txtid", me.currentRecord.get("txtid"));
                                            $.ajax({
                                                url: theUrl + '?rqstyp=ylptxtmsg&type=updatetext&dev=' + window.YLPMain.config.Settings.YLP.device,
                                                data: me.formDataToUpload,
                                                type: "POST",
                                                contentType: false,
                                                processData: false,
                                                error: function (data) {

                                                },
                                                success: function (data) {
                                                    Ext.MessageBox.hide();
                                                    me.updatetextwindow.destroy();
                                                    Ext.getStore('TxtMsgList').load();
                                                },
                                                complete: function () {
                                                }
                                            });
                                            
                                        }
                                    }
                                ],
                                items: [
                                    {
                                        xtype: 'textareafield',
                                        id: 'edittextareafld',
                                        name: 'txtmsgbody',
                                        padding: 10,
                                        fieldLabel: 'Message Body',
                                        allowBlank: false,
                                        grow: true,
                                        anchor: '100%',
                                        width: 400,
                                        height: 300
                                    }
                                ]
                            }]
                        }).show();
                        Ext.getCmp('updatetextmsgform').loadRecord(me.currentRecord);
                    }
                },
                {
                    text: 'Edit Properties',
                    id: 'textmsglisteditp',
                    icon: 'resources/images/edit.png',
                    handler: function () {
                        me.updatetextwindow = Ext.create('Ext.window.Window', {
                            title: 'YLP Edit Text Message Properties',
                            layout: 'fit',
                            closeAction: 'destroy',
                            constrain: true,
                            modal: true,
                            id: 'updatetextmsgwindow',
                            items: [{
                                xtype: 'form',
                                id: 'updatetextmsgform',
                                bbar: [
                                    {
                                        xtype: 'tbfill'
                                    },
                                    {
                                        xtype: 'button',
                                        text: 'Submit',
                                        formBind: true,
                                        handler: function (btn) {
                                            Ext.MessageBox.show({
                                                title: 'YLP Admin',
                                                msg: 'Updating Text Message..',
                                                width: 300,
                                                wait: true,
                                                waitConfig: {
                                                    interval: 200
                                                },
                                                progress: true,
                                                closable: false,
                                                modal: true
                                            });
                                            var theUrl = window.YLPMain.config.Settings.Url;
                                            var parms = Ext.getCmp('updatetextmsgform').getForm().getValues();
                                            Ext.apply(parms, {
                                                rqstyp: 'ylptxtmsg',
                                                type: 'updateprops',
                                                dev: window.YLPMain.config.Settings.YLP.device,
                                                txtid: me.currentRecord.get("txtid")
                                            });
                                            Ext.Ajax.request({
                                                url: theUrl,
                                                params: parms,
                                                scope: me,

                                                success: function (response) {
                                                    Ext.MessageBox.hide();
                                                    me.updatetextwindow.destroy();
                                                    Ext.getStore('TxtMsgList').load();
                                                },
                                                failure: function () {
                                                    Ext.Msg.alert('Error', 'Unable to connect to server');
                                                }
                                            });
                                        }
                                    }
                                ],
                                items: [
                                    {
                                        xtype: 'textfield',
                                        id: 'proptexttrack',
                                        name: 'txttrack',
                                        padding: 10,
                                        fieldLabel: 'Track',
                                        allowBlank: false,
                                        anchor: '95%'
                                    },
                                    {
                                        xtype: 'combobox',
                                        id: 'proptextbranch',
                                        name: 'txtbranch',
                                        store: ['INTRO', 'CW1', 'CW2', 'CW3', 'CW4', 'CW5', 'CW6', 'CW7', 'CW8'],
                                        padding: 10,
                                        fieldLabel: 'Week/Branch',
                                        allowBlank: false,
                                        editable: false,
                                        anchor: '95%'
                                    },
                                    {
                                        xtype: 'combobox',
                                        id: 'proptextpos',
                                        name: 'txtpos',
                                        store: ['1', '2', '3'],
                                        padding: 10,
                                        fieldLabel: 'Day of Text',
                                        allowBlank: false,
                                        editable: false,
                                        anchor: '95%'
                                    },
                                    {
                                        xtype: 'numberfield',
                                        anchor: '95%',
                                        id: 'proptextseq',
                                        name: 'txtseq',
                                        fieldLabel: 'Text Sequence',
                                        minValue: 1,
                                        padding: 10,
                                        hideTrigger: true,
                                        keyNavEnabled: false,
                                        mouseWheelEnabled: false
                                    },
                                    {
                                        xtype: 'combobox',
                                        id: 'proptextasses',
                                        name: 'txtassesment',
                                        store: ['True', 'False'],
                                        padding: 10,
                                        fieldLabel: 'Assesment Text',
                                        allowBlank: false,
                                        editable: false,
                                        anchor: '95%'
                                    }
                                ]
                            }]
                        }).show();
                        Ext.getCmp('updatetextmsgform').loadRecord(me.currentRecord);
                    }
                },
                {
                    text: 'Delete Text',
                    id: 'textmsglistdelete',
                    icon: 'resources/images/delete.png',
                    handler: function () {
                        Ext.Msg.show({
                            title: 'Text Message Maintenance',
                            msg: 'Are you sure you want to delete this text?',
                            buttons: Ext.Msg.YESNO,
                            icon: Ext.Msg.QUESTION,
                            fn: function (btn) {
                                if (btn === 'yes') {
                                    Ext.Msg.hide();
                                    var parms = {};
                                    Ext.apply(parms, {
                                        rqstyp: "ylptxtmsg",
                                        type: "dlttext",
                                        txtid: me.currentRecord.get("txtid"),
                                        dev: window.YLPMain.config.Settings.YLP.device
                                    });
                                    Ext.Ajax.request({
                                        url: window.YLPMain.config.Settings.Url, //'/pgms/rvimain.pgm',
                                        scope: me,
                                        params: parms,
                                        success: function () {
                                            Ext.getStore("TxtMsgList").load();
                                            YLPMain.app.getController("callMsgCurtain").onShowMsg('Delete Text', 'Complete');
                                        },
                                        failure: function () {
                                            Ext.Msg.alert('Error', 'Updating Failed');
                                        }
                                    });
                                } else {
                                    Ext.Msg.hide();
                                }
                            }
                        });
                    }
                },
                {
                    text: 'Send Selected',
                    id: 'textmsglistsend',
                    icon: 'resources/images/send.png',
                    handler: function () {
                        Ext.create('Ext.window.Window', {
                            title: 'YLP Manual Text Message',
                            layout: 'fit',
                            closeAction: 'destroy',
                            constrain: true,
                            modal: true,
                            id: 'manualtextmsgwindow',
                            items: [{
                                xtype: 'form',
                                id: 'manualtextmsgform',
                                defaults: {
                                    anchor: '100%',
                                    padding: 10
                                },
                                bbar: [
                                    {
                                        xtype: 'tbfill'
                                    },
                                    {
                                        xtype: 'button',
                                        text: 'Submit',
                                        formBind: true,
                                        handler: function (btn) {
                                            Ext.MessageBox.show({
                                                title: 'YLP Admin',
                                                msg: 'Sending Text Message..',
                                                width: 300,
                                                wait: true,
                                                waitConfig: {
                                                    interval: 200
                                                },
                                                progress: true,
                                                closable: false,
                                                modal: true
                                            });
                                            me.counter = 0;
                                            var selectedRows = me.getSelectionModel().getSelection();
                                            var userinfo = Ext.getCmp('manualtextmsgform').getForm().getValues();
                                            var loopArray = function (arr) {
                                                me.SendTxtMsgFunc(userinfo,arr[me.counter], function () {
                                                    if (me.counter < arr.length) {
                                                        loopArray(arr);
                                                    } else {
                                                        me.getSelectionModel().deselectAll();
                                                        Ext.MessageBox.hide();
                                                        YLPMain.app.getController("callMsgCurtain").onShowMsg('Selected Text', 'Sent!');
                                                        Ext.getCmp('manualtextmsgwindow').destroy();
                                                    }
                                                });
                                            };

                                            loopArray(selectedRows);
                                        }
                                    }
                                ],
                                items: [
                                    {
                                        xtype: 'textfield',
                                        name: 'txtuser',
                                        fieldLabel: 'Enter Name',
                                        allowBlank: false
                                    },
                                    {
                                        xtype: 'textfield',
                                        name: 'txtadd',
                                        vtype: 'phone',
                                        maxLength: 10,
                                        minLength: 10,
                                        enforceMaxLength: true,
                                        fieldLabel: 'Enter Phone',
                                        allowBlank: false
                                    },
                                    {
                                        name: 'txtcntry',
                                        value: 'US',
                                        fieldLabel: 'Select Country',
                                        valueField: 'code',
                                        displayField: 'name',
                                        typeAhead: true,
                                        allowBlank: false,
                                        xtype: 'combobox',
                                        queryMode: 'local',
                                        lastQuery: '',
                                        store: Ext.create('Ext.data.Store', {
                                            fields: ['code', 'name'],
                                            data: Ext.ux.Countries.CountryArray
                                        })
                                    }
                                ]
                            }]
                        }).show();
                    }
                }
            ]
        });
    },

    SendTxtMsgFunc: function (userinfo,rec, callback) {
        var me = this;
        if (rec) {
            var txttrack = rec.get("txttrack");
            var txtbranch = rec.get("txtbranch");
            var txtpos = rec.get("txtpos");
            var txtseq = rec.get("txtseq");
            if (userinfo.txtcntry === "") userinfo.txtcntry = 'US';
            var theUrl = window.YLPMain.config.Settings.Url + '?rqstyp=ylptxtmsg&type=sendtext&dev=' + window.YLPMain.config.Settings.YLP.device + "&txtpos=" + txtpos + "&txttrack=" + txttrack + "&txtseq=" + txtseq + "&txtbranch=" + txtbranch + "&txtuser=" + userinfo.txtuser + "&txtadd=" + userinfo.txtadd + "&txtcntry=" + userinfo.txtcntry;
            window.Ext.Ajax.request({
                url: theUrl,
                scope: me,
                success: function (response) {
                    me.counter += 1;
                    callback();
                }
            });
        } else {
            me.counter += 1;
            callback();
        }
    },

    buildColumns: function () {
        var email = Ext.getStore('TxtMsgList');
        var filterarray = [];
        //function to fire the trigger click and clear the filter field
        var triggerFunc = function () {
            this.setValue('');
            var dataindex = this.ownerCt.dataIndex;
            for (var i = 0; i < filterarray.length; i++) {
                var index = filterarray[i].dataindex;
                if (index === dataindex) {
                    filterarray.splice(i, 1);
                    break;
                }
            }
            filterStore();
        };
        //function to filter the store
        var filterStore = function (dataIndex, value) {
            email.clearFilter();
            for (var i = 0; i < filterarray.length; i++) {
                email.filter({
                    property: filterarray[i].dataindex,
                    value: filterarray[i].value,
                    anyMatch: false,
                    caseSensitive: false
                });
            }
        };
        return [
            {
                text: '<b>Track</b>',
                dataIndex: 'txttrack',
                flex: 0.1,
                items: {
                    xtype: 'trigger',
                    triggerCls: 'x-form-clear-trigger',
                    onTriggerClick: triggerFunc,
                    padding: '0 50 0 20',
                    dataIndex: 'txttrack',
                    enableKeyEvents: true,
                    listeners: {
                        keyup: function () {
                            for (var i = 0; i < filterarray.length; i++) {
                                var index = filterarray[i].dataindex;
                                if (index === this.dataIndex) {
                                    filterarray.splice(i, 1);
                                    break;
                                }
                            }
                            if (this.value > "") {
                                filterarray.push({
                                    dataindex: this.dataIndex,
                                    value: this.value
                                });
                            }
                            filterStore();
                        },
                        buffer: 500
                    }
                }
            },
            {
                text: '<b>Week</b>',
                dataIndex: 'txtbranch',
                flex: 0.1,
                items: {
                    xtype: 'trigger',
                    triggerCls: 'x-form-clear-trigger',
                    onTriggerClick: triggerFunc,
                    padding: '0 50 0 20',
                    dataIndex: 'txtbranch',
                    enableKeyEvents: true,
                    listeners: {
                        keyup: function () {
                            for (var i = 0; i < filterarray.length; i++) {
                                var index = filterarray[i].dataindex;
                                if (index === this.dataIndex) {
                                    filterarray.splice(i, 1);
                                    break;
                                }
                            }
                            if (this.value > "") {
                                filterarray.push({
                                    dataindex: this.dataIndex,
                                    value: this.value
                                });
                            }
                            filterStore();
                        },
                        buffer: 500
                    }
                }
            },
            {
                text: '<b>Day Of Text</b>',
                dataIndex: 'txtpos',
                flex: 0.1,
                items: {
                    xtype: 'trigger',
                    triggerCls: 'x-form-clear-trigger',
                    onTriggerClick: triggerFunc,
                    padding: '0 50 0 20',
                    dataIndex: 'txtpos',
                    enableKeyEvents: true,
                    listeners: {
                        keyup: function () {
                            for (var i = 0; i < filterarray.length; i++) {
                                var index = filterarray[i].dataindex;
                                if (index === this.dataIndex) {
                                    filterarray.splice(i, 1);
                                    break;
                                }
                            }
                            if (this.value > "") {
                                filterarray.push({
                                    dataindex: this.dataIndex,
                                    value: this.value
                                });
                            }
                            filterStore();
                        },
                        buffer: 500
                    }
                }
            },
            {
                text: '<b>Sequence</b>',
                dataIndex: 'txtseq',
                flex: 0.1,
                items: {
                    xtype: 'trigger',
                    triggerCls: 'x-form-clear-trigger',
                    onTriggerClick: triggerFunc,
                    padding: '0 50 0 20',
                    dataIndex: 'txtseq',
                    enableKeyEvents: true,
                    listeners: {
                        keyup: function () {
                            for (var i = 0; i < filterarray.length; i++) {
                                var index = filterarray[i].dataindex;
                                if (index === this.dataIndex) {
                                    filterarray.splice(i, 1);
                                    break;
                                }
                            }
                            if (this.value > "") {
                                filterarray.push({
                                    dataindex: this.dataIndex,
                                    value: this.value
                                });
                            }
                            filterStore();
                        },
                        buffer: 500
                    }
                }
            },
            {
                text: '<b>Text Message</b>',
                dataIndex: 'txtmsgbody',
                flex: 1,
                tdCls: "txtbodycls",
                renderer: function (val,meta,record) {
                    if (record.get("txtmsgmedia") > "" && val === "") {
                        return "(GIF)";
                    } else {
                        return val;
                    }
                },
                items: {
                    xtype: 'trigger',
                    triggerCls: 'x-form-clear-trigger',
                    onTriggerClick: triggerFunc,
                    padding: '0 50 0 20',
                    dataIndex: 'txtmsgbody',
                    enableKeyEvents: true,
                    listeners: {
                        keyup: function () {
                            for (var i = 0; i < filterarray.length; i++) {
                                var index = filterarray[i].dataindex;
                                if (index === this.dataIndex) {
                                    filterarray.splice(i, 1);
                                    break;
                                }
                            }
                            if (this.value > "") {
                                filterarray.push({
                                    dataindex: this.dataIndex,
                                    value: this.value
                                });
                            }
                            filterStore();
                        },
                        buffer: 500
                    }
                }
            }
        ];
    }
});
