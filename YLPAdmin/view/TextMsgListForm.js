﻿/* global Ext */

Ext.define('YLPMain.view.TextMsgListForm', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.textmsglistform',

    requires: [
        'YLPMain.view.TextMsgList',
        'YLPMain.view.TextMsgDisplay'
    ],

    initComponent: function () {
        var me = this;
        Ext.apply(me, {
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: me.buildItems()

        });
        me.callParent(arguments);
    },

    buildItems: function () {
        return [
            {
                xtype: 'panel',
                layout: {
                    type: 'hbox',
                    align: 'stretch'
                },
                flex: 1,
                items: [
                    {
                        xtype: 'textmsglist',
                        flex: 1
                    },
                    {
                        xtype: 'textmsgdisplay',
                        hidden: true,
                        flex: 1
                    }
                ]
            }
        ];
    }
});
