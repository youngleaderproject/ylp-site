﻿/* global Ext */

Ext.define('YLPMain.view.TextMsgRecList', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.textmsgreclist',

    initComponent: function () {
        var me = this;
        me.checkboxCounter = 0;
        function createTooltip(view) {
            view.tip = window.Ext.create("Ext.tip.ToolTip", {
                tpl: new window.Ext.XTemplate(
                    "<b>Track: </b>{txttrack}<br/><br/>",
                    '<tpl if="txtbranch == \'INTRO\'">',
                    "<b>Time: </b>Intro<br/>",
                    '</tpl>',
                    '<tpl if="txtbranch == \'CW1\'">',
                    "<b>Time: </b>Week 1<br/>",
                    '</tpl>',  
                    '<tpl if="txtbranch == \'CW2\'">',
                    "<b>Time: </b>Week 2<br/>",
                    '</tpl>',
                    '<tpl if="txtbranch == \'CW3\'">',
                    "<b>Time: </b>Week 3<br/>",
                    '</tpl>',
                    '<tpl if="txtbranch == \'CW4\'">',
                    "<b>Time: </b>Week 4<br/>",
                    '</tpl>',
                    '<tpl if="txtbranch == \'CW5\'">',
                    "<b>Time: </b>Week 5<br/>",
                    '</tpl>',
                    '<tpl if="txtbranch == \'CW6\'">',
                    "<b>Time: </b>Week 6<br/>",
                    '</tpl>',
                    '<tpl if="txtbranch == \'CW7\'">',
                    "<b>Time: </b>Week 7<br/>",
                    '</tpl>',
                    '<tpl if="txtbranch == \'CW8\'">',
                    "<b>Time: </b>Week 8<br/>",
                    '</tpl>',
                    '<tpl if="txtpos == \'1\'">',
                    "<b>Time: </b>Day 1<br/>",
                    '</tpl>',
                    '<tpl if="txtbranch == \'2\'">',
                    "<b>Time: </b>Day 2<br/>",
                    '</tpl>',
                    '<tpl if="txtbranch == \'3\'">',
                    "<b>Time: </b>Day 3<br/>",
                    '</tpl>',
                    '<tpl if="txtbranch == \'4\'">',
                    "<b>Time: </b>Day 3 - Assessment<br/>",
                    '</tpl>',
                    '<tpl if="txttrack == \'1\'">',
                    "<b><br/>Track: </b>Confidence<br/>",
                    '</tpl>',
                    '<tpl if="txttrack == \'2\'">',
                    "<b><br/>Track: </b>Stress<br/>",
                    '</tpl>',
                    '<tpl if="txttrack == \'3\'">',
                    "<b><br/>Track: </b>Social Intelligence<br/>",
                    '</tpl>',
                    '<tpl if="txttrack == \'4\'">',
                    "<b><br/>Track: </b>Relationships<br/>",
                    '</tpl>',
                    '<tpl if="txttrack == \'5\'">',
                    "<b><br/>Track: </b>Self-Control<br/>",
                    '</tpl>',
                    '<tpl if="txttrack == \'6\'">',
                    "<b><br/>Track: </b>Thoughtful Decisions<br/>",
                    '</tpl>',
                    "<br/><b>Text Date: </b>{txtmsgdate} <br/>",
                    "<br/><b>Phone Number: </b>{txtphone} <br/>"
                ),
                target: view.el,
                delegate: ".txtbodycls",
                trackMouse: true,
                dismissDelay: 0,
                renderTo: window.Ext.getBody(),
                listeners: {
                    show: function (tip) {
                        var task = new window.Ext.util.DelayedTask(function () {
                            var id = tip.anchorTarget.parentElement.dataset.recordindex;
                            var record = view.store.getAt(id);
                            if (record) {
                                tip.update(record.data);
                            } else {
                                tip.hide();
                            }
                        });
                        task.delay(10);
                    }
                }
            });
        }
        Ext.apply(me, {
            store: 'TxtMsgRecList',
            forceFit: true,
            frame: true,
            autoScroll: true,
            padding: 5,
            stripeRows: true,
            columnLines: true,
            layout: 'fit',
            viewConfig: {
                deferEmptyText: true,
                emptyText: '<b><center>*** No Records Loaded ***</center></b>',
                loadingText: 'Loading Records...',
                listeners: {
                    render: createTooltip
                }
            },
            columns: me.buildColumns(),
            tbar: [
                {
                    xtype: 'tbfill'
                },
                {
                    xtype: 'button',
                    text: 'Close',
                    handler: function () {
                        var main = YLPMain.app.getController("main");
                        main.onHideCenter();
                        main.onHideEast();
                        main.getViewport().down("#east-region").collapse();
                        main.onShowMenu();
                    }

                }
            ]
        });
        me.callParent(arguments);
    },

    buildColumns: function () {
        var email = Ext.getStore('TxtMsgRecList');
        var filterarray = [];
        //function to fire the trigger click and clear the filter field
        var triggerFunc = function () {
            this.setValue('');
            var dataindex = this.ownerCt.dataIndex;
            for (var i = 0; i < filterarray.length; i++) {
                var index = filterarray[i].dataindex;
                if (index === dataindex) {
                    filterarray.splice(i, 1);
                    break;
                }
            }
            filterStore();
        };
        //function to filter the store
        var filterStore = function (dataIndex, value) {
            email.clearFilter();
            for (var i = 0; i < filterarray.length; i++) {
                email.filter({
                    property: filterarray[i].dataindex,
                    value: filterarray[i].value,
                    anyMatch: false,
                    caseSensitive: false
                });
            }
        };
        return [
            {
                text: '<b>Phone Number</b>',
                dataIndex: 'txtphone',
                flex: 1,
                items: {
                    xtype: 'trigger',
                    triggerCls: 'x-form-clear-trigger',
                    onTriggerClick: triggerFunc,
                    padding: '0 50 0 20',
                    dataIndex: 'txtphone',
                    enableKeyEvents: true,
                    listeners: {
                        keyup: function () {
                            for (var i = 0; i < filterarray.length; i++) {
                                var index = filterarray[i].dataindex;
                                if (index === this.dataIndex) {
                                    filterarray.splice(i, 1);
                                    break;
                                }
                            }
                            if (this.value > "") {
                                filterarray.push({
                                    dataindex: this.dataIndex,
                                    value: this.value
                                });
                            }
                            filterStore();
                        },
                        buffer: 500
                    }
                }
            },
            {
                text: '<b>Track</b>',
                dataIndex: 'txttrack',
                flex: 1,
                items: {
                    xtype: 'trigger',
                    triggerCls: 'x-form-clear-trigger',
                    onTriggerClick: triggerFunc,
                    padding: '0 50 0 20',
                    dataIndex: 'txttrack',
                    enableKeyEvents: true,
                    listeners: {
                        keyup: function () {
                            for (var i = 0; i < filterarray.length; i++) {
                                var index = filterarray[i].dataindex;
                                if (index === this.dataIndex) {
                                    filterarray.splice(i, 1);
                                    break;
                                }
                            }
                            if (this.value > "") {
                                filterarray.push({
                                    dataindex: this.dataIndex,
                                    value: this.value
                                });
                            }
                            filterStore();
                        },
                        buffer: 500
                    }
                },
                renderer: function (value) {
                    switch (value) {
                        case "1":
                            return 'Confidence(1)';
                        case "2":
                            return 'Stress(2)';
                        case "3":
                            return 'Social Intelligence(3)';
                        case "4":
                            return 'Relationships(4)';
                        case "5":
                            return 'Thoughtful Decisions(5)';
                        case "6":
                            return 'Self-Control(6)';
                        default:
                            return value;
                    }
                }
            },
            {
                text: '<b>Week</b>',
                dataIndex: 'txtbranch',
                flex: 1,
                tdCls: "txtbodycls",
                renderer: function (val, meta, record) {
                    switch (val) {
                        case "INTRO":
                            return "INTRO";
                        case "CW1":
                            return "Week 1";
                        case "CW2":
                            return "Week 2";
                        case "CW3":
                            return "Week 3";
                        case "CW4":
                            return "Week 4";
                        case "CW5":
                            return "Week 5";
                        case "CW6":
                            return "Week 6";
                        case "CW7":
                            return "Week 7";
                        case "CW8":
                            return "Week 8";
                        default:
                            return val;
                    }
                },
                items: {
                    xtype: 'trigger',
                    triggerCls: 'x-form-clear-trigger',
                    onTriggerClick: triggerFunc,
                    padding: '0 50 0 20',
                    dataIndex: 'txtbranch',
                    enableKeyEvents: true,
                    listeners: {
                        keyup: function () {
                            for (var i = 0; i < filterarray.length; i++) {
                                var index = filterarray[i].dataindex;
                                if (index === this.dataIndex) {
                                    filterarray.splice(i, 1);
                                    break;
                                }
                            }
                            if (this.value > "") {
                                filterarray.push({
                                    dataindex: this.dataIndex,
                                    value: this.value
                                });
                            }
                            filterStore();
                        },
                        buffer: 500
                    }
                }
            },
            {
                text: '<b>Day Of Text</b>',
                dataIndex: 'txtpos',
                flex: 1,
                items: {
                    xtype: 'trigger',
                    triggerCls: 'x-form-clear-trigger',
                    onTriggerClick: triggerFunc,
                    padding: '0 50 0 20',
                    dataIndex: 'txtpos',
                    enableKeyEvents: true,
                    listeners: {
                        keyup: function () {
                            for (var i = 0; i < filterarray.length; i++) {
                                var index = filterarray[i].dataindex;
                                if (index === this.dataIndex) {
                                    filterarray.splice(i, 1);
                                    break;
                                }
                            }
                            if (this.value > "") {
                                filterarray.push({
                                    dataindex: this.dataIndex,
                                    value: this.value
                                });
                            }
                            filterStore();
                        },
                        buffer: 500
                    }
                }
            },
            {
                text: '<b>Sequence</b>',
                dataIndex: 'txtseq',
                flex: 1,
                items: {
                    xtype: 'trigger',
                    triggerCls: 'x-form-clear-trigger',
                    onTriggerClick: triggerFunc,
                    padding: '0 50 0 20',
                    dataIndex: 'txtseq',
                    enableKeyEvents: true,
                    listeners: {
                        keyup: function () {
                            for (var i = 0; i < filterarray.length; i++) {
                                var index = filterarray[i].dataindex;
                                if (index === this.dataIndex) {
                                    filterarray.splice(i, 1);
                                    break;
                                }
                            }
                            if (this.value > "") {
                                filterarray.push({
                                    dataindex: this.dataIndex,
                                    value: this.value
                                });
                            }
                            filterStore();
                        },
                        buffer: 500
                    }
                }
            },
            {
                text: '<b>Time Stamp</b>',
                dataIndex: 'txtmsgdate',
                flex: 1,
                items: {
                    xtype: 'trigger',
                    triggerCls: 'x-form-clear-trigger',
                    onTriggerClick: triggerFunc,
                    padding: '0 50 0 20',
                    dataIndex: 'txtmsgdate',
                    enableKeyEvents: true,
                    listeners: {
                        keyup: function () {
                            for (var i = 0; i < filterarray.length; i++) {
                                var index = filterarray[i].dataindex;
                                if (index === this.dataIndex) {
                                    filterarray.splice(i, 1);
                                    break;
                                }
                            }
                            if (this.value > "") {
                                filterarray.push({
                                    dataindex: this.dataIndex,
                                    value: this.value
                                });
                            }
                            filterStore();
                        },
                        buffer: 500
                    }
                }
            }
        ];
    }
});
