﻿/* global Ext */

Ext.define('YLPMain.view.TxtMsgListUsers', {
    extend: 'Ext.grid.Panel',
    requires: 'Ext.ux.grid.FiltersFeature',
    alias: 'widget.txtmsglistusers',

    initComponent: function () {
        var me = this;

        Ext.apply(me, {
            forceFit: true,
            frame: true,
            autoScroll: true,
            padding: 5,
            stripeRows: true,
            columnLines: true,
            checkboxCounter: 0,
            viewConfig: {
                deferEmptyText: true,
                emptyText: '<b><center>*** No Text Messages Loaded ***</center></b>',
                loadingText: 'Loading Text Messages...'
            },
            selType: 'checkboxmodel',
            selModel: {
                ignoreRightMouseSelection: true,
                checkOnly: true,
                showHeaderCheckbox: true,
                onHeaderClick: function (headerCt, header, e) {
                    if (header.isCheckerHd) {
                        e.stopEvent();
                        var isChecked = header.el.hasCls(Ext.baseCSSPrefix + 'grid-hd-checker-on');
                        var checkboxtext = Ext.ComponentQuery.query('#txtmsglistusers-checkboxcount')[0];
                        if (isChecked) {
                            this.deselectAll(true);
                            me.checkboxCounter = 0;
                            checkboxtext.hide();
                        } else {
                            this.selectAll(true);
                            var checkedrows = me.getSelectionModel().getSelection();
                            var checkcount = checkedrows.length;
                            me.checkboxCounter = checkcount;
                            checkboxtext.setText('Selected: ' + checkcount);
                            checkboxtext.show();
                        }
                    }
                },
                listeners: {
                    select: function (model, record, index, opts) {
                        me.checkboxCounter += 1;
                        var checkboxtext = Ext.ComponentQuery.query('#txtmsglistusers-checkboxcount')[0];
                        checkboxtext.setText('Selected: ' + me.checkboxCounter);
                        checkboxtext.show();
                    },
                    deselect: function (model, record, index, opts) {
                        me.checkboxCounter -= 1;
                        var checkboxtext = Ext.ComponentQuery.query('#txtmsglistusers-checkboxcount')[0];
                        if (me.checkboxCounter === 0) {
                            checkboxtext.hide();
                        } else {
                            checkboxtext.setText('Selected: ' + me.checkboxCounter);
                            checkboxtext.show();
                        }
                    }
                }
            },
            columns: me.buildColumns(),
            tbar: me.buildTbar()
        });
        me.callParent(arguments);
    },

    buildColumns: function () {
        var me = this;
        var filterarray = [];
        //function to fire the trigger click and clear the filter field
        var triggerFunc = function () {
            this.setValue('');
            var dataindex = this.ownerCt.dataIndex;
            for (var i = 0; i < filterarray.length; i++) {
                var index = filterarray[i].dataindex;
                if (index === dataindex) {
                    filterarray.splice(i, 1);
                    break;
                }
            }
            filterStore();
        };
        //function to filter the store
        var filterStore = function (dataIndex, value) {
            var txtstore = me.getStore();
            txtstore.clearFilter();
            for (var i = 0; i < filterarray.length; i++) {
                txtstore.filter({
                    property: filterarray[i].dataindex,
                    value: filterarray[i].value,
                    anyMatch: false,
                    caseSensitive: false
                });
            }
        };
        return [
            {
                text: '<b>User Name</b>',
                dataIndex: 'txtuser',
                flex: 1,
                items: {
                    xtype: 'trigger',
                    triggerCls: 'x-form-clear-trigger',
                    onTriggerClick: triggerFunc,
                    flex: 1,
                    margin: 2,
                    dataIndex: 'txtuser',
                    enableKeyEvents: true,
                    listeners: {
                        keyup: function () {
                            for (var i = 0; i < filterarray.length; i++) {
                                var index = filterarray[i].dataindex;
                                if (index === this.dataIndex) {
                                    filterarray.splice(i, 1);
                                    break;
                                }
                            }
                            if (this.value > "") {
                                filterarray.push({
                                    dataindex: this.dataIndex,
                                    value: this.value
                                });
                            }
                            filterStore();
                        },
                        buffer: 500
                    }
                }
            },
            {
                text: '<b>Phone Number</b>',
                dataIndex: 'txtadd',
                flex: 1,
                items: {
                    xtype: 'trigger',
                    triggerCls: 'x-form-clear-trigger',
                    onTriggerClick: triggerFunc,
                    flex: 1,
                    margin: 2,
                    dataIndex: 'txtadd',
                    enableKeyEvents: true,
                    listeners: {
                        keyup: function () {
                            for (var i = 0; i < filterarray.length; i++) {
                                var index = filterarray[i].dataindex;
                                if (index === this.dataIndex) {
                                    filterarray.splice(i, 1);
                                    break;
                                }
                            }
                            if (this.value > "") {
                                filterarray.push({
                                    dataindex: this.dataIndex,
                                    value: this.value
                                });
                            }
                            filterStore();
                        },
                        buffer: 500
                    }
                }
            }
        ];
    },
    buildTbar: function () {
        return [
            {
                xtype: 'tbtext',
                text: 'Selected: ',
                itemId: 'txtmsglistusers-checkboxcount',
                hidden: true
            },
            {
                xtype: 'tbfill'
            },
            {
                xtype: 'button',
                text: 'Send Text Messages',
                handler: function () {
                    YLPMain.app.getController("main").onSendTxtMsgTemplate();
                }
            }
        ];
    }
});
