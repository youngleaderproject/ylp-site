﻿/* global Ext */

Ext.define('YLPMain.view.UserList', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.userlist',

    initComponent: function () {
        var me = this;
        function createTooltip(view) {
            view.tip = window.Ext.create("Ext.tip.ToolTip", {
                tpl: new window.Ext.XTemplate(
                    "<b>User Name: </b>{fullname} <br/>",
                    "<br/><b>User Email: </b>{user_email} <br/>",
                    "<br/><b>User Phone: </b>{user_phone} <br/>",
                    '<tpl if="user_track == \'1\'">',
                    "<b><br/>Track: </b>Confidence<br/>",
                    '</tpl>',
                    '<tpl if="user_track == \'2\'">',
                    "<b><br/>Track: </b>Stress<br/>",
                    '</tpl>',
                    '<tpl if="user_track == \'3\'">',
                    "<b><br/>Track: </b>Social Intelligence<br/>",
                    '</tpl>',
                    '<tpl if="user_track == \'4\'">',
                    "<b><br/>Track: </b>Relationships<br/>",
                    '</tpl>',
                    '<tpl if="user_track == \'5\'">',
                    "<b><br/>Track: </b>Self-Control<br/>",
                    '</tpl>',
                    '<tpl if="user_track == \'6\'">',
                    "<b><br/>Track: </b>Thoughtful Decisions<br/>",
                    '</tpl>',
                    '<tpl if="user_tracktime == \'1\'">',
                    "<b><br/>Track Time: </b>8:00 A.M.<br/>",
                    '</tpl>',
                    '<tpl if="user_tracktime == \'2\'">',
                    "<b><br/>Track Time: </b>3:00 P.M.<br/>",
                    '</tpl>',
                    '<tpl if="user_tracktime == \'3\'">',
                    "<b><br/>Track Time: </b>6:00 P.M.<br/>",
                    '</tpl>',
                    '<tpl if="user_tracktime == \'4\'">',
                    "<b><br/>Track Time: </b>8:00 P.M.<br/>",
                    '</tpl>',
                    '<tpl if="user_status == \'0\'">',
                    "<b><br/>User Status: </b>Not Subscribed<br/>",
                    '</tpl>',
                    '<tpl if="user_status == \'1\'">',
                    "<b><br/>User Status: </b>Payed Subscription<br/>",
                    '</tpl>',
                    '<tpl if="user_status == \'2\'">',
                    "<b><br/>User Status: </b>Free Subscription<br/>",
                    '</tpl>',
                    '<tpl if="user_status == \'3\'">',
                    "<b><br/>User Status: </b>Temporary Subscription<br/>",
                    '</tpl>',
                    '<tpl if="user_status == \'999\'">',
                    "<b><br/>User Status: </b>Un-Subscribed<br/>",
                    '</tpl>',
                    '<tpl if="user_status == \'901\'">',
                    "<b><br/>User Status: </b>Canceled Text<br/>",
                    '</tpl>',
                    
                ),
                target: view.el,
                trackMouse: true,
                dismissDelay: 0,
                delegate: ".usrbodycls",
                renderTo: window.Ext.getBody(),
                listeners: {
                    show: function (tip) {
                        var task = new window.Ext.util.DelayedTask(function () {
                            var id = tip.anchorTarget.parentElement.dataset.recordindex;
                            var record = view.store.getAt(id);
                            if (record) {
                                tip.update(record.data);
                            } else {
                                tip.hide();
                            }
                        });
                        task.delay(10);
                    }
                }
            });
        }
        Ext.apply(me, {
            store: 'UserList',
            forceFit: true,
            frame: true,
            autoScroll: true,
            padding: 5,
            stripeRows: true,
            columnLines: true,
            layout: 'fit',
            viewConfig: {
                deferEmptyText: true,
                emptyText: '<b><center>*** No Users Loaded ***</center></b>',
                loadingText: 'Loading Users...',
                listeners: {
                    render: createTooltip
                }
            },
            tbar: me.buildTbar(),
            columns: {
                defaults: {
                    renderer: function (value, metaData, record) {
                        if (parseInt(record.get('user_status')) > 900) {
                            metaData.tdAttr = 'style="background-color:rgb(255,0,0,0.5);"';
                        } else if (record.get('unsubscribe_date').substr(0, 1) !== '0' && record.get('unsubscribe_date') > '') {
                            metaData.tdAttr = 'style="background-color:rgb(255,106,26,0.5);"';
                        } else if (parseInt(record.get('user_status')) > 0 && parseInt(record.get('user_status')) < 900) {
                            metaData.tdAttr = 'style="background-color:rgb(163, 212, 154,0.75);"';
                        }
                        return value;
                    }
                },
                items: me.buildColumns(),
            },
            menu: me.buildMenu(),
            listeners:  {
                itemclick: function (grid, record, item, index) {
                    var main = YLPMain.app.getController("main");
                    //main.onShowEmailTemplate(record);
                    //main.getViewport().down("#east-region").collapse();
                },
                itemcontextmenu: function (view, record, el, index, event) {
                    event.stopEvent();
                    if (me.getSelectionModel().getSelection().length > 0) {
                     //   Ext.getCmp('textmsglistsend').show();
                     //   Ext.getCmp('textmsglistedit').hide();
                    } else {
                     //   Ext.getCmp('textmsglistsend').hide();
                     //   Ext.getCmp('textmsglistedit').show();
                    }
                    me.currentRecord = record;
                    me.menu.showAt(event.getXY());
                }
            }
        });
        me.callParent(arguments);
    },


    buildMenu: function () {
        var me = this;
        return Ext.create('Ext.menu.Menu', {
            items: [
                {
                    text: 'Edit User',
                    id: 'userlistedit',
                    icon: 'resources/images/edit.png',
                    handler: function () {
                        me.updateuserwindow = Ext.create('Ext.window.Window', {
                            title: 'YLP Edit User',
                            layout: 'fit',
                            closeAction: 'destroy',
                            constrain: true,
                            modal: true,
                            id: 'updateuserwindow',
                            items: [{
                                xtype: 'form',
                                id: 'updateuserform',
                                bbar: [
                                    {
                                        xtype: 'tbfill'
                                    },
                                    {
                                        xtype: 'button',
                                        text: 'Submit',
                                        formBind: true,
                                        handler: function (btn) {
                                            Ext.MessageBox.show({
                                                title: 'YLP Admin',
                                                msg: 'Updating User..',
                                                width: 300,
                                                wait: true,
                                                waitConfig: {
                                                    interval: 200
                                                },
                                                progress: true,
                                                closable: false,
                                                modal: true
                                            });
                                            var theUrl = window.YLPMain.config.Settings.Url;
                                            var parms = Ext.getCmp('updateuserform').getForm().getValues();
                                            Ext.apply(parms, {
                                                rqstyp: 'ylpusrmaint',
                                                type: 'upduser',
                                                dev: window.YLPMain.config.Settings.YLP.device
                                            });
                                            Ext.Ajax.request({
                                                url: theUrl,
                                                params: parms,
                                                scope: me,

                                                success: function (response) {
                                                    Ext.MessageBox.hide();
                                                    me.updateuserwindow.destroy();
                                                    Ext.getStore('UserList').load();
                                                },
                                                failure: function () {
                                                    Ext.Msg.alert('Error', 'Unable to connect to server');
                                                }
                                            });
                                        }
                                    }
                                ],
                                items: [
                                    {
                                        xtype: 'textfield',
                                        id: 'useremailupd',
                                        name: 'user_email',
                                        padding: 10,
                                        fieldLabel: 'Email',
                                        allowBlank: false,
                                        anchor: '95%'
                                    },
                                    {
                                        xtype: 'textfield',
                                        name: 'user_phone',
                                        vtype: 'phone',
                                        padding: 10,
                                        minLength: 10,
                                        fieldLabel: 'Phone',
                                        allowBlank: false
                                    },
                                    {
                                        xtype: 'combobox',
                                        id: 'usertrackupd',
                                        name: 'user_track',
                                        store: Ext.create('Ext.data.Store', {
                                            fields: ['val', 'desc'],
                                            data: [
                                                {
                                                    "val": "0", "desc": "Not Subscribed(0)"
                                                },
                                                {
                                                    "val": "1", "desc": "Confidence(1)"
                                                },
                                                {
                                                    "val": "2", "desc": "Stress(2)"
                                                },
                                                {
                                                    "val": "3", "desc": "Social Intelligence(3)"
                                                },
                                                {
                                                    "val": "4", "desc": "Relationships(4)"
                                                },
                                                {
                                                    "val": "5", "desc": "Self-Control(5)"
                                                },
                                                {
                                                    "val": "6", "desc": "Thoughtful Decisions(6)"
                                                }
                                            ]
                                        }),
                                        padding: 10,
                                        queryMode: 'local',
                                        displayField: 'desc',
                                        valueField: 'val',
                                        fieldLabel: 'Track',
                                        allowBlank: false,
                                        editable: false,
                                        anchor: '95%'
                                    },
                                    {
                                        xtype: 'combobox',
                                        id: 'usertracktimeupd',
                                        name: 'user_tracktime',
                                        store: Ext.create('Ext.data.Store', {
                                            fields: ["val", "desc"],
                                            data: [
                                                {
                                                    "val": "0", "desc": "Not Subscribed(0)"
                                                },
                                                {
                                                    "val": "1", "desc": "8:00 A.M.(1)"
                                                },
                                                {
                                                    "val": "2", "desc": "3:00 P.M.(2)"
                                                },
                                                {
                                                    "val": "3", "desc": "6:00 P.M.(3)"
                                                },
                                                {
                                                    "val": "4", "desc": "8:00 P.M.(4)"
                                                }
                                            ]
                                        }),
                                        padding: 10,
                                        valueField: "val",
                                        displayField: 'desc',
                                        fieldLabel: 'Track Time',
                                        allowBlank: false,
                                        editable: false,
                                        anchor: '95%'
                                    },
                                    {
                                        xtype: 'combobox',
                                        id: 'userstatusupd',
                                        name: 'user_status',
                                        store: Ext.create('Ext.data.Store', {
                                            fields: ["val", "desc"],
                                            data: [
                                                {
                                                    "val": "0", "desc": "Not Subscribed(0)"
                                                },
                                                {
                                                    "val": "1", "desc": "Payed Subscription(1)"
                                                },
                                                {
                                                    "val": "2", "desc": "Free Subscription(2)"
                                                },
                                                {
                                                    "val": "3", "desc": "Free Subscription(3)"
                                                }
                                            ]
                                        }),
                                        padding: 10,
                                        valueField: "val",
                                        displayField: 'desc',
                                        fieldLabel: 'Status',
                                        allowBlank: false,
                                        editable: false,
                                        anchor: '95%'
                                    },
                                    {
                                        xtype: 'hiddenfield',
                                        name: 'user_id'
                                    }
                                ]
                            }]
                        }).show();
                        Ext.getCmp('updateuserform').loadRecord(me.currentRecord);
                    }
                },
                {
                    text: 'Delete User',
                    id: 'userlistdelete',
                    icon: 'resources/images/delete.png',
                    handler: function () {
                        Ext.Msg.show({
                            title: 'User Maintenance',
                            msg: 'Are you sure you want to delete this user?',
                            buttons: Ext.Msg.YESNO,
                            icon: Ext.Msg.QUESTION,
                            fn: function (btn) {
                                if (btn === 'yes') {
                                    Ext.Msg.hide();
                                    var parms = {};
                                    Ext.apply(parms, {
                                        rqstyp: "ylpusrmaint",
                                        type: "dltuser",
                                        id: me.currentRecord.get("user_id"),
                                        dev: window.YLPMain.config.Settings.YLP.device
                                    });
                                    Ext.Ajax.request({
                                        url: window.YLPMain.config.Settings.Url, //'/pgms/rvimain.pgm',
                                        scope: me,
                                        params: parms,
                                        success: function () {
                                            Ext.getStore("UserList").load();
                                            YLPMain.app.getController("callMsgCurtain").onShowMsg('Delete User', 'Complete');
                                        },
                                        failure: function () {
                                            Ext.Msg.alert('Error', 'Updating Failed');
                                        }
                                    });
                                } else {
                                    Ext.Msg.hide();
                                }
                            }
                        });
                    }
                }
            ]
        });
    },

    buildTbar: function () {
        return [
            {
                xtype: 'tbfill'
            },
          /*  {
                xtype: 'button',
                text: 'Add User',
                handler: function () {
                    me.adduserwindow = Ext.create('Ext.window.Window', {
                        title: 'YLP Add User',
                        layout: 'fit',
                        closeAction: 'destroy',
                        constrain: true,
                        modal: true,
                        id: 'adduserwindow',
                        items: [{
                            xtype: 'form',
                            id: 'adduserform',
                            bbar: [
                                {
                                    xtype: 'tbfill'
                                },
                                {
                                    xtype: 'button',
                                    text: 'Submit',
                                    formBind: true,
                                    handler: function (btn) {
                                        Ext.MessageBox.show({
                                            title: 'YLP Admin',
                                            msg: 'Updating User..',
                                            width: 300,
                                            wait: true,
                                            waitConfig: {
                                                interval: 200
                                            },
                                            progress: true,
                                            closable: false,
                                            modal: true
                                        });
                                        var theUrl = window.YLPMain.config.Settings.Url;
                                        var parms = Ext.getCmp('adduserform').getForm().getValues();
                                        Ext.apply(parms, {
                                            rqstyp: 'ylpusrmaint',
                                            type: 'adduser',
                                            dev: window.YLPMain.config.Settings.YLP.device
                                        });
                                        Ext.Ajax.request({
                                            url: theUrl,
                                            params: parms,
                                            scope: me,

                                            success: function (response) {
                                                Ext.MessageBox.hide();
                                                me.updateuserwindow.destroy();
                                                Ext.getStore('UserList').load();
                                            },
                                            failure: function () {
                                                Ext.Msg.alert('Error', 'Unable to connect to server');
                                            }
                                        });
                                    }
                                }
                            ],
                            items: [
                                {
                                    xtype: 'textfield',
                                    name: 'user_email',
                                    padding: 10,
                                    fieldLabel: 'Email',
                                    allowBlank: false,
                                    anchor: '95%'
                                },
                                {
                                    xtype: 'textfield',
                                    name: 'user_phone',
                                    vtype: 'phone',
                                    padding: 10,
                                    minLength: 10,
                                    fieldLabel: 'Phone',
                                    allowBlank: false
                                },
                                {
                                    xtype: 'combobox',
                                    name: 'user_track',
                                    store: Ext.create('Ext.data.Store', {
                                        fields: ['val', 'desc'],
                                        data: [
                                            {
                                                "val": "1", "desc": "Confidence(1)"
                                            },
                                            {
                                                "val": "2", "desc": "Stress(2)"
                                            },
                                            {
                                                "val": "3", "desc": "Social Intelligence(3)"
                                            },
                                            {
                                                "val": "4", "desc": "Relationships(4)"
                                            },
                                            {
                                                "val": "5", "desc": "Self-Control(5)"
                                            },
                                            {
                                                "val": "6", "desc": "Thoughtful Decisions(6)"
                                            }
                                        ]
                                    }),
                                    padding: 10,
                                    queryMode: 'local',
                                    displayField: 'desc',
                                    valueField: 'val',
                                    fieldLabel: 'Track',
                                    allowBlank: false,
                                    editable: false,
                                    anchor: '95%'
                                },
                                {
                                    xtype: 'combobox',
                                    name: 'user_tracktime',
                                    store: Ext.create('Ext.data.Store', {
                                        fields: ["val", "desc"],
                                        data: [
                                            {
                                                "val": "1", "desc": "8:00 A.M.(1)"
                                            },
                                            {
                                                "val": "2", "desc": "3:00 P.M.(2)"
                                            },
                                            {
                                                "val": "3", "desc": "6:00 P.M.(3)"
                                            },
                                            {
                                                "val": "4", "desc": "8:00 P.M.(4)"
                                            }
                                        ]
                                    }),
                                    padding: 10,
                                    valueField: "val",
                                    displayField: 'desc',
                                    fieldLabel: 'Track Time',
                                    allowBlank: false,
                                    editable: false,
                                    anchor: '95%'
                                },
                                {
                                    xtype: 'combobox',
                                    id: 'userstatusupd',
                                    name: 'user_status',
                                    store: Ext.create('Ext.data.Store', {
                                        fields: ["val", "desc"],
                                        data: [
                                            {
                                                "val": "2", "desc": "Free Subscription"
                                            }
                                        ]
                                    }),
                                    padding: 10,
                                    valueField: "val",
                                    displayField: 'desc',
                                    fieldLabel: 'Status',
                                    allowBlank: false,
                                    editable: false,
                                    anchor: '95%'
                                }
                            ]
                        }]
                    }).show();
                }
            },*/
            {
                xtype: 'exporterbutton',
                hidden: true,
                id: 'userlistexportersubmit',
                format: 'excel'
            },
            {
                text: 'Grid To Excel',
                id: 'userlistexporter',
                icon: 'resources/images/excel-file_page2.png',
                handler: function () {
                    document.getElementById("userlistexportersubmit").click();
                }
            },
            {
                xtype: 'button',
                text: 'Close',
                handler: function () {
                    var main = YLPMain.app.getController("main");
                    main.onHideCenter();
                    main.onHideEast();
                    main.getViewport().down("#east-region").collapse();
                    main.onShowMenu();
                }
            }
        ];
    },

    buildColumns: function () {
        var email = Ext.getStore('UserList');
        var filterarray = [];
        //function to fire the trigger click and clear the filter field
        var triggerFunc = function () {
            this.setValue('');
            var dataindex = this.ownerCt.dataIndex;
            for (var i = 0; i < filterarray.length; i++) {
                var index = filterarray[i].dataindex;
                if (index === dataindex) {
                    filterarray.splice(i, 1);
                    break;
                }
            }
            filterStore();
        };
        //function to filter the store
        var filterStore = function (dataIndex, value) {
            email.clearFilter();
            for (var i = 0; i < filterarray.length; i++) {
                email.filter({
                    property: filterarray[i].dataindex,
                    value: filterarray[i].value,
                    anyMatch: false,
                    caseSensitive: false
                });
            }
        };
        return [
            {
                text: '<b>Name</b>',
                dataIndex: 'fullname',
                tdCls: "usrbodycls",
                flex: 0.75,
                items: {
                    xtype: 'trigger',
                    triggerCls: 'x-form-clear-trigger',
                    onTriggerClick: triggerFunc,
                    flex: 1,
                    margin: 2,
                    dataIndex: 'fullname',
                    enableKeyEvents: true,
                    listeners: {
                        keyup: function () {
                            for (var i = 0; i < filterarray.length; i++) {
                                var index = filterarray[i].dataindex;
                                if (index === this.dataIndex) {
                                    filterarray.splice(i, 1);
                                    break;
                                }
                            }
                            if (this.value > "") {
                                filterarray.push({
                                    dataindex: this.dataIndex,
                                    value: this.value
                                });
                            }
                            filterStore();
                        },
                        buffer: 500
                    }
                }
            },
            {
                text: '<b>Email</b>',
                dataIndex: 'user_email',
                tdCls: "usrbodycls",
                flex: 1,
                items: {
                    xtype: 'trigger',
                    triggerCls: 'x-form-clear-trigger',
                    onTriggerClick: triggerFunc,
                    flex: 1,
                    margin: 2,
                    dataIndex: 'user_email',
                    enableKeyEvents: true,
                    listeners: {
                        keyup: function () {
                            for (var i = 0; i < filterarray.length; i++) {
                                var index = filterarray[i].dataindex;
                                if (index === this.dataIndex) {
                                    filterarray.splice(i, 1);
                                    break;
                                }
                            }
                            if (this.value > "") {
                                filterarray.push({
                                    dataindex: this.dataIndex,
                                    value: this.value
                                });
                            }
                            filterStore();
                        },
                        buffer: 500
                    }
                }
            },
            {
                text: '<b>Phone</b>',
                dataIndex: 'user_phone',
                tdCls: "usrbodycls",
                flex: 0.5,
                items: {
                    xtype: 'trigger',
                    triggerCls: 'x-form-clear-trigger',
                    onTriggerClick: triggerFunc,
                    flex: 1,
                    margin: 2,
                    dataIndex: 'user_phone',
                    enableKeyEvents: true,
                    listeners: {
                        keyup: function () {
                            for (var i = 0; i < filterarray.length; i++) {
                                var index = filterarray[i].dataindex;
                                if (index === this.dataIndex) {
                                    filterarray.splice(i, 1);
                                    break;
                                }
                            }
                            if (this.value > "") {
                                filterarray.push({
                                    dataindex: this.dataIndex,
                                    value: this.value
                                });
                            }
                            filterStore();
                        },
                        buffer: 500
                    }
                }
            },
            {
                text: '<b>Track</b>',
                dataIndex: 'user_track',
                tdCls: "usrbodycls",
                flex: 0.75,
                items: {
                    xtype: 'trigger',
                    triggerCls: 'x-form-clear-trigger',
                    onTriggerClick: triggerFunc,
                    flex: 1,
                    margin: 2,
                    dataIndex: 'user_track',
                    enableKeyEvents: true,
                    listeners: {
                        keyup: function () {
                            for (var i = 0; i < filterarray.length; i++) {
                                var index = filterarray[i].dataindex;
                                if (index === this.dataIndex) {
                                    filterarray.splice(i, 1);
                                    break;
                                }
                            }
                            if (this.value > "") {
                                filterarray.push({
                                    dataindex: this.dataIndex,
                                    value: this.value
                                });
                            }
                            filterStore();
                        },
                        buffer: 500
                    }
                },
                renderer: function (value, metaData, record) {
                    if (parseInt(record.get('user_status')) > 900) {
                        metaData.tdAttr = 'style="background-color:rgb(255,0,0,0.5);"';
                    } else if (record.get('unsubscribe_date').substr(0, 1) !== '0' && record.get('unsubscribe_date') > '') {
                        metaData.tdAttr = 'style="background-color:rgb(255,106,26,0.5);"';
                    } else if (parseInt(record.get('user_status')) > 0 && parseInt(record.get('user_status')) < 900) {
                        metaData.tdAttr = 'style="background-color:rgb(163, 212, 154,0.75);"';
                    }
                    switch (value) {
                        case "0":
                            return 'Not Subscribed(0)';
                        case "1":
                            return 'Confidence(1)';
                        case "2":
                            return 'Stress(2)';
                        case "3":
                            return 'Social Intelligence(3)';
                        case "4":
                            return 'Relationships(4)';
                        case "5":
                            return 'Thoughtful Decisions(5)';
                        case "6":
                            return 'Self-Control(6)';
                        default:
                            return '';
                    }
                }
            },
            {
                text: '<b>Track Time</b>',
                dataIndex: 'user_tracktime',
                tdCls: "usrbodycls",
                flex: 0.5,
                items: {
                    xtype: 'trigger',
                    triggerCls: 'x-form-clear-trigger',
                    onTriggerClick: triggerFunc,
                    flex: 1,
                    margin: 2,
                    dataIndex: 'user_tracktime',
                    enableKeyEvents: true,
                    listeners: {
                        keyup: function () {
                            for (var i = 0; i < filterarray.length; i++) {
                                var index = filterarray[i].dataindex;
                                if (index === this.dataIndex) {
                                    filterarray.splice(i, 1);
                                    break;
                                }
                            }
                            if (this.value > "") {
                                filterarray.push({
                                    dataindex: this.dataIndex,
                                    value: this.value
                                });
                            }
                            filterStore();
                        },
                        buffer: 500
                    }
                },
                renderer: function (value, metaData, record) {
                    if (parseInt(record.get('user_status')) > 900) {
                        metaData.tdAttr = 'style="background-color:rgb(255,0,0,0.5);"';
                    } else if (record.get('unsubscribe_date').substr(0, 1) !== '0' && record.get('unsubscribe_date') > '') {
                        metaData.tdAttr = 'style="background-color:rgb(255,106,26,0.5);"';
                    } else if (parseInt(record.get('user_status')) > 0 && parseInt(record.get('user_status')) < 900) {
                        metaData.tdAttr = 'style="background-color:rgb(163, 212, 154,0.75);"';
                    }
                    switch (value) {
                        case "0":
                            return 'Not Subscribed(0)';
                        case "1":
                            return '8:00 A.M.(1)';
                        case "2":
                            return '3:00 P.M.(2)';
                        case "3":
                            return '6:00 P.M.(3)';
                        case "4":
                            return '8:00 P.M.(4)';
                        default:
                            return '';
                    }
                }
            },
            {
                text: '<b>Status</b>',
                dataIndex: 'user_status',
                tdCls: "usrbodycls",
                flex: 0.75,
                items: {
                    xtype: 'trigger',
                    triggerCls: 'x-form-clear-trigger',
                    onTriggerClick: triggerFunc,
                    flex: 1,
                    margin: 2,
                    dataIndex: 'user_status',
                    enableKeyEvents: true,
                    listeners: {
                        keyup: function () {
                            for (var i = 0; i < filterarray.length; i++) {
                                var index = filterarray[i].dataindex;
                                if (index === this.dataIndex) {
                                    filterarray.splice(i, 1);
                                    break;
                                }
                            }
                            if (this.value > "") {
                                filterarray.push({
                                    dataindex: this.dataIndex,
                                    value: this.value
                                });
                            }
                            filterStore();
                        },
                        buffer: 500
                    }
                },
                renderer: function (value, metaData, record) {
                    if (parseInt(record.get('user_status')) > 900) {
                        metaData.tdAttr = 'style="background-color:rgb(255,0,0,0.5);"';
                    } else if (record.get('unsubscribe_date').substr(0, 1) !== '0' && record.get('unsubscribe_date') > '') {
                        metaData.tdAttr = 'style="background-color:rgb(255,106,26,0.5);"';
                    } else if (parseInt(record.get('user_status')) > 0 && parseInt(record.get('user_status')) < 900) {
                        metaData.tdAttr = 'style="background-color:rgb(163, 212, 154,0.75);"';
                    }
                    switch (value) {
                        case "0":
                            return 'Not Subscribed(0)';
                        case "1":
                            return 'Payed Subscription(1)';
                        case "2":
                            return 'Free Subscription(2)';
                        case "3":
                            return 'Temp Subscription(3)';
                        case "999":
                            return 'Un-Subscribed(999)';
                        case "901":
                            return 'Canceled Text(999)';
                        default:
                            return '';
                    }
                }
            },
            {
                text: '<b>Date Registered</b>',
                dataIndex: 'user_registered',
                tdCls: "usrbodycls",
                flex: 0.5,
                items: {
                    xtype: 'trigger',
                    triggerCls: 'x-form-clear-trigger',
                    onTriggerClick: triggerFunc,
                    flex: 1,
                    margin: 2,
                    dataIndex: 'user_registered',
                    enableKeyEvents: true,
                    listeners: {
                        keyup: function () {
                            for (var i = 0; i < filterarray.length; i++) {
                                var index = filterarray[i].dataindex;
                                if (index === this.dataIndex) {
                                    filterarray.splice(i, 1);
                                    break;
                                }
                            }
                            if (this.value > "") {
                                filterarray.push({
                                    dataindex: this.dataIndex,
                                    value: this.value
                                });
                            }
                            filterStore();
                        },
                        buffer: 500
                    }
                }
            },
            {
                text: '<b>Activity</b>',
                dataIndex: 'user_activity',
                tdCls: "usrbodycls",
                hidden: true,
                flex: 0.5,
                items: {
                    xtype: 'trigger',
                    triggerCls: 'x-form-clear-trigger',
                    onTriggerClick: triggerFunc,
                    flex: 1,
                    margin: 2,
                    dataIndex: 'user_activity',
                    enableKeyEvents: true,
                    listeners: {
                        keyup: function () {
                            for (var i = 0; i < filterarray.length; i++) {
                                var index = filterarray[i].dataindex;
                                if (index === this.dataIndex) {
                                    filterarray.splice(i, 1);
                                    break;
                                }
                            }
                            if (this.value > "") {
                                filterarray.push({
                                    dataindex: this.dataIndex,
                                    value: this.value
                                });
                            }
                            filterStore();
                        },
                        buffer: 500
                    }
                }
            },
            {
                text: '<b>Country</b>',
                hidden:true,
                dataIndex: 'user_country',
                tdCls: "usrbodycls",
                flex: 0.5,
                items: {
                    xtype: 'trigger',
                    triggerCls: 'x-form-clear-trigger',
                    onTriggerClick: triggerFunc,
                    flex: 1,
                    margin: 2,
                    dataIndex: 'user_country',
                    enableKeyEvents: true,
                    listeners: {
                        keyup: function () {
                            for (var i = 0; i < filterarray.length; i++) {
                                var index = filterarray[i].dataindex;
                                if (index === this.dataIndex) {
                                    filterarray.splice(i, 1);
                                    break;
                                }
                            }
                            if (this.value > "") {
                                filterarray.push({
                                    dataindex: this.dataIndex,
                                    value: this.value
                                });
                            }
                            filterStore();
                        },
                        buffer: 500
                    }
                }
            },
            {
                text: '<b>Sub End-Date</b>',
                dataIndex: 'sub_enddate',
                flex: 0.5,
                items: {
                    xtype: 'trigger',
                    triggerCls: 'x-form-clear-trigger',
                    onTriggerClick: triggerFunc,
                    flex: 1,
                    margin: 2,
                    dataIndex: 'sub_enddate',
                    enableKeyEvents: true,
                    listeners: {
                        keyup: function () {
                            for (var i = 0; i < filterarray.length; i++) {
                                var index = filterarray[i].dataindex;
                                if (index === this.dataIndex) {
                                    filterarray.splice(i, 1);
                                    break;
                                }
                            }
                            if (this.value > "") {
                                filterarray.push({
                                    dataindex: this.dataIndex,
                                    value: this.value
                                });
                            }
                            filterStore();
                        },
                        buffer: 500
                    }
                }
            },
            {
                text: '<b>Unsubscribe Date</b>',
                dataIndex: 'unsubscribe_date',
                flex: 0.5,
                items: {
                    xtype: 'trigger',
                    triggerCls: 'x-form-clear-trigger',
                    onTriggerClick: triggerFunc,
                    flex: 1,
                    margin: 2,
                    dataIndex: 'unsubscribe_date',
                    enableKeyEvents: true,
                    listeners: {
                        keyup: function () {
                            for (var i = 0; i < filterarray.length; i++) {
                                var index = filterarray[i].dataindex;
                                if (index === this.dataIndex) {
                                    filterarray.splice(i, 1);
                                    break;
                                }
                            }
                            if (this.value > "") {
                                filterarray.push({
                                    dataindex: this.dataIndex,
                                    value: this.value
                                });
                            }
                            filterStore();
                        },
                        buffer: 500
                    }
                },
                renderer: function (value, metaData, record) {
                    if (parseInt(record.get('user_status')) > 900) {
                        metaData.tdAttr = 'style="background-color:rgb(255,0,0,0.5);"';
                    } else if (record.get('unsubscribe_date').substr(0, 1) !== '0' && record.get('unsubscribe_date') > '') {
                        metaData.tdAttr = 'style="background-color:rgb(255,106,26,0.5);"';
                    } else if (parseInt(record.get('user_status')) > 0 && parseInt(record.get('user_status')) < 900) {
                        metaData.tdAttr = 'style="background-color:rgb(163, 212, 154,0.75);"';
                    }
                    if (value.substr(0, 1) !== '0') {
                        return value;
                    } else {
                        return "";
                    }
                    
                }
            }
        ];
    }
});
