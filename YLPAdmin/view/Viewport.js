/* global Ext */

Ext.define('YLPMain.view.Viewport', {
    extend: 'Ext.container.Viewport',

    requires: [
        'YLPMain.view.Signon',
        'YLPMain.view.MenuList',
        'YLPMain.view.TextMsgList',
        'YLPMain.view.TextMsgRecList',
        'YLPMain.view.EmailAddDisplay',
        'YLPMain.view.GoogleDisplay',
        'YLPMain.view.EmailListUsers',
        'YLPMain.view.TextMsgDisplay',
        'YLPMain.view.TxtMsgListUsers',
        'YLPMain.view.TextMsgListForm',
        'YLPMain.view.EmailListForm',
        'YLPMain.view.UserList'
    ],

    initComponent: function () {
        var me = this;
        Ext.apply(me, {
            layout: 'card',
            items: me.buidlItems()
        });
        me.callParent(arguments);
    },

    buidlItems: function () {
        return [
            {
                id: 'vp-signon',
                layout: {
                    type: 'hbox',
                    align: 'pack',
                    pack: 'center'
                },
                items: [
                    {
                        xtype: 'signon',
                        bodyStyle: {
                            backgroundImage: 'url(/images/Low-poly-bg.png) !important',
                            backgroundRepeat: 'no-repeat',
                            backgroundSize: '100% 100%'
                        }
                    }
                ]
            },
            {
                id: 'vp-main',
                xtype: 'container',
                layout: 'card',
                activeItem: 0,
                
                items: [
                    {
                        layout: 'border',
                        title: 'Young Leader Project Admin',
                        border: false,
                        frame: false,
                        items: [
                            {
                                region: 'center',
                                itemId: 'center-region',
                                layout: {
                                    type: 'hbox',
                                    align: 'stretch'
                                },
                                items: [
                                    {
                                        xtype: 'emailadddisplay',
                                        title: 'YLP Add Email Template',
                                        flex: 1,
                                        hidden: true
                                    },
                                    {
                                        xtype: 'googledisplay',
                                        title: 'YLP Google Analytics Events',
                                        flex: 1,
                                        hidden: true
                                    },
                                    {
                                        xtype: 'textmsglistform',
                                        title: 'YLP Text Messages',
                                        flex: 1,
                                        hidden: true
                                    },
                                    {
                                        xtype: 'textmsgreclist',
                                        title: 'YLP Text Message Records',
                                        flex: 1,
                                        hidden: true
                                    },
                                    {
                                        xtype: 'emaillistform',
                                        title: 'YLP Emails',
                                        flex: 1,
                                        hidden: true
                                    },
                                    {
                                        xtype: 'userlist',
                                        title: 'HeyKiddo Users',
                                        flex: 1,
                                        hidden: true
                                    }
                                ]
                            },
                            {
                                region: 'west',
                                itemId: 'west-region',
                                collapsible: true,
                                autoScroll: true,
                                titleCollapse: true,
                                collapsed: false,
                                width: '25%',
                                layout: {
                                    type: 'vbox',
                                    align: 'stretch'
                                },
                                items: [
                                    {
                                        xtype: 'menulist',
                                        title: 'YLP Menu',
                                        height: 600
                                    }
                                ]
                            },
                            {
                                region: 'east',
                                itemId: 'east-region',
                                collapsible: true,
                                autoScroll: true,
                                titleCollapse: true,
                                collapsed: true,
                                width: '30%',
                                layout: {
                                    type: 'vbox',
                                    align: 'stretch'
                                },
                                items: [
                                    {
                                        xtype: 'emaillistusers',
                                        title: 'YLP User Emails',
                                        height: 800,
                                        hidden: true
                                    },
                                    {
                                        xtype: 'txtmsglistusers',
                                        title: 'YLP User Phone Numbers',
                                        height: 800,
                                        hidden: true
                                    }
                                ]
                            },
                            {
                                region: 'south',
                                itemId: 'south-region',
                                collapsible: true,
                                autoScroll: true,
                                titleCollapse: true,
                                collapsed: true,
                                width: '25%',
                                layout: {
                                    type: 'hbox',
                                    align: 'stretch'
                                },
                                items: [

                                ]
                            }
                        ]
                    }
                ]
            }
        ];
    }
});
