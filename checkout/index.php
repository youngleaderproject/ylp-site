<?php
if(!isset($_SESSION))
{
    session_start();
}
require_once str_replace('\\', '/', $_SERVER["DOCUMENT_ROOT"]) . '/PHP/shared/auto_load.php';
// Check if the user is logged in, if not then redirect him to login page
$rqstyp = (isset($_REQUEST['rqstyp'])) ? $_REQUEST['rqstyp'] : '';
if(!isset($_SESSION["user"]["loggedin"]) || $_SESSION["user"]["loggedin"] !== true){
   // Redirect("../login/");
    //exit;
}

if($_SERVER["REQUEST_METHOD"] == "POST"){
    $paymenttype = $_POST['paymenttype'];
    if(strlen($paymenttype) > 0){
        require str_replace('\\', '/', $_SERVER["DOCUMENT_ROOT"]) . '/PHP/commands/ylpsubscription.php';
        $charged = $charge;
        if(is_null($charged)){
            $charged = new stdClass();
            $charged->status = 'failed';
        }
    }else{
        $charged = new stdClass();
        $charged->status = 'failed';
    }
}
if(isset($charged) && $charge->status === "succeeded"){
    Redirect("../paid/");
}else if(isset($charged)){
    Redirect("../PHP/html/404.html");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>YLP | Landing Page</title>
<meta name="description" content="
The Young Leader Project develops the leaders of tomorrow by acquainting them with essential leadership skills today.">
<!-- Bootstrap -->
<link href="../css/bootstrap.css" rel="stylesheet">
<link rel="stylesheet" href="../css/landingscripts.css">
<script src="https://www.paypal.com/sdk/js?client-id=AemoZTCghPRClgHoE_fSAJSAVTmPEReSPIXkODaocVqrvxx08U0iZDPQWdMILGIN85oj6HKV9GUg-dTv&vault=true"></script>
</head>
<body class="particles_bg ">
    <div id="particles-js"></div>
    <nav class="navbar navbar-expand-lg navbar-light bg-light navextend">
        <a class="navbar-brand" href="/">
            <img src="../images/YLP Logo_Tall_Black@4x.png" alt="" width="150" class="img-fluid ylpicon" />
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto nav-padder">
                <?php if (!(isset( $_SESSION[ "user" ][ "loggedin" ] ) && $_SESSION[ "user" ][ "loggedin" ] === true)) { ?>
                <li class="nav-item active nav-padder">
                    <a class="nav-link" href="/login/">Log In</a>
                </li>
                <?php
              }else{
                ?>
                <li class="nav-item active nav-padder">
                    <a class="nav-link" href="../PHP/commands/ylplogout.php">Log Out</a>
                </li>
                <?php
              }
              if(isset($_SESSION['YLP']['ACTIVE']) && $_SESSION['YLP']['ACTIVE'] === 'Y'){
                ?>
                <li class="nav-item active nav-padder">
                    <a style="color: white" class="subscribe-now nav-link " href="/dashboard/">Misson Control</a>
                </li>
                <?php
              }else{
                ?>
                <li class="nav-item active nav-padder">
                    <a style="color: white" class="subscribe-now nav-link " href="/subscription/">Subscribe Now</a>
                </li>
                <?php } ?>

                <!--<li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Dropdown </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown"> <a class="dropdown-item" href="#">Action</a> <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a> </div>
      </li>-->
            </ul>
        </div>
    </nav>
    <div class="main-wrapper">
        <div class="paypal-buttont" id="paypal-plan-container"></div>
        
            <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                <input type="hidden" name="cmd" value="_s-xclick" />
                <input type="hidden" name="hosted_button_id" value="5AS3W4TFAV3FG" />
                <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_subscribeCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!" />
                <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1" />
            </form>

    </div>
            <br />

            <footer class="text-center footer-container container-fluid">
                <div class="row footer-row">
                    <div class="col-12 col-xl-4">
                        <p>© Young Leader Project, LLC. All Rights Reserved.</p>
                    </div>
                    <div class="col-12 offset-3 footer-adjust offset-xl-3 col-xl-5 align-content-center">
                        <nav>
                            <ul>
                                <li class="li-inline">
                                    <a href="#">About</a>
                                </li>
                                <li class="li-inline">
                                    <a href="#">FAQ</a>
                                </li>
                                <li class="li-inline">
                                    <a href="#">Services</a>
                                </li>
                                <li class="li-inline">
                                    <a href="#">Blog</a>
                                </li>
                                <li class="li-inline">
                                    <a href="../contact">Contact</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </footer>
    <script>
        console.log(document.getElementById("paypal-plan-container"));
        paypal.Buttons({
             createSubscription: function(data, actions) {
               return actions.subscription.create({
                 'plan_id': 'P-0HU12709T42054404LWX33AQ'
               });
             },
             onApprove: function(data, actions) {
               alert(data.subscriptionID);
             }
        }).render('#paypal-plan-container');
        setTimeout(function () {
            document.getElementById("paypal-plan-container").click();
        }, 1000);
    </script>
            <script src="./js/Stripe.js"></script>
            <script src="../js/popper.min.js"></script>
            <script src="../js/bootstrap-4.2.1.js"></script>
            <script src="../js/particles.js"></script>
</body>
</html>
