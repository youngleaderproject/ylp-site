<?php
if(!isset($_SESSION)) 
{ 
    session_start(); 
}
use PHP\CLASSES\CKIDS;
use PHP\CLASSES\CMEDIA;
use PHP\CLASSES\CPROGRESS;
use PHP\CLASSES\CCIPHER;


require_once str_replace('\\', '/', $_SERVER["DOCUMENT_ROOT"]) . '/PHP/shared/auto_load.php';
if(!isset($_SESSION["user"]["loggedin"]) || $_SESSION["user"]["loggedin"] !== true){
   // Redirect("../");
   // exit;
}
$kidsclass = new CKIDS();
$picclass = new CMEDIA();
$progressclass = new CPROGRESS();
$cipher = new CCIPHER();
$userid = (isset($_SESSION["user"]["usrid"])) ? $_SESSION["user"]["usrid"] : '';
$kidsinfo = $kidsclass->GET_BY_USERID($userid, 1);
$picinfo = $picclass->GetImage($userid, 1);
$accesscode = $cipher->GetCipher($userid);
$kidid = (isset($kidsinfo['kid_id'])) ? $kidsinfo['kid_id'] : '1';
$ylpemail = (isset($_SESSION["user"]["ylpemail"])) ? $_SESSION["user"]["ylpemail"] : 'noreply@gmail.com';
$ylpfirstname = (isset($_SESSION["user"]["firstname"])) ? $_SESSION["user"]["firstname"] : 'Lauren';
$ylplastname = (isset($_SESSION["user"]["lastname"])) ? $_SESSION["user"]["lastname"] : 'Bennett';
$subtype = (isset($_SESSION['user']['subtype'])) ? $_SESSION['user']['subtype'] : '1';
$ylpname = (isset($kidsinfo['FirstName'])) ? $kidsinfo['FirstName'] : 'Zoey';
$userage = (isset($kidsinfo['Age'])) ? $kidsinfo['Age'] : '8';
$inboxcount = '';
$sentcount = '';
$draftcount = '';
$picpath = (isset($picinfo['pic_path'])) ? $picinfo['pic_path'] : '../images/Avatar-pic-empty.png';
$missionControlperc = $progressclass->MISSION_CONTROL_PROGRESS($userid,$kidid);
$planetSelfieperc = $progressclass->PLANET_SELFIE_PROGRESS($userid,$kidid);
$planetOhmperc = $progressclass->PLANET_OHM_PROGRESS($userid,$kidid);
$planetNeuroperc = $progressclass->PLANET_NEURO_PROGRESS($userid,$kidid);
$subtypedesc = '';
if($subtype === '1'){
    $subtypedesc = "Monthly";
}else if($subtype === '5'){
    $subtypedesc = 'Donated';
}
?>
<!DOCTYPE html>
<html>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>Young Leader Project | Dashboard</title>
<meta name="description" content="
The Young Leader Project develops the leaders of tomorrow by acquainting them with essential leadership skills today." />
<!-- <link href="../css/bootstrap.css" rel="stylesheet" /> -->
<link href="../css/bootstrap.css" rel="stylesheet">
<link rel="stylesheet" href="../css/landingscripts.css" />

<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />
    <link rel="icon" href="/images/favicon.ico" type="favicon/ico" />
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Open+Sans' />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="css/styles.css" />
<script type="text/javascript" src="https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>

<body>
	<div class="landing-wrapper dashboard-wrapper">
	<nav class="dashboard-nav navbar navbar-expand-lg navbar-light">
		 <a class="dashboard-icon" href="../"><img src="../images/Back-Icon-White.png" alt="" width="30" height="30" class="img-fluid"/></a>
		<div class="navbar-center text-center">Mission Control<div>
	</nav>

    <!-- Page Container -->
    <div id="maincontainer" class="w3-container dashcontainer" >
        <!-- The Grid -->
        <div class="dashwrapper">
        <div class="w3-row">
            <!-- Left Column -->
            <div class="w3-col m3">
                <!-- Profile -->
                <div class="w3-card w3-round w3-white w3-margin-bottom">
                    <div id="maincontainer2" class="w3-container zprofiler dont-break-out">
                        <div class="topright"><img src="../images/Edit-Icon.png" alt="" width="30" class=""/></div>
						<br>
						<br>
                        
                        <p class="w3-center">
                            <img src="<?php echo $picpath ?>" class="w3-circle" style="height:106px;width:106px" alt="Avatar" />
                        </p>
						<h3 class="w3-center dashboard-messages"><?php echo $ylpname . ", " . $userage ?></h3>
						<h6 style="color: #6B459C;margin-top: -20px;" class="w3-center"><b>Access Code: <?php echo $accesscode ?></b></h6>
                        <hr />
                        
                        <p>
                            <b>Email:</b> <?php echo $ylpemail ?>
                        </p>
                        <p>
                            <b>Name:</b> <?php echo $ylpfirstname . " " . $ylplastname ?>
                        </p>
                        <p>
                            <b>Subscription:</b> <?php echo $subtypedesc ?>
                        </p>
                    </div>
					<br>
                </div>
                <div hidden class="w3-card w3-round w3-white w3-margin-bottom">
                    <div id="maincontainer3" class="w3-container zprofiler">
                        <div class="topright"><img src="../images/Edit-Icon.png" alt="" width="30" class=""/></div>
						<br>
						<br>
                        
                        <p class="w3-center">
                            <img src="<?php echo $picpath ?>" class="w3-circle" style="height:106px;width:106px" alt="Avatar" />
                        </p>
						<h4 class="w3-center"><?php echo $ylpname . ", " . $userage ?></h4>
                        <hr />
                        
                        <pre>
                            <b>Email:</b> <?php echo $ylpemail ?>
                        </pre>
                        <p>
                            <b>Name:</b> <?php echo $ylpfirstname . " " . $ylplastname ?>
                        </p>
                        <p>
                            <b>Subscription:</b> <?php echo $subtypedesc ?>
                        </p>
                    </div>
					<br>
                </div>
                <!-- Accordion -->
                <div class="w3-card w3-round w3-margin-bottom">
                    <div class="w3-white dashboard-messagesgrp">
                         <ul class="list-group ">
                            <li class="dashboard-messagesimg list-group-item text-right "><span class="pull-left dashboard-messages">Messages</span><img src="../images/TakeVideo-button.png" alt="" class="dashboard-messagesimg2"/></li>
                            <li class="list-group-item text-right"><span class="pull-left">Inbox</span> <span <?php if(strlen($inboxcount) == 0) echo 'hidden' ?> class="dashboard-messagesnum"><?php echo $inboxcount ?></span></li>
                            <li class="list-group-item text-right"><span class="pull-left">Sent</span> <span <?php if(strlen($sentcount) == 0) echo 'hidden' ?> class="dashboard-messagesnum"><?php echo $sentcount ?></span></li>
                            <li class="list-group-item text-right"><span class="pull-left">Draft</span> <span <?php if(strlen($draftcount) == 0) echo 'hidden' ?> class="dashboard-messagesnum"><?php echo $draftcount ?></span></li>
                          </ul> 
                    </div>
                </div>
                <div id="soundcloud1" class="w3-card w3-round">
                    <div class="w3-white">
                        <iframe width="100%" height="100%" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/users/637007598&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>
                    </div>
                    </div>

                <!-- End Left Column -->
            </div>

            <!-- Middle Column -->
            <div class="w3-col m9">

                <div id="maincontainer4" class="w3-container w3-card w3-white w3-round w3-margin-left w3-margin-right">
                    <br />
                    <!--<span class="w3-right w3-opacity">View All</span> Need to add hyperlink to view all planets progress -->
                    <span class="dashboard-messages">Learning Path</span>
                    <br />
					<div id="maincontainer5" class="w3-container ">
						<br>
                        <div class="circle-holder">
                            <ul>
                                <h6 class="planet-titletxt">Planet Selfie</h6>
                              <li class="planetselfie text-right"><span class="pull-left">Added Strength Creative</span><span>April 13</span></li>
                                <h6 class="planet-titletxt">Mission Control</h6>
                              <li class="missioncontrol text-right"><span class="pull-left">Complete Mission Control</span> <span>Mar 20</span></li>
                                <h6 class="planet-titletxt">Mission Control</h6>
                              <li class="missioncontrol text-right"><span class="pull-left">Learned About 4 leaders</span> <span>Mar 9</span></li>
                                <h6 class="planet-titletxt">Mission Control</h6>
                              <li class="missioncontrol text-right"><span class="pull-left">Captured Young Leader Badge</span> <span>Mar 3</span></li>
                            </ul>
                        </div>
					</div>
					<br>
					<br>
                </div>

                <div id="maincontainer6" class="w3-container w3-card w3-white w3-round w3-margin">
                    <br />
                    <span class="dashboard-messages">Downloads</span>
                    <br />
                    <div class="container text-center">
                        <div class="row">
                        <div class="col-xs-2">
                            <figure>
                            <img src="../images/PDF-Icon.png" alt="" class="img-fluid"/>
                                <figcaption>Caregiver PDF</figcaption>
                            </figure>
                            <br>
                            <a href="../images/Caregiver PDF--Mission Control.pdf" download><img src="../images/Download-button.png" alt="" class="img-fluid"/></a>
                        </div>
                        <div class="col-xs-2">
                            <figure>
                            <img src="../images/PDF-Icon.png" alt="" class="img-fluid"/>
                                <figcaption>Leader Vision Board</figcaption>
                            </figure>
                            <br>
                            <a href="../images/Offline Activity--Mission Control.pdf" download><img src="../images/Download-button.png" alt="" class="img-fluid"/></a>
                        </div>
                        <!--<div class="col-xs-2">
                            <figure>
                            <img src="../images/PDF-Icon.png" alt="" class="img-fluid"/>
                                <figcaption>File Name</figcaption>
                            </figure>
                            <br>
                            <img src="../images/Download-button.png" alt="" class="img-fluid"/>
                        </div> -->   
                    </div>
                    </div>
                    <br>
                    <br>
                </div>
              <div id="soundcloud2" class="w3-card w3-round">
                    <div class="w3-white">
                        <iframe width="100%" height="100%" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/users/637007598&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>
                    </div>
                    </div>
                <!-- End Middle Column -->
            </div>


            <!-- End Grid -->
        </div>
        </div>
        <!-- End Page Container -->
    </div>
    <br />

    <div class="text-center footer-container container-fluid">
		<div class="row">
			<div class="col-12 col-xl-4">
				<p>
					<font size="-1" color="white">© Young Leader Project, LLC. All Rights Reserved.</font>
				</p>
			</div>
			<div class="col-12 offset-3 footer-adjust offset-xl-3 col-xl-5 align-content-center text-center">
				<nav>
					<ul>
						<li class="li-inline"><a href="../team">Meet The Team</a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
		</div>
</div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../js/jquery-3.3.1.min.js"></script>
    <script src="js/scripts.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap-4.2.1.js"></script>
<script>
		$(function(){
		  var $ppc = $('.progress-pie-chart'),
			percent = parseInt($ppc.data('percent')),
			deg = 360*percent/100;
		  if (percent > 50) {
			$ppc.addClass('gt-50');
		  }
		  $('.ppc-progress-fill').css('transform','rotate('+ deg +'deg)');
		  $('.ppc-percents span').html(percent+'%');
		});
        // Accordion
        function myFunction(id) {
            var x = document.getElementById(id);
            if (x.className.indexOf("w3-show") == -1) {
                x.className += " w3-show";
                x.previousElementSibling.className += " w3-theme-d1";
            } else {
                x.className = x.className.replace("w3-show", "");
                x.previousElementSibling.className =
                    x.previousElementSibling.className.replace(" w3-theme-d1", "");
            }
        }

        // Used to toggle the menu on smaller screens when clicking on the menu button
        function openNav() {
            var x = document.getElementById("navDemo");
            if (x.className.indexOf("w3-show") == -1) {
                x.className += " w3-show";
            } else {
                x.className = x.className.replace(" w3-show", "");
            }
        }
    </script>

</body>
</html>