<?php
error_reporting(E_ALL);

$linkpos = strpos(strtolower($_SERVER[ "DOCUMENT_ROOT" ]),"heykiddo");
if( $linkpos > 0){
	$_SERVER[ "DOCUMENT_ROOT" ] = substr($_SERVER[ "DOCUMENT_ROOT" ],0,$linkpos-1);
}

require_once str_replace( '\\', '/', $_SERVER[ "DOCUMENT_ROOT" ] ) . '/PHP/shared/auto_load.php';
require_once str_replace('\\', '/', $_SERVER["DOCUMENT_ROOT"]) . '/PHP/shared/arraytoxml.php';
$_REQUEST = array_change_key_case($_REQUEST, CASE_LOWER);
$dev = (isset($_REQUEST['dev']) ? $_REQUEST['dev'] : '');
if (trim($dev) === '' || $dev === false) {
    Redirect( "../" );
}
try {
    session_id($dev);
    session_start();
}
catch(\Exception $ex) {
    //  file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/heykiddosign.txt' , print_r($_SESSION,true)."\r\n", FILE_APPEND);
}

use PHP\CLASSES\CYLPDB;

$_REQUEST = array_change_key_case($_REQUEST, CASE_LOWER);
$rqstyp = (isset($_REQUEST['rqstyp']) ? $_REQUEST['rqstyp'] : '');
if ($rqstyp == "") {
    exit;
}

if (strtoupper($rqstyp) === "YLPDEV") {
    require $_SERVER[ "DOCUMENT_ROOT" ] . "/PHP/commands/ylpdev.php";
    exit;
}

if (strtoupper($rqstyp) === "YLPSIGNOFF") {
    $_SESSION = array();
    session_destroy();
    exit;
}

$dev = (isset($_SESSION["user"]["dev"]) && $_SESSION["user"]["dev"] == session_id() ?$_SESSION["user"]["dev"] : '');
if (trim($dev) === '' && $rqstyp !== 'ylplogin') {
   // file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/ylpmain.txt', "Bad device \r\n", FILE_APPEND);
   // Redirect( "../" );
}

// create ylpdb database access variable
$db = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);

switch (strtoupper($rqstyp)) {
    case "YLPSTRIPE":
        require $_SERVER[ "DOCUMENT_ROOT" ] . '/PHP/vendor/autoload.php';
        require $_SERVER[ "DOCUMENT_ROOT" ] . "/PHP/commands/ylpstripe.php";
        exit;
    case "YLPUSRMAINT":
        require $_SERVER[ "DOCUMENT_ROOT" ] . "/PHP/commands/ylpusrmaint.php";
        exit;
    case "YLPIMG":
        require $_SERVER[ "DOCUMENT_ROOT" ] . "/PHP/commands/ylpimgimport.php";
        exit;
    case "YLPVID":
        require $_SERVER[ "DOCUMENT_ROOT" ] . "/PHP/commands/ylpvidimport.php";
        exit;
    case "YLPLOGIN":
        require $_SERVER[ "DOCUMENT_ROOT" ] . "/PHP/commands/ylplogin.php";
        exit;
    case "YLPEMAIL":
        require $_SERVER[ "DOCUMENT_ROOT" ] . "/PHP/commands/ylpemail.php";
        exit;
    case "YLPTXTMSG":
        require $_SERVER[ "DOCUMENT_ROOT" ] . "/PHP/commands/ylptxtmsg.php";
        exit;
    case "YLPCHKPROMO":
        require $_SERVER[ "DOCUMENT_ROOT" ] . "/PHP/commands/ylpchkpromo.php";
        exit;
}