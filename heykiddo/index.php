﻿<?php
error_reporting(E_ALL);
try {
    @session_start();
    $dev = session_id();
}
catch(\Exception $ex) {
    //  file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/heykiddosign.txt' , print_r($_SESSION,true)."\r\n", FILE_APPEND);
}
error_reporting( E_ALL );
$linkpos = strpos( strtolower( $_SERVER[ "DOCUMENT_ROOT" ] ), "heykiddo" );
if ( $linkpos > 0 ) {
    $_SERVER[ "DOCUMENT_ROOT" ] = substr( $_SERVER[ "DOCUMENT_ROOT" ], 0, $linkpos - 1 );
}

?>
<html lang="en">
<head>
<!-- Google Tag Manager --> 
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NLW5ZB7');</script> 
<!-- End Google Tag Manager -->
<meta charset="utf-8" />
<meta name="robots" content="noindex, nofollow" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>HeyKiddo&trade; Portal | Home</title>
<meta name="description" content="
HeyKiddo&trade; develops the leaders of tomorrow by acquainting them with essential leadership skills today." />
<!-- Bootstrap -->
<meta property='og:title' content="HeyKiddo&trade;" />
<meta property='og:image' content="https://hey-kiddo.com/images/HeyKiddoLanding.PNG" />
<meta property='og:description' content="HeyKiddo&trade; develops the leaders of tomorrow by acquainting them with essential leadership skills today." />
<meta property='og:url' content="https://hey-kiddo.com" />
<link rel="icon" href="images/Hey-Kiddo-favicon.ico" type="favicon/ico" />
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet" />
<link rel="stylesheet" href="css/HKLandingScripts2.css?v=1" />
<link rel="stylesheet" href="css/intlTelInput.css?v=1" />
<link rel="stylesheet" href="css/bootstrap-select-country.min.css" />

<link rel="stylesheet" href="css/animate.changethewords.css" />
</head>

<body onload="checkCookiePolicy()">
    <div class="loading" hidden>Loading&#8230;</div>
    <div class="toppromo"><center>We have your back! 
HeyKiddo™ is offering free subscriptions during the current global crisis.</center>
</div>
    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NLW5ZB7"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->
    <!--Modal: modalCookie-->
    <div class="modal fade" id="cookieModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" data-backdrop="true">
        <div class="modal-dialog" role="document">
            <!--Content-->
            <div class="modal-content">
                <!--Body-->
                <div class="modal-body">
                    <center>
                        <div class="row">
                            <p class="p-3">Hey there! We use cookies on our site so we can make sure we are delivering the best possible web experience. We also use them to understand our website traffic. You can learn more about cookies, and how to disable them, by checking out our cookie policy. By clicking "Ok" on this banner, it means that you are okay with the use of cookies unless you have disabled them.</p>
                        </div>
                        <button onclick="window.open('https://www.youngleaderproject.com/cookiepolicy.html', 'YLP Cookie Policy', 'width=600,height=450');" class="btn btn-primary"> Learn more </button>
                        <button class="btn btn-outline-primary waves-effect" onclick="setCookiePolicy();" data-dismiss="modal">Ok, thanks</button>
                    </center>
                </div>
            </div>
            <!--/.Content-->
        </div>
    </div>
    <!--Modal: modalCookie-->
    <!--Modal: modalCookie-->
    <div class="modal fade" id="addCountryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" data-backdrop="true">
        <div class="modal-dialog" role="document">
            <!--Content-->
            <div class="modal-content">
                <!--Body-->
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HeyKiddo&trade; Add Country</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <center>
                        <form id="addcountry-form">
                            <center>
                                <p> HeyKiddo™ is available in English for subscribers in the US and Canada. We are excited to bring HeyKiddo™ to other countries and translate the texts into different languages. </p>
                                <p>If you want to stay connected and learn when HeyKiddo™ is available in your country, enter your email here:</p>
                            </center>
                            <div class="form-group row">
                                <label for="HKaddcountryemail" class="col-sm-5 col-form-label">Email Address:</label>
                                <div class="col-sm-7">
                                    <input type="email" class="form-control" id="HKaddcountryemail" name="HKemail" placeholder="Email" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="HKcountrylanguage" class="col-sm-5 col-form-label">Preferred Language:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="HKcountrylanguage" id="HKcountrylanguage" required placeholder="Language" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="HKcountry" class="col-sm-5 col-form-label">Preferred Country:</label>
                                <div class="col-sm-7">
                                    <select id="HKcountry" class="custom-select countrypicker" data-default="US" data-live-search="true"></select>
                                </div>
                            </div>
                            <span class="help-block" hidden id="addcountryformhelp"></span>
                        </form>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="HKaddcountrybtn" style="margin-top: 10px; max-width: 250px;">Submit</button>
                        </div>
                    </center>
                </div>
            </div>
            <!--/.Content-->
        </div>
    </div>
    <!--Modal: modalCookie-->

    <div class="modal fade" id="HKEULA" tabindex="-1" role="dialog" aria-labelledby="HKEULAlLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">HeyKiddo&trade; Portal End User License Agreement</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>This is where the HeyKiddo&trade; EULA will be</p>
                </div>
                <div class="modal-footer">
                    <center>
                        <button type="button" class="btn btn-primary" data-dismiss="modal" id="HKEULAaccept">Accept</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </center>
                </div>
            </div>
        </div>
    </div>
    <nav class="navbar navbar-expand-lg navbar-light justify-content-between text-white pt-5" style="background-color: #6AD0B3;">
        <div class="container-fluid">
            <a class="navbar-brand" href="/">
                <img src="images/logo-horizontal-tagline-darkBG@2x.png" width="200" class="img-fluid" alt="" />
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active pl-3">
                        <a id="HKlanding-home" style="color:inherit; cursor: pointer;" class="nav-link" href="#" onclick="return false;">Home</a>
                    </li>
                    <li class="nav-item active pl-3">
                        <a style="color:inherit; cursor: pointer;" class="nav-link" id="ourstorylink" href="#" onclick="return false;">Our Story</a>
                    </li>
                    <li class="nav-item active pl-3">
                        <a style="color:inherit; cursor: pointer;" class="nav-link" id="bloglink" href="https://hey-kiddo.com/blog" target="_blank">Blog</a>
                    </li>
                    <li class="nav-item active pl-3">
                        <a style="color:inherit; cursor: pointer;" class="nav-link" id="faqlink" href="#" onclick="return false;">FAQ</a>
                    </li>
                    <?php
                    if ( !( isset( $_SESSION[ "user" ][ "loggedin" ] ) ) || !( $_SESSION[ "user" ][ "loggedin" ] ) ) {
                        echo '<li class="nav-item active pl-3"><a style="color:inherit; cursor: pointer;" class="nav-link HKSignIn" id="signinlink" onclick="return false;">Sign In</a></li>';
                    } else {
                        echo '<li class="nav-item active pl-3"><a style="color:inherit;cursor: pointer;" class="nav-item nav-link HKNavselected HKSignIn" id="portallink" href="/portal">HeyKiddo&trade; Portal</a></li>';
                    }
                    ?>
                    <li class="nav-item active pl-3">
                        <a id="HKlanding-join" class="nav-link HKSignupBtn" style="color:inherit;min-width: 275px; text-align: center;" <?php if((isset($_SESSION["user"]["loggedin"])) && ($_SESSION["user"]["loggedin"])) echo 'hidden'; ?>>TRY FOR FREE</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container-fluid p-0" id="maincontainer">
        <div class="container-fluid HKSection1 text-white" style="position: relative;">
			<div class="container">
                <div class="row pt-5 pb-5">
                    <div class="col-12">

                        <div id="changethewords">
                            <div style="font-size:30px;word-wrap:break-word;">
                                Hey Kiddo, lets talk about
                                <span id="firstchangeword" data-id="1"> Confidence</span>
                            </div>
                        </div>
                        <p style="max-width:450px;padding:10px 0px;">We text you curated conversation starters and activities. You help your child build skills for a bright future.</p>
                        <div <?php if((isset($_SESSION["user"]["loggedin"])) && ($_SESSION["user"]["loggedin"])) echo 'hidden'; ?>>
                            <button type="button" id="HKlanding-join2" class="btn btn-lg HKSignupBtn" style="max-width: 300px;" <?php if((isset($_SESSION["user"]["loggedin"])) && ($_SESSION["user"]["loggedin"])) echo 'hidden'; ?>>TRY FOR FREE</button>
                        </div>
                            <br />
                            <p style="color:white;">
                                Best for parents with children ages 6-11
                            </p>
                    </div>
            </div>
				</div>
			<center>
                    <div style="max-width: 350px; font-weight: 100; font-size: 12px; margin-top: 70px;">HeyKiddo&trade; powered by the Young Leader Project&reg;</div>
                </center>
            <div class="HKfamily">
                <img src="images/hero-image@2x.png" class="img-fluid float-right" width="450px" alt="" />
            </div>
        </div>
        <div class="container HKSection1-2 pb-5">
            <center>
                <div style="max-width: 575px;padding-bottom:50px;">
                    <h4>
                        <b>How HeyKiddo&trade; Works</b>
                    </h4>
                </div>
            </center>
            <div class="row">
                <div class="col-xl-4 my-auto text-center offset-lg-3 col-lg-6 offset-xl-0 offset-sm-1 col-sm-10 offset-md-2 col-md-8 offset-2 col-8">
                    <img src="images/how-to-1.png" alt="" width="300" class="img-fluid" />
                    <center>
                        <div style="font-weight: 500; font-size: 17px;">Choose the topic you think would be most helpful for your child.</div>
                    </center>
                </div>
                <div class="col-xl-4 my-auto text-center col-lg-6 offset-lg-3 offset-xl-0 offset-sm-1 col-sm-10 offset-md-2 col-md-8 offset-2 col-8">
                    <img src="images/how-to-2.png" alt="" width="300" class="img-fluid" />
                    <center>
                        <div style="font-weight: 500; font-size: 17px;">We’ll text you curated conversation starters, coaching tips and activities each week.</div>
                    </center>
                </div>
                <div class="col-xl-4 my-auto text-center col-lg-6 offset-lg-3 offset-xl-0 offset-sm-1 col-sm-10 offset-md-2 col-md-8 offset-2 col-8">
                    <img src="images/how-to-3.png" alt="" width="300" class="img-fluid" />
                    <center>
                        <div style="font-weight: 500; font-size: 17px;">At the end of 8 weeks, choose a new topic.</div>
                    </center>
                </div>
            </div>
        </div>
        <center>
            <div class="embed-responsive embed-responsive-16by9" id="hkiframevideo" style="max-width:600px;">
                <!--<iframe  class="embed-responsive-item" src="https://www.youtube.com/embed/2V0967MM89w" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>-->
            </div>
        </center>
        <div class="container-fluid HKSection1-3">
            <div class="HKQuoteLine">
                <div></div>
            </div>
            <center>
                <div class="container">
                    <div id="carouselExampleIndicators1" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators1" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators1" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators1" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner" role="listbox">
                            <div class="carousel-item active">
                                <div style="max-width: 700px;">
                                    <h5>
                                        <b>My daughter and I did the emotional understanding activity. It really helped me understand how she expresses emotions through facial expressions and she was able to tune in the next time she felt nervous without my help.</b>
                                    </h5>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div style="max-width: 700px;">
                                    <h5>
                                        <b>Before HeyKiddo&trade;, I didn’t know how to talk to my son when he asked tough questions about life. Having these tools makes me feel more confident as a parent.</b>
                                    </h5>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div style="max-width: 700px;">
                                    <h5>
                                        <b>HeyKiddo&trade; made talking with my kids an everyday part of our life. I love watching them learn to see their own potential as changemakers!</b>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </center>
        </div>
        <div class="container-fluid HKSection1-1" hidden <?php if((isset($_SESSION["user"]["loggedin"])) && ($_SESSION["user"]["loggedin"])) echo 'hidden'; ?>>
            <div hidden <?php if((isset($_SESSION["user"]["loggedin"])) && ($_SESSION["user"]["loggedin"])) echo 'hidden'; ?>>
                <center>
                    <div style="max-width: 575px;">
                        <h4>
                            <b>Subscribe now to get one free week...</b>
                        </h4>
                    </div>
                </center>
                <div class="row text-center HKSection1-1-1">
                    <img src="images/dad-greeting.png" style="position:absolute;left:50%;margin-left:-270px;transform: rotate(-40deg);" class="img1-1-1" width="100" alt="" />

                    <div class="card mb-4 mx-auto box-shadow">
                        <img src="images/cat-turtle.png" style="position:absolute;right:-80px; bottom: -20px;" class="img1-1-1" width="100" alt="" />
                        <center>
                            <div class="card-body" style="z-index:100 !important;">

                                <h1 class="card-title pricing-card-title">
                                    <b>$0</b>
                                </h1>
                                <h6>
                                    First Week
                                    <!--<div class="valueblock text-center" style="border-style: solid;border-color: #FFCB1B">$99.99/yearly <img src="images/tag-bestValue.png" width="40" class="float-right" style="margin-top: -30px;margin-right: -10px;margin-left: -25px;" alt=""></div>-->
                                    <div class="valueblock text-center">$9.99/monthly</div>
                                </h6>
                                <button type="button" id="HKlanding-join3" style="max-width: 350px;" class="btn btn-lg HKSignupBtn"> TRY FOR FREE </button>
                                <!--<div style="max-width: 350px; font-weight: 100; font-size: 12px; margin-top: 10px;">After your free 7-day trial ends, you will be auto-enrolled in HeyKiddo™ for $9.99 per month or $99.99 per year. Cancel anytime.</div>-->
                                <div style="max-width: 350px; font-weight: 100; font-size: 12px; margin-top: 10px;">After your free 7-day trial ends, you will be auto-enrolled in HeyKiddo&trade; for $9.99 per month. Cancel anytime.</div>
                            </div>
                        </center>
                    </div>
                </div>
            </div>
        </div>
        <div class="container featurecontainer" style="padding-bottom:350px;padding-top:100px;">
            <div id="feature1" class="row pt-3">
                <div class="col-md-5 offset-md-1 my-auto">
                    <h4>
                        <b>Put the <i>quality</i> back in quality time</b>
                    </h4>
                    <p>We text curated conversation starters, coaching tips and activities straight to your phone so you can spend less time Googling and more time talking with your child.</p>
                    <button type="button" id="HKlanding-join4" style="max-width: 350px;" class="btn btn-lg HKSignupBtn" <?php if((isset($_SESSION["user"]["loggedin"])) && ($_SESSION["user"]["loggedin"])) echo 'hidden'; ?>> TRY FOR FREE </button>
                </div>
                <div class="col-md-5 offset-md-1 my-auto text-center">
                    <img src="images/feature-1.png" alt="HeyKiddo Feature 1" width="400" class="img-fluid" />
                </div>
            </div>
            <div id="feature1-1" class="row pt-3">
                <div class="col-md-5 offset-md-1 my-auto text-center">
                    <img src="images/feature-1.png" alt="HeyKiddo Feature 1" width="400" class="img-fluid" />
                </div>
                <div class="col-md-5 offset-md-1 my-auto">
                    <h4>
                        <b>Put the <i>quality</i> back in quality time</b>
                    </h4>
                    <p>We text curated conversation starters, coaching tips and activities straight to your phone so you can spend less time Googling and more time talking with your child.</p>
                    <button type="button" id="HKlanding-join5" style="max-width: 350px;" class="btn btn-lg HKSignupBtn" <?php if((isset($_SESSION["user"]["loggedin"])) && ($_SESSION["user"]["loggedin"])) echo 'hidden'; ?>> TRY FOR FREE </button>
                </div>
            </div>
            <div class="row pt-5" id="feature2">
                <div class="col-md-5 offset-md-1 my-auto mx-auto">
                    <img src="images/feature-2.png" alt="HeyKiddo Feature 2" width="400" class="img-fluid" />
                </div>
                <div class="col-md-5 offset-md-1 my-auto">
                    <h4>
                        <b>On your phone or on the web</b>
                    </h4>
                    <p>A HeyKiddo&trade; portal to keep all your activities in one place so you don’t have to scroll through your texts.</p>
                    <button type="button" id="HKlanding-join6" style="max-width: 350px;" class="btn btn-lg HKSignupBtn" <?php if((isset($_SESSION["user"]["loggedin"])) && ($_SESSION["user"]["loggedin"])) echo 'hidden'; ?>> TRY FOR FREE </button>
                </div>
            </div>


            <div id="feature3" class="row pt-5">
                <div class="col-md-5 offset-md-1 my-auto">
                    <h4>
                        <b>Curated by experts</b>
                    </h4>
                    <p>All of our content is developed by psychologists and child experts to help you develop a habit of meaningful conversations with your child. </p>
                    <button type="button" id="HKlanding-join7" style="max-width: 350px;" class="btn btn-lg HKSignupBtn" <?php if((isset($_SESSION["user"]["loggedin"])) && ($_SESSION["user"]["loggedin"])) echo 'hidden'; ?>> TRY FOR FREE </button>
                </div>
                <div class="col-md-5 offset-md-1 my-auto text-center">
                    <img src="images/feature-3.png" alt="HeyKiddo Feature 3" width="400" class="img-fluid" />
                </div>
            </div>
            <div id="feature3-1" class="row pt-5">
                <div class="col-md-5 offset-md-1 my-auto text-center">
                    <img src="images/feature-3.png" alt="HeyKiddo Feature 3" width="400" class="img-fluid" />
                </div>
                <div class="col-md-5 offset-md-1 my-auto">
                    <h4>
                        <b>Curated by experts</b>
                    </h4>
                    <p>All of our content is developed by psychologists and child experts to help you develop a habit of meaningful conversations with your child. </p>
                    <button type="button" id="HKlanding-join8" style="max-width: 350px;" class="btn btn-lg HKSignupBtn" <?php if((isset($_SESSION["user"]["loggedin"])) && ($_SESSION["user"]["loggedin"])) echo 'hidden'; ?>> TRY FOR FREE </button>
                </div>
            </div>
            <div id="feature4" class="row pt-5">
                <div class="col-md-5 offset-md-1 my-auto mx-auto">
                    <img src="images/feature-4.png" alt="HeyKiddo Feature 4" width="400" class="img-fluid" />
                </div>
                <div class="col-md-5 offset-md-1 my-auto">
                    <h4>
                        <b>New topics added monthly</b>
                    </h4>
                    <p>Our psychologists are hard at work creating new topics for you and your child, including the tough topics like bullying, self-esteem and stress.</p>
                    <button type="button" id="HKlanding-join9" style="max-width: 350px;" class="btn btn-lg HKSignupBtn" <?php if((isset($_SESSION["user"]["loggedin"])) && ($_SESSION["user"]["loggedin"])) echo 'hidden'; ?>> TRY FOR FREE </button>
                </div>
            </div>
            <div id="feature5" class="row pt-5">
                <div class="col-md-5 offset-md-1 my-auto">
                    <h4>
                        <b>Tips on how to talk about important world events</b>
                    </h4>
                    <p>Expert tips on how to talk to your child about tough topics in the news as it breaks.</p>
                    <button type="button" id="HKlanding-join10" style="max-width: 350px;" class="btn btn-lg HKSignupBtn" <?php if((isset($_SESSION["user"]["loggedin"])) && ($_SESSION["user"]["loggedin"])) echo 'hidden'; ?>> TRY FOR FREE </button>
                </div>
                <div class="col-md-5 offset-md-1 my-auto text-center">
                    <img src="images/feature-5.png" alt="HeyKiddo Feature 5" width="400" class="img-fluid" />
                </div>
            </div>
            <div id="feature5-1" class="row pt-5">
                <div class="col-md-5 offset-md-1 my-auto text-center">
                    <img src="images/feature-5.png" alt="HeyKiddo Feature 5" width="400" class="img-fluid" />
                </div>
                <div class="col-md-5 offset-md-1 my-auto">
                    <h4>
                        <b>Tips on how to talk about important world events</b>
                    </h4>
                    <p>Expert tips on how to talk to your child about tough topics in the news as it breaks.</p>
                    <button type="button" id="HKlanding-join11" style="max-width: 350px;" class="btn btn-lg HKSignupBtn" <?php if((isset($_SESSION["user"]["loggedin"])) && ($_SESSION["user"]["loggedin"])) echo 'hidden'; ?>> TRY FOR FREE </button>
                </div>
            </div>
        </div>

        <div class="HKSectionB">
            <div class="toppromo"><center>We have your back! 
            HeyKiddo™ is offering free subscriptions during the current global crisis.</center>
            </div>
            <div class="HKSCHARS">
                <img src="images/we-got-you-covered.png" class="img-fluid text-center" width="600" alt="" />
            </div>
            <div class="sectionbageflag">
                <center>
                    <img src="images/age-group-flag.png" class="img-fluid" alt="Age group flag " />
                </center>
            </div>
            <div class="col-12 pt-4" <?php if((isset($_SESSION["user"]["loggedin"])) && ($_SESSION["user"]["loggedin"])) echo 'hidden'; ?>>
                <center>
                    <div class="HKsignupform flip-container my-auto" id="flip-this">
                        <div class="flipper">

                            <div class="front text-center" style="position: absolute;top: 50%;left: 50%;margin-right: -50%;transform: translate(-50%, -50%)">
                                <form id="landing-form" class="text-center">
                                    <center>
                                        <h4>
                                            <b>Sign In</b>
                                        </h4>
                                        <p>
                                            New to HeyKiddo&trade;?
                                            <a href="#flip-this" class="flip-it" onclick="return false;" style="color: #55B588">
                                                <b>Create an account</b>
                                            </a>
                                        </p>
                                        </center>
                                        
                                        <div class="form-group">
                                            <input type="email" class="form-control" id="HKemail" name="HKemail" placeholder="Email" />
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control" id="HKpassword" name="HKpassword" placeholder="Password" />
                                        </div>
                                        <input type="hidden" name="rqstyp" value="ylplogin" />
                                        <button type="button" class="btn btn-lg HKSignupBtn" id="HKSignInBtn" style="margin-top: 10px; max-width: 250px;"> SIGN IN </button>
                                        <br />
                                        <br />
                                        <a href="#" id="forgotpass" onclick="return false;" style="color: #55B588">
                                            <b>Forgot your password?</b>
                                        </a>
                                        <br />
                                        <span class="help-block" style="color: red" hidden id="signinformhelp"></span>
                                    
                                </form>
                                <form id="forgot-form" class="text-center" style="margin-left:-25px;" hidden>
                                    <center>
                                        <h4>
                                            <b>Password Assistance</b>
                                        </h4>
                                        <p> Enter the email address associated with your HeyKiddo&trade; account. </p>
                                    </center>
                                    <div class="form-group">
                                        <input type="email" class="form-control" id="HKforgotEmail" name="HKemail" placeholder="Email" />
                                    </div>
                                    <input type="hidden" name="rqstyp" value="ylpusrmaint" />
                                    <input type="hidden" name="type" value="fpass" />
                                    <button type="button" class="btn btn-lg HKSignupBtn" id="HKForgotPBtn" style="margin-top: 10px; max-width: 250px;">Continue</button>
                                    <br />
                                    <span class="help-block" style="color: red" hidden id="forgotformhelp"></span>
                                    <span class="help-block" hidden id="forgotformdone"></span>
                                </form>
                            </div>
                            <div class="back" style="display:none;">
                                <form id="HKlanding-form">
                                    <center>
                                        <h3>
                                            <b>To get started, create an account</b>
                                        </h3>
                                        <p>
                                            Already have an account?
                                            <a href="#flip-this" class="flip-it" onclick="return false;" style="color: #55B588">
                                                <b>Sign In</b>
                                            </a>
                                        </p>
                                    </center>
                                    <div class="row">
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="HKInputFName" id="HKInputFName" required placeholder="First Name" />
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="HKInputLName" id="HKInputLName" required placeholder="Last Name" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="email" class="form-control" name="HKInputEmail" id="HKInputEmail" required placeholder="Email" />
                                    </div>
                                    <div class="form-group" style="display:block !important;">
                                        <input type="tel" class="form-control" name="HKInputPhone" id="phone" />
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" name="HKInputPassword" id="HKInputPassword" placeholder="Password" />
                                    </div>
                                    <input type="hidden" name="type" value="hksignup" />
                                    <label class="custom-checkbox">
                                        <input class="form-check-input" type="checkbox" id="HKTermscheckbox" />
                                        <span>
                                            Accept the
                                            <a href="javascript:window.open('https://www.youngleaderproject.com/termssubscription.html', 'HeyKiddo&trade; Terms of Subscription', 'width=600,height=450');">Terms of Subscription</a>
                                        </span>
                                    </label>
                                    <button type="button" class="btn btn-lg HKSignupBtn" id="HKSignupBtn" style="margin-top: 10px;"> SIGN UP </button>
                                    <span class="help-block" style="color: red;padding-bottom:5px" hidden id="signupformhelp"></span>
                                </form>
                            </div>
                        </div>
                    </div>
                </center>
            </div>
            <center>
                <br />
                <p style="color:white;">
                    Available in English for subscribers in the US and Canada. Other languages and countries are coming soon.
                    <a href="#" id="HKaddcountry" onclick="return false;">Let us know where to start!</a>
                </p>
            </center>
        </div>
        <footer style="position: relative;">
            <div class="container-fluid" style="padding: 20px;">
                <div class="row">
                    <div class="col-12 my-auto">
                        <p style="text-align: center; margin: auto; padding-bottom: 5px;">© Young Leader Project®, LLC. All Rights Reserved.</p>
                        <p style="text-align: center; margin: auto; font-weight: 100; font-size: 12px; max-width: 550px; padding-bottom: 10px;">
                            HeyKiddo&trade; and
                            <a target="_blank" style="color:inherit; cursor:pointer;" href="https://youngleaderproject.com">Young Leader Project®</a> do not provide medical or psychological advice, diagnosis, or treatment.
                            <a style="all: unset;text-decoration: underline;color:blue; cursor:pointer;" href="javascript:window.open('https://www.youngleaderproject.com/disclaimer.html', 'YLP Disclaimer', 'width=600,height=450');">See additional information</a>.
                        </p>
                        <div class="container-fluid pb-2 pt-2">
                            <div class="row mx-auto text-center" style="width: 85%;">
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                                    <a href="https://www.facebook.com/youngleaderproject/" target="_blank">
                                        <img width="50px" src="images/facebook.png" alt="YLP Facebook" />
                                    </a>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                                    <a href="https://www.instagram.com/youngleaderproject/" target="_blank">
                                        <img width="50px" src="images/instagram.png" alt="YLP Facebook" />
                                    </a>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                                    <a href="https://www.linkedin.com/company/young-leader-project/" target="_blank">
                                        <img width="50px" src="images/linkedin.png" alt="YLP Facebook" />
                                    </a>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                                    <a href="https://twitter.com/YoungLeaderPro" target="_blank">
                                        <img width="50px" src="images/twitter.png" alt="YLP Facebook" />
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="container-fluid">
                            <div class="mx-auto text-center">
                                <a style="font-size: 12px; color: inherit;white-space:normal" target="_blank" href="https://www.youngleaderproject.com">Young Leader Project® Website</a>
                            </div>
                            <div class="mx-auto text-center">
                                <a style="font-size: 12px; color: inherit;" href="javascript:window.open('https://www.youngleaderproject.com/cookiepolicy.html', 'YLP Cookie Policy', 'width=600,height=450');">Cookie Policy</a>
                            </div>
                            <div class="mx-auto text-center">
                                <a style="font-size: 12px; color: inherit;" href="javascript:window.open('https://www.youngleaderproject.com/privacypolicy.html', 'YLP Privacy Policy', 'width=600,height=450');">Privacy Policy</a>
                            </div>
                            <div class="mx-auto text-center">
                                <a style="font-size: 12px; color: inherit;" href="javascript:window.open('https://www.youngleaderproject.com/termsconditions.html', 'YLP Terms & Conditions;', 'width=600,height=450');">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <div class="container-fluid p-0" id="ourstorycontainer" style="display: none;">
        <div class="container-fluid p-0 pt-5" style="vertical-align: bottom;">
            <center>
                <div style="max-width: 575px;padding-bottom:50px;">
                    <h5>
                        <b>About HeyKiddo&trade;</b>
                    </h5>
                </div>
            </center>
            <div class="container">
                <div class="row w-75 mx-auto" style="padding-bottom:150px;">
                    <p style="position: relative; z-index:999;">
                        Our revolutionary text-based subscription program HeyKiddo™ was developed by Dr. Nicole Lipkin and her team of psychologists and child experts at the Young Leader Project® to make it easier for parents to talk to their kids. It sounds simple, but after years of research funded in part by a National Science Foundation grant, we learned that parents wanted to help their kids develop social, emotional, and leadership skills but don’t always know how to do that. But workbooks and online courses weren’t going to cut it–parents wanted quick, easy to implement ideas that seamlessly fit into everyday life. That’s how HeyKiddo™ was born.
                        <br />
                        <br />
                        To learn more, visit
                        <a style="color: #55B588;cursor:pointer;" href="https://youngleaderproject.com/team" target="_blank">Our Team</a>.
                    </p>
                </div>
            </div>
            <center>
                <div class="col-12 pt-5 text-center mx-auto" style="background-color:#6AD0B3;padding:125px 0px;">
                    <img src="images/psychologist-s.png" width="1000" style="position:absolute;left:50%;top:-250px;margin-left:-500px;" class="text-center" alt="Alternate Text" />
                </div>
            </center>
        </div>

        <footer style="position: relative;">

            <div class="container">
                <div class="row  mx-auto">
                    <div class="col-12 my-auto">
                        <p style="text-align: center; margin: auto; padding-bottom: 5px; max-width:400px">© Young Leader Project®, LLC. All Rights Reserved.</p>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <div class="container-fluid" id="faqcontainer" style="display: none;">
        <div class="container pt-5 pb-5">
            <h4 style="color:#70c08f;">
                <b>General Information</b>
            </h4>
            <div class="accordion" style="padding:30px 0;" id="accordionExample1">
                <div class="card" style="padding-right: 30px;">
                    <div class="card-header" id="headingOneF">
                        <h2 class="mb-0">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionExample1" href="#collapseOneF">What is HeyKiddo&trade;? </a>
                        </h2>
                    </div>
                    <div id="collapseOneF" class="collapse" aria-labelledby="headingOneF" data-parent="#accordionExample1">
                        <div class="card-body">
                            <p>
                                HeyKiddo™ is a text-based subscription that provides curated conversation starters, coaching tips and activities to help support your child’s development. Does contact us work? Current topics include: ways to build and manage confidence, stress, relationships, social intelligence, self-control and thoughtful decisions. We’re always building new topics to discuss with your child! Please
                                <a style="all: unset;text-decoration: underline;color:blue;" href="mailto:support@hey-kiddo.com">contact us</a> if you have special requests!
                            </p>
                            <br />
                            <p>Children grow by watching others model new skills, having open communication about experiences related to these skills, and by engaging in activities that reinforce new skills. We have included all of these methods in HeyKiddo™, making it a playbook for their individual growth.</p>
                            <br />
                            <p>Although we will mainly communicate with you via text, all of your activities are accessible in the HeyKiddo™ parent portal, just in case you want to do them more than once, maybe with the whole family!</p>
                            <br />
                            <p>And we know as a parent you are super busy, so HeyKiddo™ is flexible and lets you choose the time of day that works best for you and your child to receive our conversation starters - whether on the way to school, in the middle of the day over lunch, in the afternoon after school, or before bedtime.</p>
                        </div>
                    </div>
                </div>
                <div class="card" style="padding-right: 30px;">
                    <div class="card-header" id="headingOne">
                        <h2 class="mb-0">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionExample1" href="#collapseOne"> What will my family learn through HeyKiddo&trade;? </a>
                        </h2>
                    </div>
                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample1">
                        <div class="card-body">
                            <p>Each week, you’ll learn new ways of connecting with your child through conversation starters and activities that develop social, emotional and leadership skills vital for future success.</p>
                            <br />
                            <p>All of our conversation starters and activities are geared toward helping you and your child build critical skills by deepening your conversations with them regarding difficult issues that may arise day-to-day.</p>
                            <br />
                            <p>Sometimes kids aren’t as used to talking about these things as parents are … so although it may feel strange for some at first, stick with it! Opening up the lines of communication between you and your child will help them reach out and talk with you with more ease. When things arise - both good and bad in your child’s life - they will know they can come to you to talk about anything.</p>
                        </div>
                    </div>
                </div>
                <div class="card" style="padding-right: 30px;">
                    <div class="card-header" id="headingOne2">
                        <h2 class="mb-0">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionExample1" href="#collapseOne2"> Who created HeyKiddo&trade;? </a>
                        </h2>
                    </div>
                    <div id="collapseOne2" class="collapse" aria-labelledby="headingOne2" data-parent="#accordionExample1">
                        <div class="card-body">
                            <p>
                                <a href="https://www.linkedin.com/in/nicolelipkin/" style="all: unset;text-decoration: underline;color:blue;" target="_blank">Dr. Nicole Lipkin</a> is the founder of HeyKiddo™. She enlisted a team of psychologists and child experts who specialize in leadership, social and emotional development. Their expertise meets at the intersection of psychology and leadership. Our content and research team have the perfect combination of experience and training necessary to help children build a strong foundation of social, emotional and leadership skills. Meet <a href="https://youngleaderproject.com/team" style="all: unset;text-decoration: underline;color:blue;" target="_blank">our team</a>!
                            </p>
                        </div>
                    </div>
                </div>
                <div class="card" style="padding-right: 30px;">
                    <div class="card-header" id="headingOne3">
                        <h2 class="mb-0">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionExample1" href="#collapseOne3">What is the Young Leader Project® and how is it related to HeyKiddo™?</a>
                        </h2>
                    </div>
                    <div id="collapseOne3" class="collapse" aria-labelledby="headingOne3" data-parent="#accordionExample1">
                        <div class="card-body">
                            <p>HeyKiddo™ is one of the products of Young Leader Project®, which is a company dedicated to building the leadership, social and emotional skills of children and their caregivers. Young Leader Project® believes that the best way to help children learn these skills is through individual, parent-led and peer-led learning. HeyKiddo™ is the parent-led learning arm. </p>
                            <br />
                             <p></p>Young Leader Project® is also developing a mobile video game designed for children to learn leadership, social and emotional skills individually and with their peers. Caregivers will be able to monitor progress and join in on game play through conversation and multi-player gaming. This game is in development and coming soon!</p>
                            <br />
                            <p>Our evidence-based activities have been tested by hundreds of parents just like you! Young Leader Project® has been awarded a National Science Foundation (NSF) Small Business Innovation Research (SBIR) to conduct research and development (R&D) work on helping children develop leadership skills. This material is based upon work supported by the National Science Foundation under Grant No. 1842707</p>
                        </div>
                    </div>
                </div>
            </div>
            <h4 style="padding-top:20px;color:#70c08f;">
                <b>How to use HeyKiddo&trade;</b>
            </h4>
            <div class="accordion" style="padding:30px 0;" id="accordionExample2">
                <div class="card" style="padding-right: 30px;">
                    <div class="card-header" id="headingTwo">
                        <h2 class="mb-0">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionExample2" href="#collapseTwo">How to use HeyKiddo&trade;? </a>
                        </h2>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample2">
                        <div class="card-body">
                            <img style="float: left;margin-right: 20px;" src="images/How-to-Icon1.png" alt="" />
                            <p style="padding-top: 10px;">Once you are signed up for HeyKiddo&trade;, you will begin receiving texts three times each week that include expertly curated content that helps your child grow and develop.</p>
                            <br />
                            <img style="float: left;margin-right: 20px;" src="images/How-to-Icon2.png" alt="" />
                            <p style="padding-top: 10px;">After receiving each text, take time to read and think about the content of the text. Some texts will be geared toward you, the parent, with areas for you to self-reflect on, whereas at other times, there will be texts that help guide your conversations with your child.</p>
                            <br />
                            <img style="float: left;margin-right: 20px;" src="images/How-to-Icon3.png" alt="" />
                            <p style="padding-top: 10px;">HeyKiddo&trade; texts that offer targeted suggestions should help guide you through conversations with your child, not dictate exactly what you should say to them. You know your child better than anyone, so use what you know about them to make the content as impactful as possible, meeting them where they are.</p>
                            <br />
                            <img style="float: left;margin-right: 20px;" src="images/How-to-Icon4.png" alt="" />
                            <p style="padding-top: 10px;">As a part of this, use your own voice and style when interacting with and presenting the HeyKiddo&trade; content to your child. Focus on making the interactions natural and genuine.</p>
                            <br />
                            <img style="float: left;margin-right: 20px;" src="images/How-to-Icon5.png" alt="" />
                            <p style="padding-top: 10px;">Implementing a new routine, learning new skills, and having deeper conversations are all things that can be difficult when we first try them, but if you stick with it and make it a consistent practice, deeper conversations will become a natural part of you and your child’s daily experience.</p>
                        </div>
                    </div>
                </div>
                <div class="card" style="padding-right: 30px;">
                    <div class="card-header" id="headingThree">
                        <h2 class="mb-0">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionExample2" href="#collapseThree">HeyKiddo&trade; the basics:</a>
                        </h2>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample2">
                        <div class="card-body">
                            <p>
                                Within HeyKiddo™, you will see repeated symbols and prompts. Here are a few to be on the lookout for:
                                <ul>
                                    <li>
                                        Some texts are just for you, the parent, and are geared toward your own self-reflection. When you see “
                                        <b style="color:red;">Self-Reflection Questions…</b>” it means that these questions are for you to reflect on.
                                    </li>
                                    <li>
                                        HeyKiddo™ is also packed full of questions and guided actions for you to try with your kid. These questions are noted by “
                                        <b style="color:red;">Hey Kiddo! Ask & Share…</b>”
                                        <ul style="list-style-type:circle;">
                                            <li>
                                                <b style="color:red;">❓</b> prompts you to ask yourself or your child a question.
                                            </li>
                                            <li>
                                                <b style="color:red;">❗</b> indicates it is time for you to take action by interacting with your child.
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="card" style="padding-right: 30px;">
                    <div class="card-header" id="heading3Four">
                        <h2 class="mb-0">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionExample2" href="#collapse3Four">Adjusting HeyKiddo&trade; for your child:</a>
                        </h2>
                    </div>
                    <div id="collapse3Four" class="collapse" aria-labelledby="heading3Four" data-parent="#accordionExample2">
                        <div class="card-body">
                            <p>One thing we cannot say enough is that all of the content provided as a part of HeyKiddo&trade; are suggestions to help facilitate deeper conversations with your child. While using HeyKiddo&trade;, it is important that you adjust based on your child’s needs and responses to the content and activities.</p>
                            <br />
                            <p>
                                <u>Adjusting Activities</u> - Sometimes your child will need help reading and understanding the directions and goals of the activities, other times they might need help finding the supplies but can do the activity on their own. And sometimes they might be able to do the entire activity on their own. You know your child best and you will have to gauge the amount of support they will need from you!
                            </p>
                            <br />
                            <p>
                                <u>Adjusting Questions</u> - The questions we provide for you are meant to be general enough so that you can find a way to ask them to your child in a way that is natural and will make the most sense to them. It is recommended that questions be adapted to fit the child’s age and communication level. Also be sure to use words you would generally use and that your child can understand.
                            </p>
                            <br />
                            <p>
                                <u>Adjusting Prompts</u> - Whenever you are prompted to model a skill, it might be something that feels uncomfortable for you because it is something new. That’s okay and is actually our goal … it means growth is happening! Our goal is to help you build skills that help you engage your child in deeper conversations, and doing that might require some reflecting and practicing on your end.
                            </p>
                            <br />
                        </div>
                    </div>
                </div>
            </div>
            <h4 style="padding-top:20px;color:#70c08f;">
                <b>Technical Support and Getting In Touch</b>
            </h4>
            <div class="accordion" style="padding:30px 0;" id="accordionExample3">
                <div class="card" style="padding-right: 30px;">
                    <div class="card-header" id="headingFour">
                        <h2 class="mb-0">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionExample3" href="#collapseFour">How to contact HeyKiddo™ for technical support</a>
                        </h2>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample3">
                        <div class="card-body">
                            <p>
                                If you run into any issues with HeyKiddo™, please contact our support team immediately at
                                <a style="all: unset;text-decoration: underline;color:blue;" href="mailto:support@hey-kiddo.com">support@hey-kiddo.com</a>. Our team is standing by to resolve any technical issues or concerns that arise. Your feedback is valuable in making this an even more powerful tool for parents and children everywhere.
                            </p>
                            <br />
                        </div>
                    </div>
                </div>
                <div class="card" style="padding-right: 30px;">
                    <div class="card-header" id="headingFive">
                        <h2 class="mb-0">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionExample3" href="#collapseFive">I have some recommendations for HeyKiddo™</a>
                        </h2>
                    </div>
                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample3">
                        <div class="card-body">
                            <p>
                                We are constantly looking for ways to improve the connection between caregivers and their children. Please drop us a line with feedback or suggestions at
                                <a style="all: unset;text-decoration: underline;color:blue;" href="mailto:support@hey-kiddo.com">support@hey-kiddo.com</a>
                            </p>
                            <br />
                        </div>
                    </div>
                </div>
                <div class="card" style="padding-right: 30px;">
                    <div class="card-header" id="headingsix">
                        <h2 class="mb-0">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionExample3" href="#collapsesix">How to unsubscribe from HeyKiddo™</a>
                        </h2>
                    </div>
                    <div id="collapsesix" class="collapse" aria-labelledby="headingsix" data-parent="#accordionExample3">
                        <div class="card-body">
                            <p>
                                There are two ways to unsubscribe:
                                <ol>
                                    <li>
                                        Visit
                                        <a style="all: unset;text-decoration: underline;color:blue;" target="_blank" href="https://www.paypal.com/cgi-bin/webscr?cmd=_subscr-find&alias=D7U7FLTV2BXNY">Paypal</a> and login to your account. Find your subscription for HeyKiddo&trade; and click unsubscribe. Paypal will automatically unsubscribe you from the product and stop any subsequent payments.
                                    </li>
                                    <li>
                                        Email us directly at
                                        <a style="all: unset;text-decoration: underline;color:blue;" href="mailto:support@hey-kiddo.com">support@hey-kiddo.com</a> and we will unsubscribe you. This may take up to 5 business days and we will manually unsubscribe you from the product and stop any subsequent payments.
                                    </li>
                                </ol>
                            </p>
                            <div class="row mx-auto text-center mt-2">
                                <div class="col-xl-4 mx-auto col-lg-4">
                                    <img data-enlargable style="cursor: zoom-in;" src="images/paypal1.png" width="200" class="img-fluid" alt="" />
                                </div>
                                <div class="col-xl-4 mx-auto col-lg-4">
                                    <img data-enlargable style="cursor: zoom-in;" src="images/paypal2.png" width="200" class="img-fluid" alt="" />
                                </div>
                                <div class="col-xl-4 mx-auto col-lg-4">
                                    <img data-enlargable style="cursor: zoom-in;" src="images/paypal3.png" width="200" class="img-fluid" alt="" />
                                </div>
                            </div>
                            <div class="row mx-auto text-center mt-2">
                                <div class="col-xl-3 mx-auto col-lg-3">
                                    <img data-enlargable style="cursor: zoom-in;" src="images/paypal4.png" width="200" class="img-fluid" alt="" />
                                </div>
                                <div class="col-xl-3 mx-auto col-lg-3">
                                    <img data-enlargable style="cursor: zoom-in;" src="images/paypal5.png" width="200" class="img-fluid" alt="" />
                                </div>
                                <div class="col-xl-3 mx-auto col-lg-3">
                                    <img data-enlargable style="cursor: zoom-in;" src="images/paypal6.png" width="200" class="img-fluid" alt="" />
                                </div>
                                <div class="col-xl-3 mx-auto col-lg-3">
                                    <img data-enlargable style="cursor: zoom-in;" src="images/paypal7.png" width="200" class="img-fluid" alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="https://cdn.rawgit.com/nnattawat/flip/master/dist/jquery.flip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="js/HKLandingJS2.js?v=5"></script>
    <script src="js/hkglobals.js"></script>
    <script src="js/intlTelInput.js"></script>
    <script src="js/bootstrap-select-country.min.js"></script>
    <script type="text/javascript">
    (function(e,t,o,n,p,r,i){e.visitorGlobalObjectAlias=n;e[e.visitorGlobalObjectAlias]=e[e.visitorGlobalObjectAlias]||function(){(e[e.visitorGlobalObjectAlias].q=e[e.visitorGlobalObjectAlias].q||[]).push(arguments)};e[e.visitorGlobalObjectAlias].l=(new Date).getTime();r=t.createElement("script");r.src=o;r.async=true;i=t.getElementsByTagName("script")[0];i.parentNode.insertBefore(r,i)})(window,document,"https://diffuser-cdn.app-us1.com/diffuser/diffuser.js","vgo");
    vgo('setAccount', '25992310');
    vgo('setTrackByDefault', true);

        vgo('process');
    <?php echo "window.ylp.dev='$dev'";?>
    </script>
    <script src="js/jquery.changethewords.js"></script>
    <script type="text/javascript">
    $(function() {
      $("#changethewords").changeWords({
        time: 3000,
        animate: "tada",
        selector: "span",
        repeat:true
      });
    });
        $("#firstchangeword").after('<span data-id="2"> Mindset</span><span data-id="3"> Relationships</span><span data-id="4"> Self-Control</span><span data-id="5"> Stress</span><span data-id="6"> Friends</span><span data-id="7"> Perfectionism</span><span data-id="8"> Failure</span><span data-id="9"> Happiness</span><span data-id="10"> Mindfulness</span><span data-id="11"> Resilience</span><span data-id="12"> Peer Pressure</span><span data-id="13"> Emotions</span><span data-id="14"> Focus</span><span data-id="15"> Well-Being</span><span data-id="16"> Courage</span><span data-id="17"> Empathy</span>!')
    </script>

</body>
</html>