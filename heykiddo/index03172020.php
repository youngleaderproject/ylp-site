<?php
error_reporting(E_ALL);
try {
    @session_start();
    $dev = session_id();
}
catch(\Exception $ex) {
    //  file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/heykiddosign.txt' , print_r($_SESSION,true)."\r\n", FILE_APPEND);
}
error_reporting( E_ALL );
$linkpos = strpos( strtolower( $_SERVER[ "DOCUMENT_ROOT" ] ), "heykiddo" );
if ( $linkpos > 0 ) {
  $_SERVER[ "DOCUMENT_ROOT" ] = substr( $_SERVER[ "DOCUMENT_ROOT" ], 0, $linkpos - 1 );
}

?>
<html lang="en">
<head>
<!-- Google Tag Manager --> 
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NLW5ZB7');</script> 
<!-- End Google Tag Manager -->
<meta charset="utf-8" />
<meta name="robots" content="noindex, nofollow" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>HeyKiddo&trade; Portal | Home</title>
<meta name="description" content="
HeyKiddo&trade; develops the leaders of tomorrow by acquainting them with essential leadership skills today." />
<!-- Bootstrap -->
<meta property='og:title' content="HeyKiddo&trade;" />
<meta property='og:image' content="https://hey-kiddo.com/images/HeyKiddoLanding.PNG" />
<meta property='og:description' content="HeyKiddo&trade; develops the leaders of tomorrow by acquainting them with essential leadership skills today." />
<meta property='og:url' content="https://hey-kiddo.com" />
<link rel="icon" href="images/Hey-Kiddo-favicon.ico" type="favicon/ico" />
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet" />
<link rel="stylesheet" href="css/HKLandingScripts.css" />
<link rel="stylesheet" href="css/bootstrap-formhelpers.min.css" />
<link rel="stylesheet" href="css/bootstrap-select-country.min.css" />
</head>

<body onload="checkCookiePolicy()">
    <div class="loading" hidden>Loading&#8230;</div>
    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NLW5ZB7"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->
    <!--Modal: modalCookie-->
    <div class="modal fade" id="cookieModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" data-backdrop="true">
        <div class="modal-dialog" role="document">
            <!--Content-->
            <div class="modal-content">
                <!--Body-->
                <div class="modal-body">
                    <center>
                        <div class="row">
                            <p class="p-3">Hey there! We use cookies on our site so we can make sure we are delivering the best possible web experience. We also use them to understand our website traffic. You can learn more about cookies, and how to disable them, by checking out our cookie policy. By clicking "Ok" on this banner, it means that you are okay with the use of cookies unless you have disabled them.</p>
                        </div>
                        <a type="button" href="javascript:window.open('https://www.youngleaderproject.com/cookiepolicy.html', 'YLP Cookie Policy', 'width=600,height=450');" class="btn btn-primary"> Learn more </a>
                        <a type="button" class="btn btn-outline-primary waves-effect" onclick="setCookiePolicy();" data-dismiss="modal">Ok, thanks</a>
                    </center>
                </div>
            </div>
            <!--/.Content-->
        </div>
    </div>
    <!--Modal: modalCookie-->
    <!--Modal: modalCookie-->
    <div class="modal fade" id="addCountryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" data-backdrop="true">
        <div class="modal-dialog" role="document">
            <!--Content-->
            <div class="modal-content">
                <!--Body-->
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">HeyKiddo&trade; Add Country</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <center>
                        <form id="addcountry-form">
                            <center>
                                <p> HeyKiddo™ is available in English for subscribers in the US and Canada. We are excited to bring HeyKiddo™ to other countries and translate the texts into different languages. </p>
                                <p>If you want to stay connected and learn when HeyKiddo™ is available in your country, enter your email here:</p>
                            </center>
                            <div class="form-group row">
                                <label for="HKaddcountryemail" class="col-sm-5 col-form-label">Email Address:</label>
                                <div class="col-sm-7">
                                    <input type="email" class="form-control" id="HKaddcountryemail" name="HKemail" placeholder="Email" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="HKcountrylanguage" class="col-sm-5 col-form-label">Preferred Language:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="HKcountrylanguage" id="HKcountrylanguage" required placeholder="Language" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="HKcountry" class="col-sm-5 col-form-label">Preferred Country:</label>
                                <div class="col-sm-7">
                                    <select id="HKcountry" class="custom-select countrypicker" data-default="US" data-live-search="true"></select>
                                </div>
                            </div>
                            <span class="help-block" hidden id="addcountryformhelp"></span>
                        </form>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="HKaddcountrybtn" style="margin-top: 10px; max-width: 250px;">Submit</button>
                        </div>
                    </center>
                </div>
            </div>
            <!--/.Content-->
        </div>
    </div>
    <!--Modal: modalCookie-->

    <div class="modal fade" id="HKEULA" tabindex="-1" role="dialog" aria-labelledby="HKEULAlLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">HeyKiddo&trade; Portal End User License Agreement</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>This is where the HeyKiddo&trade; EULA will be</p>
                </div>
                <div class="modal-footer">
                    <center>
                        <button type="button" class="btn btn-primary" data-dismiss="modal" id="HKEULAaccept">Accept</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </center>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid HKSection1" style="position: relative;">
        <nav class="navbar navbar-expand-lg navbar-light bg-transparent">
            <div class="container-fluid">
                <a class="navbar-brand" href="/">
                    <img src="images/logo-horizontal-tagline.png" width="200" class="img-fluid" alt="" />
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a style="color:inherit; cursor: pointer;" hidden class="nav-link HKSignIn" href="https://hey-kiddo.com/blog" target="_blank">Our Story</a>
                        </li>
                        <li class="nav-item active pl-3">
                            <a style="color:inherit; cursor: pointer;" class="nav-link HKSignIn" href="https://hey-kiddo.com/blog" target="_blank">Blog</a>
                        </li>
                        <?php
                        if ( !( isset( $_SESSION[ "user" ][ "loggedin" ] ) ) || !( $_SESSION[ "user" ][ "loggedin" ] ) ) {
                            echo '<li class="nav-item active pl-3"><a style="color:inherit; cursor: pointer;" class="nav-link HKSignIn" onclick="return false;">Sign In</a></li>';
                        } else {
                            echo '<li class="nav-item active pl-3"><a style="color:inherit;cursor: pointer;" class="nav-item nav-link HKNavselected HKSignIn" href="/portal">HeyKiddo&trade; Portal</a></li>';
                        }
                        ?>
                        <li class="nav-item active pl-3">
                            <a id="HKlanding-join" class="nav-item nav-link HKSignupBtn" style="min-width: 275px; margin-left: 10px; color:white;text-align: center;" <?php if((isset($_SESSION["user"]["loggedin"])) && ($_SESSION["user"]["loggedin"])) echo 'hidden'; ?>>START FREE TRIAL</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <br />
        <br />
        <br />
        <div class="container" style="position: relative;z-index: 1;">
            <div class="row ">
                <div class="col-10 offset-1 col-sm-10 offset-sm-1 offset-md-0 offset-xl-1 offset-lg-1 col-xl-11 col-lg-10 col-md-12">
                    <h2>Talk to your kid like their future depends on it.</h2>
                    <p>Ever wish there was a user manual for your kid? Good news! You’ve found it. HeyKiddo™ takes the guesswork out of developing your child’s social-emotional and leadership skills. We send curated conversation starters (developed by psychologists!) and activities straight to your phone so you can spend less time Googling and more time being an awesome parent.</p>
                    <ul class="custom-list">
                        <li>Curated conversation starters</li>
                        <li>Evidenced-based activities developed by psychologists</li>
                        <li>Tips for how to talk to your child about tough topics like bullying, self-esteem, and stress</li>
                        <li>A HeyKiddo™ portal to keep all your activities in one place</li>
                        <li>Expert advice on how to talk to your child about tough topics in the news</li>
                    </ul>
                    <br />
                    <div <?php if((isset($_SESSION["user"]["loggedin"])) && ($_SESSION["user"]["loggedin"])) echo 'hidden'; ?>>
                        <button type="button" id="HKlanding-join2" class="btn btn-lg HKSignupBtn" style="max-width: 400px;" <?php if((isset($_SESSION["user"]["loggedin"])) && ($_SESSION["user"]["loggedin"])) echo 'hidden'; ?>>SIGN UP</button>
                    </div>
                </div>
            </div>
            <center>
                <div style="max-width: 350px; font-weight: 100; font-size: 12px; margin-top: 70px;">HeyKiddo&trade; Portal powered by the Young Leader Project </div>
            </center>
        </div>
        <div class="HKfamily">
            <img src="images/Hero-Characters@2x.png" class="img-fluid float-right" width="500px" alt="" />
        </div>
    </div>
    <div class="container-fluid HKSection1-1" <?php if((isset($_SESSION["user"]["loggedin"])) && ($_SESSION["user"]["loggedin"])) echo 'hidden'; ?>>
        <div class="HKShapeR">
            <img src="images/Shapes-R-long@2x.png" class="img-fluid float-right" width="1000px" alt="" />
        </div>
        <div class="HKShapeL">
            <img src="images/Shapes-L-long@2x.png" class="img-fluid float-right" width="1000px" alt="" />
        </div>
        <div <?php if((isset($_SESSION["user"]["loggedin"])) && ($_SESSION["user"]["loggedin"])) echo 'hidden'; ?>>
            <center>
                <div style="max-width: 575px;">
                    <h5>
                        <b>Subscribe now to get one free week...</b>
                    </h5>
                </div>
            </center>
            <div class="row text-center HKSection1-1-1">
                <div class="card mb-4 mx-auto box-shadow">
                    <center>
                        <div class="card-body">
                            <h1 class="card-title pricing-card-title">
                                <b>$0</b>
                            </h1>
                            <h6>
                                First Week
                                <!--<div class="valueblock text-center" style="border-style: solid;border-color: #FFCB1B">$99.99/yearly <img src="images/tag-bestValue.png" width="40" class="float-right" style="margin-top: -30px;margin-right: -10px;margin-left: -25px;" alt=""></div>-->
                                <div class="valueblock text-center">$9.99/monthly</div>
                            </h6>
                            <button type="button" id="HKlanding-join3" style="max-width: 350px;" class="btn btn-lg HKSignupBtn"> TRY FOR FREE </button>
                            <!--<div style="max-width: 350px; font-weight: 100; font-size: 12px; margin-top: 10px;">After your free 7-day trial ends, you will be auto-enrolled in HeyKiddo™ for $9.99 per month or $99.99 per year. Cancel anytime.</div>-->
                            <div style="max-width: 350px; font-weight: 100; font-size: 12px; margin-top: 10px;">After your free 7-day trial ends, you will be auto-enrolled in HeyKiddo&trade; for $9.99 per month. Cancel anytime.</div>
                        </div>
                    </center>
                </div>
            </div>
        </div>
    </div>
    <div class="container HKSection1-2">
        <center>
            <div style="max-width: 575px;padding-bottom:50px;">
                <h5>
                    <b>How HeyKiddo&trade; Works</b>
                </h5>
            </div>
        </center>
        <div class="row">
            <div class="col-xl-4 my-auto text-center offset-lg-3 col-lg-6 offset-xl-0 offset-sm-1 col-sm-10 offset-md-2 col-md-8 offset-2 col-8">
                <img src="images/how-to-1.png" alt="" width="300" class="img-fluid" />
                <center>
                    <div style="font-weight: 500; font-size: 17px;">Choose what would be the most helpful topic for your child right now.</div>
                </center>
            </div>
            <div class="col-xl-4 my-auto text-center col-lg-6 offset-lg-3 offset-xl-0 offset-sm-1 col-sm-10 offset-md-2 col-md-8 offset-2 col-8">
                <img src="images/how-to-2.png" alt="" width="300" class="img-fluid" />
                <center>
                    <div style="font-weight: 500; font-size: 17px;">We'll text you curated conversation starters, coaching tips, and activities each week.</div>
                </center>
            </div>
            <div class="col-xl-4 my-auto text-center col-lg-6 offset-lg-3 offset-xl-0 offset-sm-1 col-sm-10 offset-md-2 col-md-8 offset-2 col-8">
                <img src="images/how-to-3.png" alt="" width="300" class="img-fluid" />
                <center>
                    <div style="font-weight: 500; font-size: 17px;">All of the activities you recieved by text can be accessed at any time in your HeyKiddo&trade; portal at hey-kiddo.com.</div>
                </center>
            </div>
        </div>
        <div class="row pt-5">
            <div class="col-xl-4 offset-xl-2 my-auto text-center offset-lg-3 col-lg-6 offset-xl-0 offset-sm-1 col-sm-10 offset-md-2 col-md-8 offset-2 col-8">
                <img src="images/how-to-4.png" alt="" width="300" class="img-fluid" />
                <center>
                    <div style="font-weight: 500; font-size: 17px;">At the end of 8 weeks, choose a new topic!</div>
                </center>
            </div>
            <div class="col-xl-4  my-auto text-center col-lg-6 offset-lg-3 offset-xl-0 offset-sm-1 col-sm-10 offset-md-2 col-md-8 offset-2 col-8">
                <img src="images/how-to-5.png" alt="" width="300" class="img-fluid" />
                <center>
                    <div style="font-weight: 500; font-size: 17px;">Our psychologists are hard at work creating new topics for your child.</div>
                </center>
            </div>
        </div>
    </div>
    <div class="container-fluid HKSection1-3">
        <div class="HKQuoteLine">
            <div></div>
        </div>
        <center>
            <div class="container">
                <div id="carouselExampleIndicators1" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators1" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators1" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators1" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner" role="listbox">
                        <div class="carousel-item active">
                            <div style="max-width: 700px;">
                                <h5>
                                    <b>My daughter and I did the emotional understanding activity. It really helped me understand how she expresses emotions through facial expressions and she was able to tune in the next time she felt nervous without my help.</b>
                                </h5>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div style="max-width: 700px;">
                                <h5>
                                    <b>Before HeyKiddo&trade;, I didn’t know how to talk to my son when he asked tough questions about life. Having these tools makes me feel more confident as a parent.</b>
                                </h5>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div style="max-width: 700px;">
                                <h5>
                                    <b>HeyKiddo&trade; made talking with my kids an everyday part of our life. I love watching them learn to see their own potential as changemakers!</b>
                                </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </center>
    </div>
    <div class="container-fluid HKFAQ">
        <div class="container">
            <div class="accordion" style="padding:30 0;" id="accordionExample">
                <center>
                    <h3>
                        <b>Frequently Asked Questions</b>
                    </h3>
                </center>
                <br />
                <div class="card" style="padding-right: 30px;">
                    <div class="card-header" id="headingOneF">
                        <h2 class="mb-0">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionExample" href="#collapseOneF">What is HeyKiddo&trade;? </a>
                        </h2>
                    </div>
                    <div id="collapseOneF" class="collapse" aria-labelledby="headingOneF" data-parent="#accordionExample">
                        <div class="card-body">
                            <p>
                                HeyKiddo™ is a subscription-based texting app that provides curated conversation starters for you that help support your child’s development. Current topics include: ways to build and manage confidence, stress, relationships, social intelligence, self-control and thoughtful decisions. We’re always building new topics to discuss with your child! Please
                                <a style="all: unset;text-decoration: underline;color:blue;" href="mailto:support@hey-kiddo.com">contact us</a> if you have special requests!
                            </p>
                            <br />
                            <p>Children grow by watching others model new skills, having open communication about experiences related to these skills, and by engaging in activities that reinforce new skills ... so we have included all of these methods in HeyKiddo™, making it a playbook for their individual growth.</p>
                            <br />
                            <p>Although we will mainly communicate with you via text, all of your activities are accessible on the HeyKiddo™ parent portal, just in case you want to do them more than once, maybe with the whole family!</p>
                            <br />
                            <p>And we know as a parent you are super busy, so HeyKiddo™ is flexible and lets you choose the time of day that works best for you and your child to receive our conversation starters - whether on the way to school, in the middle of the day over lunch, in the afternoon after school, or before bedtime.</p>
                        </div>
                    </div>
                </div>
                <div class="card" style="padding-right: 30px;">
                    <div class="card-header" id="headingOne">
                        <h2 class="mb-0">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionExample" href="#collapseOne"> What will my family learn through HeyKiddo&trade;? </a>
                        </h2>
                    </div>
                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <div class="card-body">
                            <p>Each week, you’ll learn new ways of connecting with your child through conversation starters and activities that develop social, emotional and leadership skills vital for future success.</p>
                            <br />
                            <p>All of our conversation starters and activities are geared toward helping you and your child build critical skills by deepening your conversations with them regarding difficult issues that may arise day-to-day.</p>
                            <br />
                            <p>Sometimes kids aren’t as used to talking about these things as parents are … so although it may feel strange for some at first, stick with it! Opening up the lines of communication between you and your child will help them reach out and talk with you with more ease. When things arise - both good and bad in your child’s life - they will know they can come to you to talk about anything.</p>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h2 class="mb-0">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionExample" href="#collapseTwo"> Who created HeyKiddo&trade;? </a>
                        </h2>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body">
                            The creators are psychologists who specialize in leadership, social and emotional development.
                            <a style="all: unset;text-decoration: underline;color:blue;" target="_blank" href="https://www.linkedin.com/in/nicolelipkin/">Dr Nicole Lipkin</a> is the founder. Her expertise meets at the intersection of psychology and leadership. Our content and research team have the perfect combination of experience and training necessary to help children build a strong foundation of social, emotional and leadership skills.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree">
                        <h2 class="mb-0">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionExample" href="#collapseThree">I have some recommendations for HeyKiddo&trade;</a>
                        </h2>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                        <div class="card-body">
                            We are constantly looking for ways to improve the connection between caregivers and their children. Please drop us a line with feedback or suggestions at
                            <a style="all: unset;text-decoration: underline;color:blue;" href="mailto:support@hey-kiddo.com">support@hey-kiddo.com</a>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour">
                        <h2 class="mb-0">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionExample" href="#collapseFour">I’m having trouble with my HeyKiddo&trade; account!</a>
                        </h2>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                        <div class="card-body">
                            If you’d like to unsubscribe or have any other issue with your HeyKiddo™ Account, email us at
                            <a style="all: unset;text-decoration: underline;color:blue;" href="mailto:support@hey-kiddo.com">support@hey-kiddo.com</a>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFive">
                        <h2 class="mb-0">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionExample" href="#collapseFive">What is the Young Leader Project™ and how is it related to HeyKiddo&trade;? </a>
                        </h2>
                    </div>
                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                        <div class="card-body">
                            HeyKiddo™ is one of the products of Young Leader Project™, which is a company dedicated to building the leadership, social and emotional skills of children and their caregivers. Young Leader Project™ believes that the best way to help children learn these skills is through individual, parent-led and peer-led learning. HeyKiddo™ is the parent-led learning arm. Young Leader Project™ is also developing a mobile video game designed for children to learn leadership, social and emotional skills individually and with their peers. Caregivers will be able to monitor progress and join in on game play through conversation and multi-player gaming. This game is in development and coming soon!
                            <br />
                            <br />
                            Our evidence-based activities have been tested by hundreds of parents just like you! Young Leader Project has been awarded a National Science Foundation (NSF) Small Business Innovation Research (SBIR) to conduct research and development (R&amp;D) work on helping children develop leadership skills. This material is based upon work supported by the National Science Foundation under Grant No. 1842707
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="HKSectionB">
        <div class="HKSCHARS">
            <img src="images/Subscribe-Characters@2x.png" class="img-fluid float-right" width="300px" alt="" />
        </div>
        <div class="col-12" <?php if((isset($_SESSION["user"]["loggedin"])) && ($_SESSION["user"]["loggedin"])) echo 'hidden'; ?>>
            <center>
                <div class="HKsignupform flip-container mx-auto" id="flip-this">
                    <div class="flipper">
                        <div class="front">
                            <form id="landing-form">
                                <center>
                                    <h4>
                                        <b>Sign In</b>
                                    </h4>
                                    <p>
                                        New to HeyKiddo&trade;?
                                        <a href="#flip-this" class="flip-it" onclick="return false;" style="color: #55B588">
                                            <b>Create an account</b>
                                        </a>
                                    </p>

                                    <div class="form-group">
                                        <input type="email" class="form-control" id="HKemail" name="HKemail" placeholder="Email" />
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" id="HKpassword" name="HKpassword" placeholder="Password" />
                                    </div>
                                    <input type="hidden" name="rqstyp" value="ylplogin" />
                                    <button type="button" class="btn btn-lg HKSignupBtn" id="HKSignInBtn" style="margin-top: 10px; max-width: 250px;"> SIGN IN </button>
                                    <br />
                                    <br />
                                    <a href="#" id="forgotpass" onclick="return false;" style="color: #55B588">
                                        <b>Forgot your password?</b>
                                    </a>
                                    <br />
                                    <span class="help-block" style="color: red" hidden id="signinformhelp"></span>
                                </center>
                            </form>
                            <form id="forgot-form" class="text-center" style="margin-left:-25px;" hidden>
                                <center>
                                    <h4>
                                        <b>Password Assistance</b>
                                    </h4>
                                    <p> Enter the email address associated with your HeyKiddo&trade; account. </p>
                                </center>
                                <div class="form-group">
                                    <input type="email" class="form-control" id="HKforgotEmail" name="HKemail" placeholder="Email" />
                                </div>
                                <input type="hidden" name="rqstyp" value="ylpusrmaint" />
                                <input type="hidden" name="type" value="fpass" />
                                <button type="button" class="btn btn-lg HKSignupBtn" id="HKForgotPBtn" style="margin-top: 10px; max-width: 250px;">Continue</button>
                                <br />
                                <span class="help-block" style="color: red" hidden id="forgotformhelp"></span>
                                <span class="help-block" hidden id="forgotformdone"></span>
                            </form>
                        </div>
                        <div class="back">
                            <form id="HKlanding-form">
                                <center>
                                    <h3>
                                        <b>To get started, create an account</b>
                                    </h3>
                                    <p>
                                        Already have an account?
                                        <a href="#flip-this" class="flip-it" onclick="return false;" style="color: #55B588">
                                            <b>Sign In</b>
                                        </a>
                                    </p>
                                </center>
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="HKInputFName" id="HKInputFName" required placeholder="First Name" />
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="HKInputLName" id="HKInputLName" required placeholder="Last Name" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" name="HKInputEmail" id="HKInputEmail" required placeholder="Email" />
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control disabled" value="" name="HKInputPhone" id="HKInputPhone" data-format="(ddd) ddd-dddd" placeholder="Phone#" />
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" name="HKInputPassword" id="HKInputPassword" placeholder="Password" />
                                </div>
                                <input type="hidden" name="type" value="hksignup" />
                                <label class="custom-checkbox">
                                    <input class="form-check-input" type="checkbox" id="HKTermscheckbox" />
                                    <span>
                                        Accept the
                                        <a href="javascript:window.open('https://www.youngleaderproject.com/termssubscription.html', 'HeyKiddo&trade; Terms of Subscription', 'width=600,height=450');">Terms of Subscription</a>
                                    </span>
                                </label>
                                <button type="button" class="btn btn-lg HKSignupBtn" id="HKSignupBtn" style="margin-top: 10px;"> SIGN UP </button>
                                <span class="help-block" style="color: red;padding-bottom:5px" hidden id="signupformhelp"></span>
                            </form>
                        </div>
                    </div>
                </div>
            </center>
        </div>
        <center>
            <p style="color:white;">
                Available in English for subscribers in the US and Canada. Other languages and countries are coming soon.
                <a href="#" id="HKaddcountry" onclick="return false;">Let us know where to start!</a>
            </p>
        </center>
    </div>
    <footer style="position: relative;">
        <div class="container-fluid" style="padding: 20px;">
            <div class="row">
                <div class="col-12 my-auto">
                    <p style="text-align: center; margin: auto; padding-bottom: 5px;">© Young Leader Project®, LLC. All Rights Reserved.</p>
                    <p style="text-align: center; margin: auto; font-weight: 100; font-size: 12px; max-width: 550px; padding-bottom: 10px;">
                        HeyKiddo&trade; and
                        <a target="_blank" style="color:inherit; cursor:pointer;" href="https://youngleaderproject.com">Young Leader Project&trade;</a> do not provide medical or psychological advice, diagnosis, or treatment.
                        <a style="all: unset;text-decoration: underline;color:blue; cursor:pointer;" href="javascript:window.open('https://www.youngleaderproject.com/disclaimer.html', 'YLP Disclaimer', 'width=600,height=450');">See additional information</a>.
                    </p>
                    <div class="container-fluid pb-2 pt-2">
                        <div class="row mx-auto text-center" style="width: 85%;">
                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                                <a href="https://www.facebook.com/youngleaderproject/" target="_blank">
                                    <img width="50px" src="images/facebook.png" alt="YLP Facebook" />
                                </a>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                                <a href="https://www.instagram.com/youngleaderproject/" target="_blank">
                                    <img width="50px" src="images/instagram.png" alt="YLP Facebook" />
                                </a>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                                <a href="https://www.linkedin.com/company/young-leader-project/" target="_blank">
                                    <img width="50px" src="images/linkedin.png" alt="YLP Facebook" />
                                </a>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                                <a href="https://twitter.com/YoungLeaderPro" target="_blank">
                                    <img width="50px" src="images/twitter.png" alt="YLP Facebook" />
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid">
                        <div class="mx-auto text-center">
                            <a style="font-size: 12px; color: inherit;white-space:normal" target="_blank" href="https://www.youngleaderproject.com">Young Leader Project Website</a>
                        </div>
                        <div class="mx-auto text-center">
                            <a style="font-size: 12px; color: inherit;" href="javascript:window.open('https://www.youngleaderproject.com/cookiepolicy.html', 'YLP Cookie Policy', 'width=600,height=450');">Cookie Policy</a>
                        </div>
                        <div class="mx-auto text-center">
                            <a style="font-size: 12px; color: inherit;" href="javascript:window.open('https://www.youngleaderproject.com/privacypolicy.html', 'YLP Privacy Policy', 'width=600,height=450');">Privacy Policy</a>
                        </div>
                        <div class="mx-auto text-center">
                            <a style="font-size: 12px; color: inherit;" href="javascript:window.open('https://www.youngleaderproject.com/termsconditions.html', 'YLP Terms & Conditions;', 'width=600,height=450');">Terms &amp; Conditions</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="https://cdn.rawgit.com/nnattawat/flip/master/dist/jquery.flip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="js/HKLandingJS.js"></script>
    <script src="js/bootstrap-formhelpers.min.js"></script>
    <script src="js/hkglobals.js"></script>
    <script src="js/bootstrap-select-country.min.js"></script>
    <script type="text/javascript">
    (function(e,t,o,n,p,r,i){e.visitorGlobalObjectAlias=n;e[e.visitorGlobalObjectAlias]=e[e.visitorGlobalObjectAlias]||function(){(e[e.visitorGlobalObjectAlias].q=e[e.visitorGlobalObjectAlias].q||[]).push(arguments)};e[e.visitorGlobalObjectAlias].l=(new Date).getTime();r=t.createElement("script");r.src=o;r.async=true;i=t.getElementsByTagName("script")[0];i.parentNode.insertBefore(r,i)})(window,document,"https://diffuser-cdn.app-us1.com/diffuser/diffuser.js","vgo");
    vgo('setAccount', '25992310');
    vgo('setTrackByDefault', true);

        vgo('process');
    <?php echo "window.ylp.dev='$dev'";?>
    </script>

</body>
</html>