<?php
try {
    session_start();
}
catch(\Exception $ex) {
    //  file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/heykiddosign.txt' , print_r($_SESSION,true)."\r\n", FILE_APPEND);
}
error_reporting( E_ALL );
$linkpos = strpos( strtolower( $_SERVER[ "DOCUMENT_ROOT" ] ), "heykiddo" );
if ( $linkpos > 0 ) {
  $_SERVER[ "DOCUMENT_ROOT" ] = substr( $_SERVER[ "DOCUMENT_ROOT" ], 0, $linkpos - 1 );
}
require_once str_replace( '\\', '/', $_SERVER[ "DOCUMENT_ROOT" ] ) . '/PHP/shared/auto_load.php';

use PHP\CLASSES\CYLPDB;
use PHP\CLASSES\CEMAIL;
use PHP\CLASSES\CUSER;

$input_err = "";
$email = "";
$phone = "";
$username = "";
$success = false;
if ( $_SERVER[ "REQUEST_METHOD" ] == "POST" ) {
    if(isset($_REQUEST['rqstyp'])){
        if($_REQUEST['rqstyp'] === "ylpbetasignup"){
            if ( !empty( trim( $_POST[ "ylpname" ] ) ) ) {
                $username = trim( $_POST[ "ylpname" ] );
            }
            if ( !empty( trim( $_POST[ "ylpemail" ] ) ) ) {
                $email = trim( $_POST[ "ylpemail" ] );
            }
			$badphone = false;
			if ( isset($_POST[ "ylpphone" ])) {
				if(strlen(trim( $_POST[ "ylpphone" ] )) === 10){
					$phone = trim( $_POST[ "ylpphone" ] );
                }else{
					$badphone = true;
                }
            }
            if(strlen($email) === 0 || strlen($username) === 0 || $badphone === true){
                $input_err = "<font color=red><b>You need to first correct your invalid inputs before you can submit.<b></font>";
            }else{
				$user = new CUSER();
				$country = $user->getLocationInfoByIp()['country'];
                $country = ($country !== '' ? $country : 'US');
                $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
                $loada = Array();
                $loada['user_name'] = "'$username'";
                $loada['user_email'] = "'$email'";
				$loada['user_country'] = "'$country'";
                $dba->ADD_RECORD('ylpbetasub', $loada);
                $emailclass = new CEMAIL($username,$email);
                $emailclass->BetaSignupHK($username,$email);
                $url = YLP_ACURL;
                $params = array(
                    'api_key'      => YLP_ACKEY,
                    'api_action'   => 'contact_add',
                    'api_output'   => 'serialize'
                );
                $name = explode(" ",$username);
                $fname = isset($name[0]) ? $name[0] : '';
                $lname = isset($name[1]) ? $name[1] : '';
                $post = array(
                    'email'                    => $email,
                    'first_name'               => $fname,
                    'last_name'                => $lname,
                    'tags'                     => 'Beta Signup List',
                    'ip4'                      => $user->getUserIp(),
                    "p[1]"                => 1
                );

                $query = "";
                foreach( $params as $key => $value ) $query .= urlencode($key) . '=' . urlencode($value) . '&';
                $query = rtrim($query, '& ');

                $data = "";
                foreach( $post as $key => $value ) $data .= urlencode($key) . '=' . urlencode($value) . '&';
                $data = rtrim($data, '& ');

                $url = rtrim($url, '/ ');
                if ( !function_exists('curl_init') ) die('CURL not supported. (introduced in PHP 4.0.2)');
                if ( $params['api_output'] == 'json' && !function_exists('json_decode') ) {
                    // die('JSON not supported. (introduced in PHP 5.2.0)');
                }
                $api = $url . '/admin/api.php?' . $query;
                $request = curl_init($api);
                curl_setopt($request, CURLOPT_HEADER, 0);
                curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($request, CURLOPT_POSTFIELDS, $data);
                curl_setopt($request, CURLOPT_FOLLOWLOCATION, true);

                $response = (string)curl_exec($request);
                curl_close($request);

                if ( !$response ) {
                    file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/cuser.txt' , "active campain add user fail $email\r\n", FILE_APPEND);
                    // die('Nothing was returned. Do you have a connection to Email Marketing server?');
                }
                if (DEBUG == 1)
                {
                    $result = unserialize($response);
                    file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/cuser.txt' , print_r($result,true)."\r\n", FILE_APPEND);
                    file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/cuser.txt' , print_r($response,true)."\r\n", FILE_APPEND);
                    file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/cuser.txt' , print_r($post,true)."\r\n", FILE_APPEND);
                }
                $success = true;
            }
        }
    }
}

?>
<html lang="en">
<head>
        <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NLW5ZB7');</script>
<!-- End Google Tag Manager -->
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>HeyKiddo&trade; Portal | Home</title>
<meta name="description" content="
HeyKiddo&trade; develops the leaders of tomorrow by acquainting them with essential leadership skills today." />
<!-- Bootstrap -->
<meta property='og:title' content="HeyKiddo&trade;" />
<link rel="icon" href="images/Hey-Kiddo-favicon.ico" type="favicon/ico" />
    <meta property='og:image' content="https://hey-kiddo.com/images/HeyKiddoLanding.PNG" />
<meta property='og:description' content="HeyKiddo&trade; develops the leaders of tomorrow by acquainting them with essential leadership skills today." />
<meta property='og:url' content="https://hey-kiddo.com" />
<link rel="icon" href="images/favicon.ico" type="favicon/ico" />
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet" />
<link rel="stylesheet" href="css/HKLandingScripts.css" />
</head>

<body onload="checkCookiePolicy()">
        <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NLW5ZB7"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <!--Modal: modalCookie-->
    <div class="modal fade" id="cookieModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true" data-backdrop="true">
        <div class="modal-dialog" role="document">
            <!--Content-->
            <div class="modal-content">
                <!--Body-->
                <div class="modal-body">
                    <center>
                        <div class="row">

                            <p class="p-3">Hey there! We use cookies on our site so we can make sure we are delivering the best possible web experience. We also use them to understand our website traffic. You can learn more about cookies, and how to disable them, by checking out our cookie policy. By clicking "Ok" on this banner, it means that you are okay with the use of cookies unless you have disabled them.</p>
                        </div>

                        <a type="button" href="javascript:window.open('https://www.youngleaderproject.com/cookiepolicy.html', 'YLP Cookie Policy', 'width=600,height=450');" class="btn btn-primary">
                            Learn more
                        </a>
                        <a type="button" class="btn btn-outline-primary waves-effect" onclick="setCookiePolicy();" data-dismiss="modal">Ok, thanks</a>
                    </center>
                </div>
            </div>
            <!--/.Content-->
        </div>
    </div>
    <!--Modal: modalCookie-->
<div class="modal fade" id="HKEULA" tabindex="-1" role="dialog" aria-labelledby="HKEULAlLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">HeyKiddo&trade; Portal End User License Agreement</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      </div>
      <div class="modal-body">
        <p>This is where the HeyKiddo&trade; EULA will be</p>
      </div>
      <div class="modal-footer">
        <center>
          <button type="button" class="btn btn-primary" data-dismiss="modal" id="HKEULAaccept">Accept</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </center>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid HKSection1"  style="position: relative;">
  <div class="row">
    <div class="col-xl-6"> <a class="logoHeader" href="./"> <img src="images/logo-horizontal-tagline.png" alt="Hey Kiddo Logo" class="img-fluid" /> </a> </div>
    <div class="col-xl-6 my-auto text-center" hidden> 
		HeyKiddo&trade; Portal
      <button type="button" id="HKlanding-join" class="btn btn-lg HKSignupBtn" style="max-width: 300px; margin-left: 40px;">START FREE TRIAL</button>
    </div>
  </div>
  <br>
  <br>
  <br>
  <div class="container" style="position: relative;z-index: 1;">
    <div class="row">
      <div class="col-10 offset-1 col-sm-10 offset-sm-1 offset-md-0 offset-xl-1 offset-lg-1 col-xl-11 col-lg-10 col-md-12">
        <h2>Talk to your kid like their future depends on it.</h2>
        <p>Ever wish there was a user manual for your kid? Good news! You’ve found it. HeyKiddo™ takes the guesswork out of developing your child’s social-emotional and leadership skills. We send curated conversation starters (developed by psychologists!) and activities straight to your phone so you can spend less time Googling and more time being an awesome parent.</p>
        <ul class="custom-list">
          <li>Curated conversation starters</li>
          <li>Evidenced-based activities developed by psychologists</li>
          <li>Tips for how to talk to your child about tough topics like bullying, self-esteem, and stress</li>
          <li>A HeyKiddo™ portal to keep all your activities in one place</li>
          <li>Expert advice on how to talk to your child about tough topics in the news</li>
        </ul>
        <br>
        <button type="button" id="HKlanding-joinold" class="btn btn-lg HKSignupBtn" style="max-width: 400px;">Join the Waitlist!</button>
		  <center><div style="max-width: 350px; font-weight: 100; font-size: 12px; margin-top: 70px; position: relative;">HeyKiddo&trade; Portal powered by the Young Leader Project</div></center>
      </div>
    </div>
  </div>
	<div class="HKfamily"><img src="images/Hero-Characters@2x.png" class="img-fluid float-right" width="500px" alt="" /> </div>
</div>
<div class="container-fluid HKSection1-1" hidden>
	<div class="HKShapeR"><img src="images/Shapes-R-long@2x.png" class="img-fluid float-right" width="1000px" alt="" /> </div>
	<div class="HKShapeL"><img src="images/Shapes-L-long@2x.png" class="img-fluid float-right" width="1000px" alt="" /> </div>
  <center>
    <div style="max-width: 575px;">
      <h5> <b>Subscribe now to get one free week...</b> </h5>
    </div>
	</center>
    <div class="row text-center HKSection1-1-1">
      <div class="card mb-4 mx-auto box-shadow">
        <center>
          <div class="card-body">
            <h1 class="card-title pricing-card-title"> <b>$0</b> </h1>
            <h6> More empowered parenting for less than a cup of coffee <br />
              <br />
              Start your free trial today </h6>
            <button type="button" id="HKlanding-join3" style="max-width: 350px;" class="btn btn-lg HKSignupBtn"> TRY FOR FREE </button>
            <div style="max-width: 350px; font-weight: 100; font-size: 12px; margin-top: 10px;">After your no obligation 7-day trial ends, you will be auto-enrolled in HeyKiddo™ for $9.99 per month. Cancel anytime.</div>
          </div>
        </center>
      </div>
    </div>
</div>
<div class="container HKSection1-2">
  <div class="row">
    <div class="col-xl-4 my-auto text-center offset-lg-3 col-lg-6 offset-xl-0 offset-sm-1 col-sm-10 offset-md-2 col-md-8 offset-2 col-8"> <img src="images/callout1.png" alt="" width="300" class="img-fluid" />
      <center>
        <div style="font-weight: 500; font-size: 17px;">Creative activities make social, emotional and leadership development fun and easy</div>
      </center>
    </div>
    <div class="col-xl-4 my-auto text-center col-lg-6 offset-lg-3 offset-xl-0 offset-sm-1 col-sm-10 offset-md-2 col-md-8 offset-2 col-8"> <img src="images/callout2.png" alt="" width="300" class="img-fluid" />
      <center>
        <div style="font-weight: 500; font-size: 17px;">Conversational tips, curated by psychologists, help open up the lines of communication between you and your child</div>
      </center>
    </div>
    <div class="col-xl-4 my-auto text-center col-lg-6 offset-lg-3 offset-xl-0 offset-sm-1 col-sm-10 offset-md-2 col-md-8 offset-2 col-8"> <img src="images/callout3.png" alt="" width="300" class="img-fluid" />
      <center>
        <div style="font-weight: 500; font-size: 17px;">Covers everything from big world news to everyday challenges</div>
      </center>
    </div>
  </div>
</div>
<div class="container-fluid HKSection1-3">
  <div class="HKQuoteLine">
    <div></div>
  </div>
  <center>
    <div class="container">
      <div id="carouselExampleIndicators1" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators1" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators1" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators1" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
          <div class="carousel-item active">
            <div style="max-width: 700px;">
              <h5> <b>My daughter and I did the emotional understanding activity. It really helped me understand how she expresses emotions through facial expressions and she was able to tune in the next time she felt nervous without my help.</b> </h5>
            </div>
          </div>
          <div class="carousel-item">
            <div style="max-width: 700px;">
              <h5> <b>Before HeyKiddo&trade;, I didn’t know how to talk to my son when he asked tough questions about life. Having these tools makes me feel more confident as a parent.</b> </h5>
            </div>
          </div>
          <div class="carousel-item">
            <div style="max-width: 700px;">
              <h5> <b>HeyKiddo&trade; made talking with my kids an everyday part of our life. I love watching them learn to see their own potential as changemakers!</b> </h5>
            </div>
          </div>
        </div>
      </div>
    </div>
  </center>
	</div>
	<div class="container-fluid HKFAQ">
  <div class="container" >
    <div class="accordion" style="padding:30 0;" id="accordionExample">
      <center>
        <h3> <b>Frequently Asked Questions</b> </h3>
      </center>
      <br />
      <div class="card">
        <div class="card-header" id="headingOne">
          <h2 class="mb-0"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionExample" href="#collapseOne"> What will my family learn through  HeyKiddo&trade;? </a> </h2>
        </div>
        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
          <div class="card-body">Each week, you will learn new ways of connecting with your child through conversation starters and activities that develop vital social-emotional and leadership skills. Core competencies like self-awareness, self-management, social-awareness, relationship skills and decision making skills are the strongest predictors of future success. All of our conversation starters and activities are geared toward helping you help your child build these core competencies, as well as deepen your conversations around difficult day-to-day issues that may arise. <br>
			Although it may feel strange at first, stick with it! Opening up the lines of communication on a daily basis around these core competencies and day-to-day issues has the potential to build a strong foundation between you and your child so that they know they can talk with you about anything. 
			</div>
        </div>
      </div>
      <div class="card">
        <div class="card-header" id="headingTwo">
          <h2 class="mb-0"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionExample" href="#collapseTwo"> Who created HeyKiddo&trade;? </a> </h2>
        </div>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
          <div class="card-body">The creators are psychologists who specialize in leadership, social and emotional development. <a target="_blank" href="https://www.linkedin.com/in/nicolelipkin/">Dr Nicole Lipkin</a> is the founder. Her expertise meets at the intersection of psychology and leadership. Our content and research team have the perfect combination of experience and training necessary to help children build a strong foundation of social, emotional and leadership skills. </div>
        </div>
      </div>
      <div class="card">
        <div class="card-header" id="headingThree">
          <h2 class="mb-0"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionExample" href="#collapseThree">I have some recommendations for HeyKiddo&trade;</a> </h2>
        </div>
        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
          <div class="card-body">We are constantly looking for ways to improve the connection between caregivers and their children. Please drop us a line with feedback or suggestions at <a HREF="mailto:support@youngleaderproject.com">support@youngleaderproject.com</a></div>
        </div>
      </div>
      <div class="card">
        <div class="card-header" id="headingFour">
          <h2 class="mb-0"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionExample" href="#collapseFour">I’m having trouble with my HeyKiddo&trade; account!</a> </h2>
        </div>
        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
          <div class="card-body">If you’d like to unsubscribe or have any other issue with your HeyKiddo™ Account, email us at <a HREF="mailto:support@youngleaderproject.com">support@youngleaderproject.com</a></div>
        </div>
        </div>
	  <div class="card">
		  <div class="card-header" id="headingFive">
          <h2 class="mb-0"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionExample" href="#collapseFive">What is the Young Leader Project™ and how is it related to HeyKiddo&trade;? </a> </h2>
        </div>
		  <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
          <div class="card-body">HeyKiddo™ is one of the products of Young Leader Project™, which is a company dedicated to building the leadership, social and emotional skills of children and their caregivers. Young Leader Project™ believes that the best way to help children learn these skills is through individual, parent-led and peer-led learning. HeyKiddo™ is the parent-led learning arm. Young Leader Project™ is also developing a mobile video game designed for children to learn leadership, social and emotional skills individually and with their peers. Caregivers will be able to monitor progress and join in on game play through conversation and multi-player gaming. This game is in development and coming soon!
			<br>
			<br>
			Our evidence-based activities have been tested by hundreds of parents just like you! Young Leader Project has been awarded a National Science Foundation (NSF) Small Business Innovation Research (SBIR) to conduct research and development (R&amp;D) work on helping children develop leadership skills. This material is based upon work supported by the National Science Foundation under Grant No. 1842707
			</div>
        </div>
      </div>
    </div>
  </div>
	</div>
<div class="container-fluid HKSectionB" id="HKlanding-form">
	<div class="HKSCHARS"><img src="images/Subscribe-Characters@2x.png" class="img-fluid float-right" width="300px" alt="" /> </div>
	<div class="HKsignupform" <?php if($success) echo "hidden"; ?>>
		<form class="landing-form" action="." method="post">
			<center>
				<div style="max-width: 700px; padding-bottom: 10px">
					<h5>Set your child up for a bright future, one conversation at a time. Sign up to get notified for our February 2020 launch!</h5>
				</div>
			</center>
			<div class="form-group">
				<input type="text" class="form-control" name="ylpname" placeholder="Name" />
			</div>
			<div class="form-group">
				<input type="email" class="form-control" name="ylpemail" placeholder="Email" />
			</div>
			<input type="hidden" name="rqstyp" value="ylpbetasignup" />
			<span class="help-block">
				<?php echo $input_err; ?>
			</span>
			<button type="submit" class="btn btn-lg HKSignupBtn" style="margin-top: 10px; max-width: 250px;">
                Join the Waitlist!
			</button>
		</form>
		<!--<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" id="HKlanding-form">
			<center>
				<div style="max-width: 700px; padding-bottom: 10px">
					<h5>Set your young leader up for a bright future. Start your free trial today!</h5>
				</div>
			</center>
			<div class="form-group">
				<input type="text" class="form-control" id="HKInputName" required placeholder="Name" />
			</div>
			<div class="form-group">
				<input type="email" class="form-control" id="HKInputEmail" required placeholder="Email" />
			</div>
			<div class="form-group">
				<input type="tel" class="form-control" id="HKInputPhone" placeholder="Phone # to recieve your texts" />
			</div>
			<span class="help-block" style="color: red" hidden id="paypalformhelp"></span>
			<input type="hidden" name="cmd" value="_s-xclick" />
			<input type="hidden" name="hosted_button_id" value="5AS3W4TFAV3FG" />
			<input type="hidden" name="custom" id="paypalcustombtn" />
			<button type="button" class="btn btn-lg HKSignupBtn" style="margin-top: 10px;" id="HKSignUpBtn">
				START FREE TRIAL
			</button>
		</form>-->
	</div>
	<div class="HKsignupsuccess" <?php if(!$success) echo "hidden"; ?>>
		<div class="container">
			<center>
				<h3 style="color: white">
					<b>Success!</b>
				</h3>
			</center>
			<center>
				<h4 style="max-width: 600px;">Check your inbox for more information and give yourself a high five for proactive parenting!</h4>
			</center>
		</div>
	</div>
	<br>
	<br>
</div>
<footer style="position: relative;">
  <div class="container-fluid" style="padding: 20px;">
    <div class="row">
      <div class="col-12 my-auto">
        <p style="text-align: center; margin: auto; padding-bottom: 5px;">© Equilibria Leadership Consulting, LLC. &amp; Young Leader Project All Rights Reserved.</p>
        <p style="text-align: center; margin: auto; font-weight: 100; font-size: 12px; max-width: 550px; padding-bottom: 10px;">HeyKiddo&trade; and <a target="_blank" style="color:inherit;" href="https://youngleaderproject.com">Young Leader Project&trade;</a> do not provide medical or psychological advice, diagnosis, or treatment. <a style="color:inherit;" href="javascript:window.open('https://www.youngleaderproject.com/disclaimer.html', 'YLP Disclaimer', 'width=600,height=450');">See additional information</a>.</p>
		  
	  <div class="container-fluid pb-2">
		  <div class="row mx-auto text-center" style="width: 85%;">
			  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3"><a href="https://www.facebook.com/youngleaderproject/" target="_blank"><img width="50px" src="images/facebook.png" alt="YLP Facebook"></a></div>
			  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3"><a href="https://www.instagram.com/youngleaderproject/" target="_blank"><img width="50px" src="images/instagram.png" alt="YLP Facebook"></a></div>
			  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3"><a href="https://www.linkedin.com/company/young-leader-project/" target="_blank"><img width="50px" src="images/linkedin.png" alt="YLP Facebook"></a></div>
			  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3"><a href="https://twitter.com/YoungLeaderPro" target="_blank"><img width="50px" src="images/twitter.png" alt="YLP Facebook"></a></div>
		  </div>
        </div>
        <div class="container-fluid pb-2">
        <div class="row mx-auto text-center">
          <div class="col-md-3 my-auto col-sm-3"> <a style="font-size: 12px; color: inherit;white-space:normal" target="_blank" href="https://www.youngleaderproject.com">Young Leader Project Website</a></div>
          <div class="col-md-3 my-auto col-sm-3"> <a style="font-size: 12px; color: inherit;" href="javascript:window.open('https://www.youngleaderproject.com/cookiepolicy.html', 'YLP Cookie Policy', 'width=600,height=450');">Cookie Policy</a> </div>
          <div class="col-md-3 my-auto col-sm-3"> <a style="font-size: 12px; color: inherit;" href="javascript:window.open('https://www.youngleaderproject.com/privacypolicy.html', 'YLP Privacy Policy', 'width=600,height=450');">Privacy Policy</a> </div>
          <div class="col-md-3 my-auto col-sm-3"> <a style="font-size: 12px; color: inherit;" href="javascript:window.open('https://www.youngleaderproject.com/termsconditions.html', 'YLP Terms & Conditions;', 'width=600,height=450');">Terms &amp; Conditions</a> </div>
        </div>
        </div>
      </div>
    </div>
  </div>
</footer>
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script> 
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script> 
<script src="js/HKLandingJS.js"></script> 
<script src="js/hkglobals.js"></script>
    <?php
        if($success){
            echo '<script type="text/javascript">
                          document.getElementById("HKlanding-form").scrollIntoView(true)
                      </script>';
          }
    ?>
    <script type="text/javascript">
        (function (e, t, o, n, p, r, i) { e.visitorGlobalObjectAlias = n; e[e.visitorGlobalObjectAlias] = e[e.visitorGlobalObjectAlias] || function () { (e[e.visitorGlobalObjectAlias].q = e[e.visitorGlobalObjectAlias].q || []).push(arguments) }; e[e.visitorGlobalObjectAlias].l = (new Date).getTime(); r = t.createElement("script"); r.src = o; r.async = true; i = t.getElementsByTagName("script")[0]; i.parentNode.insertBefore(r, i) })(window, document, "https://diffuser-cdn.app-us1.com/diffuser/diffuser.js", "vgo");
        vgo('setAccount', '25992310');
        vgo('setTrackByDefault', true);

        vgo('process');
    </script>
</body>
</html>