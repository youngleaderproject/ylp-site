var currentflip = "";
$(document).ready(function () {
    $(".HKInputPhone").val();
    var frontflip = false;
	$(".HKSignIn,#HKlanding-join,#HKlanding-join2,#HKlanding-join3,#HKlanding-joinold").click(function (e) {
		$(".navbar-nav a").removeClass("HKNavselected");
		$(this).addClass("HKNavselected");
		if ($(this).attr('href') > "" ) {
			return;
		}
		if (e.currentTarget.id.indexOf("HKlanding-joinold") > -1) {
			$('html, body').animate({
				scrollTop: $("#HKlanding-form").offset().top
			}, 1000);
			return false;
		}
		window.dataLayer.push({
			eventCategory: "hk signup button click",
			eventAction: "hk signup scroll",
			eventLabel: e.currentTarget.id,
			event: "trackEvent"
		});
		if (e.currentTarget.id.indexOf("HKlanding-join") > -1) {
			flipitfunc("SU");
		} else {
			flipitfunc("");
		}

        $('html, body').animate({
			scrollTop: $("#flip-this").offset().top
        }, 1000);
        return false;
	});
    $("#HKEULAaccept").click(function (event) {
        $("#HKlanding-form").submit();
    });

	$('.flip-it').click(function () {
		$('.flip-container').toggleClass('hover');
	});
/*	$('.navbar-nav a').on('click', function () {
		$('.navbar-nav a').removeClass('HKNavselected');
		$(this).addClass('HKNavselected');
	});*/
	

	$("#HKSignInBtn").click(function (event) {
		event.preventDefault();
		var emailfield = $("#HKemail").val().trim();
		var passwordfield = $('#HKpassword').val().trim();
		var signinhelp = $('#signinformhelp');
		$(".loading").attr("hidden", false);
		if (passwordfield !== "" && emailfield !== "") {
			signinhelp.attr("hidden", true);
			$.post(window.ylp.Url, {
				rqstyp: "ylplogin", usrname: emailfield, passw: passwordfield, signin: 'Y', dev: window.ylp.dev
			}).done(function (data) {
				if (data.success && data.isActive) {
					window.dataLayer.push({
						eventCategory: "form submission",
						eventAction: "hk signin button click",
						eventLabel: "HK Sign In",
						event: "trackEvent"
					});
					$.redirectPost('/portal/', { dev: window.ylp.dev });
					return;
				} else {
					$(".loading").attr("hidden", true);
					signinhelp.html("<b>" + data.statusmsg + "</b>");
					signinhelp.attr("hidden", false);
				}
			});
		} else {
			$(".loading").attr("hidden", false);
			signinhelp.html("<b>Please fill out all required information</b>");
			signinhelp.attr("hidden", false);
			return;
		}
	});

	$("#HKSignupBtn").click(function (event) {
		var signuphelp = $('#signupformhelp');
		var $inputs = $('#HKlanding-form :input');
		var flag = false;
		var parms = {
			rqstyp: 'ylpusrmaint'
		};
		signuphelp.attr("hidden", true);
		$(".loading").attr("hidden", false);
		if(!$("#HKTermscheckbox").is(":checked")){
			signuphelp.html("<b>Please accept Terms of Subscription.</b>");
			signuphelp.attr("hidden", false);
			$(".loading").attr("hidden", true);
			return;
		}
		$inputs.each(function () {
			if (this.name > "") {
				if ($(this).val() > "") {
					parms[this.name] = $(this).val();
				} else {
					signuphelp.html("<b>Please fill in all required fields.</b>");
					signuphelp.attr("hidden", false);
					$(".loading").attr("hidden", true);
					flag = true;
					return;
				}
			}
		});
		if (flag) return;
		if (parms.HKInputPhone.length > 0 && !phonenumber(parms.HKInputPhone)) {
			signuphelp.html("<b>Please provide a 10 digit phone number</b>");
			signuphelp.attr("hidden", false);
			$(".loading").attr("hidden", true);
			return;
		}
		parms.dev = window.ylp.dev;
		parms.hktimezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
		$.post(window.ylp.Url, parms).done(function (data) {
			if (data === "EXISTS") {
				signuphelp.html("<b>User already exists</b>");
				signuphelp.attr("hidden", false);
				$(".loading").attr("hidden", true);
				return;
			} else if (data === "PHONEEXISTS") {
				signuphelp.html("<b>Phone already exists</b>");
				signuphelp.attr("hidden", false);
				$(".loading").attr("hidden", true);
				return;
			} else {
				window.dataLayer.push({
					eventCategory: "form submission",
					eventAction: "hk signup button click",
					eventLabel: "HK Landing Signup",
					event: "trackEvent"
				});
				$.redirectPost('/sign/', { dev: window.ylp.dev });
				return;
			}
		});
	});

	$("#forgotpass").click(function () {
		$("#landing-form").attr("hidden", true);
		$("#forgot-form").attr("hidden", false);
	});

	$("#HKForgotPBtn").click(function (event) {
		var signuphelp = $('#forgotformhelp');
		var signupdone = $('#forgotformdone');
		var $inputs = $('#forgot-form :input');
		var flag = false;
		var parms = {
			rqstyp: 'ylpusrmaint'
		};
		signuphelp.attr("hidden", true);
		signupdone.attr("hidden", true);
		$(".loading").attr("hidden", false);
		$inputs.each(function () {
			if (this.name > "") {
				if ($(this).val() > "") {
					parms[this.name] = $(this).val();
				} else {
					signuphelp.html("<b>Please fill in all required fields.</b>");
					signuphelp.attr("hidden", false);
					$(".loading").attr("hidden", true);
					flag = true;
					return;
				}
			}
		});
		if (flag) return;
		parms.dev = window.ylp.dev;
		$.post(window.ylp.Url, parms).done(function (data) {
			if (data === "DONE") {
				signupdone.html("<b>Please check email for password.</b>");
				signupdone.attr("hidden", false);
				$(".loading").attr("hidden", true);
				setTimeout(function () {
					$("#landing-form").attr("hidden", false);
					$("#forgot-form").attr("hidden", true);
					$('#forgotformdone').attr("hidden", true);
				}, 5000);
				return;
			} else {
				signuphelp.html("<b>User doesnt exist.</b>");
				signuphelp.attr("hidden", false);
				$(".loading").attr("hidden", true);
				return;
			}
		});
	});

	$("#HKaddcountry").click(function (event) {
		$('#addCountryModal').modal('toggle');
	});

	$("#HKaddcountrybtn").click(function (event) {
		var signuphelp = $('#addcountryformhelp');
		var parms = {
			rqstyp: "ylpemail",
			type: "addcountry",
			country: $(".countrypicker option:selected").text(),
			email: $("#HKaddcountryemail").val(),
			language: $("#HKcountrylanguage").val()
		};
		$(".loading").attr("hidden", false);
		if (parms.email > "") {
			signuphelp.attr("hidden", true);
			signuphelp.css('color', 'red');
			parms.dev = window.ylp.dev;
			$.post(window.ylp.Url, parms).done(function (data) {
				if (data === "DONE") {
					signuphelp.css('color', 'none');
					signuphelp.html("<b>Thank you for your feedback! We will be in contact shortly.</b>");
					signuphelp.attr("hidden", false);
					$(".loading").attr("hidden", true);
					setTimeout(function () {
						signuphelp.attr("hidden", true);
						$('#addCountryModal').modal('toggle');
					}, 5000);
					return;
				} else {
					signuphelp.html('<b>Submit failed. Please contact <a HREF="mailto:support@hey-kiddo.com">support@hey-kiddo.com.</a></b>');
					signuphelp.attr("hidden", false);
					return;
				}
			});
		} else {
			signuphelp.html('<b>Please fill in required fields.</a></b>');
			signuphelp.attr("hidden", false);
			$(".loading").attr("hidden", true);
		}
		
	});
	$.extend(
		{
			redirectPost: function (location, args) {
				var form = $('<form></form>');
				form.attr("method", "post");
				form.attr("action", location);

				$.each(args, function (key, value) {
					var field = $('<input></input>');

					field.attr("type", "hidden");
					field.attr("name", key);
					field.attr("value", value);

					form.append(field);
				});
				$(form).appendTo('body').submit();
			}
		});
});

function flipitfunc(type) {
	if (currentflip !== type) {
		$('.flip-container').toggleClass('hover');
		currentflip = type;
	}
	return false;
}

function setCookiePolicy() {
    document.cookie = "cookiePolicy=true;";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function checkCookiePolicy(cookiename) {
    var cookie = getCookie("cookiePolicy");
    if (cookie === "") {
        $('#cookieModal').modal('toggle');
    }
}

function phonenumber(inputtxt) {
	var phoneno = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/;
	if (inputtxt.match(phoneno)){
		return true;
	}
	else {
		return false;
	}
}