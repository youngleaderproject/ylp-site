var currentflip = "front";

$(document).ready(function () {
	
	var phoneinput = document.querySelector("#phone");
	var iti = window.intlTelInput(phoneinput, {
		dropdownContainer: document.body,
		separateDialCode: true,
		utilsScript: "js/utils.js",
		onlyCountries: ['us', 'au','ca']
	});
	$(".iti").css("width", "100%");
	var currenttab = $("#maincontainer");
	var frontflip = false;
	$('.navbar-nav>li>a').on('click', function () {
		$('.navbar-collapse').collapse('hide');
	});
	$(".HKSignIn,#HKlanding-join,#HKlanding-join2,#HKlanding-join3,#HKlanding-joinold,.HKSignupBtn").click(function (e) {
		if ($(this).attr('href') > "" || e.currentTarget.id.indexOf("HKSignInBtn") > -1 || e.currentTarget.id.indexOf("HKForgotPBtn") > -1) {
			return;
		}
		if (e.currentTarget.id.indexOf("HKlanding-joinold") > -1) {
			$('html, body').animate({
				scrollTop: $(".HKsignupform").offset().top
			}, 1000);
			return false;
		}
		window.dataLayer.push({
			eventCategory: "hk signup button click",
			eventAction: "hk signup scroll",
			eventLabel: e.currentTarget.id,
			event: "trackEvent"
		});
		
		if (e.currentTarget.id.indexOf("HKlanding-join") > -1 || $(e.target).hasClass('HKSignupBtn')) {
			flipitfunc("back");
		} else {
			flipitfunc("front");
		}

        $('html, body').animate({
			scrollTop: $(".HKsignupform").offset().top
        }, 1000);
        return false;
	});
    $("#HKEULAaccept").click(function (event) {
        $("#HKlanding-form").submit();
    });

	$('.flip-it').click(function () {
		flipitfunc(currentflip === "front" ? "back" : "front");
	});

	$("#HKSignInBtn").click(function (event) {
		event.preventDefault();
		var emailfield = $("#HKemail").val().trim();
		var passwordfield = $('#HKpassword').val().trim();
		var signinhelp = $('#signinformhelp');
		$(".loading").attr("hidden", false);
		if (passwordfield !== "" && emailfield !== "") {
			signinhelp.attr("hidden", true);
			$.post(window.ylp.Url, {
				rqstyp: "ylplogin", usrname: emailfield, passw: passwordfield, signin: 'Y', dev: window.ylp.dev
			}).done(function (data) {
				if (data.success && data.isActive) {
					window.dataLayer.push({
						eventCategory: "form submission",
						eventAction: "hk signin button click",
						eventLabel: "HK Sign In",
						event: "trackEvent"
					});
					$.redirectPost('/portal/', { dev: window.ylp.dev });
					return;
				} else {
					$(".loading").attr("hidden", true);
					signinhelp.html("<b>" + data.statusmsg + "</b>");
					signinhelp.attr("hidden", false);
				}
			});
		} else {
			$(".loading").attr("hidden", false);
			signinhelp.html("<b>Please fill out all required information</b>");
			signinhelp.attr("hidden", false);
			return;
		}
	});

	$("#HKSignupBtn").click(function (event) {
		var signuphelp = $('#signupformhelp');
		var $inputs = $('#HKlanding-form :input');
		var flag = false;
		var parms = {
			rqstyp: 'ylpusrmaint'
		};
		signuphelp.attr("hidden", true);
		$(".loading").attr("hidden", false);
		if(!$("#HKTermscheckbox").is(":checked")){
			signuphelp.html("<b>Please accept Terms of Subscription.</b>");
			signuphelp.attr("hidden", false);
			$(".loading").attr("hidden", true);
			return;
		}
		$inputs.each(function () {
			if (this.name > "") {
				if ($(this).val() > "") {
					parms[this.name] = $(this).val();
				} else {
					signuphelp.html("<b>Please fill in all required fields.</b>");
					signuphelp.attr("hidden", false);
					$(".loading").attr("hidden", true);
					flag = true;
					return;
				}
			}
		});
		if (flag) return;
		var dialcode = iti.getSelectedCountryData().dialCode;
		if (!iti.isValidNumber()) {
			phoneinput.value = "";
			signuphelp.html("<b>Please provide a valid phone number</b>");
			signuphelp.attr("hidden", false);
			$(".loading").attr("hidden", true);
			return;
		}
		if (!emailvalidate(parms.HKInputEmail)) {
			$("#HKInputEmail").val();
			signuphelp.html("<b>Please provide a valid email</b>");
			signuphelp.attr("hidden", false);
			$(".loading").attr("hidden", true);
			return;
		}
		parms.dev = window.ylp.dev;
		parms.hkcountry = iti.getSelectedCountryData().iso2;
		parms.hktimezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
		$.post(window.ylp.Url, parms).done(function (data) {
			if (data === "EXISTS") {
				signuphelp.html("<b>User already exists</b>");
				signuphelp.attr("hidden", false);
				$(".loading").attr("hidden", true);
				return;
			} else if (data === "PHONEEXISTS") {
				phoneinput.value = "";
				signuphelp.html("<b>Phone already exists</b>");
				signuphelp.attr("hidden", false);
				$(".loading").attr("hidden", true);
				return;
			} else {
				window.dataLayer.push({
					eventCategory: "form submission",
					eventAction: "hk signup button click",
					eventLabel: "HK Landing Signup",
					event: "trackEvent"
				});
				$.redirectPost('/sign/', { dev: window.ylp.dev });
				return;
			}
		});
	});

	$("#forgotpass").click(function () {
		$("#landing-form").attr("hidden", true);
		$("#forgot-form").attr("hidden", false);
	});

	$("#HKForgotPBtn").click(function (event) {
		var signuphelp = $('#forgotformhelp');
		var signupdone = $('#forgotformdone');
		var $inputs = $('#forgot-form :input');
		var flag = false;
		var parms = {
			rqstyp: 'ylpusrmaint'
		};
		signuphelp.attr("hidden", true);
		signupdone.attr("hidden", true);
		$(".loading").attr("hidden", false);
		$inputs.each(function () {
			if (this.name > "") {
				if ($(this).val() > "") {
					parms[this.name] = $(this).val();
				} else {
					signuphelp.html("<b>Please fill in all required fields.</b>");
					signuphelp.attr("hidden", false);
					$(".loading").attr("hidden", true);
					flag = true;
					return;
				}
			}
		});
		if (flag) return;
		parms.dev = window.ylp.dev;
		$.post(window.ylp.Url, parms).done(function (data) {
			if (data === "DONE") {
				signupdone.html("<b>Please check email for password.</b>");
				signupdone.attr("hidden", false);
				$(".loading").attr("hidden", true);
				setTimeout(function () {
					$("#landing-form").attr("hidden", false);
					$("#forgot-form").attr("hidden", true);
					$('#forgotformdone').attr("hidden", true);
				}, 5000);
				return;
			} else {
				signuphelp.html("<b>User doesnt exist.</b>");
				signuphelp.attr("hidden", false);
				$(".loading").attr("hidden", true);
				return;
			}
		});
	});

	$("#HKaddcountry").click(function (event) {
		$('#addCountryModal').modal('toggle');
	});

	$("#HKaddcountrybtn").click(function (event) {
		var signuphelp = $('#addcountryformhelp');
		var parms = {
			rqstyp: "ylpemail",
			type: "addcountry",
			country: $(".countrypicker option:selected").text(),
			email: $("#HKaddcountryemail").val(),
			language: $("#HKcountrylanguage").val()
		};
		$(".loading").attr("hidden", false);
		if (parms.email > "") {
			signuphelp.attr("hidden", true);
			signuphelp.css('color', 'red');
			parms.dev = window.ylp.dev;
			$.post(window.ylp.Url, parms).done(function (data) {
				if (data === "DONE") {
					signuphelp.css('color', 'none');
					signuphelp.html("<b>Thank you for your feedback! We will be in contact shortly.</b>");
					signuphelp.attr("hidden", false);
					$(".loading").attr("hidden", true);
					setTimeout(function () {
						signuphelp.attr("hidden", true);
						$('#addCountryModal').modal('toggle');
					}, 5000);
					return;
				} else {
					$(".loading").attr("hidden", true);
					signuphelp.html('<b>Submit failed. Please contact <a HREF="mailto:support@hey-kiddo.com">support@hey-kiddo.com.</a></b>');
					signuphelp.attr("hidden", false);
					return;
				}
			});
		} else {
			signuphelp.html('<b>Please fill in required fields.</a></b>');
			signuphelp.attr("hidden", false);
			$(".loading").attr("hidden", true);
		}
		
	});
	$.extend(
		{
			redirectPost: function (location, args) {
				var form = $('<form></form>');
				form.attr("method", "post");
				form.attr("action", location);

				$.each(args, function (key, value) {
					var field = $('<input></input>');

					field.attr("type", "hidden");
					field.attr("name", key);
					field.attr("value", value);

					form.append(field);
				});
				$(form).appendTo('body').submit();
			}
		});
	$('img[data-enlargable]').addClass('img-enlargable').click(function () {
		var src = $(this).attr('src');
		$('<div>').css({
			background: 'RGBA(0,0,0,.5) url(' + src + ') no-repeat center',
			backgroundSize: 'contain',
			width: '100%', height: '100%',
			position: 'fixed',
			zIndex: '10000',
			top: '0', left: '0',
			cursor: 'zoom-out'
		}).click(function () {
			$(this).remove();
		}).appendTo('body');
	});

	$("#ourstorylink").on('click', function () {
		$("#HKlanding-home").parent("li").attr("hidden", false);
		$("#signinlink").css("border-color", "#F2D633");
		$(".navbar").css("background-color", "white");
		$(".navbar").removeClass("text-white");
		$(".navbar").addClass("text-black");
		$('.navbar-nav a').removeClass('HKNavselected');
		$(this).addClass('HKNavselected');
		$(".navbar-brand img").attr("src", "images/logo-horizontal-tagline.png");
		fade($("#ourstorycontainer"), currenttab,false,false);
		currenttab = $("#ourstorycontainer");
	});
	$("#faqlink").on('click', function () {
		$("#HKlanding-home").parent("li").attr("hidden", false);
		$("#signinlink").css("border-color", "#F2D633");
		$(".navbar").css("background-color", "white");
		$(".navbar").removeClass("text-white");
		$(".navbar").addClass("text-black");
		$('.navbar-nav a').removeClass('HKNavselected');
		$(this).addClass('HKNavselected');
		$(".navbar-brand img").attr("src", "images/logo-horizontal-tagline.png");
		fade($("#faqcontainer"), currenttab,false,false);
		currenttab = $("#faqcontainer");
	});
	$("#HKlanding-join,#HKlanding-home,#signinlink").on('click', function () {
		$(".navbar").removeClass("text-black");
		$(".navbar").addClass("text-white");
		$("#signinlink").css("border-color", "white");
		$('.navbar-nav a').removeClass('HKNavselected');
		$(".navbar-brand img").attr("src", "images/logo-horizontal-tagline-darkBG@2x.png");
		if ($(this)[0].id === "HKlanding-home") {
			if (currenttab.selector === "#maincontainer") {
				window.location.reload();
				return;
			}
			fade($("#maincontainer"), currenttab, false,true);
		} else {
			if (currenttab.selector === "#maincontainer") return;
			fade($("#maincontainer"), currenttab, true,true);
		}
		currenttab = $("#maincontainer");
	});
});

function fade(fadeinBox, fadeoutBox,scroll,landing) {
	fadeoutBox.stop(true, true).fadeOut(300, function () {
		if (landing) $(".navbar").css("background-color", "#6AD0B3");
		fadeinBox.stop(true, true).fadeIn(300);
		if (scroll) {
			$('html, body').animate({
				scrollTop: $(".HKsignupform").offset().top
			}, 1000);
			flipitfunc("back");
		}
	});
}

function flipitfunc(type) {
	if (currentflip !== type) {
		$("." + currentflip).stop(true, true).fadeOut(300, function () {
			$("."+type).stop(true, true).fadeIn(300);
		});
		currentflip = type;
	}
	return false;
}

function setCookiePolicy() {
    document.cookie = "cookiePolicy=true;";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function checkCookiePolicy(cookiename) {
    var cookie = getCookie("cookiePolicy");
    if (cookie === "") {
        $('#cookieModal').modal('toggle');
    }
}

function emailvalidate(inputemail) {
	var emailpattern = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
	if (emailpattern.test(inputemail)) {
		return true;
	}
	else {
		return false;
	}
}
var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
var player;
function onYouTubeIframeAPIReady() {
	player = new YT.Player('hkiframevideo', {
		width: '600',
		videoId: '2V0967MM89w',
		events: {
			'onReady': onPlayerReady,
			'onStateChange': onPlayerStateChange
		}
	});
}
function onPlayerReady(event) {
	$("#hkiframevideo iframe").addClass("embed-responsive-item");
}
function onPlayerStateChange(event) {
	if (event.data === YT.PlayerState.PLAYING) {
		window.dataLayer.push({
			eventCategory: "hkvideos",
			eventAction: "hkvideoplay",
			eventLabel: "videoplay",
			event: "trackEvent"
		});
	}
}