window.onload = hideURLParams;
var workbook = false;
function getURLParameter(name) {
	return decodeURI((RegExp(name + '=' + '(.+?)(&|$)').exec(location.search) || [, null])[1]);
}
function hideURLParams() {
	//Parameters to hide (ie ?success=value, ?error=value, etc)
	var hide = ['success', 'error'];
	for (var h in hide) {
		if (getURLParameter(h)) {
			history.replaceState(null, document.getElementsByTagName("title")[0].innerHTML, window.location.pathname);
		}
	}
}

$(document).ready(function () {
	checkCookie();
	var totalSteps = 6;
	var count = 0;
	var me = this;
	var counter = 1;
	var counter2 = 1;
	var currenttab = $("#HKcontainer1");
	var currentaccttab = $("#HKAccountForm");
	$(".outercarouselback").click(function (fld) {
		$('#carousel').carousel('prev');
		count--;
	});
	$('#carousel').on('slide.bs.carousel', function (e) {
		if(e.to > 0){
			$(".outercarouselback").removeAttr('hidden');
		}else{
			$(".outercarouselback").attr("hidden", true);
		}
		if (e.direction === "left") {
			counter++;
		} else {
			counter--;
		}
		var percent = (parseInt(counter) / totalSteps) * 100;

		$('.progress-bar').css({ width: percent + '%' });

	});
	$('#card-carousel').bind('slide.bs.carousel', function (e) {
		if (e.to > 0) {
			$(".card-carouselback").removeAttr('hidden');
		} else {
			$(".card-carouselback").attr("hidden", true);
			$('html, body').scrollTop(0);
			workbook = false;
		}
		
	});
	$('.HKaccountSave').on('click', function(btn){
		var valueField = $('#HKaccountMobal input').val();
		var currentUser = $('#hkemail').val();
		var inputField = "";
		switch (me.fieldType) {
			case "user_name":
				inputField = $('#hkname');
				break;
			case "user_email":
				inputField = $('#hkemail');
				var emailReg = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
				if(!emailReg.test(valueField) || valueField == '')
				{
					 alert('Please enter a valid email address.');
					 return false;
				}
				break;
			case "user_pass":
				inputField = $('#hkpassword');
				break; 
			case "user_phone":
				inputField = $('#hkphone');
				var intRegex = /[0-9 -()+]+$/;
				if((valueField.length < 10) || (!intRegex.test(valueField)))
				{
					 alert('Please enter a valid phone number.');
					 return false;
				}
				break; 
		}
        $.post(window.ylp.Url, {
			rqstyp: "ylpusrmaint", fieldtype: me.fieldType, fieldvalue: valueField, type: 'U', email: currentUser, dev: window.ylp.dev }).done(function (data) {
			if (data === "SUCCESS") {
				inputField.val(valueField);
                $('#HKaccountModal').modal('hide');
				$('.toast').toast('show');
            } else {
                alert("Something Went Wrong");
            }
        });
	});
	$('.HKaccountEdit').on('click', function(btn){
		var fieldtype = btn.delegateTarget.dataset.accountproperty;
		var field = $('#HKaccountMobal');
		var title = $('#HKaccountEditT');
		var label = $('#HKaccountEditL');
		me.fieldType = fieldtype;
		field.empty();
		switch (fieldtype) {
			case "user_name":
				input = '<input class="form-control" type="text" id="' + fieldtype + '" name="' + fieldtype + '" value="' + $('#hkname').val() + '" />';
				title.text("Edit Name");
				label.text("Name");
				break;
			case "user_email":
				input = '<input class="form-control" type="email" id="' + fieldtype + '" name="' + fieldtype + '" value="' + $('#hkemail').val() + '" />';
				title.text("Edit Email");
				label.text("Email");
				break;
			case "user_pass":
				input = '<input class="form-control" type="password" id="' + fieldtype + '" name="' + fieldtype + '" value="' + $('#hkpassword').val() + '" />';
				title.text("Edit Password");
				label.text("Password");
				break; 
			case "user_phone":
				input = '<input class="form-control" type="tel" id="' + fieldtype + '" name="' + fieldtype + '" value="' + $('#hkphone').val() + '" />';
				title.text("Edit Phone");
				label.text("Phone");
				break; 
		}
		field.append(input);
		$('#HKaccountModal').modal('show');
	});
	$('.navbar-nav a').on('click', function(){
		$('.navbar-nav a').removeClass('HKNavselected');
		$(this).addClass('HKNavselected');
	});
	$("#HKintroModalChk").on('click', function () {
		
		$('#HKintroModal').modal('hide');
		return;
		/*var parms = {
			rqstyp: 'ylpusrmaint',
			type: 'hkintro'
		};
		$.post(window.ylp.Url, parms).done(function (data) {

		});*/
	});
	$("#HKkillAccount").on('click', function () {
		var signuphelp = $('#unsubscribeformhelp');
		$(".loading").attr("hidden", false);
		var flag = false;
		var selection = $("input[name='HKUnsubscribeSel']:checked").val();
		var other = $('#HKOtherReason').val();
		var canimprove = $('#HKCanweimprove').val();
		var currentUser = $('#hkemail').val();
		var parms = {
			rqstyp: 'ylpusrmaint',
			type: 'unsubscribe',
			unsubscribesel: selection,
			otherreason: other,
			canweimprove: canimprove,
			email: currentUser,
			dev: window.ylp.dev
		};
		signuphelp.attr("hidden", true);
		
		$.post(window.ylp.Url, parms).done(function (data) {
			$(".loading").attr("hidden", true);
			if (data === "DONE") {
				$('#HKUnsubscribeSurvery').stop(true, true).fadeOut(300, function () {
					$('#HKFeedback').stop(true, true).fadeIn(300);
				}); 
				return;
			} else {
				signuphelp.html('<b>Submit failed. Please contact <a HREF="mailto:support@hey-kiddo.com">support@hey-kiddo.com.</a></b>');
				signuphelp.attr("hidden", false);
				return;
			}
		});
	});
	$("#HKshowUnsubscribe").on('click', function () {
		fadeAccount($("#HKUnsubscribeForm"), currentaccttab);
		currentaccttab = $("#HKUnsubscribeForm");
	});
	$("#HKkeepSignedIn").on('click', function () {
		fadeAccount($("#HKAccountForm"), currentaccttab);
		currentaccttab = $("#HKAccountForm");
	});
	$("#HKportalLink").on('click', function () {
		fade($("#HKcontainer1"), currenttab);
		currenttab = $("#HKcontainer1");
	});
	$("#HKinfoLink").on('click', function (e) {
		fade($("#HKcontainer2"), currenttab);
		currenttab = $("#HKcontainer2");
	});
	$("#HKaccountLink").on('click', function (e) {
		fade($("#HKcontainer3"), currenttab);
		currenttab = $("#HKcontainer3");
	});
	$("#HKlogout").on('click',function(){
		$.post(window.ylp.Url, {
			rqstyp: "ylpsignoff",
			dev: window.ylp.dev
		}).done(function (data) {
			window.location.replace("/");
		});
	});
	$(".HKnextBtn").on('click',function(){
		if(count === totalSteps-1){
			$('#HKintro').fadeOut(1000);
			$('#HKmainPortal').fadeIn(1000);
			$('#HKmainPortal').removeAttr('hidden');
			$('.HKSignupFoot').removeAttr('hidden');
			$("#HKintroModal").modal({
				backdrop: 'static',
				keyboard: false
			});
			$('#HKintroModal').modal('show');
			
		}else{
			$('#carousel').carousel('next');
			count++;
		}
	});
	$(".card-carouselback").on('click', function () {
		showActivityContainer(window.ylp.currentTrack);
		$('#card-carousel').carousel('prev');
	});
	$("#HKCcard").on('click', function () {
		if ($(this).hasClass('disabled-card')) return;
		showActivityContainer('1');
		$('#card-carousel').carousel('next');
	});
	$("#HKScard").on('click', function () {
		if ($(this).hasClass('disabled-card')) return;
		showActivityContainer('2');
		$('#card-carousel').carousel('next');
	});
	$("#HKFIcard").on('click', function () {
		if ($(this).hasClass('disabled-card')) return;
		showActivityContainer('3');
		$('#card-carousel').carousel('next');
	});
	$("#HKFScard").on('click', function () {
		if ($(this).hasClass('disabled-card')) return;
		showActivityContainer('4');
		$('#card-carousel').carousel('next');
	});
	$("#HKICcard").on('click', function () {
		if ($(this).hasClass('disabled-card')) return;
		showActivityContainer('5');
		$('#card-carousel').carousel('next');
	});
	$("#HKDCcard").on('click', function () {
		if ($(this).hasClass('disabled-card')) return;
		showActivityContainer('6');
		$('#card-carousel').carousel('next');
	});
	$("#HKWBclick").on('click', function () {
		if ($(this).hasClass('disabled-card')) return;
		showActivityContainer('7');
		$('#card-carousel').carousel('next');
	});
	$('.card-activity').on('click', function (btn) {
		if ($(this).hasClass('disabled-card')) return;
		var link = btn.delegateTarget.dataset.activitylink;
		var track = btn.delegateTarget.dataset.usertrack;
		var activity = btn.delegateTarget.dataset.activity;
		window.dataLayer.push({
			eventCategory: "hk portal button click",
			eventAction: "hk activityopen",
			eventLabel: "Track: "+track+" - Activity:"+activity,
			event: "trackEvent"
		});
		var iframe = $("#HkActivityIframe");
		iframe.empty();
		iframe.append('<iframe onload="setIframeHeight(this.id)" id="iframeview" src="' + link + '"  allowfullscreen></iframe>');
		$('#card-carousel').carousel('next');

	});
	$('.card-workbook').on('click', function (btn) {
		if ($(this).hasClass('disabled-card')) return;
		var link = btn.delegateTarget.dataset.workbooklink;
		var seq = btn.delegateTarget.dataset.workbookseq;
		window.dataLayer.push({
			eventCategory: "hk portal button click",
			eventAction: "hk workbookopen",
			eventLabel: "Workbook: " + seq,
			event: "trackEvent"
		});
		var iframe = $("#HkActivityIframe");
		iframe.empty();
		iframe.append('<iframe onload="setIframeHeight(this.id)" id="iframeview" src="' + link + '"  allowfullscreen></iframe>');
		$('#card-carousel').carousel('next');

	});

	$('img[data-enlargable]').addClass('img-enlargable').click(function () {
		var src = $(this).attr('src');
		$('<div>').css({
			background: 'RGBA(0,0,0,.5) url(' + src + ') no-repeat center',
			backgroundSize: 'contain',
			width: '100%', height: '100%',
			position: 'fixed',
			zIndex: '10000',
			top: '0', left: '0',
			cursor: 'zoom-out'
		}).click(function () {
			$(this).remove();
		}).appendTo('body');
	});
	$("#firsttime1ex").on('click', function (btn) {
		setCookie("1");
		$("#firsttime1").fadeOut();
	});
	$(".firsttime2ex").on('click', function (btn) {
		setCookie("2");
		$(".firsttime2").fadeOut();
	});
	$('input[name="HKUnsubscribeSel"]').change(function () {
		if ($('#HKUnsubscribeSel4').prop('checked')) {
			$('#HKOtherReason').attr("disabled", false);
		} else {
			$('#HKOtherReason').attr("disabled", true);
		}
	});
});

function fade(fadeinBox, fadeoutBox) {
	fadeoutBox.stop(true, true).fadeOut(300, function () {
		fadeinBox.stop(true, true).fadeIn(300);
	});
}

function fadeAccount(fadeinBox, fadeoutBox) {
	fadeoutBox.stop(true, true).fadeOut(300, function () {
		fadeinBox.stop(true, true).fadeIn(300);
	});
}


function setIframeHeight(id) {
	var ifrm = document.getElementById(id);
	var doc = ifrm.contentDocument ? ifrm.contentDocument :
		ifrm.contentWindow.document;
	ifrm.style.visibility = 'hidden';
	ifrm.style.height = "10px"; // reset to minimal height ...
	// IE opt. for bing/msn needs a bit added or scrollbar appears
	ifrm.style.height = getDocHeight(doc) + 4 + "px";
	$('#HkActivityIframeRow').css("min-height", ifrm.style.height);
	ifrm.style.visibility = 'visible';
}

function getDocHeight(doc) {
	doc = doc || document;
	// stackoverflow.com/questions/1145850/
	var body = doc.body, html = doc.documentElement;
	var height = Math.max(body.scrollHeight, body.offsetHeight,
		html.clientHeight, html.scrollHeight, html.offsetHeight);
	return height;
}


function showActivityContainer(track) {
	window.ylp.currentTrack = track;
	$('html, body').scrollTop(0);
	$("#Hktrack1activities").attr("hidden", true);
	$("#Hktrack2activities").attr("hidden", true);
	$("#Hktrack3activities").attr("hidden", true);
	$("#Hktrack4activities").attr("hidden", true);
	$("#Hktrack5activities").attr("hidden", true);
	$("#Hktrack6activities").attr("hidden", true);
	$("#Hkworkbooks").attr("hidden", true);
	if (workbook) track = '7';
	switch (track) {
		case '1':
			$("#Hktrack1activities").attr("hidden", false);
			break;
		case '2':
			$("#Hktrack2activities").attr("hidden", false);
			break;
		case '3':
			$("#Hktrack3activities").attr("hidden", false);
			break;
		case '4':
			$("#Hktrack4activities").attr("hidden", false);
			break;
		case '5':
			$("#Hktrack5activities").attr("hidden", false);
			break;
		case '6':
			$("#Hktrack6activities").attr("hidden", false);
			break;
		case '7':
			$("#Hkworkbooks").attr("hidden", false);
			workbook = true;
			break;
	}
}

function setCookie(cookie) {
	console.log(cookie);
	switch (cookie) {
		case '1':
			document.cookie = "firsttime1=true;";
			break;
		case '2':
			document.cookie = "firsttime2=true;";
			break;
	}
}

function getCookie(cname) {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) === ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) === 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}

function checkCookie() {
	var cookie1 = getCookie("firsttime1");
	console.log(cookie1);
	if (cookie1 === "") {
		$('#firsttime1').attr("hidden", false);
	} else {
		$('#firsttime1').attr("hidden", true);
	}
	var cookie2 = getCookie("firsttime2");
	if (cookie2 === "") {
		$('.firsttime2').attr("hidden", false);
	} else {
		$('.firsttime2').attr("hidden", true);
	}
}