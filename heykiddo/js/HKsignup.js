window.onload = hideURLParams;

function getURLParameter(name) {
	return decodeURI((RegExp(name + '=' + '(.+?)(&|$)').exec(location.search) || [, null])[1]);
}
function hideURLParams() {
	//Parameters to hide (ie ?success=value, ?error=value, etc)
	var hide = ['type'];
	for (var h in hide) {
		if (getURLParameter(h)) {
			history.replaceState(null, document.getElementsByTagName("title")[0].innerHTML, window.location.pathname);
		}
	}
}

var totalSteps = 3;
var firstSelection = 0;
var secondSelection = 0;
var thirdSelection = 0;
var counter = 1;
var promoval = "";
var echoiceflag = "N";
var me = this;
$(document).ready(function () {

	$(".outercarouselback").click(function (fld) {
		$('#HKcardfoot').attr("hidden", false);
		counter--;
		showContainer(counter.toString(),"prev");
		updateProgressBar();
	});
	
	$('.flip-it').click(function () {
		$('.flip-container').toggleClass('hover');
		$('.flip-container .flipper').css('transform, rotateY(180deg)');
	});
	$('#firstoutercarousel').carousel();
	$('#outercarousel').carousel();
	$('.carousel').on('slide.bs.carousel', function (e) {
		if(e.target.id === "carouselExample"){
			var $e = $(e.relatedTarget);
			var idx = $e.index();
			var itemsPerSlide = 4;
			var totalItems = $('#carouselExample .carousel-item').length;

			if (idx >= totalItems-(itemsPerSlide-1)) {
				var it = itemsPerSlide - (totalItems - idx);
				for (var i=0; i<it; i++) {
					// append slides to end
					if (e.direction=="left") {
						$('#carouselExample .carousel-item').eq(i).appendTo('#carouselExample .carousel-inner');
					}
					else {
						$('#carouselExample .carousel-item').eq(0).appendTo('#carouselExample .carousel-inner');
					}
				}
			}
			return;
		}
	});	
	$(".card-click").on('click',function(btn){
		$(".card-click").css("background-color","");
		$(this).css("background-color","lightgrey");
		me.secondSelection = btn.delegateTarget.dataset.timeselection;
		window.dataLayer.push({
			eventCategory: "hk signup button click",
			eventAction: "hk choosetime",
			eventLabel: "Track Time: " + me.secondSelection,
			event: "trackEvent"
		});
		if(me.secondSelection === 0){
			$('#signuptimehelp').attr("hidden", false);
		}else{
			$('#signuptimehelp').attr("hidden", true);
			var finishlist = $('#finish-list');
			finishlist.empty();
			$('#HKnext4').attr("hidden", false);
			$('#HKcardfoot').attr("hidden", true);
			$('#signupplanhelp').attr("hidden", true);
			$('#paypalparagraph').attr("hidden", true);
			me.thirdSelection = '3';
			promoval = "hkFREETEMP";
			finishlist.append(buildFinishList("FREETEMP"));
			showContainer('3');
			counter++;
			updateProgressBar();
		}
	});
	$(".card-plan").on('click', function (btn) {
		$(".card-plan").css("background-color", "");
		$(this).css("background-color", "lightgrey");
		$('#paypal-plan-container').attr("hidden", false);
		$('#HKnext4').attr("hidden", true);
		me.thirdSelection = btn.delegateTarget.dataset.planselection;
		if(me.thirdSelection === '1'){
			$('#paypal-plan-container1').attr("hidden", false);
			$('#paypal-plan-container2').attr("hidden", true);
			$('#paypal-plan-container3').attr("hidden", true);
			$('#paypal-plan-container4').attr("hidden", true);
			$('#paypal-plan-container5').attr("hidden", true);
			$('#paypalparagraph').attr("hidden", false);
			window.dataLayer.push({
				eventCategory: "hk signup button click",
				eventAction: "hk chooseplan",
				eventLabel: "Paypal 99.99",
				event: "trackEvent"
			});
		}
		if(me.thirdSelection === '2'){
			$('#paypal-plan-container1').attr("hidden", true);
			$('#paypal-plan-container2').attr("hidden", false);
			$('#paypal-plan-container3').attr("hidden", true);
			$('#paypal-plan-container4').attr("hidden", true);
			$('#paypal-plan-container5').attr("hidden", true);
			$('#paypalparagraph').attr("hidden", false);
			window.dataLayer.push({
				eventCategory: "hk signup button click",
				eventAction: "hk chooseplan",
				eventLabel: "Paypal 9.99",
				event: "trackEvent"
			});
		}
		if(me.thirdSelection === '3'){
			$('#paypal-plan-container1').attr("hidden", true);
			$('#paypal-plan-container2').attr("hidden", true);
			$('#paypal-plan-container3').attr("hidden", true);
			$('#paypal-plan-container4').attr("hidden", true);
			$('#paypal-plan-container5').attr("hidden", true);
			$('#promomodal').modal('show');
			$('#paypalparagraph').attr("hidden", true);
			
			return;
		}
		if (me.thirdSelection === 0) {
			$('#signupplanhelp').attr("hidden", false);
		} else {
			var finishlist = $('#finish-list');
			finishlist.empty();
			$('#HKcardfoot').attr("hidden", true);
			$('#signupplanhelp').attr("hidden", true);
			finishlist.append(buildFinishList());
			showContainer('4');
			counter++;
			updateProgressBar();
		}
	});
	$(".HKpromoSave").on('click',function(){
		var promocode = $("#hkpromocode").val();
		$('#signuppromohelp').attr("hidden", true);
		if (promocode > "") {
			$.post(window.ylp.Url, { rqstyp: "ylpchkpromo", promocode: promocode, dev: window.ylp.dev}).done(function (data) {
				promoval = "hk" + data.trim();
				var finishlist = $('#finish-list');
				if (data.trim() === "FREE" || data.trim() === "FREE30" || data.trim() === "FREETEMP") {
					$('#HKnext4').attr("hidden", false);
					$('#signuppromohelp').attr("hidden", true);
					$('#promomodal').modal('hide');
					finishlist.empty();
					$('#signupplanhelp').attr("hidden", true);
					finishlist.append(buildFinishList(data.trim()));
					showContainer('4');
					counter++;
					updateProgressBar();
					$('#HKcardfoot').attr("hidden", true);
					window.dataLayer.push({
						eventCategory: "hk signup button click",
						eventAction: "hk chooseplan",
						eventLabel: "Promo: HK" + data.trim(),
						event: "trackEvent"
					});
					return;
				} else if (data.trim() === "14FREE" || data.trim() === "30FREE" || data.trim() === "30FREE1") {
					if (data.trim() === "14FREE") {
						$('#paypal-plan-container3').attr("hidden", false);
					} else if (data.trim() === "30FREE"){
						$('#paypal-plan-container4').attr("hidden", false);
					} else if (data.trim() === "30FREE1"){
						$('#paypal-plan-container5').attr("hidden", false);
					}
					$('#paypalparagraph').attr("hidden", false);
					$('#signuppromohelp').attr("hidden", true);
					$('#promomodal').modal('hide');
					finishlist.empty();
					$('#signupplanhelp').attr("hidden", true);
					finishlist.append(buildFinishList(data.trim()));
					showContainer('4');
					counter++;
					updateProgressBar();
					window.dataLayer.push({
						eventCategory: "hk signup button click",
						eventAction: "hk chooseplan",
						eventLabel: "Promo: HK" + data.trim(),
						event: "trackEvent"
					});
					$('#HKcardfoot').attr("hidden", true);
					return;
				} else {
					$('#HKnext4').attr("hidden", true);
					$('#signuppromohelp').attr("hidden", false);
					return;
				}
			});
		}else{
			$('#signuppromohelp').attr("hidden", false);
		}
	});
	$("#HKnext4").on('click', function (btn) {
		$(".loading").attr("hidden", false);
		SigningIn(promoval,"");
	});
	$(".fadecontainerback").on('click', function (btn) {
		fadeInOut();
		$("#HKcardfoot p").attr("hidden", false);
		$("#HKcardfoot button").attr("hidden", true);
	});
	$(".HKSelect").on('click', function (btn) {
		$(".HKSelect").css("background-color", "");
		$(this).css("background-color", "lightgrey");
		if($(this).data('dragging')) return;
		me.firstSelection = btn.delegateTarget.dataset.optionchoice;
		if (me.firstSelection === "7") {
			echoiceflag = "Y";
			me.firstSelection = (Math.floor(Math.random() * 6) + 1).toString();
		} else {
			echoiceflag = "N";
		}
		$(".outercarouselback").attr("hidden", false);
		window.dataLayer.push({
			eventCategory: "hk signup button click",
			eventAction: "choosetrack",
			eventLabel: "Track: " + me.firstSelection,
			event: "trackEvent"
		});
		showContainer('2');
		counter++;
		updateProgressBar();
	});
	$.extend(
		{
			redirectPost: function (location, args) {
				var form = $('<form></form>');
				form.attr("method", "post");
				form.attr("action", location);

				$.each(args, function (key, value) {
					var field = $('<input></input>');

					field.attr("type", "hidden");
					field.attr("name", key);
					field.attr("value", value);

					form.append(field);
				});
				$(form).appendTo('body').submit();
			}
		});
});

function SigningIn(type, id) {
	$(".loading").attr("hidden", false);
	var parms = {
		rqstyp: 'ylpusrmaint',
		type: type,
		hktrack: me.firstSelection,
		hktime: me.secondSelection,
		echoice: echoiceflag,
		id: id
	};
	parms.dev = window.ylp.dev;
	$.post(window.ylp.Url, parms).done(function (data) {
		if (data === "COMPLETE") {
			$("#signuppaymenthelp").attr("hidden", true);
			$.redirectPost('/portal/', { dev: window.ylp.dev });
			return;
		} else {
			$(".loading").attr("hidden", true);
			$("#signuppaymenthelp").html('Something went wrong. Please contact <A HREF="mailto:support@hey-kiddo.com">Hey-Kiddo&trade; support.</a>');
			$("#signuppaymenthelp").attr("hidden", false);
		}
	});
}

function updateProgressBar() {
	$('html, body').scrollTop(0);
	var percent = (parseInt(counter) / totalSteps) * 100;

	$('.progress-bar').css({ width: percent + '%' });
}

function showContainer(container,direction) {
	var container1 = $('#HKcontainer1');
	var container2 = $('#HKcontainer2');
	var container3 = $('#HKcontainer4');
	var container4 = $('#HKcontainer4');
	switch (container) {
		case '1':
			fade(container2, container1);
			$(".outercarouselback").attr("hidden", true);
			break;
		case '2':
			if (direction === "prev") {
				fade(container3, container2);
			} else {
				fade(container1, container2);
			}
			$(".outercarouselback").attr("hidden", false);
			break;
		case '3':
			if (direction === "prev") {
				fade(container4, container3);
			} else {
				fade(container2, container3);
			}
			$(".outercarouselback").attr("hidden", false);
			break;
		case '4':
			fade(container3, container4);
			$(".outercarouselback").attr("hidden", false);
			break;
	}
}

function fade(fadeoutBox, fadeinBox) {
	
	fadeoutBox.stop(true, true).fadeOut(1000, function () {
		fadeinBox.stop(true, true).fadeIn(1000);
	});
}

function buildFinishList(data){
	window.ylp.firstSelection = me.firstSelection;
	window.ylp.secondSelection = me.secondSelection;
	window.ylp.thirdSelection = me.thirdSelection;
	console.log(window.ylp);
	var liststring = '<ul class="finish-list">';
	if (echoiceflag === "N") {
		switch (me.firstSelection) {
			case '1':
				liststring += "<li><h4>Help your child understand their unique personality</h4></li>";
				break;
			case '2':
				liststring += "<li><h4>Help your child stay positive and manage stress</h4></li>";
				break;
			case '3':
				liststring += "<li><h4>Help your child understand others and different perspectives</h4></li>";
				break;
			case '4':
				liststring += "<li><h4>Help your child develop strong relationships with family and peers</h4></li>";
				break;
			case '5':
				liststring += "<li><h4>Help your child set goals and stay organized</h4></li>";
				break;
			case '6':
				liststring += "<li><h4>Help your child respond instead of react</h4></li>";
				break;
			case '7':
				liststring += "<li><h4>Help your child develop a great foundation</h4></li>";
				break;
		}
	} else {
		liststring += "<li><h4>Have us pick for you</h4></li>";
	}

	switch(me.secondSelection){
		case '1':
			liststring += "<li><h4>Interact with your child in the morning</h4></li>";
			break;
		case '2':
			liststring += "<li><h4>Interact with your child right after school</h4></li>";
			break;
		case '3':
			liststring += "<li><h4>Interact with your child during dinner</h4></li>";
			break;
		case '4':
			liststring += "<li><h4>Interact with your child around bedtime</h4></li>";
			break;	
	}
	switch(me.thirdSelection){
		case '1':
			liststring += "<li><h4>Get the first 7 days free and then pay $99.99/year</h4></li></ul>";
			break;
		case '2':
			liststring += "<li><h4>Get the first 7 days free and then pay $9.99/month</h4></li></ul>";
			break;
		case '3':
			if (data === "FREE" || data === "FREE30" || data === "FREETEMP") {
				liststring += "<li><h4>Enjoy your free membership!</h4></li></ul>";
			} else if(data === "14FREE"){
				liststring += "<li><h4>Get the first 14 days free and then pay $9.99/month</h4></li></ul>";
			} else if (data === "30FREE" || data === "30FREE1") {
				liststring += "<li><h4>Get the first 30 days free and then pay $9.99/month</h4></li></ul>";
			}
			break;
	}
	
	return liststring;
}