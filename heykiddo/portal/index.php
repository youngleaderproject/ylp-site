<?php
error_reporting(E_ALL);
try {
    session_start();
}
catch(\Exception $ex) {
      file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/heykiddosign.txt' , print_r($_SESSION,true)."\r\n", FILE_APPEND);
}

$linkpos = strpos(strtolower($_SERVER[ "DOCUMENT_ROOT" ]),"heykiddo");
if( $linkpos > 0){
	$_SERVER[ "DOCUMENT_ROOT" ] = substr($_SERVER[ "DOCUMENT_ROOT" ],0,$linkpos-1);
}

require_once str_replace( '\\', '/', $_SERVER[ "DOCUMENT_ROOT" ] ) . '/PHP/shared/auto_load.php';
use PHP\CLASSES\CUSER;
use PHP\CLASSES\CYLPDB;
use PHP\CLASSES\CCIPHER;
use PHP\CLASSES\CACTIVITY;
$_REQUEST = array_change_key_case($_REQUEST, CASE_LOWER);
$firsttime = (isset($_SESSION["signupuser"]["firsttime"]) ? $_SESSION["signupuser"]["firsttime"] : false);
$user = new CUSER();
$activities = new CACTIVITY();
$key = (isset($_REQUEST["k"]) ? $_REQUEST["k"] : '');
$activity = (isset($_REQUEST["a"]) ? $_REQUEST["a"] : '');
$directaccess = false;
$dev = (isset($_SESSION["user"]["dev"]) ? $_SESSION["user"]['dev'] : '');
if(strlen($key) > 0 && strlen($activity) > 0){
    $cipher = new CCIPHER();
    if(sizeof($user->GET_USER_BY_ID($key)) > 0  && $user->ylpstatus !== 0){
        $useremail = $user->ylpemail;
        $_SESSION["user"]["loggedin"] = true;
        $directaccess = true;
        $dev = session_id();
        session_id($dev);
        session_reset();
    }else{
        Redirect( "../" );
        exit;
    }
}else{
    file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/heykiddosign.txt' , print_r($_REQUEST,true)."\r\n", FILE_APPEND);
    if (trim($dev) === '') {
        ClearSession();
        Redirect( "../" );
    }
    session_id($dev);
    session_reset();
    $useremail = (isset($_SESSION["user"]["usremail"]) ? $_SESSION["user"]["usremail"] : '');
}
$loggedin = (isset($_SESSION["user"]["loggedin"]) ? $_SESSION["user"]["loggedin"] : false);
if(!$loggedin && strlen($useremail) === 0){
    ClearSession();
    Redirect( "../" );
}else{
    $SAenabled = false;
    $SMenabled = false;
    $SOAenabled = false;
    $RSenabled = false;
    $ICenabled = false;
    $DMenabled = false;
    if(sizeof($user->GET_USER_BY_EMAIL($useremail)) > 0  && $user->ylpstatus !== 0){
        $firstname = $user->ylpname;
        $lastname = $user->ylplname;
        $username = $firstname . " " . $lastname;
        $userpassword = isset($_SESSION["user"]["usrpw"]) ? $_SESSION["user"]["usrpw"] : $user->ylppwr;
        $userphone = $user->ylpphone;
        $actcount = $user->ylpactivity;
        $actcount1=0;
        $actcount2=0;
        $actcount3=0;
        $actcount4=0;
        $actcount5=0;
        $actcount6=0;
        $tracks = explode("-",$user->ylptrack);
        $currenttrack = $tracks[sizeof($tracks)-1];
        $directtrack = (isset($_REQUEST["t"]) ? $_REQUEST["t"] : $currenttrack);
        if($directaccess && !isset($_REQUEST["t"])){
            $directtrack = $tracks[0];
        }
        $activitylist1 = [];
        $activitylist2 = [];
        $activitylist3 = [];
        $activitylist4 = [];
        $activitylist5 = [];
        $activitylist6 = [];
        $workbookList = $activities->GET_WORKBOOKS();
        $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
        $query = "SELECT a.access_code from ylpaccess a inner join ylpusrtb b on a.user_id = b.user_id where b.user_phone = '$user->ylpphone'";
        if($dba->GETALLDBDATAQUERYSTRING($query, $ActualDataArray2) > 0){
            $firsttime = false;
        }else{
            $firsttime = true;
            $selection = $user->ylptrack;
            $userphone = $user->ylpphone;
            $country = $user->getLocationInfoByIp()['country'];
            $country = ($country !== '' ? $country : 'US');
            $user->UserIntro($useremail,$firstname,$lastname,$userphone,$selection,$country);
        }
        for($m=0;$m<sizeof($tracks);$m++){
            switch($tracks[$m]){
                case '1':
                    $SAenabled = true;
                    $activitylist1 = $activities->GET_ALLACTIVITIES('1');
                    if($m === sizeof($tracks)-1){
                        $actcount1 = $actcount;
                    }else{
                        $actcount1 = sizeof($activitylist1);
                    }
                    break;
                case '2':
                    $SMenabled = true;
                    $activitylist2 = $activities->GET_ALLACTIVITIES('2');
                    if($m === sizeof($tracks)-1){
                        $actcount2 = $actcount;
                    }else{
                        $actcount2 = sizeof($activitylist2);
                    }
                    break;
                case '3':
                    $SOAenabled = true;
                    $activitylist3 = $activities->GET_ALLACTIVITIES('3');
                    if($m === sizeof($tracks)-1){
                        $actcount3 = $actcount;
                    }else{
                        $actcount3 = sizeof($activitylist3);
                    }
                    break;
                case '4':
                    $RSenabled = true;
                    $activitylist4 = $activities->GET_ALLACTIVITIES('4');
                    if($m === sizeof($tracks)-1){
                        $actcount4 = $actcount;
                    }else{
                        $actcount4 = sizeof($activitylist4);
                    }
                    break;
                case '5':
                    $ICenabled = true;
                    $activitylist5 = $activities->GET_ALLACTIVITIES('5');
                    if($m === sizeof($tracks)-1){
                        $actcount5 = $actcount;
                    }else{
                        $actcount5 = sizeof($activitylist5);
                    }
                    break;
                case '6':
                    $DMenabled = true;
                    $activitylist6 = $activities->GET_ALLACTIVITIES('6');
                    if($m === sizeof($tracks)-1){
                        $actcount6 = $actcount;
                    }else{
                        $actcount6 = sizeof($activitylist6);
                    }
                    break;
                case '7':

                    break;
                case '8':

                    break;
            }
        }
    }else{
        ClearSession();
        Redirect( "../" );
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NLW5ZB7');</script>
<!-- End Google Tag Manager -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta property='og:title' content="HeyKiddo&trade; Parent Portal" />
    <meta property='og:image' content="https://hey-kiddo.com/images/HeyKiddoParentPortal.PNG" />
    <meta property='og:description' content="HeyKiddo&trade; develops the leaders of tomorrow by acquainting them with essential leadership skills today." />
    <meta property='og:url' content="https://hey-kiddo.com/portal" />
    <title>HeyKiddo&trade; Portal | Home</title>
    <link rel="icon" href="../images/Hey-Kiddo-favicon.ico" type="favicon/ico" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet" />
    <link href="css/styles.css?v=2" rel="stylesheet" />
</head>
<body>
    <div class="loading" hidden>Loading&#8230;</div>
    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NLW5ZB7"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div <?php if($firsttime === false)echo 'hidden';?> id="HKintro" class="container-fluid">
        <div class="progress" style="max-height:10px;">
            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="6" style="width: 17%;background-color: #F2D632;"></div>
        </div>
        <a href="#" onclick="return false;" hidden class="outercarouselback">
            <img src="../images/Back-Icon@2x.png" style="position: fixed; z-index: 100; top: 50; left: 50;" width="50" class="img-fluid" />
        </a>
        <div id="carousel" class="carousel slide" data-interval="false">
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <div class="carousel-caption" style="top: 45%;">
                        <h3>
                            You did it! Welcome to HeyKiddo&trade;
                            <br />
                            We're so glad you're here.
                        </h3>
                        <img src="../images/WarmHug1-graphic@2x.png" width="200" height="auto" style="position: absolute;top: -260px;left: 50%;margin-left: -100px;" alt="" />
                        <div class="row">
                            <a href="#" onclick="return false;" class="pt-3 mx-auto">
                                <img src="../images/btn-Next.png" width="75" class="img-fluid HKnextBtn" alt="" />
                            </a>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="carousel-caption" style="top: 45%;">
                        <h3>
                            Your relationship with your child is about to get even better!
                        </h3>
                        <img src="../images/WarmHug2-graphic@2x.png" width="300" height="auto" style="position: absolute;top: -260px;left: 50%;margin-left: -150px;" alt="" />
                        <div class="row">
                            <a href="#" onclick="return false;" class="pt-3 mx-auto">
                                <img src="../images/btn-Next.png" width="75" class="img-fluid HKnextBtn" alt="" />
                            </a>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="carousel-caption" style="top: 45%;">
                        <h3>Research shows that children who have regular conversations with caring adults are more successful and well-adjusted later in life.</h3>
                        <img src="../images/WarmHug3-graphic@2x.png" width="275" height="auto" style="position: absolute;top: -260px;left: 50%;margin-left: -150px;" alt="" />
                        <div class="row">
                            <a href="#" onclick="return false;" class="pt-3 mx-auto">
                                <img src="../images/btn-Next.png" width="75" class="img-fluid HKnextBtn" alt="" />
                            </a>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="carousel-caption" style="top: 45%;">
                        <h3>You're an amazing parent for caring so much about your child's future!</h3>
                        <img src="../images/WarmHug4-graphic@2x.png" width="300" height="auto" style="position: absolute;top: -250px;left: 50%;margin-left: -150px;" alt="" />
                        <div class="row">
                            <a href="#" onclick="return false;" class="pt-3 mx-auto">
                                <img src="../images/btn-Next.png" width="75" class="img-fluid HKnextBtn" alt="" />
                            </a>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="carousel-caption" style="top: 42.5%;">
                        <h3>We'll text you a few times each week with fun activites and talking tips written by psychologists so you can supercharge your everyday conversations with your child.</h3>
                        <img src="../images/WarmHug5-graphic@2x.png" width="200" height="auto" style="position: absolute;top: -260px;left: 50%;margin-left: -100px;" alt="" />
                        <div class="row">
                            <a href="#" onclick="return false;" class="pt-3 mx-auto">
                                <img src="../images/btn-Next.png" width="75" class="img-fluid HKnextBtn" alt="" />
                            </a>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="carousel-caption" style="top: 45%;">
                        <h3>If you ever miss an activity from us, just sign into your HeyKiddo&trade; portal and it will be waiting for you!</h3>
                        <img src="../images/WarmHug6-graphic@2x.png" width="400" height="auto" style="position: absolute;top: -200px;left: 50%;margin-left: -200px;" alt="" />
                        <div class="row">
                            <a href="#" onclick="return false;" class="pt-3 mx-auto">
                                <img src="../images/btn-Next.png" width="75" class="img-fluid HKnextBtn" alt="" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div <?php if($firsttime === true)echo 'hidden';?> id="HKmainPortal" class="container-fluid p-0" style="background-color: #FCFCFC;">
        <div id="HKintroModal" class="modal-custom modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    `
                    <div class="modal-body text-center">
                        <h4>Welcome to the HeyKiddo&trade; Portal</h4>
                        <p>Here is your first activity! Check your text messages for more information and give yourself a high five for proactive parenting!</p>
                        <br />
                        <img src="../images/btn-Check@2x.png" width="75" id="HKintroModalChk" class="img-fluid" alt="" />
                    </div>
                </div>
            </div>
        </div>
        <nav class="navbar navbar-expand-lg navbar-light sticky-top bg-white">
            <div class="container-fluid">
                <a class="navbar-brand" href="/">
                    <img src="../images/logo-horizontal.png" width="200" class="img-fluid" alt="" />
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div class="navbar-nav ml-auto p-2">
                        <a class="nav-item nav-link active HKNavselected" href="#" id="HKportalLink" onclick="return false;">
                            HeyKiddo&trade; Portal
                        </a>
                        <a class="nav-item nav-link active" href="#" id="HKinfoLink" onclick="return false;">
                            Using HeyKiddo&trade;
                        </a>
                        <a class="nav-item nav-link active" href="#" id="HKaccountLink" onclick="return false;">
                            Account
                        </a>
                        <a class="nav-item nav-link active" href="#" id="HKlogout" onclick="return false;">
                            Log Out
                        </a>
                    </div>
                </div>
            </div>
        </nav>

        <div class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-delay="2000" style="position: absolute; top: 150px; left:45%;z-index: 1000;">
            <div class="toast-header">
                <strong class="mr-auto">Account Settings</strong>
            </div>
            <div class="toast-body">
                Saved!
            </div>
        </div>
        <div class="modal" id="HKaccountModal" tabindex="-1" role="dialog"  aria-hidden="true" data-toggle="modal" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <center>
                            <h5 id="HKaccountEditT"></h5>
                        </center>
                        <div class="form-group">
                            <label id="HKaccountEditL" class="control-label">Name</label>
                            <div id="HKaccountMobal"></div>
                        </div>
                        <br />
                        <center>
                            <button type="button" class="btn HKaccountSave">SAVE</button>
                        </center>
                    </div>
                </div>
            </div>
        </div>

        <div id="HKcontainer1" class="container-fluid align-items-center justify-content-center">
            <a href="#" onclick="return false;" <?php if($directaccess === false)echo 'hidden';?> class="card-carouselback">
                <img src="../images/Back-Icon@2x.png" style="position: fixed; z-index: 999; top: 125px; left:10px;" width="40" class="img-fluid" />
            </a>
            <div id="card-carousel" class="carousel slide" data-interval="false">
                <div class="carousel-inner">
                    <!-- Slide One - Set the background image for this slide in the line below -->
                    <div class="carousel-item mt-3 mb-5 <?php if(!(strlen($activity) > 0))echo 'active'; ?>">
                        <div class="row my-auto">
                            <div class="row w-75 text-center mx-auto p-2">
                                <div class="col-md-12 col-sm-12 my-auto pb-3" id="firsttime1">
                                    <div class="card p-3">
                                        <div class="row my-auto"> 
                                            <div class="col-10 col-md-10 col-sm-10 mx-auto my-auto text-left">
                                                After you complete a topic, you can choose which one to unlock next via text.
                                            </div>
                                            <div class="col-2 col-md-2 col-sm-2 mx-auto my-auto ">
                                                <img src="../images/Cancel-Icon@3x.png" width="25" id="firsttime1ex" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 my-auto">
                                    <div class="card p-3 <?php if(!$SAenabled){echo 'disabled-card';}else{echo 'card-hover';} ?>" id="HKCcard">
                                        <div class="row my-auto">
                                            <div class="col-xs-7 col-sm-7 mx-auto my-auto">
                                                <div>
                                                    <h5>Confidence</h5>
                                                </div>
                                            </div>
                                            <div class="col-xs-4 col-sm-4 my-auto offset-xs-1 offset-sm-1">
                                                <img src="../images/icon-Self-Awareness@2x.png" width="100" class="img-fluid" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6  my-auto cardspacer">
                                    <div class="card p-3 <?php if(!$SMenabled){echo 'disabled-card';}else{echo 'card-hover';} ?>" id="HKScard">
                                        <div class="row my-auto">
                                            <div class="col-xs-7 col-sm-7 mx-auto my-auto">
                                                <div>
                                                    <h5>Stress</h5>
                                                </div>
                                            </div>
                                            <div class="col-xs-4 col-sm-4 my-auto offset-xs-1 offset-sm-1">
                                                <img src="../images/icon-Self-Management@2x.png" width="100" class="img-fluid" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row w-75 text-center mx-auto p-2">
                                <div class="col-md-6  my-auto">
                                    <div class="card p-3 <?php if(!$SOAenabled){echo 'disabled-card';}else{echo 'card-hover';} ?>" id="HKFIcard">
                                        <div class="row my-auto">
                                            <div class="col-xs-7 col-sm-7 mx-auto my-auto">
                                                <div>
                                                    <h5>Social Intelligence</h5>
                                                </div>
                                            </div>
                                            <div class="col-xs-4 col-sm-4 my-auto offset-xs-1 offset-sm-1">
                                                <img src="../images/icon-Social Awareness@2x.png" width="100" class="img-fluid" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 my-auto cardspacer">
                                    <div class="card p-3 <?php if(!$RSenabled){echo 'disabled-card';}else{echo 'card-hover';}   ?>" id="HKFScard">
                                        <div class="row my-auto">
                                            <div class="col-xs-7 col-sm-7 mx-auto my-auto">
                                                <div>
                                                    <h5>Relationships</h5>
                                                </div>
                                            </div>
                                            <div class="col-xs-4 col-sm-4 my-auto offset-xs-1 offset-sm-1">
                                                <img src="../images/icon-Relationship Skills@2x.png" width="100" class="img-fluid" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row w-75 text-center mx-auto p-2">
                                <div class="col-md-6 my-auto">
                                    <div class="card p-3 <?php if(!$ICenabled){echo 'disabled-card';}else{echo 'card-hover';}   ?>" id="HKICcard">
                                        <div class="row my-auto">
                                            <div class="col-xs-7 col-sm-7 mx-auto my-auto">
                                                <div>
                                                    <h5>Thoughtful Decisions</h5>
                                                </div>
                                            </div>
                                            <div class="col-xs-4 col-sm-4 my-auto offset-xs-1 offset-sm-1">
                                                <img src="../images/icon-Impulse Control@2x.png" width="100" class="img-fluid" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 my-auto cardspacer">
                                    <div class="card p-3 <?php if(!$DMenabled){echo 'disabled-card';}else{echo 'card-hover';}   ?>" id="HKDCcard">
                                        <div class="row my-auto">
                                            <div class="col-xs-7 col-sm-7 mx-auto my-auto">
                                                <div>
                                                    <h5>Self-Control</h5>
                                                </div>
                                            </div>
                                            <div class="col-xs-4 col-sm-4 my-auto offset-xs-1 offset-sm-1">
                                                <img src="../images/icon-Decision Making@2x.png" width="100" class="img-fluid" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row w-75 text-center mx-auto mt-4 pt-3 dottedline">
                                <div class="col-md-6 my-auto">
                                    <img id="HKWBclick" src="../images/Lessons@2x.png" width="250" style="cursor:pointer;" class="img-fluid" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item mb-5 mt-3" style="z-index: 110;">
                        <div hidden id="Hktrack1activities" class="container-fluid align-items-center justify-content-center">
                            <div class="row my-auto">
                                <div class="my-auto col-xl-6 offset-xl-3 col-md-8 offset-md-2  pb-3 firsttime2">
                                    <div class="card p-3">
                                        <div class="row my-auto">
                                            <div class="col-10 col-md-10 col-sm-10 col-xs-10 mx-auto my-auto text-left">
                                                Each activity will unlock as you recieve them via text.
                                            </div>
                                            <div class="col-2 col-md-2 col-sm-2 col-xs-2 mx-auto my-auto">
                                                <img src="../images/Cancel-Icon@3x.png" width="25" class="firsttime2ex" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                        $counter = 1;
                                        for($a=0;$a<sizeof($activitylist1);$a++){
                                            $title = $activitylist1[$a]["ylpActivityTitle"];
                                            $desc = $activitylist1[$a]["ylpActivityDesc"];
                                            $link = $activitylist1[$a]["ylpActivityLink"];
                                            if($counter > $actcount1){$cardtype = 'disabled-card';}else{$cardtype = 'card-hover';}
                                            echo "<div class='row w-100 text-left mx-auto' style='padding-top: 10px;'>
                                                <div class='my-auto col-xl-6 offset-xl-3'>
                                                    <div class='card p-3 card-activity $cardtype' style='min-height: 140px;' data-activitylink='$link' data-usertrack='1' data-activity='$counter'>
                                                        <div class='row my-auto'>
                                                            <div class='col-12 my-auto'>
                                                                <div class='text-left'>
                                                                    <h4>$title</h4>
                                                                    <br>
                                                                    <h6>$desc</h6>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>";
                                            $counter++;
                                        }
                                ?>
                            </div>
                        </div>
                        <div hidden id="Hktrack2activities" class="container-fluid align-items-center justify-content-center">
                            <div class="row my-auto">
                                <div class="my-auto col-xl-6 offset-xl-3 col-md-8 offset-md-2 pb-3 firsttime2">
                                    <div class="card p-3">
                                        <div class="row my-auto">
                                            <div class="col-10 col-md-10 col-sm-10 col-xs-10 mx-auto my-auto text-left">
                                                Each activity will unlock as you recieve them via text.
                                            </div>
                                            <div class="col-2 col-md-2 col-sm-2 col-xs-2 mx-auto my-auto">
                                                <img src="../images/Cancel-Icon@3x.png" width="25" class="firsttime2ex" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                        $counter = 1;
                                        for($b=0;$b<sizeof($activitylist2);$b++){
                                            $title = $activitylist2[$b]["ylpActivityTitle"];
                                            $desc = $activitylist2[$b]["ylpActivityDesc"];
                                            $link = $activitylist2[$b]["ylpActivityLink"];
                                            if($counter > $actcount2){$cardtype = 'disabled-card';}else{$cardtype = 'card-hover';}
                                            echo "<div class='row w-100 text-left mx-auto' style='padding-top: 10px;'>
                                                <div class='my-auto col-xl-6 offset-xl-3 col-md-8 offset-md-2'>
                                                    <div class='card p-3 card-activity $cardtype' style='min-height: 140px;' data-activitylink='$link' data-usertrack='2' data-activity='$counter'>
                                                        <div class='row my-auto'>
                                                            <div class='col-12 my-auto'>
                                                                <div class='text-left'>
                                                                    <h4>$title</h4>
                                                                    <br>
                                                                    <h6>$desc</h6>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>";
                                            $counter++;
                                        }
                                ?>
                            </div>
                        </div>
                        <div hidden id="Hktrack3activities" class="container-fluid align-items-center justify-content-center">
                            <div class="row my-auto">
                                <div class="my-auto col-xl-6 offset-xl-3 col-md-8 offset-md-2 pb-3 firsttime2">
                                    <div class="card p-3">
                                        <div class="row my-auto">
                                            <div class="col-10 col-md-10 col-sm-10 col-xs-10 mx-auto my-auto text-left">
                                                Each activity will unlock as you recieve them via text.
                                            </div>
                                            <div class="col-2 col-md-2 col-sm-2 col-xs-2 mx-auto my-auto">
                                                <img src="../images/Cancel-Icon@3x.png" width="25" class="firsttime2ex" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                        $counter = 1;
                                        for($c=0;$c<sizeof($activitylist3);$c++){
                                            $title = $activitylist3[$c]["ylpActivityTitle"];
                                            $desc = $activitylist3[$c]["ylpActivityDesc"];
                                            $link = $activitylist3[$c]["ylpActivityLink"];
                                            if($counter > $actcount3){$cardtype = 'disabled-card';}else{$cardtype = 'card-hover';}
                                            echo "<div class='row w-100 text-left mx-auto' style='padding-top: 10px;'>
                                                <div class='my-auto col-xl-6 offset-xl-3 col-md-8 offset-md-2'>
                                                    <div class='card p-3 card-activity $cardtype' style='min-height: 140px;' data-activitylink='$link' data-usertrack='3' data-activity='$counter'>
                                                        <div class='row my-auto'>
                                                            <div class='col-12 my-auto'>
                                                                <div class='text-left'>
                                                                    <h4>$title</h4>
                                                                    <br>
                                                                    <h6>$desc</h6>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>";
                                            $counter++;
                                        }
                                ?>
                            </div>
                        </div>
                        <div hidden id="Hktrack4activities" class="container-fluid align-items-center justify-content-center">
                            <div class="row my-auto">
                                <div class="my-auto col-xl-6 offset-xl-3 col-md-8 offset-md-2 pb-3 firsttime2">
                                    <div class="card p-3">
                                        <div class="row my-auto">
                                            <div class="col-10 col-md-10 col-sm-10 col-xs-10 mx-auto my-auto text-left">
                                                Each activity will unlock as you recieve them via text.
                                            </div>
                                            <div class="col-2 col-md-2 col-sm-2 col-xs-2 mx-auto my-auto">
                                                <img src="../images/Cancel-Icon@3x.png" width="25" class="firsttime2ex" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                        $counter = 1;
                                        for($d=0;$d<sizeof($activitylist4);$d++){
                                            $title = $activitylist4[$d]["ylpActivityTitle"];
                                            $desc = $activitylist4[$d]["ylpActivityDesc"];
                                            $link = $activitylist4[$d]["ylpActivityLink"];
                                            if($counter > $actcount4){$cardtype = 'disabled-card';}else{$cardtype = 'card-hover';}
                                            echo "<div class='row w-100 text-left mx-auto' style='padding-top: 10px;'>
                                                <div class='my-auto col-xl-6 offset-xl-3 col-md-8 offset-md-2'>
                                                    <div class='card p-3 card-activity $cardtype' style='min-height: 140px;' data-activitylink='$link'  data-usertrack='4' data-activity='$counter'>
                                                        <div class='row my-auto'>
                                                            <div class='col-12 my-auto'>
                                                                <div class='text-left'>
                                                                    <h4>$title</h4>
                                                                    <br>
                                                                    <h6>$desc</h6>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>";
                                            $counter++;
                                        }
                                ?>
                            </div>
                        </div>
                        <div hidden id="Hktrack5activities" class="container-fluid align-items-center justify-content-center">
                            <div class="row my-auto">
                                <div class="my-auto col-xl-6 offset-xl-3 col-md-8 offset-md-2 pb-3 firsttime2">
                                    <div class="card p-3">
                                        <div class="row my-auto">
                                            <div class="col-10 col-md-10 col-sm-10 col-xs-10 mx-auto my-auto text-left">
                                                Each activity will unlock as you recieve them via text.
                                            </div>
                                            <div class="col-2 col-md-2 col-sm-2 col-xs-2 mx-auto my-auto">
                                                <img src="../images/Cancel-Icon@3x.png" width="25" class="firsttime2ex" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                        $counter = 1;
                                        for($e=0;$e<sizeof($activitylist5);$e++){
                                            $title = $activitylist5[$e]["ylpActivityTitle"];
                                            $desc = $activitylist5[$e]["ylpActivityDesc"];
                                            $link = $activitylist5[$e]["ylpActivityLink"];
                                            if($counter > $actcount5){$cardtype = 'disabled-card';}else{$cardtype = 'card-hover';}
                                            echo "<div class='row w-100 text-left mx-auto' style='padding-top: 10px;'>
                                                <div class='my-auto col-xl-6 offset-xl-3 col-md-8 offset-md-2'>
                                                    <div class='card p-3 card-activity $cardtype' style='min-height: 140px;' data-activitylink='$link' data-usertrack='5' data-activity='$counter'>
                                                        <div class='row my-auto'>
                                                            <div class='col-12 my-auto'>
                                                                <div class='text-left'>
                                                                    <h4>$title</h4>
                                                                    <br>
                                                                    <h6>$desc</h6>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>";
                                            $counter++;
                                        }
                                ?>
                            </div>
                        </div>
                        <div hidden id="Hktrack6activities" class="container-fluid align-items-center justify-content-center">
                            <div class="row my-auto">
                                <div class="my-auto col-xl-6 offset-xl-3 col-md-8 offset-md-2 pb-3 firsttime2">
                                    <div class="card p-3">
                                        <div class="row my-auto">
                                            <div class="col-10 col-md-10 col-sm-10 col-xs-10 mx-auto my-auto text-left">
                                                Each activity will unlock as you recieve them via text.
                                            </div>
                                            <div class="col-2 col-md-2 col-sm-2 col-xs-2 mx-auto my-auto">
                                                <img src="../images/Cancel-Icon@3x.png" width="25" class="firsttime2ex" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                $counter = 1;
                                for($f=0;$f<sizeof($activitylist6);$f++){
                                    $title = $activitylist6[$f]["ylpActivityTitle"];
                                    $desc = $activitylist6[$f]["ylpActivityDesc"];
                                    $link = $activitylist6[$f]["ylpActivityLink"];
                                    if($counter > $actcount6){$cardtype = 'disabled-card';}else{$cardtype = 'card-hover';}
                                    echo "<div class='row w-100 text-left mx-auto' style='padding-top: 10px;'>
                                                <div class='my-auto col-xl-6 offset-xl-3 col-md-8 offset-md-2'>
                                                    <div class='card p-3 card-activity $cardtype' style='min-height: 140px;' data-activitylink='$link' data-usertrack='6' data-activity='$counter'>
                                                        <div class='row my-auto'>
                                                            <div class='col-12 my-auto'>
                                                                <div class='text-left'>
                                                                    <h4>$title</h4>
                                                                    <br>
                                                                    <h6>$desc</h6>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>";
                                    $counter++;
                                }
                                ?>
                            </div>
                        </div>
                        <div hidden id="Hkworkbooks" class="container-fluid align-items-center justify-content-center mb-5">
                            <div class="row my-auto">
                                <?php
                                $inactivecounter = 1;
                                $activestring = "";
                                $inactivestring = "<div class='row w-100 text-left mx-auto' style='padding-top: 10px;'>
                                                    <div class='my-auto col-xl-6 offset-xl-3 col-md-8 offset-md-2'>
                                                        <div class='row my-auto'>
                                                            <h4 class='text-left' style='margin-left:20px;'>What's Next:</h4>
                                                        </div>
                                                      </div>
                                                    </div>";
                                for($g=0;$g<sizeof($workbookList);$g++){
                                    $title = $workbookList[$g]["ylpActivityTitle"];
                                    $desc = $workbookList[$g]["ylpActivityDesc"];
                                    $link = $workbookList[$g]["ylpActivityLink"];
                                    $seq = $workbookList[$g]["ylpActivitySeq"];
                                    $enabled = (int)$workbookList[$g]["ylpEnabled"];
                                    if($enabled === 0){
                                        $cardtype = 'disabled-card';
                                        $inactivestring .= "<div class='row w-100 text-left mx-auto' style='padding-top: 10px;'>
                                                    <div class='my-auto col-xl-6 offset-xl-3 col-md-8 offset-md-2'>
                                                        <div class='card p-3 card-workbook $cardtype' style='min-height: 140px;' data-workbooklink='$link' data-workbookseq='$seq'>
                                                            <div class='row my-auto'>
                                                                <div class='col-12 my-auto'>
                                                                    <div class='text-left'>
                                                                        <h4>$title</h4>
                                                                        <br>
                                                                        <h6>$desc</h6>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>";
                                        $inactivecounter++;
                                    }else{
                                        $cardtype = 'card-hover';
                                        $activestring .= "<div class='row w-100 text-left mx-auto' style='padding-top: 10px;'>
                                                    <div class='my-auto col-xl-6 offset-xl-3 col-md-8 offset-md-2'>
                                                        <div class='card p-3 card-workbook $cardtype' style='min-height: 140px;' data-workbooklink='$link' data-workbookseq='$seq'>
                                                            <div class='row my-auto'>
                                                                <div class='col-12 my-auto'>
                                                                    <div class='text-left'>
                                                                        <h4>$title</h4>
                                                                        <br>
                                                                        <h6>$desc</h6>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>";
                                    }

                                }
                                echo $activestring;
                                if($inactivecounter>0){
                                    echo $inactivestring;
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item mt-3 mb-5 <?php if($directaccess)echo 'active'; ?>" style="margin-bottom:50px;">
                        <div id="HkActivityIframeRow" class="row" style="width: 95%; min-height: 1200px; margin: 50px auto;">
                            <div id="HkActivityIframe" class="container-fluid embed-responsive">
                                <?php
                                    if($directaccess){
                                        $currentActivity = $activities->GET_ACTIVITIES($directtrack,$activity);
                                        $activitylink = $currentActivity[0]["ylpActivityLink"];
                                        //  echo '<object id="iframeview" type="text/html" data="' . $activitylink . '" ></object>';
                                        echo '<iframe onload="setIframeHeight(this.id)" id="iframeview" src="' . $activitylink . '"   allowfullscreen></iframe>';
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="HKcontainer2" class="container-fluid" style="display: none;">
            <div class="container pt-5 pb-5">
                <div class="accordion" style="padding:30px 0;" id="accordionExample">
                    <div class="card">
                        <div class="card-header" id="headingOneF">
                            <h2 class="mb-0">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionExample" href="#collapseOneF">What is HeyKiddo&trade;? </a>
                            </h2>
                        </div>
                        <div id="collapseOneF" class="collapse" aria-labelledby="headingOneF" data-parent="#accordionExample">
                            <div class="card-body">
                                <p>
                                    HeyKiddo™ is a text-based subscription that provides curated conversation starters, coaching tips and activities to help support your child’s development. Current topics include: ways to build and manage confidence, stress, relationships, social intelligence, self-control and thoughtful decisions. We’re always building new topics to discuss with your child! Please
                                    <a style="all: unset;text-decoration: underline;color:blue;" href="mailto:support@hey-kiddo.com">contact us</a> if you have special requests!
                                </p>
                                <br />
                                <p>Children grow by watching others model new skills, having open communication about experiences related to these skills, and by engaging in activities that reinforce new skills. We have included all of these methods in HeyKiddo™, making it a playbook for their individual growth.</p>
                                <br />
                                <p>Although we will mainly communicate with you via text, all of your activities are accessible in the HeyKiddo™ parent portal, just in case you want to do them more than once, maybe with the whole family!</p>
                                <br />
                                <p>And we know as a parent you are super busy, so HeyKiddo™ is flexible and lets you choose the time of day that works best for you and your child to receive our conversation starters - whether on the way to school, in the middle of the day over lunch, in the afternoon after school, or before bedtime.</p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h2 class="mb-0">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionExample" href="#collapseOne"> What will my family learn through HeyKiddo&trade;? </a>
                            </h2>
                        </div>
                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                            <div class="card-body">
                                <p>Each week, you’ll learn new ways of connecting with your child through conversation starters and activities that develop social, emotional and leadership skills vital for future success.</p>
                                <br />
                                <p>All of our conversation starters and activities are geared toward helping you and your child build critical skills by deepening your conversations with them regarding difficult issues that may arise day-to-day.</p>
                                <br />
                                <p>Sometimes kids aren’t as used to talking about these things as parents are … so although it may feel strange for some at first, stick with it! Opening up the lines of communication between you and your child will help them reach out and talk with you with more ease. When things arise - both good and bad in your child’s life - they will know they can come to you to talk about anything.</p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h2 class="mb-0">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionExample" href="#collapseTwo">How to use HeyKiddo&trade;? </a>
                            </h2>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                            <div class="card-body">
                                <img style="float: left;margin-right: 20px;" src="../images/How-to-Icon1.png" alt=""><p style="padding-top: 10px;">Once you are signed up for HeyKiddo&trade;, you will begin receiving texts three times each week that include expertly curated content that helps your child grow and develop.</p>
                                <br />
                                <img style="float: left;margin-right: 20px;" src="../images/How-to-Icon2.png" alt=""><p style="padding-top: 10px;">After receiving each text, take time to read and think about the content of the text. Some texts will be geared toward you, the parent, with areas for you to self-reflect on, whereas at other times, there will be texts that help guide your conversations with your child.</p>
                                <br />
                                <img style="float: left;margin-right: 20px;" src="../images/How-to-Icon3.png" alt=""><p style="padding-top: 10px;">HeyKiddo&trade; texts that offer targeted suggestions should help guide you through conversations with your child, not dictate exactly what you should say to them. You know your child better than anyone, so use what you know about them to make the content as impactful as possible, meeting them where they are.</p>
                                <br />
                                <img style="float: left;margin-right: 20px;" src="../images/How-to-Icon4.png" alt=""><p style="padding-top: 10px;">As a part of this, use your own voice and style when interacting with and presenting the HeyKiddo&trade; content to your child. Focus on making the interactions natural and genuine.</p>
                                <br />
                                <img style="float: left;margin-right: 20px;" src="../images/How-to-Icon5.png" alt=""><p style="padding-top: 10px;">Implementing a new routine, learning new skills, and having deeper conversations are all things that can be difficult when we first try them, but if you stick with it and make it a consistent practice, deeper conversations will become a natural part of you and your child’s daily experience.</p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h2 class="mb-0">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionExample" href="#collapseThree">HeyKiddo&trade; the basics:</a>
                            </h2>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                            <div class="card-body">
                                <p>
                                    Within HeyKiddo™, you will see repeated symbols and prompts. Here are a few to be on the lookout for:
                                    <ul>
                                        <li>
                                            Some texts are just for you, the parent, and are geared toward your own self-reflection. When you see “
                                            <b style="color:red;">Self-Reflection Questions…</b>” it means that these questions are for you to reflect on.
                                        </li>
                                        <li>
                                            HeyKiddo™ is also packed full of questions and guided actions for you to try with your kid. These questions are noted by “
                                            <b style="color:red;">Hey Kiddo! Ask & Share…</b>”
                                            <ul style="list-style-type:circle;">
                                                <li>
                                                    <b style="color:red;">❓</b> prompts you to ask yourself or your child a question.
                                                </li>
                                                <li>
                                                    <b style="color:red;">❗</b> indicates it is time for you to take action by interacting with your child.
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h2 class="mb-0">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionExample" href="#collapseFour">Adjusting HeyKiddo&trade; for your child:</a>
                            </h2>
                        </div>
                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                            <div class="card-body">
                                <p>One thing we cannot say enough is that all of the content provided as a part of HeyKiddo&trade; are suggestions to help facilitate deeper conversations with your child. While using HeyKiddo&trade;, it is important that you adjust based on your child’s needs and responses to the content and activities.</p>
                                <br />
                                <p>
                                    <u>Adjusting Activities</u> - Sometimes your child will need help reading and understanding the directions and goals of the activities, other times they might need help finding the supplies but can do the activity on their own. And sometimes they might be able to do the entire activity on their own. You know your child best and you will have to gauge the amount of support they will need from you!
                                </p>
                                <br />
                                <p>
                                    <u>Adjusting Questions</u> - The questions we provide for you are meant to be general enough so that you can find a way to ask them to your child in a way that is natural and will make the most sense to them. It is recommended that questions be adapted to fit the child’s age and communication level. Also be sure to use words you would generally use and that your child can understand.
                                </p>
                                <br />
                                <p>
                                    <u>Adjusting Prompts</u> - Whenever you are prompted to model a skill, it might be something that feels uncomfortable for you because it is something new. That’s okay and is actually our goal … it means growth is happening! Our goal is to help you build skills that help you engage your child in deeper conversations, and doing that might require some reflecting and practicing on your end.
                                </p>
                                <br />
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h2 class="mb-0">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionExample" href="#collapseFive">What if my child finds HeyKiddo&trade; challenging?</a>
                            </h2>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                            <div class="card-body">
                                <p>When kids feel challenged, it means they are pushing the limits of what they know and do, and this is exactly our goal! The HeyKiddo™ content is meant to challenge both you and your child to explore new topics and skills that will help your child succeed now and in the future. Be sure to push your child to try and learn new things and reward them when they overcome these challenges.</p>
                                <br />
                                <p>If you find that your child is having a negative response to HeyKiddo™, start by adapting it to their needs by asking them what they like, don’t like, and ways the conversations and activities could be implemented more naturally into their routine. Be open to their opinions and suggestions and work with them to make this work for everyone involved.</p>
                                <br />
                                <p>
                                    If they continue to have a negative response after you have adapted the content, please discontinue use and
                                    <a style="all: unset;text-decoration: underline;color:blue;" href="mailto:support@hey-kiddo.com">contact</a> our support team to share feedback regarding your child’s response.
                                </p>
                                <br />
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingSix">
                            <h2 class="mb-0">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionExample" href="#collapseSix">How to contact HeyKiddo&trade; for technical support</a>
                            </h2>
                        </div>
                        <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionExample">
                            <div class="card-body">
                                <p>
                                    If you run into any issues with HeyKiddo&trade;, please contact our support team immediately at
                                    <a style="all: unset;text-decoration: underline;color:blue;" href="mailto:support@hey-kiddo.com">support@hey-kiddo.com</a>. Our team is standing by to resolve any technical issues or concerns that arise. Your feedback is valuable in making this an even more powerful tool for parents and children everywhere.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingSeven">
                            <h2 class="mb-0">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionExample" href="#collapseSeven">How to unsubscribe from HeyKiddo&trade;</a>
                            </h2>
                        </div>
                        <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordionExample">
                            <div class="card-body">
                                <p>
                                    There are two ways to unsubscribe:
                                    <ol>
                                        <li>
                                            Visit
                                            <a style="all: unset;text-decoration: underline;color:blue;" target="_blank" href="https://www.paypal.com/cgi-bin/webscr?cmd=_subscr-find&alias=D7U7FLTV2BXNY">Paypal</a> and login to your account. Find your subscription for HeyKiddo&trade; and click unsubscribe. Paypal will automatically unsubscribe you from the product and stop any subsequent payments.
                                        </li>
                                        <li>
                                            Email us directly at
                                            <a style="all: unset;text-decoration: underline;color:blue;" href="mailto:support@hey-kiddo.com">support@hey-kiddo.com</a> and we will unsubscribe you. This may take up to 5 business days and we will manually unsubscribe you from the product and stop any subsequent payments.
                                        </li>
                                    </ol>
                                </p>
                                <div class="row mx-auto text-center mt-2">
                                    <div class="col-xl-4 mx-auto col-lg-4">
                                        <img data-enlargable style="cursor: zoom-in;" src="../images/paypal1.png" width="200" class="img-fluid" alt="" />
                                    </div>
                                    <div class="col-xl-4 mx-auto col-lg-4">
                                        <img data-enlargable style="cursor: zoom-in;" src="../images/paypal2.png" width="200" class="img-fluid" alt="" />
                                    </div>
                                    <div class="col-xl-4 mx-auto col-lg-4">
                                        <img data-enlargable style="cursor: zoom-in;" src="../images/paypal3.png" width="200" class="img-fluid" alt="" />
                                    </div>
                                </div>
                                <div class="row mx-auto text-center mt-2">
                                    <div class="col-xl-3 mx-auto col-lg-3">
                                        <img data-enlargable style="cursor: zoom-in;" src="../images/paypal4.png" width="200" class="img-fluid" alt="" />
                                    </div>
                                    <div class="col-xl-3 mx-auto col-lg-3">
                                        <img data-enlargable style="cursor: zoom-in;" src="../images/paypal5.png" width="200" class="img-fluid" alt="" />
                                    </div>
                                    <div class="col-xl-3 mx-auto col-lg-3">
                                        <img data-enlargable style="cursor: zoom-in;" src="../images/paypal6.png" width="200" class="img-fluid" alt="" />
                                    </div>
                                    <div class="col-xl-3 mx-auto col-lg-3">
                                        <img data-enlargable style="cursor: zoom-in;" src="../images/paypal7.png" width="200" class="img-fluid" alt="" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="HKcontainer3" class="container-fluid align-items-center justify-content-center" style="display: none;">
            <div id="HKAccountForm" class="row my-auto mx-auto pt-5 pb-5">
                <div class="col-lg-12 my-auto">
                    <div class="card w-100 p-2">
                        <!--Card content-->
                        <div class="card-body p-3">
                            <div class="form-group">
                                <label for="hkname" class="control-label">Name</label>
                                <div class="position-absolute" style="right: 30px;">
                                    <a href="#" style="color: #54B588;" data-accountproperty="user_name" class="HKaccountEdit" onclick="return false;">
                                        <b>EDIT</b>
                                    </a>
                                </div>
                                <input type="text" name="hkname" class="form-control bg-white" id="hkname" value="<?php echo $username; ?>" readonly />

                            </div>
                            <div class="form-group">
                                <label for="hkemail" class="control-label">Email</label>
                                <div class="position-absolute" style="right: 30px;">
                                    <a href="#" style="color: #54B588;" data-accountproperty="user_email" class="HKaccountEdit" onclick="return false;">
                                        <b>EDIT</b>
                                    </a>
                                </div>
                                <input type="email" name="hkemail" class="form-control bg-white" id="hkemail" value="<?php echo $useremail; ?>" readonly />
                            </div>
                            <div class="form-group">
                                <label for="hkpassword" class="control-label">Password</label>
                                <div class="position-absolute" style="right: 30px;">
                                    <a href="#" style="color: #54B588;" data-accountproperty="user_pass" class="HKaccountEdit" onclick="return false;">
                                        <b>EDIT</b>
                                    </a>
                                </div>
                                <input type="password" name="hkpassword" class="form-control bg-white" id="hkpassword" value="<?php echo $userpassword; ?>" readonly />
                            </div>
                            <div class="form-group">
                                <label for="hkphone" class="control-label">Cellphone</label>
                                <div class="position-absolute" style="right: 30px;">
                                    <a href="#" style="color: #54B588;" data-accountproperty="user_phone" class="HKaccountEdit" onclick="return false;">
                                        <b>EDIT</b>
                                    </a>
                                </div>
                                <input type="tel" name="hkphone" class="form-control bg-white" id="hkphone" value="<?php echo $userphone; ?>" readonly />
                            </div>
                            <div style="padding-top:20px;">
                                <center>
                                    <a href="#" onclick="return false;" class="HKUnsubscribe" id="HKshowUnsubscribe">UNSUBSCRIBE</a>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="HKUnsubscribeForm" class="row my-auto mx-auto pt-5 pb-5" style="display: none;">
                <div class="col-lg-12 my-auto">
                    <div class="card w-100 p-2">
                        <!--Card content-->
                        <div class="card-body p-3">
                            <div id="HKUnsubscribeSurvery">
                                <form id="unsubscribe-survey">
                                    <h6><b>We're sorry to see you go, please tell us why you're leaving:</b></h6>
                                    <br />
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="HKUnsubscribeSel" id="HKUnsubscribeSel1" value="A" checked />
                                        <label class="form-check-label" for="HKUnsubscribeSel1">
                                            I didn’t find the product useful
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="HKUnsubscribeSel" id="HKUnsubscribeSel2" value="B" />
                                        <label class="form-check-label" for="HKUnsubscribeSel2">
                                            I like other resources better
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="HKUnsubscribeSel" id="HKUnsubscribeSel3" value="C" />
                                        <label class="form-check-label" for="HKUnsubscribeSel3">
                                            Too many texts
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="HKUnsubscribeSel" id="HKUnsubscribeSel4" value="D" />
                                        <label class="form-check-label" for="HKUnsubscribeSel4">
                                            Other
                                        </label>
                                        <input type="text" class="radio-inline" id="HKOtherReason" placeholder="Please Specify" disabled />
                                    </div>
                                    <br />
                                    <div class="form-group">
                                        <label for="comment">How can we improve?</label>
                                        <textarea class="form-control" id="HKCanweimprove" rows="5"></textarea>
                                    </div>
                                    <span class="help-block" hidden id="unsubscribeformhelp"></span>
                                </form>
                                <div class="row text-center" style="padding-top:20px;">
                                    <div class="col-6">
                                        <a href="#" onclick="return false;" class="HKgreenBtn" id="HKkeepSignedIn">KEEP ME SIGNED UP</a>
                                    </div>
                                    <div class="col-6">
                                        <a href="#" onclick="return false;" class="HKUnsubscribe" id="HKkillAccount">UNSUBSCRIBE</a>
                                    </div>
                                </div>
                            </div>
                            <div id="HKFeedback" style="display: none;min-height:400px;">
                                <center>
                                    <strong>Thanks for your feedback, you're now unsubscribed.</strong>
                                </center>
                                <div class="row text-center" style="padding-top:20px;">
                                    <div class="col-12">
                                        <center>
                                            <a href="." class="HKgreenBtn">BACK TO WEBSITE</a>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <footer <?php if($firsttime === true)echo 'hidden';?> class="text-center HKSignupFoot">
        <div class="container pt-5">
            <div class="row my-auto">
                <div class="col-12 my-auto">
                    <p style="text-align: center; margin: auto; padding-bottom: 5px;">© Young Leader Project®, LLC. All Rights Reserved.</p>
                    <p style="text-align: center; margin: auto; font-weight: 100; font-size: 12px; max-width: 550px; padding-bottom: 10px;">
                        HeyKiddo&trade; and
                        <a target="_blank" style="color:inherit;" href="https://youngleaderproject.com">Young Leader Project&trade;</a> do not provide medical or psychological advice, diagnosis, or treatment.
                        <a style="all: unset;text-decoration: underline;color:blue;cursor:pointer;" href="javascript:window.open('https://www.youngleaderproject.com/disclaimer.html', 'YLP Disclaimer', 'width=600,height=450');">See additional information</a>.
                    </p>
                </div>
            </div>
        </div>
    </footer>
    <script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
    <script src="../js/HKportal.js?v=3"></script>
    <script src="../js/hkglobals.js"></script>
    <script type="text/javascript">
        <?php echo 'window.ylp.currentTrack="' . $directtrack .'";'   ?>
        <?php echo "window.ylp.dev='$dev'";?>
    </script>
    <script type="text/javascript">
    (function(e,t,o,n,p,r,i){e.visitorGlobalObjectAlias=n;e[e.visitorGlobalObjectAlias]=e[e.visitorGlobalObjectAlias]||function(){(e[e.visitorGlobalObjectAlias].q=e[e.visitorGlobalObjectAlias].q||[]).push(arguments)};e[e.visitorGlobalObjectAlias].l=(new Date).getTime();r=t.createElement("script");r.src=o;r.async=true;i=t.getElementsByTagName("script")[0];i.parentNode.insertBefore(r,i)})(window,document,"https://diffuser-cdn.app-us1.com/diffuser/diffuser.js","vgo");
    vgo('setAccount', '25992310');
    vgo('setTrackByDefault', true);

        vgo('process');

    </script>
</body>
</html>