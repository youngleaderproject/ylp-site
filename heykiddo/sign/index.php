<?php
error_reporting(E_ALL);
try {
    session_start();
}
catch(\Exception $ex) {
    file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/heykiddosign.txt' , print_r($_SESSION,true)."\r\n", FILE_APPEND);
}

$linkpos = strpos(strtolower($_SERVER[ "DOCUMENT_ROOT" ]),"heykiddo");
if( $linkpos > 0){
	$_SERVER[ "DOCUMENT_ROOT" ] = substr($_SERVER[ "DOCUMENT_ROOT" ],0,$linkpos-1);
}

require_once str_replace( '\\', '/', $_SERVER[ "DOCUMENT_ROOT" ] ) . '/PHP/shared/auto_load.php';
$_REQUEST = array_change_key_case($_REQUEST, CASE_LOWER);
$dev = (isset($_SESSION["signupuser"]["dev"]) ? $_SESSION["signupuser"]['dev'] : '');
if (trim($dev) === '' || $dev === false) {
    Redirect( "../" );
}
session_id($dev);
session_reset();

$fullname = "";

$firsttime = (isset($_SESSION["signupuser"]['firsttime']) ? $_SESSION["signupuser"]['firsttime'] :false);
if(!$firsttime){
    ClearSession();
    Redirect( "../" );
}else{
    $hkfname = $_SESSION["signupuser"]["userfname"];
    $hklname = $_SESSION["signupuser"]["userlname"];
    $hkpass = $_SESSION["signupuser"]["userpassword"];
    $hkphone = $_SESSION["signupuser"]["userphone"];
    $hkemail = $_SESSION["signupuser"]["useremail"];
    $fullname = $hkfname.' '.$hklname;
}
?>
<html>
<head>
    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
        w[l] = w[l] || []; w[l].push({
            'gtm.start':
                new Date().getTime(), event: 'gtm.js'
        }); var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-NLW5ZB7');</script>
    <!-- End Google Tag Manager -->
    <meta charset="utf-8" />
    <meta name="robots" content="noindex, nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="icon" href="../images/Hey-Kiddo-favicon.ico" type="favicon/ico" />
    <title>HeyKiddo&trade; Portal | Sign Up</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="../css/HKsignup.css?v=1" />
</head>

<body>
    <div class="loading" hidden>Loading&#8230;</div>
    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NLW5ZB7"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div class="modal" id="promomodal" tabindex="-1" role="dialog" aria-labelledby="..." aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <center>
                        <h5>Hey-Kiddo&trade; Promo Code</h5>
                    </center>
                    <br />
                    <div class="form-group">
                        <label id="HKpromoEditL" class="control-label">Enter Promo Code</label>
                        <div>
                            <input class="form-control" type="text" id="hkpromocode" name="hkpromocode" />
                        </div>
                        <div class="col-md-12 my-auto">
                            <span class="help-block" style="color: red" hidden id="signuppromohelp">Please enter a valid Promo Code.</span>
                        </div>
                    </div>
                    <center>
                        <button type="button" class="btn HKpromoSave">SUBMIT</button>
                    </center>
                </div>
            </div>
        </div>
    </div>
    <div class="progress" style="max-height:10px;">
        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="4" style="width: 25%;background-color: #7C7C7C;"></div>
    </div>
    <a href="#" onclick="return false;" hidden class="outercarouselback">
        <img src="../images/Back-Icon@2x.png" style="position: absolute; z-index: 999; top: 20; left: 10;" width="50" class="img-fluid" />
    </a>
    <div id="wrap">
        <div id="HKcontainer1" class="container-fluid pt-5 maincontainer" style="padding-left:0px;padding-right:0px;overflow-x:hidden;">
            <div class="row">
                <div class="col-12 my-auto">
                    <center>
                        <h1>
                            Welcome <?php echo $fullname;?>!
                        </h1>
                        <p>Choose your first talking topic.</p>
                        <p>I want to help my child with...</p>
                    </center>
                </div>
                <div class="container-fluid my-auto">
                    <div id="carouselExample" class="carousel slide" data-ride="carousel" data-interval="false">
                        <div class="carousel-inner row my-auto mx-auto" role="listbox">
                            <div class="carousel-item text-center col-12 col-md-6 col-lg-4 col-xl-3 active">
                                <div class="card HKSelect" style="border-top-width: 10px; border-top-color: #70C08F;" data-optionchoice="1">
                                    <div class="cardImages">
                                        <img src="../images/icon-Self-Awareness@2x.png" width="100" class="img-fluid" alt="" />
                                    </div>
                                    <h4 class="card-title pt-5">
                                        <b>Confidence</b>
                                    </h4>
                                    <div class="card-body">
                                        <h6 style="max-width: 250px;" class="mx-auto"> Help your child understand their unique personality </h6>
                                        <div class="text-left">
                                            Best for:
                                            <ul class="custom-list">
                                                <li>Shyness</li>
                                                <li>Emotion regulation</li>
                                                <li>Quitting too soon</li>
                                                <li>Good wellness habits</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item text-center col-12 col-md-6 col-lg-4 col-xl-3">
                                <div class="card HKSelect" style="border-top-width: 10px; border-top-color: #4DAAD5;" data-optionchoice="2">
                                    <div class="cardImages">
                                        <img src="../images/icon-Self-Management@2x.png" width="100" class="img-fluid" alt="" />
                                    </div>
                                    <h4 class="card-title pt-5">
                                        <b>Stress</b>
                                    </h4>
                                    <div class="card-body">
                                        <h6 style="max-width: 250px;" class="mx-auto"> Help your child stay positive and manage stress </h6>
                                        <div class="text-left">
                                            Best for:
                                            <ul class="custom-list">
                                                <li>Anxiety</li>
                                                <li>Overachieving</li>
                                                <li>Worry</li>
                                                <li>Difficulty with new things</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item text-center col-12 col-md-6 col-lg-4 col-xl-3">
                                <div class="card HKSelect" style="border-top-width: 10px; border-top-color: #4BB7BA;" data-optionchoice="3">
                                    <div class="cardImages">
                                        <img src="../images/icon-Social Awareness@2x.png" width="100" class="img-fluid" alt="" />
                                    </div>
                                    <h4 class="card-title pt-5">
                                        <b>Social Intelligence</b>
                                    </h4>
                                    <div class="card-body">
                                        <h6 style="max-width: 250px;" class="mx-auto">Help your child understand others and different perspectives</h6>
                                        <div class="text-left">
                                            Best for:
                                            <ul class="custom-list">
                                                <li>Bullying</li>
                                                <li>Building empathy</li>
                                                <li>Appreciating diversity</li>
                                                <li>Social challenges</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item text-center col-12 col-md-6 col-lg-4 col-xl-3">
                                <div class="card HKSelect" style="border-top-width: 10px; border-top-color: #F2A4CA;" data-optionchoice="4">
                                    <div class="cardImages">
                                        <img src="../images/icon-Relationship Skills@2x.png" width="100" class="img-fluid" alt="" />
                                    </div>
                                    <h4 class="card-title pt-5">
                                        <b>Relationships</b>
                                    </h4>
                                    <div class="card-body">
                                        <h6 style="max-width: 250px;" class="mx-auto">Help your child develop strong relationships with family and peers</h6>
                                        <div class="text-left">
                                            Best for:
                                            <ul class="custom-list">
                                                <li>Sibling rivalry</li>
                                                <li>Going to a new school</li>
                                                <li>Family dynamics</li>
                                                <li>Working well with others</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item text-center col-12 col-md-6 col-lg-4 col-xl-3">
                                <div class="card HKSelect" style="border-top-width: 10px; border-top-color: #FFD771;" data-optionchoice="5">
                                    <div class="cardImages">
                                        <img src="../images/icon-Impulse Control@2x.png" width="100" class="img-fluid" alt="" />
                                    </div>
                                    <h4 class="card-title pt-5">
                                        <b>Thoughtful Decisions</b>
                                    </h4>
                                    <div class="card-body">
                                        <h6 style="max-width: 250px;" class="mx-auto">Help your child set goals and stay organized</h6>
                                        <div class="text-left">
                                            Best for:
                                            <ul class="custom-list">
                                                <li>Improving organization</li>
                                                <li>Enhancing critical thinking</li>
                                                <li>Improving study skills</li>
                                                <li>Taking initiative at home and school</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item text-center col-12 col-md-6 col-lg-4 col-xl-3">
                                <div class="card HKSelect" style="border-top-width: 10px; border-top-color: #B499DD;" data-optionchoice="6">
                                    <div class="cardImages">
                                        <img src="../images/icon-Decision Making@2x.png" width="100" class="img-fluid" alt="" />
                                    </div>
                                    <h4 class="card-title pt-5">
                                        <b>Self-Control</b>
                                    </h4>
                                    <div class="card-body">
                                        <h6 style="max-width: 250px;" class="mx-auto">Help your child respond instead of react</h6>
                                        <div class="text-left">
                                            Best for:
                                            <ul class="custom-list">
                                                <li>Anger issues</li>
                                                <li>Disruptive behavior</li>
                                                <li>Being a good role model</li>
                                                <li>Thinking before acting</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item text-center col-12 col-md-6 col-lg-4 col-xl-3">
                                <div class="card HKSelect" style="border-top-width: 10px; border-top-color: #FF9353;" data-optionchoice="7">
                                    <div class="cardImages">
                                        <img src="../images/icon-Core Skills@2x.png" width="100" class="img-fluid" alt="" />
                                    </div>
                                    <h4 class="card-title pt-5">
                                        <b>Expert’s Choice</b>
                                    </h4>
                                    <div class="card-body">
                                        <h6 style="max-width: 250px;" class="mx-auto">We'll pick for you</h6>
                                        <div class="text-left">
                                            Best for:
                                            <ul class="custom-list">
                                                <li>Building social, emotional, and leadership skills</li>
                                                <li>Learning new concepts</li>
                                                <li>Building on positive outcomes</li>
                                                <li>Personal development</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExample" role="button" data-slide="prev">
                            <i class="fa fa-chevron-circle-left fa-lg"></i>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next text-faded" href="#carouselExample" role="button" data-slide="next">
                            <i class="fa fa-chevron-circle-right fa-lg"></i>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>

                </div>
                
            </div>
            <div class="row text-center w-100 mx-auto">
                <div class="col-12">
                    There may be several topics that are important for your child. Choose what would be the most helpful topic right now.
                </div>
                <div class="col-12">
                    At the end of 8 weeks, you'll be able to unlock a new one.
                </div>
            </div>
        </div>


        <div id="HKcontainer2" class="container-fluid maincontainer" style="display: none;padding-left:0px;padding-right:0px;overflow-x:hidden;">
            <div class="row w-75 mx-auto">
                <div class="col-12 my-auto" style="margin-bottom:100px !important;">
                    <center>
                        <br />
                        <h4>When is the best time of day for you to receive our texts?</h4>
                    </center>
                </div>
                <div class="row w-100 text-center mx-auto pb-lg-2">
                    <div class="col-lg-6 my-auto">
                        <div class="card p-3 card-click" style="cursor:pointer;" data-timeselection="1">
                            <div class="row my-auto">
                                <div class="col-7 my-auto">
                                    <div class="text-left">
                                        <h5>In the morning</h5>
                                    </div>
                                </div>
                                <div class="col-4 my-auto offset-1">
                                    <img src="../images/icon-morning@2x.png" width="100" class="img-fluid" alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 my-auto">
                        <div class="card p-3 card-click" style="cursor:pointer;" data-timeselection="2">
                            <div class="row my-auto">
                                <div class="col-7 my-auto">
                                    <div class="text-left">
                                        <h5>Right after school</h5>
                                    </div>
                                </div>
                                <div class="col-4 my-auto offset-1">
                                    <img src="../images/icon-afternoon@2x.png" width="100" class="img-fluid" alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row w-100 text-center mx-auto">
                    <div class="col-lg-6 my-auto">
                        <div class="card p-3 card-click" style="cursor:pointer;" data-timeselection="3">
                            <div class="row my-auto">
                                <div class="col-7 my-auto">
                                    <div class="text-left">
                                        <h5>During dinner</h5>
                                    </div>
                                </div>
                                <div class="col-4 my-auto offset-1">
                                    <img src="../images/icon-dinner@2x.png" width="100" class="img-fluid" alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 my-auto">
                        <div class="card p-3 card-click" style="cursor:pointer;" data-timeselection="4">
                            <div class="row my-auto">
                                <div class="col-7 my-auto">
                                    <div class="text-left">
                                        <h5>Around bedtime</h5>
                                    </div>
                                </div>
                                <div class="col-4 my-auto offset-1">
                                    <img src="../images/icon-bedtime@2x.png" width="100" class="img-fluid" alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row w-100 text-center mx-auto" style="padding-top: 10px;">
                    <div class="col-lg-12 my-auto">
                        <span class="help-block" style="color: red" hidden id="signuptimehelp">Please select a time.</span>
                    </div>
                </div>
            </div>
        </div>


        <div id="HKcontainer3" class="container-fluid maincontainer" style="display: none;padding-left:0px;padding-right:0px;overflow-x:hidden;">
            <div class="row w-75 mx-auto">
                <div class="col-12 my-auto" style="margin-bottom:100px !important;">
                    <center>
                        <br />
                        <h4>Select Your Plan</h4>
                    </center>
                </div>
                <div class="row w-100 text-center mx-auto" style="margin-bottom:50px !important;">
                    <div class="col-lg-10 my-auto offset-lg-1" style="padding: 10px !important;background-color:#73C8A0;">
                        <center style="color:white;">
                            Use code
                            <b>BEHOME</b> for 1 month free to help you get through this time with your kiddos.
                        </center>
                    </div>
                </div>
                <div class="row w-100 text-center mx-auto pb-lg-2">
                    <!--<div class="col-md-6 my-auto">
                        <div class="card p-3 card-plan d-flex" style="min-height: 140px;cursor:pointer;" data-planselection="1">
                            <div class="row my-auto justify-content-center">
                                <div class="col-8 my-auto">
                                    <div class="text-left">
                                        <h5>Yearly</h5>
                                        <h4>$99.99/year</h4>
                                    </div>
                                </div>
                                <div class="col-4 my-auto">
                                    <img src="../images/tag-bestValue.png" width="80" class="img-fluid " alt="" />
                                </div>
                            </div>
                        </div>
                    </div>-->
                    <div class="col-lg-6 my-auto offset-lg-3">
                        <div class="card p-3 card-plan d-flex" style="min-height: 140px;cursor:pointer;" data-planselection="2">
                            <div class="row my-auto justify-content-center">
                                <div class="col-8 my-auto">
                                    <div class="text-left">
                                        <h5>Monthly</h5>
                                        <h4>$9.99/month</h4>
                                    </div>
                                </div>
                                <div class="col-4 my-auto"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row w-100 text-center mx-auto">
                    <div class="my-auto col-lg-6 offset-lg-3">
                        <div class="card p-3 card-plan d-flex" style="min-height: 140px;cursor:pointer;" data-planselection="3">
                            <div class="row my-auto justify-content-center">
                                <div class="col-12 my-auto">
                                    <div class="text-left">
                                        <h5>Have a Promo Code?</h5>
                                        <h4>Click Here!</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row w-100 text-center mx-auto" style="padding-top: 10px;">
                    <div class="col-md-12 my-auto">
                        <span class="help-block" style="color: red" hidden id="signupplanhelp">Please select a plan.</span>
                    </div>
                </div>
            </div>
        </div>


        <div id="HKcontainer4" class="container-fluid" style="display: none;padding-left:0px;padding-right:0px;overflow:hidden;">
            <div class="row w-75 mt-3 h-100 mx-auto" style="padding-bottom: 450px; margin-bottom: 100px !important;">
                <div class="col-12 my-auto" style="margin-bottom:50px !important;">
                    <center>
                        <h2>
                            <b>Okay! To recap...</b>
                        </h2>
                        <br />
                        <h4>You want to...</h4>
                    </center>
                </div>
                <div class="row w-100 mx-auto">
                        <div id="finish-list" class="text-left mx-auto"></div>
                </div>
            </div>
            <div class="footer2 text-center">
                <div class="container">
                    <img src="../images/recap-graphic.png" width="400" class="img-fluid" style="margin-top:-150px;" alt="" />
                    <div class="row mx-auto text-center">
                        <div class="col-12">
                            <p id="paypalparagraph" style="color: white">Looks good? You'll be brought to Paypal to finish the payment and you are all set to go!<br />If using a promo code, you will still have to enter your PayPal details but will not be charged until your promo ends. You can cancel at any time.</p>
                            <br />
                            <div hidden class="paypal-button" id="paypal-plan-container1"></div>
                            <div hidden class="paypal-button" id="paypal-plan-container2"></div>
                            <div hidden class="paypal-button" id="paypal-plan-container3"></div>
                            <div hidden class="paypal-button" id="paypal-plan-container4"></div>
                            <div hidden class="paypal-button" id="paypal-plan-container5"></div>
                            <button hidden class="btn text-white" id="HKnext4">START FOR FREE</button>
                            <div class="col-md-12 my-auto">
                                <span class="help-block" style="color: red" hidden id="signuppaymenthelp"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer id="HKcardfoot" class="footer">
        <div class="container">
            <div class="HKSignupFoot">
                <div class="row w-100 text-center">
                    <div class="col-12">
                        <p style="color: white">If you have more than one child, don't worry! HeyKiddo&trade; activities and talking tips can help the whole family!</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <script
        src="https://code.jquery.com/jquery-1.12.4.min.js"
        integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
        crossorigin="anonymous"></script>
    <script src="https://cdn.rawgit.com/nnattawat/flip/master/dist/jquery.flip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="../js/HKsignup.js?v=1"></script>
    <script src="../js/bootstrap-formhelpers.min.js"></script>
    <script src="../js/hkglobals.js"></script>
    <script src="https://www.paypal.com/sdk/js?client-id=AemoZTCghPRClgHoE_fSAJSAVTmPEReSPIXkODaocVqrvxx08U0iZDPQWdMILGIN85oj6HKV9GUg-dTv&vault=true"></script>
    <script>
        paypal.Buttons({
            style: {
                shape: 'pill',
                color: 'gold',
                layout: 'horizontal',
                label: 'paypal'
            },
            createSubscription: function (data, actions) {
                return actions.subscription.create({
                    'plan_id': 'P-9Y572735FD7385155LYN5MGA'
                });
            },
            onApprove: function (data, actions) {
                SigningIn("hkDONE", data.subscriptionID);
            }
        }).render('#paypal-plan-container2');
        paypal.Buttons({
            style: {
                shape: 'pill',
                color: 'gold',
                layout: 'horizontal',
                label: 'paypal'
            },
            createSubscription: function (data, actions) {
                return actions.subscription.create({
                    'plan_id': 'P-1LH953795D577610YLYRGXXI'
                });
            },
            onApprove: function (data, actions) {
                SigningIn("hkDONE", data.subscriptionID);
            }
        }).render('#paypal-plan-container1');
        paypal.Buttons({
            style: {
                shape: 'pill',
                color: 'gold',
                layout: 'horizontal',
                label: 'paypal'
            },
            createSubscription: function (data, actions) {
                return actions.subscription.create({
                    'plan_id': 'P-9HA21889HE6536741LZFCKII'
                });
            },
            onApprove: function (data, actions) {
                SigningIn("hkDONE", data.subscriptionID);
            }
        }).render('#paypal-plan-container3');
        paypal.Buttons({
            style: {
                shape: 'pill',
                color: 'gold',
                layout: 'horizontal',
                label: 'paypal'
            },
            createSubscription: function(data, actions) {
            return actions.subscription.create({
                'plan_id': 'P-9MJ37173X89009030LZWDI6A'
            });
            },
            onApprove: function(data, actions) {
                SigningIn("hkDONE", data.subscriptionID);
            }
        }).render('#paypal-plan-container4');
        paypal.Buttons({
            style: {
                shape: 'pill',
                color: 'gold',
                layout: 'horizontal',
                label: 'paypal'
            },
            createSubscription: function(data, actions) {
            return actions.subscription.create({
                'plan_id': 'P-2N881510RJ410564KLZ2CVSQ'
            });
            },
            onApprove: function(data, actions) {
                SigningIn("hkDONE", data.subscriptionID);
            }
        }).render('#paypal-plan-container5');
        </script>
    <script type="text/javascript">
        (function (e, t, o, n, p, r, i) { e.visitorGlobalObjectAlias = n; e[e.visitorGlobalObjectAlias] = e[e.visitorGlobalObjectAlias] || function () { (e[e.visitorGlobalObjectAlias].q = e[e.visitorGlobalObjectAlias].q || []).push(arguments) }; e[e.visitorGlobalObjectAlias].l = (new Date).getTime(); r = t.createElement("script"); r.src = o; r.async = true; i = t.getElementsByTagName("script")[0]; i.parentNode.insertBefore(r, i) })(window, document, "https://diffuser-cdn.app-us1.com/diffuser/diffuser.js", "vgo");
        vgo('setAccount', '25992310');
        vgo('setTrackByDefault', true);

        vgo('process');
        <?php echo "window.ylp.dev='$dev'";?>
    </script>
</body>
</html>