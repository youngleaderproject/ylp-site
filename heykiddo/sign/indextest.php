<?php
if ( !isset( $_SESSION ) ) {
    session_start();
}else{
    session_destroy();
    session_start();
}
error_reporting( E_ALL );
$linkpos = strpos( strtolower( $_SERVER[ "DOCUMENT_ROOT" ] ), "heykiddo" );
if ( $linkpos > 0 ) {
    $_SERVER[ "DOCUMENT_ROOT" ] = substr( $_SERVER[ "DOCUMENT_ROOT" ], 0, $linkpos - 1 );
}

$type = (isset($_REQUEST["type"]) ? $_REQUEST["type"] : 'S');
?>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="robots" content="noindex, nofollow" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="icon" href="../images/Hey-Kiddo-favicon.ico" type="favicon/ico" />
    <title>HeyKiddo&trade; Portal | Sign Up</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.css" />
    <link rel="stylesheet" href="../css/HKsignup.css" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <div class="container-fluid p-0" >
		<div class="modal" id="promomodal" tabindex="-1" role="dialog" aria-labelledby="..." aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered"  role="document">
						<div class="modal-content">
						  <div class="modal-body">
							  <center><h5>Hey-Kiddo&trade; Promo Code</h5></center>
							  <br>
							   <div class="form-group">
								  <label id="HKpromoEditL" class="control-label">Enter Promo Code</label>
								   <div><input class="form-control" type="text" id="hkpromocode" name="hkpromocode" /></div>
								   <div class="col-md-12 my-auto">
										<span class="help-block" style="color: red" hidden id="signuppromohelp">Please select a plan.</span>
									</div>
							  </div>
							<center><button type="button" class="btn HKpromoSave">SUBMIT</button></center>
							</div>
						  </div>
					  </div>
				  	</div>
        <div id="firstoutercarousel"  class="carousel slide" data-interval="false" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item" style="overflow-x: hidden;overflow-y:  auto; height: 100vh;background-color: #F6F6F6;background-image: url('../images/WallPaper@2x.png');background-size: 500px 50%;background-repeat:repeat-x; background-position:bottom">
                    <nav class="navbar navbar-expand-lg navbar-light sticky-top">
                        <div class="container-fluid">
                            <a class="navbar-brand" href="https://hey-kiddo.com">
                                <img src="../images/logo-horizontal-tagline.png" width="200" class="img-fluid" alt="" />
                            </a>
                        </div>
                    </nav>
                    <div class="HKsignupform flip-container mx-auto" id="flip-this">
                        <div class="flipper">
                            <div class="front mx-auto my-auto">
                                <form class="landing-form">
                                    <center>
                                        <h4>
                                            <b>Sign In</b>
                                        </h4>
                                        <p>
                                            New to HeyKiddo&trade;?
                                            <a href="#flip-this" class="flip-it" onclick="return false;" style="color: #55B588">
                                                <b>Create an account</b>
                                            </a>
                                        </p>
                                    </center>
                                    <div class="form-group">
                                        <input type="email" class="form-control" id="HKemail" name="HKemail" placeholder="Email" />
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" id="HKpassword" name="HKpassword" placeholder="Password" />
                                    </div>
                                    <input type="hidden" name="rqstyp" value="ylplogin" />
                                    <span class="help-block" style="color: red" hidden id="signinformhelp"></span>
                                    <button type="button" class="btn btn-lg HKSignupBtn" id="HKSignInBtn" style="margin-top: 10px; max-width: 250px;">
                                        SIGN IN
                                    </button>
                                </form>
                            </div>
                            <div class="back">
                                <form action="../signup/." method="post" id="HKlanding-form">
                                    <center>
                                        <h3>
                                            <b>To get started, create an account</b>
                                        </h3>
                                        <p>
                                            Already have an account?
                                            <a href="#flip-this" class="flip-it" onclick="return false;" style="color: #55B588">
                                                <b>Sign In</b>
                                            </a>
                                        </p>
                                    </center>
                                    <div class="row">
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="HKInputFName" id="HKInputFName" required placeholder="First Name" />
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="HKInputLName" id="HKInputLName" required placeholder="Last Name" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="email" class="form-control" name="HKInputEmail" id="HKInputEmail" required placeholder="Email" />
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control disabled" value="" name="HKInputPhone" id="HKInputPhone" data-format="(ddd) ddd-dddd" placeholder="Phone # to recieve your texts" />
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" name="HKInputPassword" id="HKInputPassword" placeholder="Password" />
                                    </div>
                                    <input type="hidden" name="type" value="hksignup" />
                                    <span class="help-block" style="color: red" hidden id="signupformhelp">
                                    </span>
                                    <button type="button" class="btn btn-lg HKSignupBtn" id="HKSignupBtn" style="margin-top: 10px;">
                                        SIGN UP
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item  active" style="padding: 0px; background-color: #F6F6F6;">
                    <div class="progress">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="4" style="width: 25%;background-color: #7C7C7C;"></div>
                    </div>
                    <div id="outercarousel" class="carousel slide" data-interval="false" data-ride="carousel">
                        <div class="carousel-inner" >
                            <div class="carousel-item active" style="overflow-x: hidden;overflow-y:  auto; height: 100vh;padding-top: 20px; background-color: #F6F6F6;">
                                <div class="container-fluid ">
									
                                    <div class="row h-100 my-auto">
                                        <div class="col-12 my-auto">
                                            <center>
                                                <h1 id="HkWelcome">
                                                    Welcome, !
                                                </h1>
                                                <p>Choose your first talking topic.</p>
                                                <p>I want to help my child with...</p>
                                            </center>
                                        </div>
										<div class="container-fluid">
											<div class="row">
											<main class="grid-item main">
    											<div class="items">
												<div class="nav-item text-center">
                                                        <div class="card h-100" style="border-top-width: 10px; border-top-color: #70C08F;">
															<div class="cardImages"><img src="../images/icon-Self-Awareness@2x.png" width="100" class="img-fluid" alt="" /></div>
															<h4 class="card-title pt-5"><b>Confidence</b></h4>
                                                            <br />
															<div class="card-body">
                                                            <h6 style="max-width: 250px;" class="mx-auto">
                                                                Help your child understand their unique personality
                                                            </h6>
                                                            <div class="text-left">
                                                                Best for:
                                                                <ul class="custom-list">
                                                                    <li>Shyness</li>
                                                                    <li>Emotion regulation</li>
                                                                    <li>Quitting too soon</li>
                                                                    <li>Good wellness habits</li>
                                                                </ul>
                                                            </div>
                                                            </div>
															<a href="#" class="HKcardCHK" data-optionchoice="1" onclick="return false;">
                                                                <img src="../images/btn-Check@2x.png" width="50" class="img-fluid" alt="" />
                                                            </a>
                                                        </div>
                                                        </div>
												<div class="nav-item text-center">
                                                        <div class="card h-100" style="border-top-width: 10px; border-top-color: #4DAAD5;">
															<div class="cardImages"><img src="../images/icon-Self-Management@2x.png" width="100" class="img-fluid" alt="" /></div>
															<h4 class="card-title pt-5"><b>Stress</b></h4>
                                                            <br />
															<div class="card-body">
                                                            <h6 style="max-width: 250px;" class="mx-auto">
                                                                Help your child stay positive and manage stress
															</h6>
                                                            <div class="text-left">
                                                                Best for:
                                                                <ul class="custom-list">
                                                                    <li>Anxiety</li>
                                                                    <li>Overachieving</li>
                                                                    <li>Worry</li>
                                                                    <li>Difficulty with new things</li>
                                                                </ul>
                                                            </div>
																
                                                            </div>
															<a href="#" class="HKcardCHK" data-optionchoice="2" onclick="return false;">
                                                                <img src="../images/btn-Check@2x.png" width="50" class="img-fluid" alt="" />
                                                            </a>
                                                        </div>
                                                        </div>
												<div class="nav-item text-center">
                                                        <div class="card h-100" style="border-top-width: 10px; border-top-color: #4BB7BA;">
															<div class="cardImages"><img src="../images/icon-Social Awareness@2x.png" width="100" class="img-fluid" alt="" /></div>
															<h4 class="card-title pt-5"><b>Fitting In</b></h4>
                                                            <br />
															<div class="card-body">
                                                            <h6 style="max-width: 250px;" class="mx-auto">
                                                                Help your child understand their place in a larger community
                                                            </h6>
                                                            <div class="text-left">
                                                                Best for:
                                                                <ul class="custom-list">
                                                                    <li>Bullying</li>
                                                                    <li>Moving to a new place</li>
                                                                    <li>Changes in family structure</li>
                                                                    <li>Appreciating diversity</li>
                                                                </ul>
                                                            </div>
                                                            </div>
															<a href="#" class="HKcardCHK" data-optionchoice="3" onclick="return false;">
                                                                <img src="../images/btn-Check@2x.png" width="50" class="img-fluid" alt="" />
                                                            </a>
                                                        </div>
                                                        </div>
												<div class="nav-item text-center">
                                                        <div class="card h-100" style="border-top-width: 10px; border-top-color: #F2A4CA;">
															<div class="cardImages"><img src="../images/icon-Relationship Skills@2x.png" width="100" class="img-fluid" alt="" /></div>
															<h4 class="card-title pt-5"><b>Friendships</b></h4>
                                                            <br />
															<div class="card-body">
                                                            <h6 style="max-width: 250px;" class="mx-auto">
                                                                Help your child develop strong relationships with family and peers
                                                            </h6>
                                                            <div class="text-left">
                                                                Best for:
                                                                <ul class="custom-list">
                                                                    <li>Sibling rivalry</li>
                                                                    <li>Going to a new school</li>
                                                                    <li>Family dynamics</li>
                                                                    <li>Working well with others</li>
                                                                </ul>
                                                            </div>
														</div>
															<a href="#" class="HKcardCHK" data-optionchoice="4" onclick="return false;">
                                                                <img src="../images/btn-Check@2x.png" width="50" class="img-fluid" alt="" />
                                                            </a>
                                                        </div>
                                                        </div>
												<div class="nav-item text-center">
                                                        <div class="card  h-100" style="border-top-width: 10px; border-top-color: #FFD771;">
															<div class="cardImages"><img src="../images/icon-Impulse Control@2x.png" width="100" class="img-fluid" alt="" /></div>
															<h4 class="card-title pt-5"><b>Impulse Control</b></h4>
                                                            <br />
															<div class="card-body">
                                                            <h6 style="max-width: 250px;" class="mx-auto">
                                                                Help your child respond instead of react                    
                                                            </h6>
                                                            <div class="text-left">
                                                                Best for:
                                                                <ul class="custom-list">
                                                                    <li>Anger issues</li>
                                                                    <li>Disruptive behavior</li>
                                                                    <li>Being a good role model</li>
                                                                    <li>Leadership development</li>
                                                                </ul>
                                                            </div>
                                                            </div>
															<a href="#" class="HKcardCHK" data-optionchoice="5" onclick="return false;">
                                                                <img src="../images/btn-Check@2x.png" width="50" class="img-fluid" alt="" />
                                                            </a>
                                                        </div>
                                                        </div>
												<div class="nav-item text-center">
                                                        <div class="card h-100" style="border-top-width: 10px; border-top-color: #B499DD;">
															<div class="cardImages"><img src="../images/icon-Decision Making@2x.png" width="100" class="img-fluid" alt="" /></div>
															<h4 class="card-title pt-5"><b>Decision-Making</b></h4>
                                                            <br />
															<div class="card-body">
                                                            <h6 style="max-width: 250px;" class="mx-auto">
                                                                Help your child set goals and stay organized
                                                            </h6>
                                                            <div class="text-left">
                                                                Best for:
                                                                <ul class="custom-list">
                                                                    <li>Improving organization</li>
                                                                    <li>Long-term planning</li>
                                                                    <li>Better study skills</li>
                                                                    <li>Take initiative at home and school</li>
                                                                </ul>
                                                            </div>
                                                            </div>
															<a href="#" class="HKcardCHK" data-optionchoice="6" onclick="return false;">
                                                                <img src="../images/btn-Check@2x.png" width="50" class="img-fluid" alt="" />
                                                            </a>
                                                            </div>
                                                        </div>
													<div class="nav-item text-center">
                                                        <div class="card h-100" style="border-top-width: 10px; border-top-color: #FF9353;">
															<div class="cardImages"><img src="../images/icon-Core Skills@2x.png" width="100" class="img-fluid" alt="" /></div>
															<h4 class="card-title pt-5"><b>Core Skills</b></h4>
                                                            <br />
															<div class="card-body">
                                                            <h6 style="max-width: 250px;" class="mx-auto">
                                                                Help your child develop a great foundation
                                                            </h6>
                                                            <div class="text-left">
                                                                Best for:
                                                                <ul class="custom-list">
                                                                    <li>Making good behavior even better</li>
                                                                    <li>Learning new concepts</li>
                                                                    <li>Building on positive outcomes</li>
                                                                    <li>Personal development</li>
                                                                </ul>
                                                            </div>
                                                            </div>
															<a href="#" class="HKcardCHK" data-optionchoice="7" onclick="return false;">
                                                                <img src="../images/btn-Check@2x.png" width="50" class="img-fluid" alt="" />
                                                            </a>
                                                        </div>
													</div>
												</div>
											</main>
											</div>
											</div>
                                        <div class="HKSignupFoot mt-5">
                                            <div class="row w-100 text-center my-auto">
                                                <div class="col-12 my-auto">
                                                    <p style="color: white">If you have more than one child, don't worry! HeyKiddo&trade; activities and talking tips can help the whole family!</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item" style="overflow-x: hidden;overflow-y:  auto; height: 100vh;padding-top: 20px; background-color: #F6F6F6;">
								<div class="row ">
									<div class="col-xl-6">
										<a href="#" onClick="return false;" class="outercarouselback">
											<img src="../images/Back-Icon@2x.png" style="margin-left: 20px;" width="50" class="img-fluid" />
										</a>
									</div>
									<div class="col-xl-6"></div>
								</div>
                                <div class="container-fluid">
                                    <div class="row h-100 my-auto">
                                        <div class="col-12 my-auto">
                                            <center>
                                                <br />
                                                <h4>When is the best time of day for you to recieve our texts?</h4>
                                            </center>
                                        </div>
                                        <div class="row w-75 text-center mx-auto" style="padding-top: 10px;">
                                            <div class="col-md-6 my-auto">
                                                <div class="card p-3 card-click" data-timeselection="1">
                                                    <div class="row my-auto">
                                                        <div class="col-7 my-auto">
                                                            <div class="text-left">
                                                                <h5>On the way to school</h5>
                                                            </div>
                                                        </div>
                                                        <div class="col-4 my-auto offset-1">
                                                            <img src="../images/icon-morning@2x.png" width="100" class="img-fluid" alt="" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 my-auto">
                                                <div class="card p-3 card-click" data-timeselection="2">
                                                    <div class="row my-auto">
                                                        <div class="col-7 my-auto">
                                                            <div class="text-left">
                                                                <h5>Right after school</h5>
                                                            </div>
                                                        </div>
                                                        <div class="col-4 my-auto offset-1">
                                                            <img src="../images/icon-afternoon@2x.png" width="100" class="img-fluid" alt="" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row w-75 text-center mx-auto" style="padding-top: 10px;">
                                            <div class="col-md-6 my-auto">
                                                <div class="card p-3 card-click" data-timeselection="3">
                                                    <div class="row my-auto">
                                                        <div class="col-7 my-auto">
                                                            <div class="text-left">
                                                                <h5>During dinner</h5>
                                                            </div>
                                                        </div>
                                                        <div class="col-4 my-auto offset-1">
                                                            <img src="../images/icon-dinner@2x.png" width="100" class="img-fluid" alt="" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 my-auto">
                                                <div class="card p-3 card-click" data-timeselection="4">
                                                    <div class="row my-auto">
                                                        <div class="col-7 my-auto">
                                                            <div class="text-left">
                                                                <h5>Around bedtime</h5>
                                                            </div>
                                                        </div>
                                                        <div class="col-4 my-auto offset-1">
                                                            <img src="../images/icon-bedtime@2x.png" width="100" class="img-fluid" alt="" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row w-100 text-center mx-auto" style="padding-top: 10px;">
                                            <div class="col-md-12 my-auto">
                                                <span class="help-block" style="color: red" hidden id="signuptimehelp">Please select a time.</span>
                                            </div>
                                        </div>
                                        <div class="HKSignupFoot">
                                            <div class="row w-100 text-center p-lg-5 my-auto">
                                                <div class="col-12 my-auto">
                                                    <button class="btn text-white" id="HKnext2">NEXT</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
							<div class="carousel-item" style="overflow-x: hidden;overflow-y:  auto; height: 100vh;padding-top: 20px; background-color: #F6F6F6;">
								<div class="row ">
									<div class="col-xl-6">
										<a href="#" onClick="return false;" class="outercarouselback">
											<img src="../images/Back-Icon@2x.png" style="margin-left: 20px;" width="50" class="img-fluid" />
										</a>
									</div>
									<div class="col-xl-6"></div>
								</div>
                                <div class="container-fluid">
                                    <div class="row h-100 my-auto">
                                        <div class="col-12 my-auto">
                                            <center>
                                                <br />
                                                <h4>Select Your Plan</h4>
                                            </center>
                                        </div>
                                        <div class="row w-75 text-center mx-auto my-auto" style="padding-top: 10px;">
                                            <div class="col-md-6 my-auto">
                                                <div class="card p-3 card-plan d-flex" style="min-height: 140px;" data-planselection="1">
                                                    <div class="row my-auto justify-content-center">
                                                        <div class="col-7 my-auto">
                                                            <div class="text-left">
                                                                <h5>Yearly</h5>
                                                                <h4>$99.99/year</h4>
                                                            </div>
                                                        </div>
                                                        <div class="col-4 my-auto offset-1">
                                                            <img src="../images/tag-bestValue.png" width="80" class="img-fluid " alt="" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 my-auto">
                                                <div class="card p-3 card-plan d-flex" style="min-height: 140px;" data-planselection="2">
                                                    <div class="row my-auto justify-content-center">
                                                        <div class="col-7 my-auto">
                                                            <div class="text-left">
                                                                <h5>Monthly</h5>
                                                                <h4>$9.99/month</h4>
                                                            </div>
                                                        </div>
                                                        <div class="col-4 my-auto offset-1">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
										<div class="row w-75 text-center mx-auto" style="padding-top: 10px;">
											<div class="my-auto col-xl-6 offset-xl-3">
												<div class="card p-3 card-plan d-flex" style="min-height: 140px;" data-planselection="3">
                                                    <div class="row my-auto justify-content-center">
                                                        <div class="col-12 my-auto">
                                                            <div class="text-center">
                                                                <h5>Have a Promo Code?</h5>
                                                                <h4>Click Here!</h4>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
											</div>
										</div>
                                        <div class="row w-100 text-center mx-auto" style="padding-top: 10px;">
                                            <div class="col-md-12 my-auto">
                                                <span class="help-block" style="color: red" hidden id="signupplanhelp">Please select a plan.</span>
                                            </div>
                                        </div>
                                        <div class="HKSignupFoot">
                                            <div class="row w-100 text-center p-lg-5 my-auto">
                                                <div class="col-12 my-auto">
													<p style="color: white">Your first 7 days are free and then you will be charged with the amount selected.</p>
													<br>
                                                    <button class="btn text-white" id="HKnext3">START FOR FREE</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item"  style="overflow-x: hidden;overflow-y:  auto; height: 100vh;padding-top: 20px; background-color: #F6F6F6;">
								<div class="row ">
									<div class="col-xl-6" style="z-index: 100">
										<a href="#" onClick="return false;" class="outercarouselback">
											<img src="../images/Back-Icon@2x.png" style="margin-left: 20px;" width="50" class="img-fluid" />
										</a>
									</div>
									<div class="col-xl-6"></div>
								</div>
                                <div class="container-fluid">
                                    <div class="row h-100 my-auto">
                                        <div class="col-12 my-auto text-center">
                                            <br />
                                            <h2>
                                                <b>Okay! To recap...</b>
                                            </h2>
                                            <br />
											<h4>You want to...</h4>
                                        </div>
										<div class="col-xl-7 offset-xl-3 pb-5">
                                            <div id="finish-list" class="text-left"></div>
										</div>
                                        <div class="HKSignupFoot" style="margin-top: 200px; padding-bottom: 50px;">
                                            <div class="row text-center my-auto">
												<img src="../images/recap-graphic.png" width="400" class="img-fluid mx-auto pb-5" style="margin-top: -220px;" alt="">
                                                <div class="col-12 my-auto">
                                                    <p style="color: white">Looks good? You'll be brought to Paypal to finish the payment and you are all set to go!</p>
                                                    <br />
                                                    <div hidden class="paypal-button my-auto" id="paypal-plan-container1"></div>
                                                    <div hidden class="paypal-button my-auto" id="paypal-plan-container2"></div>
													<button hidden class="btn text-white" id="HKnext4">START FOR FREE</button>
													<div class="col-md-12 my-auto">
														<span class="help-block" style="color: red" hidden id="signuppaymenthelp"></span>
													</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-2.2.4.min.js"
        integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
        crossorigin="anonymous"></script>
	<script src="http://code.jquery.com/ui/1.8.21/jquery-ui.min.js"></script>
    <script src="https://cdn.rawgit.com/nnattawat/flip/master/dist/jquery.flip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="../js/HKsignup.js"></script>
    <script type="text/javascript">
        flipitfunc('<?php echo $type; ?>')
    </script>
    <script src="../js/bootstrap-formhelpers.min.js"></script>
    <script src="../js/hkglobals.js"></script>
    <script src="https://www.paypal.com/sdk/js?client-id=AemoZTCghPRClgHoE_fSAJSAVTmPEReSPIXkODaocVqrvxx08U0iZDPQWdMILGIN85oj6HKV9GUg-dTv&vault=true"></script>
    <script>
        paypal.Buttons({
			style: {
				shape: 'pill',
				color: 'gold',
				layout: 'horizontal',
				label: 'paypal',

			},
            createSubscription: function (data, actions) {
                return actions.subscription.create({
                    'plan_id': 'P-9Y572735FD7385155LYN5MGA'
                });
            },
            onApprove: function (data, actions) {
                window.location.replace("https://hey-kiddo.com/portal?firsttime=Y&selection=" + window.ylp.firstSelection + "&time=" + window.ylp.secondSelection);
            }
        }).render('#paypal-plan-container2');
		paypal.Buttons({
			style: {
				shape: 'pill',
				color: 'gold',
				layout: 'horizontal',
				label: 'paypal',

			},
            createSubscription: function(data, actions) {
              return actions.subscription.create({
                'plan_id': 'P-1LH953795D577610YLYRGXXI'
              });
            },
            onApprove: function(data, actions) {
              	window.location.replace("https://hey-kiddo.com/portal");
            }
         }).render('#paypal-plan-container1');
    </script>
</body>
</html>