<?php
try {
    session_start();
}
catch(\Exception $ex) {
    //  file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/heykiddosign.txt' , print_r($_SESSION,true)."\r\n", FILE_APPEND);
}
error_reporting( E_ALL );
require_once str_replace( '\\', '/', $_SERVER[ "DOCUMENT_ROOT" ] ) . '/PHP/shared/auto_load.php';
use PHP\CLASSES\CYLPDB;
use PHP\CLASSES\CEMAIL;
use PHP\CLASSES\CUSER;

if ( isset( $_SESSION[ 'YLP' ][ 'ACTIVE' ] ) ) {
  if ( DEBUG )
    file_put_contents( $_SERVER[ "DOCUMENT_ROOT" ] . '/logs/landing.txt', "New Paid User" . "\r\n", FILE_APPEND );
}
$input_err = "";
$email = "";
$username = "";
if ( $_SERVER[ "REQUEST_METHOD" ] == "POST" ) {
  if ( isset( $_REQUEST[ 'rqstyp' ] ) ) {
    if ( $_REQUEST[ 'rqstyp' ] === "ylpbetasignup" ) {
      if ( !empty( trim( $_POST[ "ylpname" ] ) ) ) {
        $username = trim( $_POST[ "ylpname" ] );
      }
      if ( !empty( trim( $_POST[ "ylpemail" ] ) ) ) {
        $email = trim( $_POST[ "ylpemail" ] );
      }
      if ( strlen( $email ) === 0 || strlen( $username ) === 0 ) {
        $input_err = "<font color=red><b>You need to first correct your invalid inputs before you can submit.<b></font>";
      } else {
        $user = new CUSER();
        $country = $user->getLocationInfoByIp()[ 'country' ];
        $country = ( $country !== '' ? $country : 'US' );
        $dba = new CYLPDB( YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB );
        $loada = Array();
        $loada[ 'user_name' ] = "'$username'";
        $loada[ 'user_email' ] = "'$email'";
        $loada[ 'user_country' ] = "'$country'";
        $dba->ADD_RECORD( 'ylpbetasub', $loada );
        $emailclass = new CEMAIL( $username, $email );
        $emailclass->BetaSignupYLP( $username, $email );
        Redirect( "../PHP/html/signup.php" );
        die;
      }
    }
  }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
 <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NLW5ZB7');</script>
<!-- End Google Tag Manager -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Young Leader Project&trade; | Home</title>
<meta name="description" content="
The Young Leader Project develops the leaders of tomorrow by acquainting them with essential leadership skills today.">
<!-- Bootstrap -->
<meta property='og:title' content="Young Leader Project"/>
<meta property='og:image' content="http://youngleaderproject.com/images/LandingPage.png"/>
<meta property='og:description' content="The Young Leader Project develops the leaders of tomorrow by acquainting them with essential leadership skills today."/>
<meta property='og:url' content="http://youngleaderproject.com" />
<link rel="icon" href="/images/favicon.ico" type="favicon/ico" />
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet" />
<link rel="stylesheet" href="/css/landingscripts.css">
<script type="text/javascript" src="https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>
</head>

<body onload="checkCookiePolicy()">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NLW5ZB7"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
        <!--Modal: modalCookie-->
    <div class="modal fade" id="cookieModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
      aria-hidden="true" data-backdrop="true">
      <div class="modal-dialog" role="document">
        <!--Content-->
        <div class="modal-content">
          <!--Body-->
          <div class="modal-body">
              <center>
                  <div class="row">

                      <p class="p-3">Hey there! We use cookies on our site so we can make sure we are delivering the best possible web experience. We also use them to understand our website traffic. You can learn more about cookies, and how to disable them, by checking out our cookie policy. By clicking "Ok" on this banner, it means that you are okay with the use of cookies unless you have disabled them.</p>
                  </div>

                  <a type="button" href="javascript:window.open('https://www.youngleaderproject.com/cookiepolicy.html', 'YLP Cookie Policy', 'width=600,height=450');" class="btn btn-primary">
                      Learn more
                  </a>
                  <a type="button" class="btn btn-outline-primary waves-effect" onclick="setCookiePolicy();" data-dismiss="modal">Ok, thanks</a>
              </center>
          </div>
        </div>
        <!--/.Content-->
      </div>
    </div>
    <!--Modal: modalCookie-->
<nav id="landing-nav" hidden class="navbar navbar-expand-lg navbar-light bg-light navextend"> <a class="navbar-brand" href="/"><img src="images/NewYLPLogo.png" alt="" width="100" class="img-fluid ylpicon"/></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto nav-padder">
      <?php if (!(isset( $_SESSION[ "user" ][ "loggedin" ] ) && $_SESSION[ "user" ][ "loggedin" ] === true)) { ?>
      <!--<li class="nav-item active nav-padder"> <a class="nav-link" href="/login/">Log In</a> </li>-->
      <?php
      } else {
        ?>
      <!--<li class="nav-item active nav-padder"><a class="nav-link" href="../PHP/commands/ylplogout.php">Log Out</a></li>-->
      <?php
      }
      if ( isset( $_SESSION[ 'YLP' ][ 'ACTIVE' ] ) && $_SESSION[ 'YLP' ][ 'ACTIVE' ] === 'Y' ) {
        ?>
      <!--<a class="landing-form-btn" href="/dashboard/">Misson Control</a>-->
      <?php
      } else {
        ?>
      <!--<a class="landing-form-btn" href="/subscription/">Subscribe Now</a>-->
      <?php } ?>
      <button class="landing-form-btn" id="landing-join" value="blue">Join The Mission</button>
      <!--<li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Dropdown </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown"> <a class="dropdown-item" href="#">Action</a> <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a> </div>
      </li>-->
    </ul>
  </div>
</nav>
<div class="landing-wrapper">
  <div class="landing-top">
    <div class="landing-top-imgs"> <img src="/images/YLP-Logo-White.png" class="img-fluid" alt="The planet Ohm"> </div>
    <div class="landing-planet-txt">
      <center>
        <p style="max-width: 450px;"><font color="white" >Welcome to the Young Leader Project.
          Our mission is to build leadership, social, and emotional skills in children. </font></p>
      </center>
      <button class="landing-form-btn" id="landing-join2" value="blue">Join The Mission Now</button>
    </div>
    <div id="particles-js"></div>
      <a href="https://hey-kiddo.com" event-category="ylp link click" event-action="ylpclick" event-label="ylp hk click" target="_blank">
          <img src="/images/NewImages/HK-Feature@2x.png" class="landing-astronaut img-fluid" alt="The planet Ohm" />
      </a>
  </div>
  <div class="landing-first">
    <div class="landing-firstsec">
      <div class="container">
        <center>
          <p style="font-size: 18px;max-width: 800px; padding-bottom: 50px;" align="left">High impact learning happens when lessons children learn on their own are reinforced at home and with their peers. That’s why the Young Leader Project takes a three-pronged approach to social, emotional and leadership development: </p>
        </center>
        <div class="row landing-firstsec">
          <div class="col-md-6 col-sm-8 landing-second-pic1 landing-second-mobilepic offset-xl-0 col-xl-5"><img src="/images/NewImages/Prong1.png" alt="The Young Leader Track" class="img-fluid"></div>
          <div class="col-md-5 offset-md-1 my-auto landing-second-pic1txt offset-xl-2">
            <h4 style="color: #6A459B"><b>Individual Learning and Development</b></h4>
            <p class=""><i>Mae Legend and the Weaver of Wills</i> is an action-packed mobile game designed to enhance and track your child’s learning and development through play, stealth learning, and assessment. <i>Mae Legend and the Weaver of Wills</i> is coming soon!</p>
          </div>
        </div>
        <div id="landing-second-parentportal" class="row landing-firstsec">
          <div class="my-auto landing-second-pic1txt offset-lg-0 offset-xl-0 offset-md-0 col-md-6 col-xl-5">
            <h4 style="color: #6A459B"><b>Parent-Led Learning and Development</b></h4>
            <p>
                <a href="https://hey-kiddo.com" target="_blank">HeyKiddo&trade;</a> takes the guesswork out of developing your child’s social-emotional and leadership skills. We send curated conversation starters (developed by psychologists!) and activities straight to your phone so you can spend less time Googling and more time being an awesome parent. 
                <a href="https://hey-kiddo.com" target="_blank">HeyKiddo&trade;</a> is launching in February 2020!
            </p>
          </div>
          <div class="col-sm-8 landing-second-pic1 landing-second-mobilepic offset-lg-1 offset-xl-2 col-xl-5 col-md-5 offset-md-1">
            <div> 
            <a href="https://hey-kiddo.com" target="_blank"><img src="/images/NewImages/Prong2.png" alt="Young Leader Parent Portal" class="img-fluid" /></a></div>
          </div>
          <div class="offset-xl-1"></div>
        </div>
        <div id="landing-second-parentportal2" class="row landing-firstsec ">
          <div class="col-md-6 offset-xl-1 col-xl-4 landing-second-pic1 col-sm-8 landing-second-mobilepic">
            <a href="https://hey-kiddo.com" target="_blank"><img src="/images/NewImages/Prong2.png" alt="Young Leader Parent Portal" class="img-fluid" /></a></div>
          <div class="col-md-5 offset-md-1 my-auto landing-second-pic2txt">
            <h4 style="color: #6A459B"><b>Parent-Led Learning and Development</b></h4>
            <p class="landing-second-pic2txt">
                <a href="https://hey-kiddo.com" target="_blank">HeyKiddo&trade;</a> takes the guesswork out of developing your child’s social-emotional and leadership skills. We send curated conversation starters (developed by psychologists!) and activities straight to your phone so you can spend less time Googling and more time being an awesome parent. 
                <a href="https://hey-kiddo.com" target="_blank">HeyKiddo&trade;</a> is launching in February 2020!
            </p>
          </div>
        </div>
        <div class="row landing-firstsec">
          <div class="col-md-6 col-sm-8 landing-second-pic1 landing-second-mobilepic offset-xl-0 col-xl-5"><img src="/images/NewImages/Prong3.png" alt="The Young Leader Track" class="img-fluid"></div>
          <div class="col-md-5 offset-md-1 my-auto landing-second-pic1txt offset-xl-2">
            <h4 style="color: #6A459B"><b>Peer-Based Learning and Development</b></h4>
            <p class="">PlanetX Immersive Camp is a three-day offline camp that challenges your child to think and work creatively and cooperatively to solve a mystery. Teamwork challenges encourage your child to learn more about themselves and their peers, as well as how their unique skills and talents can help them succeed in the future. PlanetX is arriving summer of 2020! </p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="landing-second">
    <div class="container">
      <div class="row">
        <div class=" my-auto landing-whyus offset-xl-3 col-xl-6 offset-lg-3 col-lg-6">
          <center>
            <h3 style="color: #505050"><b>Why Us?</b></h3>
            <hr>
          </center>
          <p>The creators are psychologists who specialize in leadership, social and emotional development. <a target="_blank" href="https://www.linkedin.com/in/nicolelipkin/">Dr Nicole Lipkin</a> is the founder. Her expertise meets at the intersection of psychology and leadership. Our content and research team have the perfect combination of experience and training necessary to help children build a strong foundation of social, emotional and leadership skills.</p>
        </div>
      </div>
    </div>
  </div>
  <div class="landing-fourth">
    <section>
      <div class="col-12 text-center">
        <h5 class="landing-fourthhead"><b>PEOPLE BELIEVE IN THE MISSION</b></h5>
        <div class="landing-fourth-txt">
          <div class="row">
            <p>The Young Leader Project has been awarded a National Science Foundation (NSF) Small Business Innovation Research (SBIR) to conduct research and development (R&amp;D) work on helping children develop leadership skills. This material is based upon work supported by the National Science Foundation under Grant No. 1842707</p>
          </div>
          <img src="/images/NSF_4-Color_bitmap_Logo.png" alt="NSF Logo" width="100px" height="100px" class="rounded"> </div>
      </div>
    </section>
  </div>
  <div class="landing-fifth">
    <section>
      <div class="landing-fifth-txt">
        <div class="row">
          <div class="col-12 text-center">
            <h4 style="color: #505050"><b>YOU CAN JOIN THE MISSION</b></h4>
            <hr>
            <p>Sign up to find out when we go live!</p>
          </div>
        </div>
      </div>
      <div class="landing-form" id="landing-form">
        <div class="row">
          <form class="landing-form" action="." method="post">
            <div class="form-group">
              <input type="text" class="form-control" name="ylpname" placeholder="Name" />
            </div>
            <div class="form-group">
              <input type="email" class="form-control" name="ylpemail" placeholder="Email" />
            </div>
            <input type="hidden" name="rqstyp" value="ylpbetasignup" />
            <span class="help-block"> <?php echo $input_err; ?> </span> 
            <!--							<div class="form-group">
								<input type="password" class="form-control" name="ylppass" placeholder="Password">
							</div>-->
            <button type="submit" class="landing-form-btn" value="blue">Join the Mission</button>
          </form>
        </div>
      </div>
    </section>
  </div>
</div>
<footer style="background-color: #6A459B; color: white; position: relative;">
  <div class="container-fluid" style="padding: 20px;">
    <div class="row" >
      <div class="col-12 my-auto">
        <p style="text-align: center; margin: auto; padding-bottom: 5px;">© Young Leader Project®, LLC. All Rights Reserved.</p>
        <p style="text-align: center; margin: auto; font-weight: 100; font-size: 12px; max-width: 550px; padding-bottom: 10px;">
            <a style="color: inherit;" href="https://hey-kiddo.com" target="_blank">HeyKiddo&trade;</a> and Young Leader Project&trade; do not provide medical or psychological advice, diagnosis, or treatment.
            <a style="all: unset;text-decoration: underline;" href="javascript:window.open('https://www.youngleaderproject.com/disclaimer.html', 'YLP Disclaimer', 'width=600,height=450');">See additional information</a>.
        </p>
		  <div class="container-fluid pb-2 pt-2">
		  <div class="row mx-auto text-center" style="width: 85%;">
			  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3"><a href="https://www.facebook.com/youngleaderproject/" target="_blank"><img width="50px" src="images/facebook.png" alt="YLP Facebook"></a></div>
			  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3"><a href="https://www.instagram.com/youngleaderproject/" target="_blank"><img width="50px" src="images/instagram.png" alt="YLP Facebook"></a></div>
			  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3"><a href="https://www.linkedin.com/company/young-leader-project/" target="_blank"><img width="50px" src="images/linkedin.png" alt="YLP Facebook"></a></div>
			  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3"><a href="https://twitter.com/YoungLeaderPro" target="_blank"><img width="50px" src="images/twitter.png" alt="YLP Facebook"></a></div>
		  </div>
        </div>
	  <div class="container-fluid">
          <div class="mx-auto text-center">
			  <a style="font-size: 12px; color: inherit;" href="/team/" target="_blank">Meet the Team</a> 
		  </div>
		  <div class="mx-auto text-center">
          <a style="font-size: 12px; color: inherit;" href="javascript:window.open('https://www.youngleaderproject.com/cookiepolicy.html', 'YLP Cookie Policy', 'width=600,height=450');">Cookie Policy</a> 
		  </div>
          <div class="mx-auto text-center">
			  <a style="font-size: 12px; color: inherit;" href="javascript:window.open('https://www.youngleaderproject.com/privacypolicy.html', 'YLP Privacy Policy', 'width=600,height=450');">Privacy Policy</a> 
		  </div>
          <div class="mx-auto text-center">
			  <a style="font-size: 12px; color: inherit;white-space: nowrap;" href="javascript:window.open('https://www.youngleaderproject.com/termsconditions.html', 'YLP Terms & Conditions;', 'width=600,height=450');">Terms &amp; Conditions</a> 
		  </div>
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script> 
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script> 
<script src="js/particles.js"></script> 
<script src="js/landing_page.js"></script>
    <script type="text/javascript">
    (function(e,t,o,n,p,r,i){e.visitorGlobalObjectAlias=n;e[e.visitorGlobalObjectAlias]=e[e.visitorGlobalObjectAlias]||function(){(e[e.visitorGlobalObjectAlias].q=e[e.visitorGlobalObjectAlias].q||[]).push(arguments)};e[e.visitorGlobalObjectAlias].l=(new Date).getTime();r=t.createElement("script");r.src=o;r.async=true;i=t.getElementsByTagName("script")[0];i.parentNode.insertBefore(r,i)})(window,document,"https://diffuser-cdn.app-us1.com/diffuser/diffuser.js","vgo");
    vgo('setAccount', '25992310');
    vgo('setTrackByDefault', true);

    vgo('process');
    </script>
</body>
</html>