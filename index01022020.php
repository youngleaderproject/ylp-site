<?php
if(!isset($_SESSION))
{
    session_start();
}
error_reporting(E_ALL);
require_once str_replace( '\\', '/', $_SERVER[ "DOCUMENT_ROOT" ] ) . '/PHP/shared/auto_load.php';
use PHP\CLASSES\CYLPDB;
use PHP\CLASSES\CEMAIL;
use PHP\CLASSES\CUSER;

if ( isset( $_SESSION[ 'YLP' ][ 'ACTIVE' ] ) ) {
	if ( DEBUG )
		file_put_contents( $_SERVER[ "DOCUMENT_ROOT" ] . '/logs/landing.txt', "New Paid User" . "\r\n", FILE_APPEND );
}
$input_err = "";
$email = "";
$phone = "";
$username = "";
if ( $_SERVER[ "REQUEST_METHOD" ] == "POST" ) {
    if(isset($_REQUEST['rqstyp'])){
        if($_REQUEST['rqstyp'] === "ylpbetasignup"){
            if ( !empty( trim( $_POST[ "ylpname" ] ) ) ) {
                $username = trim( $_POST[ "ylpname" ] );
            }
            if ( !empty( trim( $_POST[ "ylpemail" ] ) ) ) {
                $email = trim( $_POST[ "ylpemail" ] );
            }
			$badphone = false;
			if ( isset($_POST[ "ylpphone" ])) {
				if(strlen(trim( $_POST[ "ylpphone" ] )) === 10){
					$phone = trim( $_POST[ "ylpphone" ] ); 
                }else{
					$badphone = true;
                }
            }
            if(strlen($email) === 0 || strlen($username) === 0 || $badphone === true){
                $input_err = "<font color=red><b>You need to first correct your invalid inputs before you can submit.<b></font>";
            }else{
				$user = new CUSER();
				$country = $user->getLocationInfoByIp()['country'];
                $country = ($country !== '' ? $country : 'US');
                $dba = new CYLPDB(YLP_DB_USER, YLP_DB_PASS, YLP_DB_HOST, YLP_DB_DB);
                $loada = Array();
                $loada['user_name'] = "'$username'";
                $loada['user_email'] = "'$email'";
				$loada['user_phone'] = "'$phone'";
				$loada['user_country'] = "'$country'";
                $dba->ADD_RECORD('ylpbetasub', $loada);
                $emailclass = new CEMAIL($username,$email);
                $emailclass->BetaSignup($username,$email);
                Redirect( "../PHP/html/signup.php" );
                die;
            }
        }
    }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Young Leader Project | Home</title>
	<meta name="description" content="
The Young Leader Project develops the leaders of tomorrow by acquainting them with essential leadership skills today.">
	<!-- Bootstrap -->
    <meta property='og:title' content="Young Leader Project"/>
   <meta property='og:image' content="http://youngleaderproject.com/images/LandingPage.png"/>
   <meta property='og:description' content="The Young Leader Project develops the leaders of tomorrow by acquainting them with essential leadership skills today."/>
   <meta property='og:url' content="http://youngleaderproject.com" />
    <link rel="icon" href="/images/favicon.ico" type="favicon/ico" />
	<link href="/css/bootstrap.css" rel="stylesheet">
	<link rel="stylesheet" href="/css/landingscripts.css">
	    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-149676926-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-149676926-1');
		gtag('set', {'user_id': 'USER_ID'}); // Set the user ID using signed-in user_id.
    </script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>
</head>

<body>
    <nav id="landing-nav" hidden class="navbar navbar-expand-lg navbar-light bg-light navextend"> <a class="navbar-brand" href="/"><img src="images/NewYLPLogo.png" alt="" width="100" class="img-fluid ylpicon"/></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto nav-padder">
        <?php if (!(isset( $_SESSION[ "user" ][ "loggedin" ] ) && $_SESSION[ "user" ][ "loggedin" ] === true)) { ?>
      <!--<li class="nav-item active nav-padder"> <a class="nav-link" href="/login/">Log In</a> </li>-->
        <?php
              }else{
        ?>
        <!--<li class="nav-item active nav-padder"><a class="nav-link" href="../PHP/commands/ylplogout.php">Log Out</a></li>-->
            <?php
              }
              if(isset($_SESSION['YLP']['ACTIVE']) && $_SESSION['YLP']['ACTIVE'] === 'Y'){
            ?>
                <!--<a class="landing-form-btn" href="/dashboard/">Misson Control</a>-->
            <?php
              }else{
            ?>
                <!--<a class="landing-form-btn" href="/subscription/">Subscribe Now</a>-->
            <?php } ?>
            <button class="landing-form-btn" id="landing-join" value="blue">Join The Mission</button>
            <!--<li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Dropdown </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown"> <a class="dropdown-item" href="#">Action</a> <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a> </div>
      </li>-->
</ul>
  </div>
</nav>
	<div class="landing-wrapper">
	<div class="landing-top">
		<div class="landing-top-imgs">
			<img src="/images/Logo+planet.png" class="img-fluid" alt="The planet Ohm">
		</div>
		<section>
			<div class="col-12 text-center">
				<div class="landing-planet-txt">
					<div class="row justify-content-center">
						<p><font color="white" >Welcome to the Young Leader Project!<br>
							Our mission is to build leadership skills in children.
							</font></p></p>
					</div>
					<button class="landing-form-btn" id="landing-join2" value="blue">Join The Mission</button>
				</div>
			</div>
		</section>
        <div class="landing-astronaut">
        </div>
        <!----><img src="/images/Astronaut.png" class="landing-astronaut img-fluid" alt="The planet Ohm">
		
		<!--
	<img src="/images/YLP-logo.png" class="landing-logo img-fluid" alt="The planet Ohm">-->
		<div id="particles-js"></div>

	</div>
    <div class="landing-prefirst">
            <br>
            <br>
            <br>
            <h4 class="landing-prefirsthead" style="color: #6A459B;"><center><b>The Young Leader Project teaches kids the skills associated with great leadership:</b></center></h4>
            <br>
			<div class="container">
			  <div style="text-align: center;" class="row justify-content-center">
				    <div class="col-lg-3">
				  		<img src="/images/Leading Self@2x.png" class="img-fluid landing-preimg"  alt="Young Leader Project Leading Self Description">
				  	</div>
				    <div class="col-lg-3 offset-lg-1">
				  		<img src="/images/Leading Others@2x.png" class="img-fluid landing-preimg"   alt="Young Leader Project Leading Others Description">
				  	</div>
                  <div class="col-lg-3 offset-lg-1">
				  		<img src="/images/Leading Holistically@2x.png" class="img-fluid landing-preimg" alt="Young Leader Project Leading Holistically Description">
				  	</div>
                  </div>
                <br>
                <h3 style="color: #505050"><center>And so much more!</center></h3>
            
            </div>
        <br>
        <br>
	</div>
	<div class="landing-first">
            <br>
            <br>
			<div class="container">
			  <div class="row">
				    <div class="col-lg-5">
				  		<h3 style="color: #505050"><b>Here’s How We Do It</b></h3>
						<hr>
						<p>The Young Leader Project is an interactive app that leverages screen time to make a powerful and positive difference. Young leaders will journey through a galaxy called the Leaderverse where they will build skills related to different leadership competencies like Resilience, Mindfulness, Critical Thinking, and Self-Control, to name a few.  Our cutting edge technology keeps kids engaged in virtual, story-driven play to maximize learning.</p>
				  	</div>
				    <div class="col-lg-5 offset-md-2 landing-whyus">
				  		<h3 style="color: #505050"><b>Why Us?</b></h3>
						<hr>
						<p>The creators are psychologists who specialize in leadership development, led by <a href="../team">Dr Nicole Lipkin</a>, whose expertise meets at the intersection of psychology and leadership. Our content and research team have the perfect combination of experience and training necessary to introduce the practice of leadership to children.</p>
				  	</div>
		      </div>
            </div>
	</div>
	<div class="landing-second">
		<div class="landing-firstsec">
			<div class="container">
					<center>
						<h3 style="color: #505050"><b>What Parents Get</b></h3>
						<hr>
					</center>
				<div id="landing-second-parentportal" class="row landing-firstsec">
					  
					  <div class="my-auto landing-second-pic1txt offset-lg-0 offset-xl-0 offset-md-0 col-md-6 col-xl-5">
						  <h4 style="color: #505050"><b>Caregiver Portal</b></h4>
						  <p>All subscriptions come with access to our Mission Control caregiver portal where you can follow your child’s progress. The portal is also chock full of fun projects to do at home to further your young leader’s development and coaching tips on how to talk to your child about leadership in real world situations.</p></div>
						<div class="col-sm-8 landing-second-pic1 landing-second-mobilepic offset-lg-1 offset-xl-2 col-xl-5 col-md-5 offset-md-1">
							<div>
							<img src="/images/ParentPortal-desktop@2x.png"  alt="Young Leader Parent Portal" class="img-fluid">
							</div>
						</div>
						<div class="offset-xl-1"></div>
                        
                </div>
				<div id="landing-second-parentportal2" class="row landing-firstsec ">
					  <div class="col-md-6 offset-xl-1 col-xl-4 landing-second-pic1 col-sm-8 landing-second-mobilepic"><img src="/images/ParentPortal-desktop@2x.png"  alt="Young Leader Parent Portal" class="img-fluid"></div>
					  <div class="col-md-5 offset-md-1 my-auto landing-second-pic2txt">
						  <h4 style="color: #505050"><b>Caregiver Portal</b></h4>
						  <p class="landing-second-pic2txt">All subscriptions come with access to our Mission Control caregiver portal where you can follow your child’s progress. The portal is also chock full of fun projects to do at home to further your young leader’s development and coaching tips on how to talk to your child about leadership in real world situations.</p></div>
                </div>
				  <div class="row landing-firstsec">
					  <div class="col-md-6 col-sm-8 landing-second-pic1 landing-second-mobilepic offset-xl-0 col-xl-5"><img src="/images/Assessment@2x.png" alt="The Young Leader Track" class="img-fluid"></div>
					  <div class="col-md-5 offset-md-1 my-auto landing-second-pic1txt offset-xl-2">
						  <h4 style="color: #505050"><b>The Young Leader Tracker</b></h4>
						  <p class="">Use our Young Leader Tracker to identify your child's leadership strengths and weaknesses. This tracker provides you with recommendations and tips you can use to help further your young leader’s development at home.</p></div>
                	</div>
				
			</div>
		</div>
	</div>
	<div class="landing-second" style="background-color: #EAF3F6; margin-top: 0px;">
		<div class="landing-firstsec">
			<div class="container">
					<center>
						<h3 style="color: #505050"><b>What Kids Get</b></h3>
						<hr>
					</center>
				<div id="landing-second-planet1" class="row landing-firstsec">
					  
					  <div  class="my-auto landing-second-pic1txt offset-lg-0 offset-xl-0 offset-md-0 col-md-6 col-xl-5">
						  <h4 style="color: #505050"><b>Unlimited access to the Young Leader App</b></h4>
						  <p>Your young leader can travel through the Leaderverse at their own pace and revisit the planets at any time to revisit exercises or engage in new ones. Leadership development does not have a time limit so neither does the Young Leader Project.</p></div>
						<div class="col-sm-8 landing-second-pic1 landing-second-mobilepic offset-lg-1 offset-xl-2 col-xl-5 col-md-5 offset-md-1">
							<img src="/images/handphone@2x.png"  alt="Young Leader Expectations" class="img-fluid">
						</div>
                        
                </div>
                <div id="landing-second-planet2" class="row landing-firstsec ">
					  <div class="col-md-6 offset-xl-1 col-xl-4 landing-second-pic1 col-sm-8 landing-second-mobilepic"><img src="/images/handphone@2x.png"  alt="Young Leader Planets" class="img-fluid"></div>
					  <div class="col-md-5 offset-md-1 my-auto landing-second-pic2txt">
						  <h4 style="color: #505050"><b>Unlimited access to the Young Leader App</b></h4>
						  <p class="">Your young leader can travel through the Leaderverse at their own pace and revisit the planets at any time to revisit exercises or engage in new ones. Leadership development does not have a time limit so neither does the Young Leader Project.</p></div>
                </div>
				<div class="row landing-firstsec">
					  <div class="col-md-6 landing-second-pic1 col-sm-8 landing-second-mobilepic offset-xl-0 col-xl-5"><img src="/images/Youtube-challenge@2x.png"  alt="Young Leader Face Filter" class="img-fluid"></div>
					  <div class="col-md-5 offset-md-1 my-auto landing-second-pic1txt offset-xl-2">
						  <h4 style="color: #505050"><b>Activities to do  with peers</b></h4>
						  <p class="">Social learning is imperative to practice the skills learned in real world situations. Your young leader will receive online and offline challenges to complete with their families and/or friends to further their development.</p></div>
                </div>
				
			</div>
		</div>
	</div>
	<div class="landing-third">
        <div class="landing-third-txt">
            <p>Young Leader Project is preparing to launch prototype testing in Summer 2019. To join our crew and be part of the testing, sign up for our mailing list below.</p>
	    </div>
	</div>
	<div class="landing-fourth">
		<section>
			
			<div class="col-12 text-center">
				<h5 class="landing-fourthhead"><b>PEOPLE BELIEVE IN THE MISSION</b></h5>
				<div class="landing-fourth-txt">
					<div class="row">
						<p>The Young Leader Project has been awarded a National Science Foundation (NSF) Small Business Innovation Research (SBIR) to conduct research and development (R&amp;D) work on helping children develop leadership skills. This material is based upon work supported by the National Science Foundation under Grant No. 1842707</p>
					</div>
					<img src="/images/NSF_4-Color_bitmap_Logo.png" alt="NSF Logo" width="100px" height="100px" class="rounded">
				</div>
			</div>
		</section>
	</div>
	<div class="landing-fifth">
        <img src="/images/Selfie.png" id="landing-form" class="landing-selfie img-fluid" alt="The planet Ohm">
		<section>
				<div class="landing-fifth-txt">
					<div class="row">
						<div class="col-12 text-center">
							<h4 style="color: #505050"><b>YOU CAN JOIN THE MISSION</b></h4>
							<hr>
                            <p>Sign up to find out when we go live!</p>
						</div>
					</div>
				</div>
				<div class="landing-form">
					<div class="row">
                        <form class="landing-form" action="/" method="post">
                            <div class="form-group">
                                <input type="text" class="form-control" name="ylpname" placeholder="Name" />
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" name="ylpemail" placeholder="Email" />
                            </div>
                            <input type="hidden" name="rqstyp" value="ylpbetasignup" />
                            <span class="help-block">
                                <?php echo $input_err; ?>
                            </span>
                            <!--							<div class="form-group">
								<input type="password" class="form-control" name="ylppass" placeholder="Password">
							</div>-->
                            <button type="submit" class="landing-form-btn" value="blue">Sign Up For Free</button>
                        </form>
					</div>
				</div>

		</section>
		
	</div>

		<div class="text-center footer-container container-fluid">
		<div class="row">
			<div class="col-12 col-xl-4">
				<p>
					<font size="-1" color="white">© Young Leader Project, LLC. All Rights Reserved.</font>
				</p>
			</div>
			<div class="col-12 offset-3 footer-adjust offset-xl-3 col-xl-5 align-content-center">
				<nav>
					<ul>
						<li hidden class="li-inline"><a href="#">About</a>
						</li>
						<li hidden class="li-inline"><a href="#">FAQ</a>
						</li>
						<li hidden class="li-inline"><a href="#">Services</a>
						</li>
						<li hidden class="li-inline"><a href="#">Blog</a>
						</li>
						<li class="li-inline"><a href="../team">Meet The Team</a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
		</div>
</div>		
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="js/jquery-3.3.1.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap-4.2.1.js"></script>
	<script src="js/particles.js"></script>
    <script src="js/landing_page.js"></script>
</body>
</html>