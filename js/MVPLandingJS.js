$("#mvplanding-join").click(function() {
    $('html, body').animate({
        scrollTop: $("#mvplanding-form").offset().top
    }, 1000);
    return false;
});
$("#mvplanding-join2").click(function () {
    $('html, body').animate({
        scrollTop: $("#mvplanding-form").offset().top
    }, 1000);
    return false;
});
$("#MVPSignUpBtn").click(function (event) {
    event.preventDefault();
    var paypalcust = $("#paypalcustombtn");
    var paypalhelp = $('#paypalformhelp');
    var namefield = $("#MVPInputName").val().trim();
    var emailfield = $("#MVPInputEmail").val().trim();
    var phonefield = $("#MVPInputPhone").val();
    if (namefield !== "" && emailfield !== "") {
        if (phonefield.length > 0 && phonefield.length !== 10) {
            paypalhelp.html("<b>Please provide a 10 digit phone number</b>");
            paypalhelp.attr("hidden", false);
            return;
        }
        paypalhelp.attr("hidden", true);
        parms.dev = window.ylp.dev;
        $.post(window.ylp.Url, {
            rqstyp: "ylpusrmaint", name: namefield, email: emailfield, phone: phonefield.trim(), type: 'A' }).done(function (data) {
            var stringdata = data;
            if (data === "EXISTS") {
                paypalhelp.html("<b>User already exists</b>");
                paypalhelp.attr("hidden", false);
                return;
            } else if (data.substring(0, 7) === "COUNTRY") {
                paypalhelp.attr("hidden", true);
                var country = data.split("=")[1];
                paypalcust.val(emailfield + "~" + namefield + "~" + phonefield + "~" + country);
                $('#mvpEULA').modal('show');
            }
        });
    } else {
        paypalhelp.html("<b>Please fill out all required information</b>");
        paypalhelp.attr("hidden", false);
        return;
    }
});

$("#mvpEULAaccept").click(function (event) {
    $("#mvplanding-form").submit();
});

function setCookiePolicy() {
    document.cookie = "cookiePolicy=true;";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function checkCookiePolicy(cookiename) {
    var cookie = getCookie("cookiePolicy");
    if (cookie === "") {
        $('#cookieModal').modal('toggle');
    }
}


