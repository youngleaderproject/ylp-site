$("#landing-join").click(function() {
    $('html, body').animate({
        scrollTop: $("#landing-form").offset().top
    }, 1000);
    return false;
});
$("#landing-join2").click(function () {
    $('html, body').animate({
        scrollTop: $("#landing-form").offset().top
    }, 1000);
    return false;
});
$(document).scroll(function(){
    if($( document ).width() < 768){
        var check = 400;
    }else{
        var check = 600;
    }
  var sticky = $('#landing-nav'),
      scroll = $(document).scrollTop();
  if (scroll >= check)sticky.attr("hidden", false);
  else sticky.attr("hidden",true);
});

function setCookiePolicy() {
    document.cookie = "cookiePolicy=true;";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function checkCookiePolicy(cookiename) {
    var cookie = getCookie("cookiePolicy");
    if (cookie === "") {
        $('#cookieModal').modal('toggle');
    }
}