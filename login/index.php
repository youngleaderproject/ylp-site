<?php
if(!isset($_SESSION))
{
    session_start();
}
require_once str_replace( '\\', '/', $_SERVER[ "DOCUMENT_ROOT" ] ) . '/PHP/shared/auto_load.php';

// Check if the user is already logged in, if yes then redirect him to welcome page
if ( isset( $_SESSION[ "user" ][ "loggedin" ] ) && $_SESSION[ "user" ][ "loggedin" ] === true ) {
    Redirect( "../" ); /// test
    exit;
}

// Define variables and initialize with empty values
$username = $password = "";
$username_err = $password_err = "";

if ( $_SERVER[ "REQUEST_METHOD" ] == "POST" ) {
    // Check if username is empty

    if ( empty( trim( $_POST[ "usrname" ] ) ) ) {
        $username_err = "Please enter username.";
    } else {
        $username = trim( $_POST[ "usrname" ] );
    }

    // Check if password is empty
    if ( empty( trim( $_POST[ "passw" ] ) ) ) {
        $password_err = "Please enter your password.";
    } else {
        $password = trim( $_POST[ "passw" ] );
    }
    // Validate credentials

    if ( empty( $username_err ) && empty( $password_err ) ) {
        require '../PHP/commands/ylplogin.php';
        if ( isset( $_SESSION[ "user" ][ "loggedin" ] ) && $_SESSION[ "user" ][ "loggedin" ] === true ) {
            Redirect( "../" );
            die;
        } else {
            $password_err = "<font color=red><b>Username Or Password is Incorrect.</b></font>";
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>YLP | Login Page</title>
    <meta name="description" content="
The Young Leader Project develops the leaders of tomorrow by acquainting them with essential leadership skills today.">
    <!-- Bootstrap -->
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/landingscripts.css">
    <link rel="stylesheet" href="/css/styles.css">
    <script type="text/javascript" src="https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>
</head>

<body>
    <div class="main-wrapper">
        <nav class="navbar navbar-expand-lg navbar-light bg-light navextend"> <a class="navbar-brand" href="/"><img src="../images/NewYLPLogo.png" alt="" width="100" class="img-fluid ylpicon"/></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto nav-padder">
                    <a style="margin: auto" href="../#landing-form"><button class="landing-form-btn" value="blue">Join The Mission</button></a>
        </ul>
          </div>
        </nav>
    
        <div class="wrapper">
            <h2>Login</h2>
            <p>Please fill in your credentials to login.</p>
            <form action="./index.php" method="post">
                <div class="form-group <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>">
                    <label>Username</label>
                    <input type="text" name="usrname" class="form-control" value="<?php echo $username; ?>"/>
                    <span class="help-block">
                        <?php echo $username_err; ?>
                    </span>
                </div>
                <div class="form-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
                    <label>Password</label>
                    <input type="password" name="passw" class="form-control"/>
                    <span class="help-block">
                        <?php echo $password_err; ?>
                    </span>
                </div>
                <input type="hidden" name="rqstyp" value="ylplogin"/>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Login"/>
                </div>
                <p>
                    Don't have an account?
                    <a href="/signup">Sign up now</a>.
                </p>
            </form>
        </div>

    <div class="text-center footer-container container-fluid">
		<div class="row">
			<div class="col-12 col-xl-4">
				<p>
					<font color="white">© Young Leader Project, LLC. All Rights Reserved.</font>
				</p>
			</div>
			<div class="col-12 offset-3 footer-adjust offset-xl-3 col-xl-5 align-content-center">
				<nav>
					<ul>
						<li class="li-inline"><a href="#">Meet The Team</a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
		</div>
		</div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../js/jquery-3.3.1.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap-4.2.1.js"></script>
    <script src="../js/particles.js"></script>
</body>
</html>