<?php
if(!isset($_SESSION))
{
    session_start();
}
require_once str_replace('\\', '/', $_SERVER["DOCUMENT_ROOT"]) . '/PHP/shared/auto_load.php';
use PHP\CLASSES\CUSER;
use PHP\CLASSES\CCIPHER;
use PHP\CLASSES\CEMAIL;

if ( isset( $_SESSION[ "user" ][ "loggedin" ] ) && $_SESSION[ "user" ][ "loggedin" ] === true ) {
    $_SESSION['YLP']['ACTIVE'] = 'Y';
    $user = new CUSER();
    $userid = $_SESSION['user']['usrid'];
    $username = $_SESSION['user']['usrname'];
    $user->SetUserStatus($username,$userid, 1);

    $cipher = new CCIPHER();
    $cipheredtext = $cipher->EncryptOrDecrypt($username,"YoungLeader", "ENCRYPT"); // ADD TO NEW TABLE FOR CIPHERED UNIQUE CODES.
    $cipherencrypt = $cipher->AddCipher($cipheredtext,$userid);
    $email = new CEMAIL();
    $email->username = $username;
    if (DEBUG)
        file_put_contents ( $_SERVER["DOCUMENT_ROOT"]. '/logs/cipher.txt' ,"cipher" . $cipherencrypt . "\r\n", FILE_APPEND);
    $email->FirstEmail($cipheredtext,$cipherencrypt);


    Redirect("../");
    exit;
}
?>