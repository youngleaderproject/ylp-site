<?php
if(!isset($_SESSION))
{
    session_start();
}
error_reporting(E_ALL);

$paysuccess = false;
if(isset($_SESSION['YLP']['ACTIVE']) && $_SESSION['YLP']['ACTIVE'] === 'Y')$paysuccess = true;
?>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Young Leader Portal | Home</title>
    <meta name="description" content="
The Young Leader Project develops the leaders of tomorrow by acquainting them with essential leadership skills today." />
    <!-- Bootstrap -->
    <meta property='og:title' content="Young Leader Project" />
    <meta property='og:image' content="https://youngleaderproject.com/images/NewImages/LandingPage.png" />
    <meta property='og:description' content="The Young Leader Project develops the leaders of tomorrow by acquainting them with essential leadership skills today." />
    <meta property='og:url' content="https://youngleaderproject.com" />
    <link rel="icon" href="../images/NewImages/favicon.ico" type="favicon/ico" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="../css/MVPLandingScripts.css" />
</head>

<body>
    <div class="modal fade" id="mvpEULA" tabindex="-1" role="dialog" aria-labelledby="mvpEULAlLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Young Leader Portal End User License Agreement</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>This is where the YLP EULA will be</p>
                </div>
                <div class="modal-footer">
                    <center>
                        <button type="button" class="btn btn-primary" data-dismiss="modal" id="mvpEULAaccept">Accept</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </center>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid mvpSection1">
        <div class="row">
            <img src="../images/NewImages/YLPMVPlogo.png" alt="Young Leader Portal" class="img-fluid logoHeader" />
        </div>
        <div class="row">
            <a class="logoQuote" style="color: gray;" href="../">
                <p>Powered By Young Leader Project</p>
            </a>
        </div>
        <div class="container">
            <div class="row mvpSection1_5">
                <div class="col-10 offset-1 col-sm-10 offset-sm-1 offset-md-0 col-md-8 offset-xl-1 col-xl-6 col-lg-7 offset-lg-1">
                    <h2>Talk to your kid like their future depends on it.</h2>
                    <p>Young Leader Portal delivers high-impact talking tips straight to your inbox. By having more consistent conversations with your child, you can help them develop the social-emotional leadership skills they need to succeed.</p>
                    <br />
					<ul>
						<li>Curated conversation starters</li>
						<li>Evidence-based activities developed by psychologists</li>
						<li>Weekly newsletter with just-in-time talking tips</li>
					</ul>
					<br>
                    <button type="button" id="mvplanding-join" class="btn btn-lg mvpSignupBtn" style="max-width: 300px;">START FREE TRIAL</button>
                </div>
				<div class="col-10 col-sm-10 col-xl-5 my-auto text-center col-lg-4 col-md-4 mvpfamily">
				  <img src="../images/NewImages/family.png" class="img-fluid" width="300px" alt="" />
				</div>
            </div>
        </div>
    </div>
    <div class="container mvpSection1-1">
        <center>
            <div style="max-width: 575px;">
                <h5>
                    <b>Subscribe to Young Leader Portal to receive the first week FREE</b>
                </h5>
            </div>
        
        <div class="row mvpSection1-1-1">
			<div class="card mb-4 mx-auto box-shadow">
				<center>
					<div class="card-body">
						<h1 class="card-title pricing-card-title">
							<b>$0</b>
						</h1>
						<h6>
							More empowered parenting for less than a cup of coffee
							<br />
							<br />Start your free trial today
						</h6>
						<button type="button" id="mvplanding-join2" class="btn btn-lg mvpSignupBtn">
							TRY FOR FREE
						</button>
						<div style="max-width: 350px; font-weight: 100; font-size: 12px; margin-top: 10px;">After your no-obligation, 7-day trial ends, you will be auto-enrolled in the Young Leader Portal for $9.99 per month. Cancel anytime.</div>
					</div>
				</center>
			</div>
        </div>
        </center>
    </div>
    <div class="container mvpSection1-2">
        <div class="row">
            <div class="col-xl-4 my-auto text-center offset-lg-3 col-lg-6 offset-xl-0 offset-sm-1 col-sm-10 offset-md-2 col-md-8 offset-2 col-8">
                <img src="../images/NewImages/callout1.png" alt="" width="300" class="img-fluid" />
                <center>
                    <div style="font-weight: 500; font-size: 17px;">Creative activities make social, emotional and leadership skills fun and easy to learn</div>
                </center>
            </div>
            <div class="col-xl-4 my-auto text-center col-lg-6 offset-lg-3 offset-xl-0 offset-sm-1 col-sm-10 offset-md-2 col-md-8 offset-2 col-8">
                <img src="../images/NewImages/callout2.png" alt="" width="300" class="img-fluid" />
                <center>
                    <div style="font-weight: 500; font-size: 17px;">Conversational tips help open up the lines of communication between you and your child</div>
                </center>
            </div>
            <div class="col-xl-4 my-auto text-center col-lg-6 offset-lg-3 offset-xl-0 offset-sm-1 col-sm-10 offset-md-2 col-md-8 offset-2 col-8">
                <img src="../images/NewImages/callout3.png" alt="" width="300" class="img-fluid" />
                <center>
                    <div style="font-weight: 500; font-size: 17px;">Covers everything from big world news to everyday challenges</div>
                </center>
            </div>
        </div>
    </div>
    <div class="container-fluid mvpSection1-3">
        <div class="mvpQuoteLine">
            <div></div>
        </div>
        <center>
            <div class="container">
                <div id="carouselExampleIndicators1" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators1" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators1" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators1" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner" role="listbox">
                        <div class="carousel-item active">
                            <div style="max-width: 700px;">
                                <h5>
                                    <b>My daughter and I did the emotional understanding activity. It really helped me understand how she expresses emotions through facial expressions and she was able to tune in the next time she felt nervous without my help.</b>
                                </h5>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div style="max-width: 700px;">
                                <h5>
                                    <b>Before Young Leader Project, I didn’t know how to talk to my son when he asked tough questions about life. Having these tools makes me feel more confident as a parent.</b>
                                </h5>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div style="max-width: 700px;">
                                <h5>
                                    <b>Young Leader Project made talking with my kids an everyday part of our life. I love watching them learn to see their own potential as changemakers!</b>
                                </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </center>
        <div class="container">
            <div class="accordion" id="accordionExample">
                <center>
                    <h3>
                        <b>Frequently Asked Questions</b>
                    </h3>
                </center>
                <br />
                <div class="card ">
                    <div class="card-header" id="headingOne">
                        <h2 class="mb-0">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionExample" href="#collapseOne">
                                What will my family learn through the Young Leader Project?
                            </a>
                        </h2>
                    </div>
                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <div class="card-body">
                            Each week, you will learn new ways of connecting with your child through conversation starters and activities that develop vital social-emotional and leadership skills. Core competencies like self-understanding, conflict resolution, communication, and mindfulness are the strongest predictors of future success.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h2 class="mb-0">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionExample" href="#collapseTwo">
                                Who created Young Leader Project?
                            </a>
                        </h2>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                        <div class="card-body">
                            The creators are psychologists who specialize in leadership development, led by Dr Nicole Lipkin, whose expertise meets at the intersection of psychology and leadership. Our content and research team have the perfect combination of experience and training necessary to introduce the practice of leadership to children.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree">
                        <h2 class="mb-0">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionExample" href="#collapseThree">
                                Who supports Young Leader Project?
                            </a>
                        </h2>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                        <div class="card-body">
                            Our evidence-based activities has been tested by hundreds of parents just like you! Young Leader Project has been awarded a National Science Foundation (NSF) Small Business Innovation Research (SBIR) to conduct research and development (R&amp;D) work on helping children develop leadership skills. This material is based upon work supported by the National Science Foundation under Grant No. 1842707
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour">
                        <h2 class="mb-0">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionExample" href="#collapseFour">
                                I have a problem with my account. What should I do?
                            </a>
                        </h2>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                        <div class="card-body">
                            If you’d like to unsubscribe or have any other issue with your Young Leader Project Account, email us at support@youngleaderproject.com
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="container-fluid mvpSection1-4" id="mvpSection1-4">
        <div class="mvpsignupform" <?php if($paysuccess) echo "hidden"; ?>>
            <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" id="mvplanding-form">
                <center>
                    <div style="max-width: 700px; padding-bottom: 10px">
                        <h5>Set your young leader up for a bright future. Start your free trial today!</h5>
                    </div>
                </center>
                <div class="form-group">
                    <input type="text" class="form-control" id="MVPInputName" required placeholder="Name" />
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" id="MVPInputEmail" required placeholder="Email" />
                </div>
                <div class="form-group">
                    <input type="tel" class="form-control" id="MVPInputPhone" placeholder="Phone # to recieve your texts" />
                </div>
                <span class="help-block" style="color: red" hidden id="paypalformhelp"></span>
                <input type="hidden" name="cmd" value="_s-xclick" />
                <input type="hidden" name="hosted_button_id" value="5AS3W4TFAV3FG" />
                <input type="hidden" name="custom" id="paypalcustombtn" />
                <button type="button" class="btn btn-lg mvpSignupBtn" style="margin-top: 10px;" id="MVPSignUpBtn">
                    START FREE TRIAL
                </button>
            </form>
        </div>
        <div class="mvpsignupsuccess" <?php if(!$paysuccess) echo "hidden"; ?>>
            <div class="container">
                <center>
                    <h3 style="color: #F77446">
                        <b>Success!</b>
                    </h3>
                </center>
                <center>
                    <h3 style="max-width: 600px;">Check your inbox for more information and give yourself a high five for proactive parenting!</h3>
                </center>
            </div>
        </div>
		<br>
		<br>
    </div>
    <footer class="text-center">
        <div class="container-fluid">
            <div class="row" style="height: 80px">
                <div class="col-12 my-auto">
                    <p style="text-align: center; margin: auto;">© Young Leader Project, LLC. All Rights Reserved.</p>
                    <a style="font-size: 12px; color: gray; padding-left:10px;" href="javascript:window.open('../disclaimer.html', 'YLP Disclaimer', 'width=600,height=450');">Disclaimer</a>
                    <a style="font-size: 12px; color: gray; padding-left:10px;" href="javascript:window.open('../cookiepolicy.html', 'YLP Cookie Policy', 'width=600,height=450');">Cookie Policy</a>
                    <a style="font-size: 12px; color: gray; padding-left:10px;" href="javascript:window.open('../privacypolicy.html', 'YLP Privacy Policy', 'width=600,height=450');">Privacy Policy</a>
                    <a style="font-size: 12px; color: gray; padding-left:10px;" href="javascript:window.open('../termsconditions.html', 'YLP Terms & Conditions;', 'width=600,height=450');">Terms & Conditions</a>
                </div>
            </div>
        </div>
    </footer>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="../js/MVPLandingJS.min.js"></script>
    <script src="../js/ylpglobals.js"></script>
</body>
</html>